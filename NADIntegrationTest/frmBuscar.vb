﻿Public Class frmBuscar

    Dim buscados As List(Of String) = New List(Of String)
    Dim queBuscaField As String = ""
    Dim itemField As Integer = 0

    Public Property QueBusca As String
        Get
            Return queBuscaField
        End Get
        Set(value As String)
            queBuscaField = value
        End Set
    End Property

    Public Property Item As Integer
        Get
            Return itemField
        End Get
        Set(value As Integer)
            itemField = value
        End Set
    End Property

    Private Sub txtABuscar_TextChanged(sender As Object, e As EventArgs) Handles txtABuscar.TextChanged
        buscados.Clear()
        lbl_documento.Text = "..."
        lbl_nombre.Text = "..."
        btnAceptar.Enabled = False
        If QueBusca = "Clientes" Then
            For Each c As cliente In frmFactura.Clientes
                If c.Documento.ToUpper.Contains(txtABuscar.Text.Trim.ToUpper) Then
                    Dim linea As String = c.Documento.ToUpper
                    linea = linea.PadRight(20, " ")
                    linea = linea & "| " & c.Nombre.ToUpper
                    buscados.Add(linea)
                    Continue For
                End If
                If c.Nombre.ToUpper.Contains(txtABuscar.Text.Trim.ToUpper) Then
                    Dim linea As String = c.Documento.ToUpper
                    linea = linea.PadRight(20, " ")
                    linea = linea & "| " & c.Nombre.ToUpper
                    buscados.Add(linea)
                    Continue For
                End If
                If c.Direccion.ToUpper.Contains(txtABuscar.Text.Trim.ToUpper) Then
                    Dim linea As String = c.Documento.ToUpper
                    linea = linea.PadRight(20, " ")
                    linea = linea & "| " & c.Nombre.ToUpper
                    buscados.Add(linea)
                    Continue For
                End If
                If c.Ciudad.ToUpper.Contains(txtABuscar.Text.Trim.ToUpper) Then
                    Dim linea As String = c.Documento.ToUpper
                    linea = linea.PadRight(20, " ")
                    linea = linea & "| " & c.Nombre.ToUpper
                    buscados.Add(linea)
                    Continue For
                End If
                If c.Departamento.ToUpper.Contains(txtABuscar.Text.Trim.ToUpper) Then
                    Dim linea As String = c.Documento.ToUpper
                    linea = linea.PadRight(20, " ")
                    linea = linea & "| " & c.Nombre.ToUpper
                    buscados.Add(linea)
                End If
            Next
        End If

        If QueBusca = "Articulos" Then
            For Each a As articulo In frmFactura.Articulos
                If a.Nombre.ToUpper.Contains(txtABuscar.Text.Trim.ToUpper) Then
                    buscados.Add(a.Nombre.ToUpper)
                End If
            Next
        End If

        lst_encontrados.Items.Clear()
        For Each b As String In buscados
            lst_encontrados.Items.Add(b)
        Next
    End Sub

    Private Sub lst_encontrados_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lst_encontrados.SelectedIndexChanged
        Try
            lbl_documento.Text = lst_encontrados.SelectedItem.ToString.Split("|")(0).Trim
            If QueBusca = "Clientes" Then
                lbl_nombre.Text = lst_encontrados.SelectedItem.ToString.Split("|")(1).Trim
            End If
            If QueBusca = "Articulos" Then
                For Each a As articulo In frmFactura.Articulos
                    If a.Nombre.ToUpper = lst_encontrados.SelectedItem.ToString.ToUpper Then
                        lbl_nombre.Text = a.Precio
                    End If
                Next
            End If
            btnAceptar.Enabled = True
        Catch
        End Try
    End Sub



    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If Not lbl_documento.Text.Trim = "..." Then
            If queBuscaField = "Clientes" Then
                Me.DialogResult = Windows.Forms.DialogResult.OK
                frmFactura.RapidatxtDocumentoReceptor.Text = lbl_documento.Text.Trim
                Me.Close()
            End If
            If queBuscaField = "Articulos" Then
                Me.DialogResult = Windows.Forms.DialogResult.OK
                If Not lbl_nombre.Text = "Sin Precio" Then
                    Select Case itemField
                        Case 1
                            frmFactura.RapidatxtNombreItem1.Text = lbl_documento.Text.Trim
                            frmFactura.RapidatxtPrecioUnitario1.Text = lbl_nombre.Text.Trim
                        Case 2
                            frmFactura.RapidatxtNombreItem2.Text = lbl_documento.Text.Trim
                            frmFactura.RapidatxtPrecioUnitario2.Text = lbl_nombre.Text.Trim
                        Case 3
                            frmFactura.RapidatxtNombreItem3.Text = lbl_documento.Text.Trim
                            frmFactura.RapidatxtPrecioUnitario3.Text = lbl_nombre.Text.Trim
                        Case 4
                            frmFactura.RapidatxtNombreItem4.Text = lbl_documento.Text.Trim
                            frmFactura.RapidatxtPrecioUnitario4.Text = lbl_nombre.Text.Trim
                        Case 5
                            frmFactura.RapidatxtNombreItem5.Text = lbl_documento.Text.Trim
                            frmFactura.RapidatxtPrecioUnitario5.Text = lbl_nombre.Text.Trim
                        Case 6
                            frmFactura.RapidatxtNombreItem6.Text = lbl_documento.Text.Trim
                            frmFactura.RapidatxtPrecioUnitario6.Text = lbl_nombre.Text.Trim
                        Case 7
                            frmFactura.RapidatxtNombreItem7.Text = lbl_documento.Text.Trim
                            frmFactura.RapidatxtPrecioUnitario7.Text = lbl_nombre.Text.Trim
                        Case 8
                            frmFactura.RapidatxtNombreItem8.Text = lbl_documento.Text.Trim
                            frmFactura.RapidatxtPrecioUnitario8.Text = lbl_nombre.Text.Trim
                        Case 9
                            frmFactura.RapidatxtNombreItem9.Text = lbl_documento.Text.Trim
                            frmFactura.RapidatxtPrecioUnitario9.Text = lbl_nombre.Text.Trim
                        Case Else
                            frmFactura.RapidatxtNombreItem10.Text = lbl_documento.Text.Trim
                            frmFactura.RapidatxtPrecioUnitario10.Text = lbl_nombre.Text.Trim
                    End Select
                Else
                    Select Case itemField
                        Case 1
                            frmFactura.RapidatxtNombreItem1.Text = lbl_documento.Text.Trim
                            frmFactura.RapidatxtPrecioUnitario1.Text = 0
                        Case 2
                            frmFactura.RapidatxtNombreItem2.Text = lbl_documento.Text.Trim
                            frmFactura.RapidatxtPrecioUnitario2.Text = 0
                        Case 3
                            frmFactura.RapidatxtNombreItem3.Text = lbl_documento.Text.Trim
                            frmFactura.RapidatxtPrecioUnitario3.Text = 0
                        Case 4
                            frmFactura.RapidatxtNombreItem4.Text = lbl_documento.Text.Trim
                            frmFactura.RapidatxtPrecioUnitario4.Text = 0
                        Case 5
                            frmFactura.RapidatxtNombreItem5.Text = lbl_documento.Text.Trim
                            frmFactura.RapidatxtPrecioUnitario5.Text = 0
                        Case 6
                            frmFactura.RapidatxtNombreItem6.Text = lbl_documento.Text.Trim
                            frmFactura.RapidatxtPrecioUnitario6.Text = 0
                        Case 7
                            frmFactura.RapidatxtNombreItem7.Text = lbl_documento.Text.Trim
                            frmFactura.RapidatxtPrecioUnitario7.Text = 0
                        Case 8
                            frmFactura.RapidatxtNombreItem8.Text = lbl_documento.Text.Trim
                            frmFactura.RapidatxtPrecioUnitario8.Text = 0
                        Case 9
                            frmFactura.RapidatxtNombreItem9.Text = lbl_documento.Text.Trim
                            frmFactura.RapidatxtPrecioUnitario9.Text = 0
                        Case Else
                            frmFactura.RapidatxtNombreItem10.Text = lbl_documento.Text.Trim
                            frmFactura.RapidatxtPrecioUnitario10.Text = 0
                    End Select
                End If

                Me.Close()
            End If
        End If
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub frmBuscar_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lst_encontrados.Items.Clear()
        buscados.Clear()
        lbl_documento.Text = "..."
        lbl_nombre.Text = "..."
        btnAceptar.Enabled = False
        If QueBusca = "Clientes" Then
            Me.Text = "Buscar Clientes..."
            lbl_1.Text = "Documento:"
            lbl_2.Text = "Nombre:"
            cargarTodos()
        End If
        If QueBusca = "Articulos" Then
            Me.Text = "Buscar Articulos..."
            lbl_1.Text = "Nombre:"
            lbl_2.Text = "Precio:"
            cargarTodos()
        End If
    End Sub
    Private Sub cargarTodos()
        buscados.Clear()
        If QueBusca = "Clientes" Then
            For Each c As cliente In frmFactura.Clientes
                Dim linea As String = c.Documento.ToUpper
                linea = linea.PadRight(20, " ")
                linea = linea & "| " & c.Nombre.ToUpper
                buscados.Add(linea)
            Next
        End If
        If QueBusca = "Articulos" Then
            For Each a As articulo In frmFactura.Articulos
                buscados.Add(a.Nombre)
            Next
        End If
        For Each b As String In buscados
            lst_encontrados.Items.Add(b)
        Next
    End Sub

    Private Sub lst_encontrados_DoubleClick(sender As Object, e As EventArgs) Handles lst_encontrados.DoubleClick
        Try
            lbl_documento.Text = lst_encontrados.SelectedItem.ToString.Split("|")(0).Trim
            lbl_nombre.Text = lst_encontrados.SelectedItem.ToString.Split("|")(1).Trim
            btnAceptar.Enabled = True
        Catch
        End Try
        btnAceptar_Click(sender, e)
    End Sub
End Class