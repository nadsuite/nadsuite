﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFacturaRetencionesPercepciones
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Btn_Aceptar = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TBx_TotalRetenido5 = New System.Windows.Forms.TextBox()
        Me.TBx_TotalRetenido4 = New System.Windows.Forms.TextBox()
        Me.TBx_TotalRetenido3 = New System.Windows.Forms.TextBox()
        Me.TBx_TotalRetenido2 = New System.Windows.Forms.TextBox()
        Me.TBx_TotalRetenido1 = New System.Windows.Forms.TextBox()
        Me.TBx_MontoSujeto5 = New System.Windows.Forms.TextBox()
        Me.TBx_MontoSujeto4 = New System.Windows.Forms.TextBox()
        Me.TBx_MontoSujeto3 = New System.Windows.Forms.TextBox()
        Me.TBx_MontoSujeto2 = New System.Windows.Forms.TextBox()
        Me.TBx_MontoSujeto1 = New System.Windows.Forms.TextBox()
        Me.TBx_Tasa5 = New System.Windows.Forms.TextBox()
        Me.TBx_Tasa4 = New System.Windows.Forms.TextBox()
        Me.TBx_Tasa3 = New System.Windows.Forms.TextBox()
        Me.TBx_Tasa2 = New System.Windows.Forms.TextBox()
        Me.TBx_Tasa1 = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TBx_Codigo5 = New System.Windows.Forms.TextBox()
        Me.TBx_Codigo4 = New System.Windows.Forms.TextBox()
        Me.TBx_Codigo3 = New System.Windows.Forms.TextBox()
        Me.TBx_Codigo2 = New System.Windows.Forms.TextBox()
        Me.TBx_Codigo1 = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'Btn_Aceptar
        '
        Me.Btn_Aceptar.Location = New System.Drawing.Point(176, 140)
        Me.Btn_Aceptar.Name = "Btn_Aceptar"
        Me.Btn_Aceptar.Size = New System.Drawing.Size(128, 39)
        Me.Btn_Aceptar.TabIndex = 81
        Me.Btn_Aceptar.Text = "Aceptar"
        Me.Btn_Aceptar.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(221, 11)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(77, 13)
        Me.Label4.TabIndex = 85
        Me.Label4.Text = "Total Retenido"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(137, 11)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(70, 13)
        Me.Label3.TabIndex = 84
        Me.Label3.Text = "Monto Sujeto"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(84, 11)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(31, 13)
        Me.Label2.TabIndex = 83
        Me.Label2.Text = "Tasa"
        '
        'TBx_TotalRetenido5
        '
        Me.TBx_TotalRetenido5.Location = New System.Drawing.Point(220, 114)
        Me.TBx_TotalRetenido5.MaxLength = 17
        Me.TBx_TotalRetenido5.Name = "TBx_TotalRetenido5"
        Me.TBx_TotalRetenido5.Size = New System.Drawing.Size(84, 20)
        Me.TBx_TotalRetenido5.TabIndex = 80
        Me.TBx_TotalRetenido5.Text = "0"
        '
        'TBx_TotalRetenido4
        '
        Me.TBx_TotalRetenido4.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.TBx_TotalRetenido4.Location = New System.Drawing.Point(220, 93)
        Me.TBx_TotalRetenido4.MaxLength = 17
        Me.TBx_TotalRetenido4.Name = "TBx_TotalRetenido4"
        Me.TBx_TotalRetenido4.Size = New System.Drawing.Size(84, 20)
        Me.TBx_TotalRetenido4.TabIndex = 76
        Me.TBx_TotalRetenido4.Text = "0"
        '
        'TBx_TotalRetenido3
        '
        Me.TBx_TotalRetenido3.Location = New System.Drawing.Point(220, 72)
        Me.TBx_TotalRetenido3.MaxLength = 17
        Me.TBx_TotalRetenido3.Name = "TBx_TotalRetenido3"
        Me.TBx_TotalRetenido3.Size = New System.Drawing.Size(84, 20)
        Me.TBx_TotalRetenido3.TabIndex = 72
        Me.TBx_TotalRetenido3.Text = "0"
        '
        'TBx_TotalRetenido2
        '
        Me.TBx_TotalRetenido2.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.TBx_TotalRetenido2.Location = New System.Drawing.Point(220, 51)
        Me.TBx_TotalRetenido2.MaxLength = 17
        Me.TBx_TotalRetenido2.Name = "TBx_TotalRetenido2"
        Me.TBx_TotalRetenido2.Size = New System.Drawing.Size(84, 20)
        Me.TBx_TotalRetenido2.TabIndex = 68
        Me.TBx_TotalRetenido2.Text = "0"
        '
        'TBx_TotalRetenido1
        '
        Me.TBx_TotalRetenido1.Location = New System.Drawing.Point(220, 29)
        Me.TBx_TotalRetenido1.MaxLength = 17
        Me.TBx_TotalRetenido1.Name = "TBx_TotalRetenido1"
        Me.TBx_TotalRetenido1.Size = New System.Drawing.Size(84, 20)
        Me.TBx_TotalRetenido1.TabIndex = 64
        Me.TBx_TotalRetenido1.Text = "0"
        '
        'TBx_MontoSujeto5
        '
        Me.TBx_MontoSujeto5.Location = New System.Drawing.Point(130, 114)
        Me.TBx_MontoSujeto5.MaxLength = 17
        Me.TBx_MontoSujeto5.Name = "TBx_MontoSujeto5"
        Me.TBx_MontoSujeto5.Size = New System.Drawing.Size(84, 20)
        Me.TBx_MontoSujeto5.TabIndex = 79
        Me.TBx_MontoSujeto5.Text = "0"
        '
        'TBx_MontoSujeto4
        '
        Me.TBx_MontoSujeto4.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.TBx_MontoSujeto4.Location = New System.Drawing.Point(130, 93)
        Me.TBx_MontoSujeto4.MaxLength = 17
        Me.TBx_MontoSujeto4.Name = "TBx_MontoSujeto4"
        Me.TBx_MontoSujeto4.Size = New System.Drawing.Size(84, 20)
        Me.TBx_MontoSujeto4.TabIndex = 75
        Me.TBx_MontoSujeto4.Text = "0"
        '
        'TBx_MontoSujeto3
        '
        Me.TBx_MontoSujeto3.Location = New System.Drawing.Point(130, 72)
        Me.TBx_MontoSujeto3.MaxLength = 17
        Me.TBx_MontoSujeto3.Name = "TBx_MontoSujeto3"
        Me.TBx_MontoSujeto3.Size = New System.Drawing.Size(84, 20)
        Me.TBx_MontoSujeto3.TabIndex = 71
        Me.TBx_MontoSujeto3.Text = "0"
        '
        'TBx_MontoSujeto2
        '
        Me.TBx_MontoSujeto2.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.TBx_MontoSujeto2.Location = New System.Drawing.Point(130, 51)
        Me.TBx_MontoSujeto2.MaxLength = 17
        Me.TBx_MontoSujeto2.Name = "TBx_MontoSujeto2"
        Me.TBx_MontoSujeto2.Size = New System.Drawing.Size(84, 20)
        Me.TBx_MontoSujeto2.TabIndex = 67
        Me.TBx_MontoSujeto2.Text = "0"
        '
        'TBx_MontoSujeto1
        '
        Me.TBx_MontoSujeto1.Location = New System.Drawing.Point(130, 29)
        Me.TBx_MontoSujeto1.MaxLength = 17
        Me.TBx_MontoSujeto1.Name = "TBx_MontoSujeto1"
        Me.TBx_MontoSujeto1.Size = New System.Drawing.Size(84, 20)
        Me.TBx_MontoSujeto1.TabIndex = 63
        Me.TBx_MontoSujeto1.Text = "0"
        '
        'TBx_Tasa5
        '
        Me.TBx_Tasa5.Location = New System.Drawing.Point(75, 114)
        Me.TBx_Tasa5.MaxLength = 6
        Me.TBx_Tasa5.Name = "TBx_Tasa5"
        Me.TBx_Tasa5.Size = New System.Drawing.Size(49, 20)
        Me.TBx_Tasa5.TabIndex = 78
        Me.TBx_Tasa5.Text = "0"
        '
        'TBx_Tasa4
        '
        Me.TBx_Tasa4.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.TBx_Tasa4.Location = New System.Drawing.Point(75, 93)
        Me.TBx_Tasa4.MaxLength = 6
        Me.TBx_Tasa4.Name = "TBx_Tasa4"
        Me.TBx_Tasa4.Size = New System.Drawing.Size(49, 20)
        Me.TBx_Tasa4.TabIndex = 74
        Me.TBx_Tasa4.Text = "0"
        '
        'TBx_Tasa3
        '
        Me.TBx_Tasa3.Location = New System.Drawing.Point(75, 72)
        Me.TBx_Tasa3.MaxLength = 6
        Me.TBx_Tasa3.Name = "TBx_Tasa3"
        Me.TBx_Tasa3.Size = New System.Drawing.Size(49, 20)
        Me.TBx_Tasa3.TabIndex = 70
        Me.TBx_Tasa3.Text = "0"
        '
        'TBx_Tasa2
        '
        Me.TBx_Tasa2.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.TBx_Tasa2.Location = New System.Drawing.Point(75, 51)
        Me.TBx_Tasa2.MaxLength = 6
        Me.TBx_Tasa2.Name = "TBx_Tasa2"
        Me.TBx_Tasa2.Size = New System.Drawing.Size(49, 20)
        Me.TBx_Tasa2.TabIndex = 66
        Me.TBx_Tasa2.Text = "0"
        '
        'TBx_Tasa1
        '
        Me.TBx_Tasa1.Location = New System.Drawing.Point(75, 29)
        Me.TBx_Tasa1.MaxLength = 6
        Me.TBx_Tasa1.Name = "TBx_Tasa1"
        Me.TBx_Tasa1.Size = New System.Drawing.Size(49, 20)
        Me.TBx_Tasa1.TabIndex = 62
        Me.TBx_Tasa1.Text = "0"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(21, 11)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(40, 13)
        Me.Label1.TabIndex = 82
        Me.Label1.Text = "Código"
        '
        'TBx_Codigo5
        '
        Me.TBx_Codigo5.Location = New System.Drawing.Point(11, 114)
        Me.TBx_Codigo5.MaxLength = 8
        Me.TBx_Codigo5.Name = "TBx_Codigo5"
        Me.TBx_Codigo5.Size = New System.Drawing.Size(58, 20)
        Me.TBx_Codigo5.TabIndex = 77
        '
        'TBx_Codigo4
        '
        Me.TBx_Codigo4.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.TBx_Codigo4.Location = New System.Drawing.Point(11, 93)
        Me.TBx_Codigo4.MaxLength = 8
        Me.TBx_Codigo4.Name = "TBx_Codigo4"
        Me.TBx_Codigo4.Size = New System.Drawing.Size(58, 20)
        Me.TBx_Codigo4.TabIndex = 73
        '
        'TBx_Codigo3
        '
        Me.TBx_Codigo3.Location = New System.Drawing.Point(11, 72)
        Me.TBx_Codigo3.MaxLength = 8
        Me.TBx_Codigo3.Name = "TBx_Codigo3"
        Me.TBx_Codigo3.Size = New System.Drawing.Size(58, 20)
        Me.TBx_Codigo3.TabIndex = 69
        '
        'TBx_Codigo2
        '
        Me.TBx_Codigo2.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.TBx_Codigo2.Location = New System.Drawing.Point(11, 51)
        Me.TBx_Codigo2.MaxLength = 8
        Me.TBx_Codigo2.Name = "TBx_Codigo2"
        Me.TBx_Codigo2.Size = New System.Drawing.Size(58, 20)
        Me.TBx_Codigo2.TabIndex = 65
        '
        'TBx_Codigo1
        '
        Me.TBx_Codigo1.Location = New System.Drawing.Point(11, 30)
        Me.TBx_Codigo1.MaxLength = 8
        Me.TBx_Codigo1.Name = "TBx_Codigo1"
        Me.TBx_Codigo1.Size = New System.Drawing.Size(58, 20)
        Me.TBx_Codigo1.TabIndex = 61
        '
        'frmFacturaRetencionesPercepciones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(314, 190)
        Me.Controls.Add(Me.Btn_Aceptar)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.TBx_TotalRetenido5)
        Me.Controls.Add(Me.TBx_TotalRetenido4)
        Me.Controls.Add(Me.TBx_TotalRetenido3)
        Me.Controls.Add(Me.TBx_TotalRetenido2)
        Me.Controls.Add(Me.TBx_TotalRetenido1)
        Me.Controls.Add(Me.TBx_MontoSujeto5)
        Me.Controls.Add(Me.TBx_MontoSujeto4)
        Me.Controls.Add(Me.TBx_MontoSujeto3)
        Me.Controls.Add(Me.TBx_MontoSujeto2)
        Me.Controls.Add(Me.TBx_MontoSujeto1)
        Me.Controls.Add(Me.TBx_Tasa5)
        Me.Controls.Add(Me.TBx_Tasa4)
        Me.Controls.Add(Me.TBx_Tasa3)
        Me.Controls.Add(Me.TBx_Tasa2)
        Me.Controls.Add(Me.TBx_Tasa1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TBx_Codigo5)
        Me.Controls.Add(Me.TBx_Codigo4)
        Me.Controls.Add(Me.TBx_Codigo3)
        Me.Controls.Add(Me.TBx_Codigo2)
        Me.Controls.Add(Me.TBx_Codigo1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmFacturaRetencionesPercepciones"
        Me.Text = "Retenciones Percepciones"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Btn_Aceptar As Button
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents TBx_TotalRetenido5 As TextBox
    Friend WithEvents TBx_TotalRetenido4 As TextBox
    Friend WithEvents TBx_TotalRetenido3 As TextBox
    Friend WithEvents TBx_TotalRetenido2 As TextBox
    Friend WithEvents TBx_TotalRetenido1 As TextBox
    Friend WithEvents TBx_MontoSujeto5 As TextBox
    Friend WithEvents TBx_MontoSujeto4 As TextBox
    Friend WithEvents TBx_MontoSujeto3 As TextBox
    Friend WithEvents TBx_MontoSujeto2 As TextBox
    Friend WithEvents TBx_MontoSujeto1 As TextBox
    Friend WithEvents TBx_Tasa5 As TextBox
    Friend WithEvents TBx_Tasa4 As TextBox
    Friend WithEvents TBx_Tasa3 As TextBox
    Friend WithEvents TBx_Tasa2 As TextBox
    Friend WithEvents TBx_Tasa1 As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents TBx_Codigo5 As TextBox
    Friend WithEvents TBx_Codigo4 As TextBox
    Friend WithEvents TBx_Codigo3 As TextBox
    Friend WithEvents TBx_Codigo2 As TextBox
    Friend WithEvents TBx_Codigo1 As TextBox
End Class
