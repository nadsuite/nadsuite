﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRespuesta
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmRespuesta))
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.pbxImagenQR = New System.Windows.Forms.PictureBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.lblCodSeguridad = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.lblUrlVerificarTexto = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.lblResIva = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblCAEVencimiento = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.lblCAENro = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblCAESerie = New System.Windows.Forms.Label()
        Me.lblCAEHasta = New System.Windows.Forms.Label()
        Me.lblCAENroAutoriz = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lblCAEDesde = New System.Windows.Forms.Label()
        Me.lblFechaFirma = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtMensaje = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.GroupBox2.SuspendLayout()
        CType(Me.pbxImagenQR, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.pbxImagenQR)
        Me.GroupBox2.Location = New System.Drawing.Point(302, 48)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(139, 148)
        Me.GroupBox2.TabIndex = 20
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Codigo QR"
        '
        'pbxImagenQR
        '
        Me.pbxImagenQR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pbxImagenQR.Location = New System.Drawing.Point(15, 24)
        Me.pbxImagenQR.Name = "pbxImagenQR"
        Me.pbxImagenQR.Size = New System.Drawing.Size(110, 110)
        Me.pbxImagenQR.TabIndex = 9
        Me.pbxImagenQR.TabStop = False
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(14, 285)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(140, 16)
        Me.Label11.TabIndex = 13
        Me.Label11.Text = "Codigo de Seguridad:"
        '
        'lblCodSeguridad
        '
        Me.lblCodSeguridad.AutoSize = True
        Me.lblCodSeguridad.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCodSeguridad.Location = New System.Drawing.Point(161, 285)
        Me.lblCodSeguridad.Name = "lblCodSeguridad"
        Me.lblCodSeguridad.Size = New System.Drawing.Size(0, 16)
        Me.lblCodSeguridad.TabIndex = 17
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(14, 243)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(147, 16)
        Me.Label10.TabIndex = 14
        Me.Label10.Text = "Url Para verificar Texto:"
        '
        'lblUrlVerificarTexto
        '
        Me.lblUrlVerificarTexto.AutoSize = True
        Me.lblUrlVerificarTexto.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUrlVerificarTexto.Location = New System.Drawing.Point(21, 265)
        Me.lblUrlVerificarTexto.Name = "lblUrlVerificarTexto"
        Me.lblUrlVerificarTexto.Size = New System.Drawing.Size(0, 16)
        Me.lblUrlVerificarTexto.TabIndex = 18
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(223, 285)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(105, 16)
        Me.Label9.TabIndex = 15
        Me.Label9.Text = "Resolucion DGI:"
        '
        'lblResIva
        '
        Me.lblResIva.AutoSize = True
        Me.lblResIva.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResIva.Location = New System.Drawing.Point(332, 285)
        Me.lblResIva.Name = "lblResIva"
        Me.lblResIva.Size = New System.Drawing.Size(0, 16)
        Me.lblResIva.TabIndex = 19
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.lblCAEVencimiento)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.lblCAENro)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.lblCAESerie)
        Me.GroupBox1.Controls.Add(Me.lblCAEHasta)
        Me.GroupBox1.Controls.Add(Me.lblCAENroAutoriz)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.lblCAEDesde)
        Me.GroupBox1.Location = New System.Drawing.Point(14, 48)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(282, 148)
        Me.GroupBox1.TabIndex = 16
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos CAE"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(23, 109)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(85, 16)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Vencimiento:"
        '
        'lblCAEVencimiento
        '
        Me.lblCAEVencimiento.AutoSize = True
        Me.lblCAEVencimiento.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCAEVencimiento.Location = New System.Drawing.Point(115, 109)
        Me.lblCAEVencimiento.Name = "lblCAEVencimiento"
        Me.lblCAEVencimiento.Size = New System.Drawing.Size(0, 16)
        Me.lblCAEVencimiento.TabIndex = 7
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(115, 33)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(59, 16)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "Numero:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(23, 33)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(43, 16)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "Serie:"
        '
        'lblCAENro
        '
        Me.lblCAENro.AutoSize = True
        Me.lblCAENro.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCAENro.Location = New System.Drawing.Point(180, 33)
        Me.lblCAENro.Name = "lblCAENro"
        Me.lblCAENro.Size = New System.Drawing.Size(0, 16)
        Me.lblCAENro.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(23, 58)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(112, 16)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Nro. Autorizacion:"
        '
        'lblCAESerie
        '
        Me.lblCAESerie.AutoSize = True
        Me.lblCAESerie.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCAESerie.Location = New System.Drawing.Point(72, 33)
        Me.lblCAESerie.Name = "lblCAESerie"
        Me.lblCAESerie.Size = New System.Drawing.Size(0, 16)
        Me.lblCAESerie.TabIndex = 1
        '
        'lblCAEHasta
        '
        Me.lblCAEHasta.AutoSize = True
        Me.lblCAEHasta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCAEHasta.Location = New System.Drawing.Point(209, 83)
        Me.lblCAEHasta.Name = "lblCAEHasta"
        Me.lblCAEHasta.Size = New System.Drawing.Size(0, 16)
        Me.lblCAEHasta.TabIndex = 5
        '
        'lblCAENroAutoriz
        '
        Me.lblCAENroAutoriz.AutoSize = True
        Me.lblCAENroAutoriz.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCAENroAutoriz.Location = New System.Drawing.Point(141, 58)
        Me.lblCAENroAutoriz.Name = "lblCAENroAutoriz"
        Me.lblCAENroAutoriz.Size = New System.Drawing.Size(0, 16)
        Me.lblCAENroAutoriz.TabIndex = 1
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(156, 83)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(47, 16)
        Me.Label6.TabIndex = 4
        Me.Label6.Text = "Hasta:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(22, 83)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(52, 16)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Desde:"
        '
        'lblCAEDesde
        '
        Me.lblCAEDesde.AutoSize = True
        Me.lblCAEDesde.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCAEDesde.Location = New System.Drawing.Point(80, 83)
        Me.lblCAEDesde.Name = "lblCAEDesde"
        Me.lblCAEDesde.Size = New System.Drawing.Size(0, 16)
        Me.lblCAEDesde.TabIndex = 3
        '
        'lblFechaFirma
        '
        Me.lblFechaFirma.AutoSize = True
        Me.lblFechaFirma.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFechaFirma.Location = New System.Drawing.Point(130, 16)
        Me.lblFechaFirma.Name = "lblFechaFirma"
        Me.lblFechaFirma.Size = New System.Drawing.Size(0, 16)
        Me.lblFechaFirma.TabIndex = 12
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(38, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(86, 16)
        Me.Label1.TabIndex = 11
        Me.Label1.Text = "Fecha Firma:"
        '
        'txtMensaje
        '
        Me.txtMensaje.Location = New System.Drawing.Point(17, 314)
        Me.txtMensaje.Multiline = True
        Me.txtMensaje.Name = "txtMensaje"
        Me.txtMensaje.ReadOnly = True
        Me.txtMensaje.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtMensaje.Size = New System.Drawing.Size(427, 108)
        Me.txtMensaje.TabIndex = 21
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(52, 210)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(17, 16)
        Me.Label12.TabIndex = 25
        Me.Label12.Text = "   "
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(11, 210)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(42, 16)
        Me.Label5.TabIndex = 24
        Me.Label5.Text = "TrnId:"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(177, 210)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(17, 16)
        Me.Label13.TabIndex = 27
        Me.Label13.Text = "   "
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(104, 210)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(73, 16)
        Me.Label14.TabIndex = 26
        Me.Label14.Text = "Nro Ticket:"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(275, 210)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(17, 16)
        Me.Label15.TabIndex = 29
        Me.Label15.Text = "   "
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(238, 210)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(37, 16)
        Me.Label16.TabIndex = 28
        Me.Label16.Text = "Lote:"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(341, 210)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(33, 16)
        Me.Label17.TabIndex = 30
        Me.Label17.Text = "Aut::"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(374, 210)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(17, 16)
        Me.Label18.TabIndex = 31
        Me.Label18.Text = "   "
        '
        'frmRespuesta
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(454, 308)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txtMensaje)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.lblCodSeguridad)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.lblUrlVerificarTexto)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.lblResIva)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.lblFechaFirma)
        Me.Controls.Add(Me.Label1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmRespuesta"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Respuesta"
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.pbxImagenQR, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents pbxImagenQR As PictureBox
    Friend WithEvents Label11 As Label
    Friend WithEvents lblCodSeguridad As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents lblUrlVerificarTexto As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents lblResIva As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Label3 As Label
    Friend WithEvents lblCAEVencimiento As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents lblCAENro As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents lblCAESerie As Label
    Friend WithEvents lblCAEHasta As Label
    Friend WithEvents lblCAENroAutoriz As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents lblCAEDesde As Label
    Friend WithEvents lblFechaFirma As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents txtMensaje As TextBox
    Friend WithEvents Label12 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents Label17 As Label
    Friend WithEvents Label18 As Label
End Class
