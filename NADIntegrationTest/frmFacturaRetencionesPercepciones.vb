﻿Public Class frmFacturaRetencionesPercepciones

    Public ReadOnly Property Codigo1 As Integer
        Get
            Return TBx_Codigo1.Text
        End Get
    End Property

    Public ReadOnly Property Codigo2 As Decimal
        Get
            Return TBx_Codigo2.Text
        End Get
    End Property

    Public ReadOnly Property Codigo3 As Decimal
        Get
            Return TBx_Codigo3.Text
        End Get
    End Property

    Public ReadOnly Property Codigo4 As Decimal
        Get
            Return TBx_Codigo4.Text
        End Get
    End Property

    Public ReadOnly Property Codigo5 As Decimal
        Get
            Return TBx_Codigo5.Text
        End Get
    End Property

    Public ReadOnly Property Tasa1 As Integer
        Get
            Return TBx_Tasa1.Text
        End Get
    End Property

    Public ReadOnly Property Tasa2 As Decimal
        Get
            Return TBx_Tasa2.Text
        End Get
    End Property

    Public ReadOnly Property Tasa3 As Decimal
        Get
            Return TBx_Tasa3.Text
        End Get
    End Property

    Public ReadOnly Property Tasa4 As Decimal
        Get
            Return TBx_Tasa4.Text
        End Get
    End Property

    Public ReadOnly Property Tasa5 As Decimal
        Get
            Return TBx_Tasa5.Text
        End Get
    End Property

    Public ReadOnly Property MontoSujeto1 As Integer
        Get
            Return TBx_MontoSujeto1.Text
        End Get
    End Property

    Public ReadOnly Property MontoSujeto2 As Decimal
        Get
            Return TBx_MontoSujeto2.Text
        End Get
    End Property

    Public ReadOnly Property MontoSujeto3 As Decimal
        Get
            Return TBx_MontoSujeto3.Text
        End Get
    End Property

    Public ReadOnly Property MontoSujeto4 As Decimal
        Get
            Return TBx_MontoSujeto4.Text
        End Get
    End Property

    Public ReadOnly Property MontoSujeto5 As Decimal
        Get
            Return TBx_MontoSujeto5.Text
        End Get
    End Property

    Public ReadOnly Property TotalRetenido1 As Integer
        Get
            Return TBx_TotalRetenido1.Text
        End Get
    End Property

    Public ReadOnly Property TotalRetenido2 As Decimal
        Get
            Return TBx_TotalRetenido2.Text
        End Get
    End Property

    Public ReadOnly Property TotalRetenido3 As Decimal
        Get
            Return TBx_TotalRetenido3.Text
        End Get
    End Property

    Public ReadOnly Property TotalRetenido4 As Decimal
        Get
            Return TBx_TotalRetenido4.Text
        End Get
    End Property

    Public ReadOnly Property TotalRetenido5 As Decimal
        Get
            Return TBx_TotalRetenido5.Text
        End Get
    End Property

    Private Function ValidarDatos() As Boolean
        If ((CTDec(TBx_Codigo1.Text.Trim) > 0 And CTDec(TBx_Codigo1.Text.Trim) < 100000) Or (CTDec(TBx_Codigo2.Text.Trim) > 0 And CTDec(TBx_Codigo2.Text.Trim) < 100000) Or (CTDec(TBx_Codigo3.Text.Trim) > 0 And CTDec(TBx_Codigo3.Text.Trim) < 100000) Or (CTDec(TBx_Codigo4.Text.Trim) > 0 And CTDec(TBx_Codigo4.Text.Trim) < 100000) Or (CTDec(TBx_Codigo5.Text.Trim) > 0 And CTDec(TBx_Codigo5.Text.Trim) < 100000)) Then
            MsgBox("El codigo de Retencion/Percepcion debe tener ente 6 y 8 digitos")
            TBx_Codigo1.Focus()
            Return False
        End If
        If CTDec(TBx_Tasa1.Text.Trim) > 100 Or CTDec(TBx_Tasa2.Text.Trim) > 100 Or CTDec(TBx_Tasa3.Text.Trim) > 100 Or CTDec(TBx_Tasa4.Text.Trim) > 100 Or CTDec(TBx_Tasa5.Text.Trim) > 100 Then
            MsgBox("La tasa de Retencion/Percepcion debe ser menor a 100")
            TBx_Tasa1.Focus()
            Return False
        End If
        If (TBx_Codigo1.Text.Trim = TBx_Codigo2.Text.Trim And CTDec(TBx_Tasa1.Text.Trim) > 0) Or (TBx_Codigo1.Text.Trim = TBx_Codigo3.Text.Trim And CTDec(TBx_Tasa1.Text.Trim) > 0) Or (TBx_Codigo1.Text.Trim = TBx_Codigo4.Text.Trim And CTDec(TBx_Tasa1.Text.Trim) > 0) Or (TBx_Codigo1.Text.Trim = TBx_Codigo5.Text.Trim And CTDec(TBx_Tasa1.Text.Trim) > 0) Or (TBx_Codigo2.Text.Trim = TBx_Codigo3.Text.Trim And CTDec(TBx_Tasa2.Text.Trim) > 0) Or (TBx_Codigo2.Text.Trim = TBx_Codigo4.Text.Trim And CTDec(TBx_Tasa2.Text.Trim) > 0) Or (TBx_Codigo2.Text.Trim = TBx_Codigo5.Text.Trim And CTDec(TBx_Tasa2.Text.Trim) > 0) Or (TBx_Codigo3.Text.Trim = TBx_Codigo4.Text.Trim And CTDec(TBx_Tasa3.Text.Trim) > 0) Or (TBx_Codigo3.Text.Trim = TBx_Codigo5.Text.Trim And CTDec(TBx_Tasa3.Text.Trim) > 0) Or (TBx_Codigo4.Text.Trim = TBx_Codigo5.Text.Trim And CTDec(TBx_Tasa4.Text.Trim) > 0) Then
            MsgBox("No puede ingresar el mismo codigo de Retencion/Percepcion mas de una vez en cada linea de detalle")
            TBx_Codigo1.Focus()
            Return False
        End If
        Return True
    End Function

    Private Sub Btn_Aceptar_Click(sender As Object, e As EventArgs) Handles Btn_Aceptar.Click

    End Sub

    Public Function CTDec(ByVal Texto As Object) As Decimal
        If IsNumeric(Texto) Then
            CTDec = CType(CStr(Texto), Decimal)
            If CTDec = Decimal.MinValue Then
                CTDec = 0
            End If
        Else
            CTDec = 0
        End If
    End Function

    Private Sub frmFacturaRetencionesPercepciones_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If frmFactura.LineaSeleccionada = 1 Then
            TBx_Codigo1.Text = frmFactura.RetPerLinea1Cod1.ToString
            TBx_Codigo2.Text = frmFactura.RetPerLinea1Cod2.ToString
            TBx_Codigo3.Text = frmFactura.RetPerLinea1Cod3.ToString
            TBx_Codigo4.Text = frmFactura.RetPerLinea1Cod4.ToString
            TBx_Codigo5.Text = frmFactura.RetPerLinea1Cod5.ToString
            TBx_Tasa1.Text = frmFactura.RetPerLinea1Tasa1.ToString
            TBx_Tasa2.Text = frmFactura.RetPerLinea1Tasa2.ToString
            TBx_Tasa3.Text = frmFactura.RetPerLinea1Tasa3.ToString
            TBx_Tasa4.Text = frmFactura.RetPerLinea1Tasa4.ToString
            TBx_Tasa5.Text = frmFactura.RetPerLinea1Tasa5.ToString
            TBx_MontoSujeto1.Text = frmFactura.RetPerLinea1Monto1.ToString
            TBx_MontoSujeto2.Text = frmFactura.RetPerLinea1Monto2.ToString
            TBx_MontoSujeto3.Text = frmFactura.RetPerLinea1Monto3.ToString
            TBx_MontoSujeto4.Text = frmFactura.RetPerLinea1Monto4.ToString
            TBx_MontoSujeto5.Text = frmFactura.RetPerLinea1Monto5.ToString
            TBx_TotalRetenido1.Text = frmFactura.RetPerLinea1Valor1.ToString
            TBx_TotalRetenido2.Text = frmFactura.RetPerLinea1Valor2.ToString
            TBx_TotalRetenido3.Text = frmFactura.RetPerLinea1Valor3.ToString
            TBx_TotalRetenido4.Text = frmFactura.RetPerLinea1Valor4.ToString
            TBx_TotalRetenido5.Text = frmFactura.RetPerLinea1Valor5.ToString
        End If

        If frmFactura.LineaSeleccionada = 2 Then
            TBx_Codigo1.Text = frmFactura.RetPerLinea2Cod1.ToString
            TBx_Codigo2.Text = frmFactura.RetPerLinea2Cod2.ToString
            TBx_Codigo3.Text = frmFactura.RetPerLinea2Cod3.ToString
            TBx_Codigo4.Text = frmFactura.RetPerLinea2Cod4.ToString
            TBx_Codigo5.Text = frmFactura.RetPerLinea2Cod5.ToString
            TBx_Tasa1.Text = frmFactura.RetPerLinea2Tasa1.ToString
            TBx_Tasa2.Text = frmFactura.RetPerLinea2Tasa2.ToString
            TBx_Tasa3.Text = frmFactura.RetPerLinea2Tasa3.ToString
            TBx_Tasa4.Text = frmFactura.RetPerLinea2Tasa4.ToString
            TBx_Tasa5.Text = frmFactura.RetPerLinea2Tasa5.ToString
            TBx_MontoSujeto1.Text = frmFactura.RetPerLinea2Monto1.ToString
            TBx_MontoSujeto2.Text = frmFactura.RetPerLinea2Monto2.ToString
            TBx_MontoSujeto3.Text = frmFactura.RetPerLinea2Monto3.ToString
            TBx_MontoSujeto4.Text = frmFactura.RetPerLinea2Monto4.ToString
            TBx_MontoSujeto5.Text = frmFactura.RetPerLinea2Monto5.ToString
            TBx_TotalRetenido1.Text = frmFactura.RetPerLinea2Valor1.ToString
            TBx_TotalRetenido2.Text = frmFactura.RetPerLinea2Valor2.ToString
            TBx_TotalRetenido3.Text = frmFactura.RetPerLinea2Valor3.ToString
            TBx_TotalRetenido4.Text = frmFactura.RetPerLinea2Valor4.ToString
            TBx_TotalRetenido5.Text = frmFactura.RetPerLinea2Valor5.ToString
        End If
        If frmFactura.LineaSeleccionada = 3 Then
            TBx_Codigo1.Text = frmFactura.RetPerLinea3Cod1.ToString
            TBx_Codigo2.Text = frmFactura.RetPerLinea3Cod2.ToString
            TBx_Codigo3.Text = frmFactura.RetPerLinea3Cod3.ToString
            TBx_Codigo4.Text = frmFactura.RetPerLinea3Cod4.ToString
            TBx_Codigo5.Text = frmFactura.RetPerLinea3Cod5.ToString
            TBx_Tasa1.Text = frmFactura.RetPerLinea3Tasa1.ToString
            TBx_Tasa2.Text = frmFactura.RetPerLinea3Tasa2.ToString
            TBx_Tasa3.Text = frmFactura.RetPerLinea3Tasa3.ToString
            TBx_Tasa4.Text = frmFactura.RetPerLinea3Tasa4.ToString
            TBx_Tasa5.Text = frmFactura.RetPerLinea3Tasa5.ToString
            TBx_MontoSujeto1.Text = frmFactura.RetPerLinea3Monto1.ToString
            TBx_MontoSujeto2.Text = frmFactura.RetPerLinea3Monto2.ToString
            TBx_MontoSujeto3.Text = frmFactura.RetPerLinea3Monto3.ToString
            TBx_MontoSujeto4.Text = frmFactura.RetPerLinea3Monto4.ToString
            TBx_MontoSujeto5.Text = frmFactura.RetPerLinea3Monto5.ToString
            TBx_TotalRetenido1.Text = frmFactura.RetPerLinea3Valor1.ToString
            TBx_TotalRetenido2.Text = frmFactura.RetPerLinea3Valor2.ToString
            TBx_TotalRetenido3.Text = frmFactura.RetPerLinea3Valor3.ToString
            TBx_TotalRetenido4.Text = frmFactura.RetPerLinea3Valor4.ToString
            TBx_TotalRetenido5.Text = frmFactura.RetPerLinea3Valor5.ToString
        End If
        If frmFactura.LineaSeleccionada = 4 Then
            TBx_Codigo1.Text = frmFactura.RetPerLinea4Cod1.ToString
            TBx_Codigo2.Text = frmFactura.RetPerLinea4Cod2.ToString
            TBx_Codigo3.Text = frmFactura.RetPerLinea4Cod3.ToString
            TBx_Codigo4.Text = frmFactura.RetPerLinea4Cod4.ToString
            TBx_Codigo5.Text = frmFactura.RetPerLinea4Cod5.ToString
            TBx_Tasa1.Text = frmFactura.RetPerLinea4Tasa1.ToString
            TBx_Tasa2.Text = frmFactura.RetPerLinea4Tasa2.ToString
            TBx_Tasa3.Text = frmFactura.RetPerLinea4Tasa3.ToString
            TBx_Tasa4.Text = frmFactura.RetPerLinea4Tasa4.ToString
            TBx_Tasa5.Text = frmFactura.RetPerLinea4Tasa5.ToString
            TBx_MontoSujeto1.Text = frmFactura.RetPerLinea4Monto1.ToString
            TBx_MontoSujeto2.Text = frmFactura.RetPerLinea4Monto2.ToString
            TBx_MontoSujeto3.Text = frmFactura.RetPerLinea4Monto3.ToString
            TBx_MontoSujeto4.Text = frmFactura.RetPerLinea4Monto4.ToString
            TBx_MontoSujeto5.Text = frmFactura.RetPerLinea4Monto5.ToString
            TBx_TotalRetenido1.Text = frmFactura.RetPerLinea4Valor1.ToString
            TBx_TotalRetenido2.Text = frmFactura.RetPerLinea4Valor2.ToString
            TBx_TotalRetenido3.Text = frmFactura.RetPerLinea4Valor3.ToString
            TBx_TotalRetenido4.Text = frmFactura.RetPerLinea4Valor4.ToString
            TBx_TotalRetenido5.Text = frmFactura.RetPerLinea4Valor5.ToString
        End If
        If frmFactura.LineaSeleccionada = 5 Then
            TBx_Codigo1.Text = frmFactura.RetPerLinea5Cod1.ToString
            TBx_Codigo2.Text = frmFactura.RetPerLinea5Cod2.ToString
            TBx_Codigo3.Text = frmFactura.RetPerLinea5Cod3.ToString
            TBx_Codigo4.Text = frmFactura.RetPerLinea5Cod4.ToString
            TBx_Codigo5.Text = frmFactura.RetPerLinea5Cod5.ToString
            TBx_Tasa1.Text = frmFactura.RetPerLinea5Tasa1.ToString
            TBx_Tasa2.Text = frmFactura.RetPerLinea5Tasa2.ToString
            TBx_Tasa3.Text = frmFactura.RetPerLinea5Tasa3.ToString
            TBx_Tasa4.Text = frmFactura.RetPerLinea5Tasa4.ToString
            TBx_Tasa5.Text = frmFactura.RetPerLinea5Tasa5.ToString
            TBx_MontoSujeto1.Text = frmFactura.RetPerLinea5Monto1.ToString
            TBx_MontoSujeto2.Text = frmFactura.RetPerLinea5Monto2.ToString
            TBx_MontoSujeto3.Text = frmFactura.RetPerLinea5Monto3.ToString
            TBx_MontoSujeto4.Text = frmFactura.RetPerLinea5Monto4.ToString
            TBx_MontoSujeto5.Text = frmFactura.RetPerLinea5Monto5.ToString
            TBx_TotalRetenido1.Text = frmFactura.RetPerLinea5Valor1.ToString
            TBx_TotalRetenido2.Text = frmFactura.RetPerLinea5Valor2.ToString
            TBx_TotalRetenido3.Text = frmFactura.RetPerLinea5Valor3.ToString
            TBx_TotalRetenido4.Text = frmFactura.RetPerLinea5Valor4.ToString
            TBx_TotalRetenido5.Text = frmFactura.RetPerLinea5Valor5.ToString
        End If
        If frmFactura.LineaSeleccionada = 6 Then
            TBx_Codigo1.Text = frmFactura.RetPerLinea6Cod1.ToString
            TBx_Codigo2.Text = frmFactura.RetPerLinea6Cod2.ToString
            TBx_Codigo3.Text = frmFactura.RetPerLinea6Cod3.ToString
            TBx_Codigo4.Text = frmFactura.RetPerLinea6Cod4.ToString
            TBx_Codigo5.Text = frmFactura.RetPerLinea6Cod5.ToString
            TBx_Tasa1.Text = frmFactura.RetPerLinea6Tasa1.ToString
            TBx_Tasa2.Text = frmFactura.RetPerLinea6Tasa2.ToString
            TBx_Tasa3.Text = frmFactura.RetPerLinea6Tasa3.ToString
            TBx_Tasa4.Text = frmFactura.RetPerLinea6Tasa4.ToString
            TBx_Tasa5.Text = frmFactura.RetPerLinea6Tasa5.ToString
            TBx_MontoSujeto1.Text = frmFactura.RetPerLinea6Monto1.ToString
            TBx_MontoSujeto2.Text = frmFactura.RetPerLinea6Monto2.ToString
            TBx_MontoSujeto3.Text = frmFactura.RetPerLinea6Monto3.ToString
            TBx_MontoSujeto4.Text = frmFactura.RetPerLinea6Monto4.ToString
            TBx_MontoSujeto5.Text = frmFactura.RetPerLinea6Monto5.ToString
            TBx_TotalRetenido1.Text = frmFactura.RetPerLinea6Valor1.ToString
            TBx_TotalRetenido2.Text = frmFactura.RetPerLinea6Valor2.ToString
            TBx_TotalRetenido3.Text = frmFactura.RetPerLinea6Valor3.ToString
            TBx_TotalRetenido4.Text = frmFactura.RetPerLinea6Valor4.ToString
            TBx_TotalRetenido5.Text = frmFactura.RetPerLinea6Valor5.ToString
        End If
        If frmFactura.LineaSeleccionada = 7 Then
            TBx_Codigo1.Text = frmFactura.RetPerLinea7Cod1.ToString
            TBx_Codigo2.Text = frmFactura.RetPerLinea7Cod2.ToString
            TBx_Codigo3.Text = frmFactura.RetPerLinea7Cod3.ToString
            TBx_Codigo4.Text = frmFactura.RetPerLinea7Cod4.ToString
            TBx_Codigo5.Text = frmFactura.RetPerLinea7Cod5.ToString
            TBx_Tasa1.Text = frmFactura.RetPerLinea7Tasa1.ToString
            TBx_Tasa2.Text = frmFactura.RetPerLinea7Tasa2.ToString
            TBx_Tasa3.Text = frmFactura.RetPerLinea7Tasa3.ToString
            TBx_Tasa4.Text = frmFactura.RetPerLinea7Tasa4.ToString
            TBx_Tasa5.Text = frmFactura.RetPerLinea7Tasa5.ToString
            TBx_MontoSujeto1.Text = frmFactura.RetPerLinea7Monto1.ToString
            TBx_MontoSujeto2.Text = frmFactura.RetPerLinea7Monto2.ToString
            TBx_MontoSujeto3.Text = frmFactura.RetPerLinea7Monto3.ToString
            TBx_MontoSujeto4.Text = frmFactura.RetPerLinea7Monto4.ToString
            TBx_MontoSujeto5.Text = frmFactura.RetPerLinea7Monto5.ToString
            TBx_TotalRetenido1.Text = frmFactura.RetPerLinea7Valor1.ToString
            TBx_TotalRetenido2.Text = frmFactura.RetPerLinea7Valor2.ToString
            TBx_TotalRetenido3.Text = frmFactura.RetPerLinea7Valor3.ToString
            TBx_TotalRetenido4.Text = frmFactura.RetPerLinea7Valor4.ToString
            TBx_TotalRetenido5.Text = frmFactura.RetPerLinea7Valor5.ToString
        End If
        If frmFactura.LineaSeleccionada = 8 Then
            TBx_Codigo1.Text = frmFactura.RetPerLinea8Cod1.ToString
            TBx_Codigo2.Text = frmFactura.RetPerLinea8Cod2.ToString
            TBx_Codigo3.Text = frmFactura.RetPerLinea8Cod3.ToString
            TBx_Codigo4.Text = frmFactura.RetPerLinea8Cod4.ToString
            TBx_Codigo5.Text = frmFactura.RetPerLinea8Cod5.ToString
            TBx_Tasa1.Text = frmFactura.RetPerLinea8Tasa1.ToString
            TBx_Tasa2.Text = frmFactura.RetPerLinea8Tasa2.ToString
            TBx_Tasa3.Text = frmFactura.RetPerLinea8Tasa3.ToString
            TBx_Tasa4.Text = frmFactura.RetPerLinea8Tasa4.ToString
            TBx_Tasa5.Text = frmFactura.RetPerLinea8Tasa5.ToString
            TBx_MontoSujeto1.Text = frmFactura.RetPerLinea8Monto1.ToString
            TBx_MontoSujeto2.Text = frmFactura.RetPerLinea8Monto2.ToString
            TBx_MontoSujeto3.Text = frmFactura.RetPerLinea8Monto3.ToString
            TBx_MontoSujeto4.Text = frmFactura.RetPerLinea8Monto4.ToString
            TBx_MontoSujeto5.Text = frmFactura.RetPerLinea8Monto5.ToString
            TBx_TotalRetenido1.Text = frmFactura.RetPerLinea8Valor1.ToString
            TBx_TotalRetenido2.Text = frmFactura.RetPerLinea8Valor2.ToString
            TBx_TotalRetenido3.Text = frmFactura.RetPerLinea8Valor3.ToString
            TBx_TotalRetenido4.Text = frmFactura.RetPerLinea8Valor4.ToString
            TBx_TotalRetenido5.Text = frmFactura.RetPerLinea8Valor5.ToString
        End If
        If frmFactura.LineaSeleccionada = 9 Then
            TBx_Codigo1.Text = frmFactura.RetPerLinea9Cod1.ToString
            TBx_Codigo2.Text = frmFactura.RetPerLinea9Cod2.ToString
            TBx_Codigo3.Text = frmFactura.RetPerLinea9Cod3.ToString
            TBx_Codigo4.Text = frmFactura.RetPerLinea9Cod4.ToString
            TBx_Codigo5.Text = frmFactura.RetPerLinea9Cod5.ToString
            TBx_Tasa1.Text = frmFactura.RetPerLinea9Tasa1.ToString
            TBx_Tasa2.Text = frmFactura.RetPerLinea9Tasa2.ToString
            TBx_Tasa3.Text = frmFactura.RetPerLinea9Tasa3.ToString
            TBx_Tasa4.Text = frmFactura.RetPerLinea9Tasa4.ToString
            TBx_Tasa5.Text = frmFactura.RetPerLinea9Tasa5.ToString
            TBx_MontoSujeto1.Text = frmFactura.RetPerLinea9Monto1.ToString
            TBx_MontoSujeto2.Text = frmFactura.RetPerLinea9Monto2.ToString
            TBx_MontoSujeto3.Text = frmFactura.RetPerLinea9Monto3.ToString
            TBx_MontoSujeto4.Text = frmFactura.RetPerLinea9Monto4.ToString
            TBx_MontoSujeto5.Text = frmFactura.RetPerLinea9Monto5.ToString
            TBx_TotalRetenido1.Text = frmFactura.RetPerLinea9Valor1.ToString
            TBx_TotalRetenido2.Text = frmFactura.RetPerLinea9Valor2.ToString
            TBx_TotalRetenido3.Text = frmFactura.RetPerLinea9Valor3.ToString
            TBx_TotalRetenido4.Text = frmFactura.RetPerLinea9Valor4.ToString
            TBx_TotalRetenido5.Text = frmFactura.RetPerLinea9Valor5.ToString
        End If
        If frmFactura.LineaSeleccionada = 10 Then
            TBx_Codigo1.Text = frmFactura.RetPerLinea10Cod1.ToString
            TBx_Codigo2.Text = frmFactura.RetPerLinea10Cod2.ToString
            TBx_Codigo3.Text = frmFactura.RetPerLinea10Cod3.ToString
            TBx_Codigo4.Text = frmFactura.RetPerLinea10Cod4.ToString
            TBx_Codigo5.Text = frmFactura.RetPerLinea10Cod5.ToString
            TBx_Tasa1.Text = frmFactura.RetPerLinea10Tasa1.ToString
            TBx_Tasa2.Text = frmFactura.RetPerLinea10Tasa2.ToString
            TBx_Tasa3.Text = frmFactura.RetPerLinea10Tasa3.ToString
            TBx_Tasa4.Text = frmFactura.RetPerLinea10Tasa4.ToString
            TBx_Tasa5.Text = frmFactura.RetPerLinea10Tasa5.ToString
            TBx_MontoSujeto1.Text = frmFactura.RetPerLinea10Monto1.ToString
            TBx_MontoSujeto2.Text = frmFactura.RetPerLinea10Monto2.ToString
            TBx_MontoSujeto3.Text = frmFactura.RetPerLinea10Monto3.ToString
            TBx_MontoSujeto4.Text = frmFactura.RetPerLinea10Monto4.ToString
            TBx_MontoSujeto5.Text = frmFactura.RetPerLinea10Monto5.ToString
            TBx_TotalRetenido1.Text = frmFactura.RetPerLinea10Valor1.ToString
            TBx_TotalRetenido2.Text = frmFactura.RetPerLinea10Valor2.ToString
            TBx_TotalRetenido3.Text = frmFactura.RetPerLinea10Valor3.ToString
            TBx_TotalRetenido4.Text = frmFactura.RetPerLinea10Valor4.ToString
            TBx_TotalRetenido5.Text = frmFactura.RetPerLinea10Valor5.ToString
        End If
    End Sub


End Class