﻿Public Class frmRespuesta

    Dim XMLRespuesta As String
    Dim soloCobrar As Boolean

    Sub New(ByVal oXMLRespuesta As String, ByVal soloCobrar As Boolean)
        InitializeComponent()
        Me.soloCobrar = soloCobrar
        XMLRespuesta = oXMLRespuesta
    End Sub

    Private Sub frmRespuesta_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Enter Then
            Me.Close()
        ElseIf e.KeyCode = Keys.Escape Then
            Me.Close()
        End If
    End Sub

    Public Sub CargarNroTransAct(ByVal TrnId As Integer, ByVal NroTicket As Integer, ByVal Lote As Integer, ByVal NroAut As Double)
        Label12.Text = TrnId
        Label13.Text = NroTicket
        Label15.Text = Lote
        Label18.Text = NroAut
    End Sub

    Private Sub frmRespuesta_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.KeyPreview = True
        If Not soloCobrar Then
            Dim objParsing As New TAFACE2ApiParsing.TAParsing_v0206
            Dim ErrorMsg As String = ""
            Dim ErrorCod As Integer = 0

            'Realizo desarmado del mensaje de respuesta 
            Dim WarningMsg As String = ""
            Dim FirmadoOk As Boolean = False
            Dim FirmaFecha, CAEVencimiento As Date
            Dim Serie As String = ""
            Dim UrlVerificarQR As String = ""
            Dim UrlVerificarTexto As String = ""
            Dim CAECodVerif As String = ""
            Dim ResolucionIva As String = ""
            Dim CAENA, CAEInicial, CAEFinal, CAENro As Long

            objParsing.NuevaFactura("", 0, TAFACE2ApiParsing.ITAParsing_v0206.enumTipoDeNegocio.GenericoPlaza, "C:/")
            If Not objParsing.UnParsingXMLRespuesta(XMLRespuesta, ErrorMsg, ErrorCod, WarningMsg, FirmadoOk, FirmaFecha, CAENA, CAEInicial, CAEFinal, CAEVencimiento, Serie, CAENro, CAECodVerif, UrlVerificarTexto, UrlVerificarQR, ResolucionIva) Then
                Throw New Exception(ErrorMsg)
            End If

            If FirmadoOk Then
                lblFechaFirma.Text = Format(FirmaFecha, "dd-MM-yyyy")
                lblCAESerie.Text = Serie
                lblCAENro.Text = CAENro
                lblCAEDesde.Text = CAEInicial
                lblCAEHasta.Text = CAEFinal
                lblCAENroAutoriz.Text = CAENA
                lblResIva.Text = ResolucionIva
                lblUrlVerificarTexto.Text = UrlVerificarTexto
                lblCodSeguridad.Text = CAECodVerif
                lblCAEVencimiento.Text = Format(CAEVencimiento, "dd-MM-yyyy")
            End If
            If ErrorMsg.Trim <> "" Then
                Me.Size = New Size(Me.Width, 438)
                txtMensaje.Text = "Error: " & ErrorMsg & vbCrLf
            End If
            If WarningMsg.Trim <> "" Then
                Me.Size = New Size(Me.Width, 438)
                txtMensaje.Text = "Advertencia: " & WarningMsg
            End If

            ''Generar codigo QR
            Dim ImgQR As Image
            Dim objTAFEApi As New TAFE2Api.TAFEApi_v0206
            If Not objTAFEApi.GenerarQRCode(ErrorMsg, ErrorCod, UrlVerificarQR, ImgQR) Then
                If Not UrlVerificarTexto = "" Then
                    Throw New Exception(ErrorMsg)
                End If
            Else
                pbxImagenQR.BackgroundImage = ImgQR
            End If
        End If
    End Sub

End Class