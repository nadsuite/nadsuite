﻿Public Class frmFacturaContingencia

    Public ReadOnly Property Serie As String
        Get
            Return txtSerie.Text
        End Get
    End Property

    Public ReadOnly Property Numero As Long
        Get
            Return txtNumero.Text
        End Get
    End Property

    Public ReadOnly Property CAENro As Long
        Get
            Return txtCAENro.Text
        End Get
    End Property

    Public ReadOnly Property CAENroDesde As Long
        Get
            Return txtCAENroDesde.Text
        End Get
    End Property

    Public ReadOnly Property CAENroHasta As Long
        Get
            Return txtCAENroHasta.Text
        End Get
    End Property

    Public ReadOnly Property CAEFchVencimiento As Date
        Get
            Return dtpCAEFechaVenc.Value
        End Get
    End Property

    Public ReadOnly Property CFCFecha As Date
        Get
            Return Dtp_CFCFecha.Value
        End Get
    End Property

    Private Sub btnComenzar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnComenzar.Click
        If Not ValidarDatos() Then
            Exit Sub
        Else
            Me.DialogResult = Windows.Forms.DialogResult.OK
            Me.Close()
        End If
    End Sub

    Private Sub frmFacturaContingencia_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        txtSerie.Focus()
    End Sub

    Private Function ValidarDatos() As Boolean
        If txtSerie.Text.Trim = "" Then
            MsgBox("Debe ingresar la serie del comprobante!")
            txtSerie.Focus()
            Return False
        End If
        If txtNumero.Text.Trim = "" OrElse Not IsNumeric(txtNumero.Text) Then
            MsgBox("Debe ingresar numero del comprobante!")
            txtNumero.Focus()
            Return False
        End If
        If txtCAENro.Text.Trim = "" OrElse Not IsNumeric(txtCAENro.Text) Then
            MsgBox("Debe ingresar numero de autorizacion!")
            txtCAENro.Focus()
            Return False
        End If
        If txtCAENroDesde.Text.Trim = "" OrElse Not IsNumeric(txtCAENroDesde.Text) Then
            MsgBox("Debe ingresar numero desde!")
            txtCAENroDesde.Focus()
            Return False
        End If
        If txtCAENroHasta.Text.Trim = "" OrElse Not IsNumeric(txtCAENroHasta.Text) Then
            MsgBox("Debe ingresar numero hasta!")
            txtCAENroHasta.Focus()
            Return False
        End If
        If dtpCAEFechaVenc.Value < Now Then
            MsgBox("Debe ingresar una fecha de vencimiento valida!")
            dtpCAEFechaVenc.Focus()
            Return False
        End If
        If Dtp_CFCFecha.Value > Now Then
            MsgBox("Debe ingresar una fecha de comprobante valida!")
            Dtp_CFCFecha.Focus()
            Return False
        End If
        Return True
    End Function

End Class