﻿Imports System.IO
Imports System.Xml.Serialization
<System.Xml.Serialization.XmlRootAttribute("CONFIGFACMANUAL", [Namespace]:="TAFACE", IsNullable:=False)>
Public Class XMLCONFIGFACMANUAL
    Private Const sNombreArchivoConfiguracion As String = "TAFACTURAMANUAL.config"
    Private sRutaArchivoConfiguracion As String = ""


    'Server
    Private uRLSERVIDORField As String
    Private tIMEOUTSERVIDORField As Integer
    'Configuracion Facturador
    Private rAZONSOCIALEMISORField As String
    Private nOMBREEMISORField As String
    Private rUTEMISORField As String
    Private gIRONEGOCIOEMISORField As String
    Private cORREOEMISORField As String
    Private dOMFISCALEMISORField As String
    Private dEPARTAMENTOEMISORField As String
    Private cIUDADEMISORField As String
    Private tELEFONOEMISORField As String
    Private tELEFONO2EMISORField As String
    Private cODDGISUCURSALField As Short
    Private nROSUCURSALField As Short
    Private nOMBRESUCURSALField As String
    Private nROCAJAField As Short
    Private nROCAJEROField As Short
    Private nROVENDEDORField As Short
    Private nOMBREVENDEDORField As String
    Private nOMBRECAJEROField As String
    Private tIPOMONEDAField As String
    Private tIPODECAMBIOField As Decimal
    Private tASABASICAIVAField As Decimal
    Private tASAMINIMAIVAField As Decimal
    Private vALORUNIDADINDEXADAField As Decimal
    Private pAISRECEPTORField As String
    Private nROFACTURAField As Double = 1
    Private ImpuestoPorDefectoField As String
    Private ConsumoFinalField As Boolean
    Private dESHABILITARConfig As Short
    Private bLOQUEARIMPUESTODEFECTOField As Short
    Private uSARETENCIONPERCEPCIONField As Short
    Private uSASUBTOTALESField As Short
    Private pREFACTURAField As Short
    Private uSAEXPORTACIONField As Short
    Private eMITEEXTRACFEField As Short

    'Configuracion impresora 
    Private iMPRIMEAPIField As Short
    Private nOMBREIMPRESORAField As String
    Private tIPOIMPRESORAField As Short
    Private cANTIDADCOPIASField As Short = 1

    Private tIPONEGOCIOField As Short
    Private cARPETAOPERACIONField As String

    'Agregados para la integracion NADSuite
    Private eMPCODField As String
    Private tERMCODField As String
    Private mODIFICARMONEDAField As Boolean
    Private mODIFICARMONTOSField As Boolean
    Private mODIFICARCUOTASField As Boolean
    Private mODIFICARFACTURAField As Boolean
    Private mODIFICARTARJETAField As Boolean
    Private mODIFICARPLANField As Boolean
    Private mODOFICARDECRETOLEYField As Boolean
    Private mODIFICARTIPOCUENTAField As Boolean
    Private iIMPRIMEVOUCHERADENDAField As Boolean



    Public Property URLSERVIDOR() As String
        Get
            Return uRLSERVIDORField
        End Get
        Set(ByVal value As String)
            uRLSERVIDORField = value
        End Set
    End Property

    Public Property TIMEOUTSERVIDOR() As Integer
        Get
            Return tIMEOUTSERVIDORField
        End Get
        Set(ByVal value As Integer)
            tIMEOUTSERVIDORField = value
        End Set
    End Property

    Public Property RAZONSOCIALENEMISOR() As String
        Get
            Return rAZONSOCIALEMISORField
        End Get
        Set(ByVal value As String)
            rAZONSOCIALEMISORField = value
        End Set
    End Property

    Public Property NOMBREEMISOR() As String
        Get
            Return nOMBREEMISORField
        End Get
        Set(ByVal value As String)
            nOMBREEMISORField = value
        End Set
    End Property

    Public Property RUTEMISOR() As String
        Get
            Return rUTEMISORField
        End Get
        Set(ByVal value As String)
            rUTEMISORField = value
        End Set
    End Property

    Public Property GIRONEGOCIOEMISOR() As String
        Get
            Return gIRONEGOCIOEMISORField
        End Get
        Set(ByVal value As String)
            gIRONEGOCIOEMISORField = value
        End Set
    End Property

    Public Property CORREOEMISOR() As String
        Get
            Return cORREOEMISORField
        End Get
        Set(ByVal value As String)
            cORREOEMISORField = value
        End Set
    End Property

    Public Property DOMFISCALEMISOR() As String
        Get
            Return dOMFISCALEMISORField
        End Get
        Set(ByVal value As String)
            dOMFISCALEMISORField = value
        End Set
    End Property

    Public Property DEPARTAMENTOEMISOR() As String
        Get
            Return dEPARTAMENTOEMISORField
        End Get
        Set(ByVal value As String)
            dEPARTAMENTOEMISORField = value
        End Set
    End Property

    Public Property CIUDADEMISOR() As String
        Get
            Return cIUDADEMISORField
        End Get
        Set(ByVal value As String)
            cIUDADEMISORField = value
        End Set
    End Property

    Public Property TELEFONOEMISOR() As String
        Get
            Return tELEFONOEMISORField
        End Get
        Set(ByVal value As String)
            tELEFONOEMISORField = value
        End Set
    End Property

    Public Property TELEFONO2EMISOR() As String
        Get
            Return tELEFONO2EMISORField
        End Get
        Set(ByVal value As String)
            tELEFONO2EMISORField = value
        End Set
    End Property

    Public Property CODDGISUCURSAL() As Short
        Get
            Return cODDGISUCURSALField
        End Get
        Set(ByVal value As Short)
            cODDGISUCURSALField = value
        End Set
    End Property

    Public Property NROSUCURSAL() As Short
        Get
            Return nROSUCURSALField
        End Get
        Set(ByVal value As Short)
            nROSUCURSALField = value
        End Set
    End Property

    Public Property NOMBRESUCURSAL() As String
        Get
            Return nOMBRESUCURSALField
        End Get
        Set(ByVal value As String)
            nOMBRESUCURSALField = value
        End Set
    End Property

    Public Property NROCAJA() As Short
        Get
            Return nROCAJAField
        End Get
        Set(ByVal value As Short)
            nROCAJAField = value
        End Set
    End Property

    Public Property NROCAJERO() As Short
        Get
            Return nROCAJEROField
        End Get
        Set(ByVal value As Short)
            nROCAJEROField = value
        End Set
    End Property

    Public Property NROVENDEDOR() As Short
        Get
            Return nROVENDEDORField
        End Get
        Set(ByVal value As Short)
            nROVENDEDORField = value
        End Set
    End Property

    Public Property NOMBREVENDEDOR() As String
        Get
            Return nOMBREVENDEDORField
        End Get
        Set(ByVal value As String)
            nOMBREVENDEDORField = value
        End Set
    End Property

    Public Property NOMBRECAJERO() As String
        Get
            Return nOMBRECAJEROField
        End Get
        Set(ByVal value As String)
            nOMBRECAJEROField = value
        End Set
    End Property

    Public Property TIPOMONEDA() As String
        Get
            Return tIPOMONEDAField
        End Get
        Set(ByVal value As String)
            tIPOMONEDAField = value
        End Set
    End Property

    Public Property TIPODECAMBIO() As Decimal
        Get
            Return tIPODECAMBIOField
        End Get
        Set(ByVal value As Decimal)
            tIPODECAMBIOField = value
        End Set
    End Property

    Public Property TASABASICAIVA() As Decimal
        Get
            Return tASABASICAIVAField
        End Get
        Set(ByVal value As Decimal)
            tASABASICAIVAField = value
        End Set
    End Property

    Public Property TASAMINIMAIVA() As Decimal
        Get
            Return tASAMINIMAIVAField
        End Get
        Set(ByVal value As Decimal)
            tASAMINIMAIVAField = value
        End Set
    End Property

    Public Property VALORUNIDADINDEXADA() As Decimal
        Get
            Return vALORUNIDADINDEXADAField
        End Get
        Set(ByVal value As Decimal)
            vALORUNIDADINDEXADAField = value
        End Set
    End Property

    Public Property PAISRECEPTOR() As String
        Get
            Return pAISRECEPTORField
        End Get
        Set(ByVal value As String)
            pAISRECEPTORField = value
        End Set
    End Property

    Public Property NROFACTURA() As Long
        Get
            Return nROFACTURAField
        End Get
        Set(ByVal value As Long)
            nROFACTURAField = value
        End Set
    End Property

    Public Property ImpuestoPorDefecto() As String
        Get
            Return ImpuestoPorDefectoField
        End Get
        Set(value As String)
            ImpuestoPorDefectoField = value
        End Set
    End Property

    Public Property ConsumoFinal() As Boolean
        Get
            Return ConsumoFinalField
        End Get
        Set(value As Boolean)
            ConsumoFinalField = value
        End Set
    End Property

    Public Property IMPRIMEAPI() As Short
        Get
            Return iMPRIMEAPIField
        End Get
        Set(ByVal value As Short)
            iMPRIMEAPIField = value
        End Set
    End Property

    Public Property NOMBREIMPRESORA() As String
        Get
            Return nOMBREIMPRESORAField
        End Get
        Set(ByVal value As String)
            nOMBREIMPRESORAField = value
        End Set
    End Property

    Public Property TIPOIMPRESORA() As Short
        Get
            Return tIPOIMPRESORAField
        End Get
        Set(ByVal value As Short)
            tIPOIMPRESORAField = value
        End Set
    End Property

    Public Property CANTIDADCOPIAS() As Short
        Get
            Return cANTIDADCOPIASField
        End Get
        Set(ByVal value As Short)
            cANTIDADCOPIASField = value
        End Set
    End Property

    Public Property TIPONEGOCIO() As Short
        Get
            Return tIPONEGOCIOField
        End Get
        Set(ByVal value As Short)
            tIPONEGOCIOField = value
        End Set
    End Property

    Public Property DESHABILITARCONFIGURACION() As Short
        Get
            Return dESHABILITARConfig
        End Get
        Set(value As Short)
            dESHABILITARConfig = value
        End Set
    End Property

    Public Property USARETENCIONPERCEPCION() As Short
        Get
            Return uSARETENCIONPERCEPCIONField
        End Get
        Set(value As Short)
            uSARETENCIONPERCEPCIONField = value
        End Set
    End Property

    Public Property USASUBTOTALES() As Short
        Get
            Return uSASUBTOTALESField
        End Get
        Set(value As Short)
            uSASUBTOTALESField = value
        End Set
    End Property

    Public Property PREFACTURA() As Short
        Get
            Return pREFACTURAField
        End Get
        Set(value As Short)
            pREFACTURAField = value
        End Set
    End Property

    Public Property BLOQUEARIMPUESTO() As Short
        Get
            Return bLOQUEARIMPUESTODEFECTOField
        End Get
        Set(value As Short)
            bLOQUEARIMPUESTODEFECTOField = value
        End Set
    End Property

    Public Property USAEXPORTACION() As Short
        Get
            Return uSAEXPORTACIONField
        End Get
        Set(value As Short)
            uSAEXPORTACIONField = value
        End Set
    End Property

    Public Property CARPETAOPERACION() As String
        Get
            Return cARPETAOPERACIONField
        End Get
        Set(value As String)
            cARPETAOPERACIONField = value
        End Set
    End Property

    Public Property EMITEEXTRACFE() As Short
        Get
            Return eMITEEXTRACFEField
        End Get
        Set(value As Short)
            eMITEEXTRACFEField = value
        End Set
    End Property

    Public Property EMPCOD As String
        Get
            Return eMPCODField
        End Get
        Set(value As String)
            eMPCODField = value
        End Set
    End Property

    Public Property TERMCOD As String
        Get
            Return tERMCODField
        End Get
        Set(value As String)
            tERMCODField = value
        End Set
    End Property

    Public Property MODIFICARMONEDA As Boolean
        Get
            Return mODIFICARMONEDAField
        End Get
        Set(value As Boolean)
            mODIFICARMONEDAField = value
        End Set
    End Property

    Public Property MODIFICARMONTOS As Boolean
        Get
            Return mODIFICARMONTOSField
        End Get
        Set(value As Boolean)
            mODIFICARMONTOSField = value
        End Set
    End Property

    Public Property MODIFICARCUOTAS As Boolean
        Get
            Return mODIFICARCUOTASField
        End Get
        Set(value As Boolean)
            mODIFICARCUOTASField = value
        End Set
    End Property

    Public Property MODIFICARFACTURA As Boolean
        Get
            Return mODIFICARFACTURAField
        End Get
        Set(value As Boolean)
            mODIFICARFACTURAField = value
        End Set
    End Property

    Public Property MODIFICARTARJETA As Boolean
        Get
            Return mODIFICARTARJETAField
        End Get
        Set(value As Boolean)
            mODIFICARTARJETAField = value
        End Set
    End Property

    Public Property MODIFICARPLAN As Boolean
        Get
            Return mODIFICARPLANField
        End Get
        Set(value As Boolean)
            mODIFICARPLANField = value
        End Set
    End Property

    Public Property MODOFICARDECRETOLEY As Boolean
        Get
            Return mODOFICARDECRETOLEYField
        End Get
        Set(value As Boolean)
            mODOFICARDECRETOLEYField = value
        End Set
    End Property

    Public Property MODIFICARTIPOCUENTA As Boolean
        Get
            Return mODIFICARTIPOCUENTAField
        End Get
        Set(value As Boolean)
            mODIFICARTIPOCUENTAField = value
        End Set
    End Property

    Public Property IIMPRIMEVOUCHERADENDA As Boolean
        Get
            Return iIMPRIMEVOUCHERADENDAField
        End Get
        Set(value As Boolean)
            iIMPRIMEVOUCHERADENDAField = value
        End Set
    End Property

    Public Function CargarConfiguracion() As XMLCONFIGFACMANUAL
        Dim sr As StreamReader = Nothing
        Try
            sRutaArchivoConfiguracion = System.AppDomain.CurrentDomain.BaseDirectory & sNombreArchivoConfiguracion
            If Not File.Exists(sRutaArchivoConfiguracion) Then
                'Si el config no existe lo creo
                'CrearConfiguracion()
                Return Nothing
            End If
            sr = New StreamReader(sRutaArchivoConfiguracion)
            Return LoadXML(sr.ReadToEnd)
        Catch ex As Exception
            Return Nothing
        Finally
            If Not IsNothing(sr) Then
                sr.Close()
            End If
        End Try
    End Function

    Private Sub CrearConfiguracion()
        tIMEOUTSERVIDORField = 1
        uRLSERVIDORField = ""
        rAZONSOCIALEMISORField = ""
        nOMBREEMISORField = ""
        rUTEMISORField = ""
        gIRONEGOCIOEMISORField = ""
        cORREOEMISORField = ""
        dOMFISCALEMISORField = ""
        dEPARTAMENTOEMISORField = ""
        cIUDADEMISORField = ""
        tELEFONOEMISORField = ""
        nROSUCURSALField = 0
        nOMBRESUCURSALField = ""
        nROCAJAField = 0
        nROCAJEROField = 0
        nROVENDEDORField = 0
        nOMBREVENDEDORField = ""
        nOMBRECAJEROField = ""
        tIPOMONEDAField = "UYU"
        tIPODECAMBIOField = 0
        tASABASICAIVAField = 0
        tASAMINIMAIVAField = 0
        vALORUNIDADINDEXADAField = 0
        pAISRECEPTORField = "UY"
        nROFACTURAField = 1
        iMPRIMEAPIField = 0
        ImpuestoPorDefectoField = "22%"
        dESHABILITARConfig = 0
        bLOQUEARIMPUESTODEFECTOField = 0
        uSARETENCIONPERCEPCIONField = 0
        uSASUBTOTALESField = 0
        pREFACTURAField = 0
        uSAEXPORTACIONField = 0
        cARPETAOPERACIONField = "C:\"
        eMITEEXTRACFEField = 0
        eMPCODField = ""
        tERMCODField = ""
        mODIFICARMONEDAField = False
        mODIFICARMONTOSField = False
        mODIFICARCUOTASField = False
        mODIFICARFACTURAField = False
        mODIFICARTARJETAField = False
        mODIFICARPLANField = False
        mODOFICARDECRETOLEYField = False
        mODIFICARTIPOCUENTAField = False
        iIMPRIMEVOUCHERADENDAField = True
        GuardarConfig()
    End Sub

    Public Sub GuardarConfig()
        Dim sw As StreamWriter
        Try
            If sRutaArchivoConfiguracion.Trim = "" Then
                sRutaArchivoConfiguracion = System.AppDomain.CurrentDomain.BaseDirectory & sNombreArchivoConfiguracion
            End If
            sw = New StreamWriter(sRutaArchivoConfiguracion)
            sw.Write(Me.ToXML)
        Catch ex As Exception
            Throw ex
        Finally
            If Not IsNothing(sw) Then
                sw.Close()
            End If
        End Try
    End Sub

    Private Function ToXML() As String
        Dim sw As New StringWriter
        Dim Serializer = New XmlSerializer(Me.GetType)
        Serializer.Serialize(sw, Me)
        Return Mid(sw.ToString, 42).Replace(" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema""", "").Replace("&#x0;", "")
    End Function

    Public Function LoadXML(XML As String) As XMLCONFIGFACMANUAL
        Dim Serializer = New XmlSerializer(Me.GetType)
        Return Serializer.Deserialize(New StringReader(XML))
    End Function


End Class
