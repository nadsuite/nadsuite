﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFacturaContingencia
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFacturaContingencia))
        Me.Dtp_CFCFecha = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dtpCAEFechaVenc = New System.Windows.Forms.DateTimePicker()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtCAENroHasta = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtCAENroDesde = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtCAENro = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtNumero = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtSerie = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnComenzar = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Dtp_CFCFecha
        '
        Me.Dtp_CFCFecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Dtp_CFCFecha.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.Dtp_CFCFecha.Location = New System.Drawing.Point(128, 59)
        Me.Dtp_CFCFecha.MinDate = New Date(2011, 10, 1, 0, 0, 0, 0)
        Me.Dtp_CFCFecha.Name = "Dtp_CFCFecha"
        Me.Dtp_CFCFecha.Size = New System.Drawing.Size(260, 31)
        Me.Dtp_CFCFecha.TabIndex = 33
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(29, 66)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(98, 20)
        Me.Label1.TabIndex = 34
        Me.Label1.Text = "Fecha CFC :"
        '
        'dtpCAEFechaVenc
        '
        Me.dtpCAEFechaVenc.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpCAEFechaVenc.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpCAEFechaVenc.Location = New System.Drawing.Point(128, 271)
        Me.dtpCAEFechaVenc.MinDate = New Date(2011, 10, 1, 0, 0, 0, 0)
        Me.dtpCAEFechaVenc.Name = "dtpCAEFechaVenc"
        Me.dtpCAEFechaVenc.Size = New System.Drawing.Size(260, 31)
        Me.dtpCAEFechaVenc.TabIndex = 24
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(32, 278)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(95, 20)
        Me.Label9.TabIndex = 32
        Me.Label9.Text = "CAE Venc. :"
        '
        'txtCAENroHasta
        '
        Me.txtCAENroHasta.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCAENroHasta.Location = New System.Drawing.Point(128, 227)
        Me.txtCAENroHasta.Name = "txtCAENroHasta"
        Me.txtCAENroHasta.Size = New System.Drawing.Size(260, 38)
        Me.txtCAENroHasta.TabIndex = 23
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(30, 240)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(97, 20)
        Me.Label8.TabIndex = 31
        Me.Label8.Text = "CAE Hasta :"
        '
        'txtCAENroDesde
        '
        Me.txtCAENroDesde.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCAENroDesde.Location = New System.Drawing.Point(128, 183)
        Me.txtCAENroDesde.Name = "txtCAENroDesde"
        Me.txtCAENroDesde.Size = New System.Drawing.Size(260, 38)
        Me.txtCAENroDesde.TabIndex = 22
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(26, 196)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(101, 20)
        Me.Label7.TabIndex = 30
        Me.Label7.Text = "CAE Desde :"
        '
        'txtCAENro
        '
        Me.txtCAENro.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCAENro.Location = New System.Drawing.Point(128, 139)
        Me.txtCAENro.Name = "txtCAENro"
        Me.txtCAENro.Size = New System.Drawing.Size(260, 38)
        Me.txtCAENro.TabIndex = 21
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(44, 152)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(83, 20)
        Me.Label6.TabIndex = 29
        Me.Label6.Text = "CAE Nro. :"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(193, 109)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(46, 20)
        Me.Label5.TabIndex = 28
        Me.Label5.Text = "Nro. :"
        '
        'txtNumero
        '
        Me.txtNumero.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNumero.Location = New System.Drawing.Point(241, 96)
        Me.txtNumero.Name = "txtNumero"
        Me.txtNumero.Size = New System.Drawing.Size(147, 38)
        Me.txtNumero.TabIndex = 20
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(77, 109)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(50, 20)
        Me.Label4.TabIndex = 27
        Me.Label4.Text = "Serie:"
        '
        'txtSerie
        '
        Me.txtSerie.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSerie.Location = New System.Drawing.Point(128, 96)
        Me.txtSerie.MaxLength = 2
        Me.txtSerie.Name = "txtSerie"
        Me.txtSerie.Size = New System.Drawing.Size(59, 38)
        Me.txtSerie.TabIndex = 19
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(62, 18)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(332, 20)
        Me.Label3.TabIndex = 25
        Me.Label3.Text = "Ingresar datos del papel preimpreso a Utilizar:"
        '
        'btnComenzar
        '
        Me.btnComenzar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnComenzar.Location = New System.Drawing.Point(241, 326)
        Me.btnComenzar.Name = "btnComenzar"
        Me.btnComenzar.Size = New System.Drawing.Size(174, 49)
        Me.btnComenzar.TabIndex = 26
        Me.btnComenzar.Text = "&Comenzar Cont."
        Me.btnComenzar.UseVisualStyleBackColor = True
        '
        'frmFacturaContingencia
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(441, 393)
        Me.Controls.Add(Me.Dtp_CFCFecha)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dtpCAEFechaVenc)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.txtCAENroHasta)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.txtCAENroDesde)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txtCAENro)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txtNumero)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtSerie)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.btnComenzar)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmFacturaContingencia"
        Me.Text = "Datos de Entrada en Contingencia"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Dtp_CFCFecha As DateTimePicker
    Friend WithEvents Label1 As Label
    Friend WithEvents dtpCAEFechaVenc As DateTimePicker
    Friend WithEvents Label9 As Label
    Friend WithEvents txtCAENroHasta As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents txtCAENroDesde As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents txtCAENro As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents txtNumero As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents txtSerie As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents btnComenzar As Button
End Class
