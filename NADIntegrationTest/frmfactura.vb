﻿Imports System.IO
Imports NADIntegration
Imports NADIntegration.NADIntegration

Public Class frmFactura

    Private Enum TipoDocumento
        VentaContado = 1
        VentaCredito = 2
        NotaDebito = 3
        NotaCredito = 4
    End Enum

    Private Enum TipoCFEFacturador
        eTicket = 101
        eFactura = 111
        eFacturaExportacion = 121
        eRemito = 181
        eRemitoExportacion = 124
        eBoleta = 151
        eBoletaNotaCred = 152
        eBoletaNotaDeb = 153
    End Enum

    Private Enum IndicadorDeFacturacionDetalle
        ExentoIVA = 1
        TasaMinima = 2
        TasaBasica = 3
        ExportacionYAsimiladas = 10
        ItemVendidoPorNoContribuyente = 13
        ItemVendidoContribuyenteMonotributo = 14
        ItemVendidoContribuyenteIMEBA = 15
    End Enum

    Enum TipoDocumentoDelReceptor
        SinDefinir = 0
        RUC = 2
        CI = 3
        Otros = 4
        Pasaporte = 5
        DNI = 6
        NIFE = 7
    End Enum

#Region "Try System"
    Private WithEvents MainMenu As ContextMenuStrip
    Private WithEvents mnuDisplayForm As ToolStripMenuItem
    Dim mnuSep1 As ToolStripSeparator
    Private WithEvents mnuExit As ToolStripMenuItem

    Private Sub mnuDisplayForm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuDisplayForm.Click
        If Me.WindowState = FormWindowState.Normal Then
            Me.ShowInTaskbar = False
            Me.WindowState = FormWindowState.Minimized
        Else
            Me.ShowInTaskbar = True
            Me.WindowState = FormWindowState.Normal
        End If
    End Sub

    Private Sub mnuExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuExit.Click
        Close()
    End Sub

    Private Sub NotifyIcon1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles NotifyIcon1.DoubleClick
        If Me.WindowState = FormWindowState.Normal Then
            Me.ShowInTaskbar = False
            Me.WindowState = FormWindowState.Minimized
        Else
            Me.ShowInTaskbar = True
            Me.WindowState = FormWindowState.Normal
        End If
    End Sub

#End Region

    'Encabezado
    Private TipoCFE As NADIntegration.NADIntegration.enumTipoDeComprobanteCFE
    Private TipoCFEAux As Integer

    Private objConfig As XMLCONFIGFACMANUAL

    Private clientesField As List(Of cliente) = New List(Of cliente)
    Private articulosField As List(Of articulo) = New List(Of articulo)

    '----------------------------------
    ' Variables usadas para FE

    Private nCantLineas As Integer = 0
    Private TotalMontoExportacionAsimiladas As Decimal = 0
    Private TotalMontoTotalTotalesEncabezado As Decimal = 0

    Private SubDescuentoValor1 As Decimal = 0
    Private SubDescuentoValor2 As Decimal = 0
    Private SubDescuentoValor3 As Decimal = 0
    Private SubDescuentoValor4 As Decimal = 0
    Private SubDescuentoValor5 As Decimal = 0
    Private SubDescuentoValor6 As Decimal = 0
    Private SubDescuentoValor7 As Decimal = 0
    Private SubDescuentoValor8 As Decimal = 0
    Private SubDescuentoValor9 As Decimal = 0
    Private SubDescuentoValor10 As Decimal = 0

    Public LineaSeleccionada As Integer = 0
    Public RetPerLinea1Cod1 As Integer = 0
    Public RetPerLinea1Cod2 As Integer = 0
    Public RetPerLinea1Cod3 As Integer = 0
    Public RetPerLinea1Cod4 As Integer = 0
    Public RetPerLinea1Cod5 As Integer = 0

    Public RetPerLinea2Cod1 As Integer = 0
    Public RetPerLinea2Cod2 As Integer = 0
    Public RetPerLinea2Cod3 As Integer = 0
    Public RetPerLinea2Cod4 As Integer = 0
    Public RetPerLinea2Cod5 As Integer = 0

    Public RetPerLinea3Cod1 As Integer = 0
    Public RetPerLinea3Cod2 As Integer = 0
    Public RetPerLinea3Cod3 As Integer = 0
    Public RetPerLinea3Cod4 As Integer = 0
    Public RetPerLinea3Cod5 As Integer = 0

    Public RetPerLinea4Cod1 As Integer = 0
    Public RetPerLinea4Cod2 As Integer = 0
    Public RetPerLinea4Cod3 As Integer = 0
    Public RetPerLinea4Cod4 As Integer = 0
    Public RetPerLinea4Cod5 As Integer = 0

    Public RetPerLinea5Cod1 As Integer = 0
    Public RetPerLinea5Cod2 As Integer = 0
    Public RetPerLinea5Cod3 As Integer = 0
    Public RetPerLinea5Cod4 As Integer = 0
    Public RetPerLinea5Cod5 As Integer = 0

    Public RetPerLinea6Cod1 As Integer = 0
    Public RetPerLinea6Cod2 As Integer = 0
    Public RetPerLinea6Cod3 As Integer = 0
    Public RetPerLinea6Cod4 As Integer = 0
    Public RetPerLinea6Cod5 As Integer = 0

    Public RetPerLinea7Cod1 As Integer = 0
    Public RetPerLinea7Cod2 As Integer = 0
    Public RetPerLinea7Cod3 As Integer = 0
    Public RetPerLinea7Cod4 As Integer = 0
    Public RetPerLinea7Cod5 As Integer = 0

    Public RetPerLinea8Cod1 As Integer = 0
    Public RetPerLinea8Cod2 As Integer = 0
    Public RetPerLinea8Cod3 As Integer = 0
    Public RetPerLinea8Cod4 As Integer = 0
    Public RetPerLinea8Cod5 As Integer = 0

    Public RetPerLinea9Cod1 As Integer = 0
    Public RetPerLinea9Cod2 As Integer = 0
    Public RetPerLinea9Cod3 As Integer = 0
    Public RetPerLinea9Cod4 As Integer = 0
    Public RetPerLinea9Cod5 As Integer = 0

    Public RetPerLinea10Cod1 As Integer = 0
    Public RetPerLinea10Cod2 As Integer = 0
    Public RetPerLinea10Cod3 As Integer = 0
    Public RetPerLinea10Cod4 As Integer = 0
    Public RetPerLinea10Cod5 As Integer = 0

    Public RetPerLinea1Tasa1 As Decimal = 0
    Public RetPerLinea1Tasa2 As Decimal = 0
    Public RetPerLinea1Tasa3 As Decimal = 0
    Public RetPerLinea1Tasa4 As Decimal = 0
    Public RetPerLinea1Tasa5 As Decimal = 0

    Public RetPerLinea2Tasa1 As Decimal = 0
    Public RetPerLinea2Tasa2 As Decimal = 0
    Public RetPerLinea2Tasa3 As Decimal = 0
    Public RetPerLinea2Tasa4 As Decimal = 0
    Public RetPerLinea2Tasa5 As Decimal = 0

    Public RetPerLinea3Tasa1 As Decimal = 0
    Public RetPerLinea3Tasa2 As Decimal = 0
    Public RetPerLinea3Tasa3 As Decimal = 0
    Public RetPerLinea3Tasa4 As Decimal = 0
    Public RetPerLinea3Tasa5 As Decimal = 0

    Public RetPerLinea4Tasa1 As Decimal = 0
    Public RetPerLinea4Tasa2 As Decimal = 0
    Public RetPerLinea4Tasa3 As Decimal = 0
    Public RetPerLinea4Tasa4 As Decimal = 0
    Public RetPerLinea4Tasa5 As Decimal = 0

    Public RetPerLinea5Tasa1 As Decimal = 0
    Public RetPerLinea5Tasa2 As Decimal = 0
    Public RetPerLinea5Tasa3 As Decimal = 0
    Public RetPerLinea5Tasa4 As Decimal = 0
    Public RetPerLinea5Tasa5 As Decimal = 0

    Public RetPerLinea6Tasa1 As Decimal = 0
    Public RetPerLinea6Tasa2 As Decimal = 0
    Public RetPerLinea6Tasa3 As Decimal = 0
    Public RetPerLinea6Tasa4 As Decimal = 0
    Public RetPerLinea6Tasa5 As Decimal = 0

    Public RetPerLinea7Tasa1 As Decimal = 0
    Public RetPerLinea7Tasa2 As Decimal = 0
    Public RetPerLinea7Tasa3 As Decimal = 0
    Public RetPerLinea7Tasa4 As Decimal = 0
    Public RetPerLinea7Tasa5 As Decimal = 0

    Public RetPerLinea8Tasa1 As Decimal = 0
    Public RetPerLinea8Tasa2 As Decimal = 0
    Public RetPerLinea8Tasa3 As Decimal = 0
    Public RetPerLinea8Tasa4 As Decimal = 0
    Public RetPerLinea8Tasa5 As Decimal = 0

    Public RetPerLinea9Tasa1 As Decimal = 0
    Public RetPerLinea9Tasa2 As Decimal = 0
    Public RetPerLinea9Tasa3 As Decimal = 0
    Public RetPerLinea9Tasa4 As Decimal = 0
    Public RetPerLinea9Tasa5 As Decimal = 0

    Public RetPerLinea10Tasa1 As Decimal = 0
    Public RetPerLinea10Tasa2 As Decimal = 0
    Public RetPerLinea10Tasa3 As Decimal = 0
    Public RetPerLinea10Tasa4 As Decimal = 0
    Public RetPerLinea10Tasa5 As Decimal = 0

    Public RetPerLinea1Monto1 As Decimal = 0
    Public RetPerLinea1Monto2 As Decimal = 0
    Public RetPerLinea1Monto3 As Decimal = 0
    Public RetPerLinea1Monto4 As Decimal = 0
    Public RetPerLinea1Monto5 As Decimal = 0

    Public RetPerLinea2Monto1 As Decimal = 0
    Public RetPerLinea2Monto2 As Decimal = 0
    Public RetPerLinea2Monto3 As Decimal = 0
    Public RetPerLinea2Monto4 As Decimal = 0
    Public RetPerLinea2Monto5 As Decimal = 0

    Public RetPerLinea3Monto1 As Decimal = 0
    Public RetPerLinea3Monto2 As Decimal = 0
    Public RetPerLinea3Monto3 As Decimal = 0
    Public RetPerLinea3Monto4 As Decimal = 0
    Public RetPerLinea3Monto5 As Decimal = 0

    Public RetPerLinea4Monto1 As Decimal = 0
    Public RetPerLinea4Monto2 As Decimal = 0
    Public RetPerLinea4Monto3 As Decimal = 0
    Public RetPerLinea4Monto4 As Decimal = 0
    Public RetPerLinea4Monto5 As Decimal = 0

    Public RetPerLinea5Monto1 As Decimal = 0
    Public RetPerLinea5Monto2 As Decimal = 0
    Public RetPerLinea5Monto3 As Decimal = 0
    Public RetPerLinea5Monto4 As Decimal = 0
    Public RetPerLinea5Monto5 As Decimal = 0

    Public RetPerLinea6Monto1 As Decimal = 0
    Public RetPerLinea6Monto2 As Decimal = 0
    Public RetPerLinea6Monto3 As Decimal = 0
    Public RetPerLinea6Monto4 As Decimal = 0
    Public RetPerLinea6Monto5 As Decimal = 0

    Public RetPerLinea7Monto1 As Decimal = 0
    Public RetPerLinea7Monto2 As Decimal = 0
    Public RetPerLinea7Monto3 As Decimal = 0
    Public RetPerLinea7Monto4 As Decimal = 0
    Public RetPerLinea7Monto5 As Decimal = 0

    Public RetPerLinea8Monto1 As Decimal = 0
    Public RetPerLinea8Monto2 As Decimal = 0
    Public RetPerLinea8Monto3 As Decimal = 0
    Public RetPerLinea8Monto4 As Decimal = 0
    Public RetPerLinea8Monto5 As Decimal = 0

    Public RetPerLinea9Monto1 As Decimal = 0
    Public RetPerLinea9Monto2 As Decimal = 0
    Public RetPerLinea9Monto3 As Decimal = 0
    Public RetPerLinea9Monto4 As Decimal = 0
    Public RetPerLinea9Monto5 As Decimal = 0

    Public RetPerLinea10Monto1 As Decimal = 0
    Public RetPerLinea10Monto2 As Decimal = 0
    Public RetPerLinea10Monto3 As Decimal = 0
    Public RetPerLinea10Monto4 As Decimal = 0
    Public RetPerLinea10Monto5 As Decimal = 0

    Public RetPerLinea1Valor1 As Decimal = 0
    Public RetPerLinea1Valor2 As Decimal = 0
    Public RetPerLinea1Valor3 As Decimal = 0
    Public RetPerLinea1Valor4 As Decimal = 0
    Public RetPerLinea1Valor5 As Decimal = 0

    Public RetPerLinea2Valor1 As Decimal = 0
    Public RetPerLinea2Valor2 As Decimal = 0
    Public RetPerLinea2Valor3 As Decimal = 0
    Public RetPerLinea2Valor4 As Decimal = 0
    Public RetPerLinea2Valor5 As Decimal = 0

    Public RetPerLinea3Valor1 As Decimal = 0
    Public RetPerLinea3Valor2 As Decimal = 0
    Public RetPerLinea3Valor3 As Decimal = 0
    Public RetPerLinea3Valor4 As Decimal = 0
    Public RetPerLinea3Valor5 As Decimal = 0

    Public RetPerLinea4Valor1 As Decimal = 0
    Public RetPerLinea4Valor2 As Decimal = 0
    Public RetPerLinea4Valor3 As Decimal = 0
    Public RetPerLinea4Valor4 As Decimal = 0
    Public RetPerLinea4Valor5 As Decimal = 0

    Public RetPerLinea5Valor1 As Decimal = 0
    Public RetPerLinea5Valor2 As Decimal = 0
    Public RetPerLinea5Valor3 As Decimal = 0
    Public RetPerLinea5Valor4 As Decimal = 0
    Public RetPerLinea5Valor5 As Decimal = 0

    Public RetPerLinea6Valor1 As Decimal = 0
    Public RetPerLinea6Valor2 As Decimal = 0
    Public RetPerLinea6Valor3 As Decimal = 0
    Public RetPerLinea6Valor4 As Decimal = 0
    Public RetPerLinea6Valor5 As Decimal = 0

    Public RetPerLinea7Valor1 As Decimal = 0
    Public RetPerLinea7Valor2 As Decimal = 0
    Public RetPerLinea7Valor3 As Decimal = 0
    Public RetPerLinea7Valor4 As Decimal = 0
    Public RetPerLinea7Valor5 As Decimal = 0

    Public RetPerLinea8Valor1 As Decimal = 0
    Public RetPerLinea8Valor2 As Decimal = 0
    Public RetPerLinea8Valor3 As Decimal = 0
    Public RetPerLinea8Valor4 As Decimal = 0
    Public RetPerLinea8Valor5 As Decimal = 0

    Public RetPerLinea9Valor1 As Decimal = 0
    Public RetPerLinea9Valor2 As Decimal = 0
    Public RetPerLinea9Valor3 As Decimal = 0
    Public RetPerLinea9Valor4 As Decimal = 0
    Public RetPerLinea9Valor5 As Decimal = 0

    Public RetPerLinea10Valor1 As Decimal = 0
    Public RetPerLinea10Valor2 As Decimal = 0
    Public RetPerLinea10Valor3 As Decimal = 0
    Public RetPerLinea10Valor4 As Decimal = 0
    Public RetPerLinea10Valor5 As Decimal = 0

    Private UltimoTipoComprobante As Integer
    Private UltimaSerieComprobante As String
    Private UltimoNumeroComprobante As Long
    Private CodigosRetPer As List(Of String)

    Private CFCSerie As String
    Private CFCNumero As String
    Private CFCCAENro As String
    Private CFCCAENroDesde As String
    Private CFCCAENroHasta As String
    Private CFCCAEFchVencimiento As Date
    Private CFCFecha As Date

    Public Property Clientes As List(Of cliente)
        Get
            Return clientesField
        End Get
        Set(value As List(Of cliente))
            clientesField = value
        End Set
    End Property

    Public Property Articulos As List(Of articulo)
        Get
            Return articulosField
        End Get
        Set(value As List(Of articulo))
            articulosField = value
        End Set
    End Property

    Private Sub Facturar()
        Dim objIntegration As New NADIntegration.NADIntegration
        Dim ErrorMsg As String = ""
        Dim ErrorCod As Integer = 0
        CalcularTotales()
        If (CTDec(TBx_MontoRetenidoPorLinea1.Text) > 0 And RapidatxtNombreItem1.Text.Trim = "") Or (CTDec(TBx_MontoRetenidoPorLinea2.Text) > 0 And RapidatxtNombreItem2.Text.Trim = "") Or (CTDec(TBx_MontoRetenidoPorLinea3.Text) > 0 And RapidatxtNombreItem3.Text.Trim = "") Or (CTDec(TBx_MontoRetenidoPorLinea4.Text) > 0 And RapidatxtNombreItem4.Text.Trim = "") Or (CTDec(TBx_MontoRetenidoPorLinea5.Text) > 0 And RapidatxtNombreItem5.Text.Trim = "") Or (CTDec(TBx_MontoRetenidoPorLinea6.Text) > 0 And RapidatxtNombreItem6.Text.Trim = "") Or (CTDec(TBx_MontoRetenidoPorLinea7.Text) > 0 And RapidatxtNombreItem7.Text.Trim = "") Or (CTDec(TBx_MontoRetenidoPorLinea8.Text) > 0 And RapidatxtNombreItem8.Text.Trim = "") Or (CTDec(TBx_MontoRetenidoPorLinea9.Text) > 0 And RapidatxtNombreItem9.Text.Trim = "") Or (CTDec(TBx_MontoRetenidoPorLinea10.Text) > 0 And RapidatxtNombreItem10.Text.Trim = "") Then
            MessageBox.Show("Si existen Retenciones/Percepciones se deben completar los datos de la linea correspondiente", "Falta completar campos", MessageBoxButtons.OK, MessageBoxIcon.Information)
            RapidatxtNombreItem1.Focus()
            Exit Sub
        End If
        If (CTDec(TBx_MontoRetenidoPorLinea1.Text) > 0 And CBx_AgenteResponsable1.SelectedIndex = 0) Or (CTDec(TBx_MontoRetenidoPorLinea2.Text) > 0 And CBx_AgenteResponsable2.SelectedIndex = 0) Or (CTDec(TBx_MontoRetenidoPorLinea3.Text) > 0 And CBx_AgenteResponsable3.SelectedIndex = 0) Or (CTDec(TBx_MontoRetenidoPorLinea4.Text) > 0 And CBx_AgenteResponsable4.SelectedIndex = 0) Or (CTDec(TBx_MontoRetenidoPorLinea5.Text) > 0 And CBx_AgenteResponsable5.SelectedIndex = 0) Or (CTDec(TBx_MontoRetenidoPorLinea6.Text) > 0 And CBx_AgenteResponsable6.SelectedIndex = 0) Or (CTDec(TBx_MontoRetenidoPorLinea7.Text) > 0 And CBx_AgenteResponsable7.SelectedIndex = 0) Or (CTDec(TBx_MontoRetenidoPorLinea8.Text) > 0 And CBx_AgenteResponsable8.SelectedIndex = 0) Or (CTDec(TBx_MontoRetenidoPorLinea9.Text) > 0 And CBx_AgenteResponsable9.SelectedIndex = 0) Or (CTDec(TBx_MontoRetenidoPorLinea10.Text) > 0 And CBx_AgenteResponsable10.SelectedIndex = 0) Then
            MessageBox.Show("Si existen Retenciones/Percepciones debe especificar si es Agente/Responsable", "Falta completar campos", MessageBoxButtons.OK, MessageBoxIcon.Information)
            CBx_AgenteResponsable1.Focus()
            Exit Sub
        End If
        If ((TBx_GlosaSubTotales1.Text.Trim <> "" And CTDec(TBx_ValorSubTotales1.Text.Trim) = 0) Or
                (TBx_GlosaSubTotales2.Text.Trim <> "" And CTDec(TBx_ValorSubTotales2.Text.Trim) = 0) Or
                (TBx_GlosaSubTotales3.Text.Trim <> "" And CTDec(TBx_ValorSubTotales3.Text.Trim) = 0) Or
                (TBx_GlosaSubTotales4.Text.Trim <> "" And CTDec(TBx_ValorSubTotales4.Text.Trim) = 0) Or
                (TBx_GlosaSubTotales5.Text.Trim <> "" And CTDec(TBx_ValorSubTotales5.Text.Trim) = 0)) Then

            MessageBox.Show("Si ingresa Glosa en SubTotales Informativos debe ingresar un valor para el mismo", "Falta completar campos", MessageBoxButtons.OK, MessageBoxIcon.Information)
            TBx_GlosaSubTotales1.Focus()
            Exit Sub
        End If

        Try
            If Not objIntegration.NuevaFactura(ErrorMsg, ErrorCod, objConfig.TIPONEGOCIO, TextBox_CarpetaOperacion.Text) Then
                Throw New Exception(ErrorMsg)
            End If

            'Obtengo el tipo de CFE con el que estoy trabajando actualmente
            TipoCFEActual(TipoCFE, TipoCFEAux)

            If chkIndicadorRefGlobal.Visible Then
                Dim IndicadorReferenciaGlobalInformacionReferencia As Short
                Dim TipoCFEInformacionReferencia As Short
                Dim SerieCFEInformacionReferencia As String = ""
                Dim NumeroCFEInformacionReferencia As Long
                Dim FechaCFEInformacionReferencia As Date = Date.MinValue
                Dim RazonInformacionReferencia As String = ""
                If chkIndicadorRefGlobal.Checked Then
                    IndicadorReferenciaGlobalInformacionReferencia = 1
                Else
                    TipoCFEInformacionReferencia = cmbTipoCFEInformacionReferencia.SelectedValue
                    SerieCFEInformacionReferencia = txtSerieCFEInformacionReferencia.Text
                    NumeroCFEInformacionReferencia = CTDec(txtNumeroCFEInformacionReferencia.Text)
                    FechaCFEInformacionReferencia = dtpFechaCFEInformacionReferencia.Value
                End If
                RazonInformacionReferencia = txtRazonInformacionReferencia.Text

                If RazonInformacionReferencia <> "" Or SerieCFEInformacionReferencia <> "" Then
                    If Not objIntegration.DGIF1addInformacionDeReferencia(ErrorMsg, ErrorCod, IndicadorReferenciaGlobalInformacionReferencia, TipoCFEInformacionReferencia, SerieCFEInformacionReferencia, NumeroCFEInformacionReferencia, RazonInformacionReferencia, FechaCFEInformacionReferencia) Then
                        Throw New Exception(ErrorMsg)
                    End If
                End If
            End If

            Dim IndicadorMontoBruto As Integer
            'Seteo el cabezal(para DGI)
            'Datos Contingencia txtSerieComprobanteEncabezado.Text, txtNumeroComprobanteEncabezado.Text, 
            IndicadorMontoBruto = CmbIndicadorMontoBrutoEncabezado.SelectedValue

            Dim FechaDeVencimiento As Date = Date.MinValue
            If cmbFormaPago.SelectedIndex <> 0 AndAlso chkVencimientoCredito.Checked Then
                FechaDeVencimiento = dtpFechaVencimiento.Value
            End If

            Dim tipoTraslado As TAFACE2ApiParsing.ITAParsing_v0206.enumIndicadorTipoTraslado = TAFACE2ApiParsing.ITAParsing_v0206.enumIndicadorTipoTraslado.SinDefinir
            If Rdb_esRemito.Checked Then
                tipoTraslado = cmbIndicadorTipoTrasladoBienesEncabezado.SelectedValue
            End If

            Dim IVAalDia As TAFACE2ApiParsing.ITAParsing_v0206.enumIVAalDia = TAFACE2ApiParsing.ITAParsing_v0206.enumIVAalDia.NoIVAalDia
            If Chk_IVAalDia.Checked Then
                IVAalDia = TAFACE2ApiParsing.ITAParsing_v0206.enumIVAalDia.IVAalDia
            End If

            Dim SecretoProfesional As TAFACE2ApiParsing.ITAParsing_v0206.enumSecretoProf = TAFACE2ApiParsing.ITAParsing_v0206.enumSecretoProf.OperacionNoAmparadaSecretoProf
            If Ckb_esSecretoProfesional.Checked Then
                SecretoProfesional = TAFACE2ApiParsing.ITAParsing_v0206.enumSecretoProf.OperacionAmparadaSecretoProf
            End If

            Dim IndicadorPropMercaderiaTransp As TAFACE2ApiParsing.ITAParsing_v0206.enumIndicadorPropiedadMercaderiaTransportada = cmbIndicadorPropMercaderiaTransp.SelectedValue
            Dim TipoDocumentoMercaderiaTransp As TAFACE2ApiParsing.ITAParsing_v0206.enumTipoDocumentoDelReceptor = cmbTipoDocMercaderiaTransp.SelectedValue
            Dim DocNacMercaderiaTranspo As String = ""
            Dim DocExtMercaderiaTranspo As String = ""
            If cmbTipoDocMercaderiaTransp.SelectedValue = TAFACE2ApiParsing.ITAParsing_v0206.enumTipoDocumentoDelReceptor.SinDefinir Then
                TipoDocumentoMercaderiaTransp = TAFACE2ApiParsing.ITAParsing_v0206.enumTipoDocumentoDelReceptor.SinDefinir
            Else
                TipoDocumentoMercaderiaTransp = cmbTipoDocMercaderiaTransp.SelectedValue
                If TipoDocumentoMercaderiaTransp = TAFACE2ApiParsing.ITAParsing_v0206.enumTipoDocumentoDelReceptor.CI Or TipoDocumentoMercaderiaTransp = TAFACE2ApiParsing.ITAParsing_v0206.enumTipoDocumentoDelReceptor.RUC Then
                    DocNacMercaderiaTranspo = textBox_NroDocPropMercaderiaTransp.Text
                    DocExtMercaderiaTranspo = ""
                Else
                    DocNacMercaderiaTranspo = ""
                    DocExtMercaderiaTranspo = textBox_NroDocPropMercaderiaTransp.Text
                End If
            End If

            Dim CodigoPais As String = cmbCodPaisPropMercaderiaTransp.SelectedValue
            Dim NombreRznSocPropMercaderiaTransp As String = ""
            If TextBox_NombreRznSocPropMercaderiaTransp.Text <> "Nombre o denominacion propietario mercaderia transportada..." Then
                NombreRznSocPropMercaderiaTransp = TextBox_NombreRznSocPropMercaderiaTransp.Text
            End If
            If Ckb_esContingencia.Checked Then
                MostrarPopupContingencia()
                If Not objIntegration.DGIA1setEncabezado(ErrorMsg, ErrorCod, TipoCFE, CFCFecha, tipoTraslado, Date.MinValue, Date.MinValue, IndicadorMontoBruto, cmbFormaPago.SelectedValue, FechaDeVencimiento, TBx_ClausulaDeVenta.Text.Trim, CBx_ModalidadVenta.SelectedValue, CBx_ViaTransporte.SelectedValue, "", IVAalDia, SecretoProfesional, IndicadorPropMercaderiaTransp, TipoDocumentoMercaderiaTransp, CodigoPais, DocNacMercaderiaTranspo, DocExtMercaderiaTranspo, NombreRznSocPropMercaderiaTransp) Then
                    Throw New Exception(ErrorMsg)
                End If
                If Not objIntegration.DATOSCONTINGENCIAsetDatos(ErrorMsg, ErrorCod, TipoCFEAux, CFCSerie, CTDec(CFCNumero), CTDec(CFCCAENro), CTDec(CFCCAENroDesde), CTDec(CFCCAENroHasta), CFCCAEFchVencimiento) Then
                    Throw New Exception(ErrorMsg)
                End If
            Else
                If Not objIntegration.DGIA1setEncabezado(ErrorMsg, ErrorCod, TipoCFE, Now, tipoTraslado, Date.MinValue, Date.MinValue, IndicadorMontoBruto, cmbFormaPago.SelectedValue, FechaDeVencimiento, TBx_ClausulaDeVenta.Text.Trim, CBx_ModalidadVenta.SelectedValue, CBx_ViaTransporte.SelectedValue, "", IVAalDia, SecretoProfesional, IndicadorPropMercaderiaTransp, TipoDocumentoMercaderiaTransp, CodigoPais, DocNacMercaderiaTranspo, DocExtMercaderiaTranspo, NombreRznSocPropMercaderiaTransp) Then
                    Throw New Exception(ErrorMsg)
                End If
            End If

            'Emisor
            If Not objIntegration.DGIA2setEmisor(ErrorMsg, ErrorCod, txtCfgRUTEmisor.Text, txtCfgRazonSocialEmisor.Text, txtCfgNombreEmisor.Text, txtCfgGiroNegocioEmisor.Text, txtCfgCorreoEmisor.Text, txtCfgNombreSucursalEmisor.Text, txtCfgCodigoSucursalEmisor.Text, txtCfgDomicilioFiscalEmisor.Text, txtCfgCiudadEmisor.Text, txtCfgDepartamentoEmisor.Text, "") Then
                Throw New Exception(ErrorMsg)
            End If

            If txtCfgTelefonoEmisor.Text.Trim <> "" Then
                If Not objIntegration.DGIA3addTelefonoEmisor(ErrorMsg, ErrorCod, txtCfgTelefonoEmisor.Text) Then
                    Throw New Exception(ErrorMsg)
                End If
            End If
            If txtCfgTelefono2Emisor.Text.Trim <> "" Then
                If Not objIntegration.DGIA3addTelefonoEmisor(ErrorMsg, ErrorCod, txtCfgTelefono2Emisor.Text) Then
                    Throw New Exception(ErrorMsg)
                End If
            End If

            'Receptor
            Dim TipoDocReceptor As TAFACE2ApiParsing.ITAParsing_v0206.enumTipoDocumentoDelReceptor
            Dim DocNac As String = ""
            Dim DocExt As String = ""
            Dim CodPais As String = ""
            Dim NomPais As String = ""
            If RapidacmbTipoDocumentoReceptor.SelectedValue = TAFACE2ApiParsing.ITAParsing_v0206.enumTipoDocumentoDelReceptor.SinDefinir Then
                TipoDocReceptor = TAFACE2ApiParsing.ITAParsing_v0206.enumTipoDocumentoDelReceptor.SinDefinir
            Else
                TipoDocReceptor = RapidacmbTipoDocumentoReceptor.SelectedValue
                If TipoDocReceptor = TAFACE2ApiParsing.ITAParsing_v0206.enumTipoDocumentoDelReceptor.CI Or TipoDocReceptor = TAFACE2ApiParsing.ITAParsing_v0206.enumTipoDocumentoDelReceptor.RUC Then
                    DocNac = RapidatxtDocumentoReceptor.Text
                    DocExt = ""
                Else
                    DocNac = ""
                    DocExt = RapidatxtDocumentoReceptor.Text
                End If
                CodPais = RapidacmbCodigoPaisReceptor.SelectedValue
                NomPais = RapidacmbCodigoPaisReceptor.Text
            End If
            If Not objIntegration.DGIA4setReceptor(ErrorMsg, ErrorCod, TipoDocReceptor, CodPais, DocNac, DocExt, RapidatxtNombreReceptor.Text, RapidatxtDireccionReceptor.Text, RapidatxtCiudadReceptor.Text, RapidatxtDepartamentoReceptor.Text, NomPais, 0, "", "", "") Then
                Throw New Exception(ErrorMsg)
            End If

            'Detalle
            Dim CantLineas As Integer = 0
            Dim AgenteResponsable As String = ""
            If RapidatxtNombreItem1.Text.Trim <> "" Then
                CantLineas += 1
                AgenteResponsable = ""
                If Not CTDec(TBx_MontoRetenidoPorLinea1.Text) > 0 Then
                    If Not objIntegration.DGIB1addDetalleNuevaLinea(ErrorMsg, ErrorCod, RapidacmbIndicadorFacturacion1.SelectedValue, AgenteResponsable.ToString, RapidatxtNombreItem1.Text, "", CTDec(RapidatxtCantidad1.Text), "N/A", CTDec(RapidatxtPrecioUnitario1.Text), CTDec(RapidatxtSubDescuentoValor1.Text), SubDescuentoValor1, 0, 0, CTDec(RapidatxtMontoItem1.Text)) Then
                        Throw New Exception(ErrorMsg)
                    End If
                Else
                    AgenteResponsable = CBx_AgenteResponsable1.Text

                    If Not objIntegration.DGIB1addDetalleNuevaLinea(ErrorMsg, ErrorCod, RapidacmbIndicadorFacturacion1.SelectedValue, AgenteResponsable.ToString, RapidatxtNombreItem1.Text, "", CTDec(RapidatxtCantidad1.Text), "N/A", CTDec(RapidatxtPrecioUnitario1.Text), CTDec(RapidatxtSubDescuentoValor1.Text), SubDescuentoValor1, 0, 0, CTDec(RapidatxtMontoItem1.Text)) Then
                        Throw New Exception(ErrorMsg)
                    End If
                    If RetPerLinea1Valor1 > 0 Then
                        If Not objIntegration.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, RetPerLinea1Cod1, RetPerLinea1Tasa1, RetPerLinea1Monto1, RetPerLinea1Valor1, "") Then
                            Throw New Exception(ErrorMsg)
                        End If
                    End If
                    If RetPerLinea1Valor2 > 0 Then
                        If Not objIntegration.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, RetPerLinea1Cod2, RetPerLinea1Tasa2, RetPerLinea1Monto2, RetPerLinea1Valor2, "") Then
                            Throw New Exception(ErrorMsg)
                        End If
                    End If
                    If RetPerLinea1Valor3 > 0 Then
                        If Not objIntegration.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, RetPerLinea1Cod3, RetPerLinea1Tasa3, RetPerLinea1Monto3, RetPerLinea1Valor3, "") Then
                            Throw New Exception(ErrorMsg)
                        End If
                    End If
                    If RetPerLinea1Valor4 > 0 Then
                        If Not objIntegration.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, RetPerLinea1Cod4, RetPerLinea1Tasa4, RetPerLinea1Monto4, RetPerLinea1Valor4, "") Then
                            Throw New Exception(ErrorMsg)
                        End If
                    End If
                    If RetPerLinea1Valor5 > 0 Then
                        If Not objIntegration.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, RetPerLinea1Cod5, RetPerLinea1Tasa5, RetPerLinea1Monto5, RetPerLinea1Valor5, "") Then
                            Throw New Exception(ErrorMsg)
                        End If
                    End If

                End If

            End If

            If RapidatxtNombreItem2.Text.Trim <> "" Then
                CantLineas += 1
                AgenteResponsable = ""
                If Not CTDec(TBx_MontoRetenidoPorLinea2.Text) > 0 Then
                    If Not objIntegration.DGIB1addDetalleNuevaLinea(ErrorMsg, ErrorCod, RapidacmbIndicadorFacturacion2.SelectedValue, AgenteResponsable.ToString, RapidatxtNombreItem2.Text, "", CTDec(RapidatxtCantidad2.Text), "N/A", CTDec(RapidatxtPrecioUnitario2.Text), CTDec(RapidatxtSubDescuentoValor2.Text), SubDescuentoValor2, 0, 0, CTDec(RapidatxtMontoItem2.Text)) Then
                        Throw New Exception(ErrorMsg)
                    End If
                Else
                    AgenteResponsable = CBx_AgenteResponsable2.Text
                    If Not objIntegration.DGIB1addDetalleNuevaLinea(ErrorMsg, ErrorCod, RapidacmbIndicadorFacturacion2.SelectedValue, AgenteResponsable.ToString, RapidatxtNombreItem2.Text, "", CTDec(RapidatxtCantidad2.Text), "N/A", CTDec(RapidatxtPrecioUnitario2.Text), CTDec(RapidatxtSubDescuentoValor2.Text), SubDescuentoValor2, 0, 0, CTDec(RapidatxtMontoItem2.Text)) Then
                        Throw New Exception(ErrorMsg)
                    End If
                    If RetPerLinea2Valor1 > 0 Then
                        If Not objIntegration.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, RetPerLinea2Cod1, RetPerLinea2Tasa1, RetPerLinea2Monto1, RetPerLinea2Valor1, "") Then
                            Throw New Exception(ErrorMsg)
                        End If
                    End If
                    If RetPerLinea2Valor2 > 0 Then
                        If Not objIntegration.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, RetPerLinea2Cod2, RetPerLinea2Tasa2, RetPerLinea2Monto2, RetPerLinea2Valor2, "") Then
                            Throw New Exception(ErrorMsg)
                        End If
                    End If
                    If RetPerLinea2Valor3 > 0 Then
                        If Not objIntegration.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, RetPerLinea2Cod3, RetPerLinea2Tasa3, RetPerLinea2Monto3, RetPerLinea2Valor3, "") Then
                            Throw New Exception(ErrorMsg)
                        End If
                    End If
                    If RetPerLinea2Valor4 > 0 Then
                        If Not objIntegration.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, RetPerLinea2Cod4, RetPerLinea2Tasa4, RetPerLinea2Monto4, RetPerLinea2Valor4, "") Then
                            Throw New Exception(ErrorMsg)
                        End If
                    End If
                    If RetPerLinea2Valor5 > 0 Then
                        If Not objIntegration.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, RetPerLinea2Cod5, RetPerLinea2Tasa5, RetPerLinea2Monto5, RetPerLinea2Valor5, "") Then
                            Throw New Exception(ErrorMsg)
                        End If
                    End If

                End If

            End If
            If RapidatxtNombreItem3.Text.Trim <> "" Then
                CantLineas += 1
                AgenteResponsable = ""
                If Not CTDec(TBx_MontoRetenidoPorLinea3.Text) > 0 Then
                    If Not objIntegration.DGIB1addDetalleNuevaLinea(ErrorMsg, ErrorCod, RapidacmbIndicadorFacturacion3.SelectedValue, AgenteResponsable.ToString, RapidatxtNombreItem3.Text, "", CTDec(RapidatxtCantidad3.Text), "N/A", CTDec(RapidatxtPrecioUnitario3.Text), CTDec(RapidatxtSubDescuentoValor3.Text), SubDescuentoValor3, 0, 0, CTDec(RapidatxtMontoItem3.Text)) Then
                        Throw New Exception(ErrorMsg)
                    End If
                Else
                    AgenteResponsable = CBx_AgenteResponsable3.Text
                    If Not objIntegration.DGIB1addDetalleNuevaLinea(ErrorMsg, ErrorCod, RapidacmbIndicadorFacturacion3.SelectedValue, AgenteResponsable.ToString, RapidatxtNombreItem3.Text, "", CTDec(RapidatxtCantidad3.Text), "N/A", CTDec(RapidatxtPrecioUnitario3.Text), CTDec(RapidatxtSubDescuentoValor3.Text), SubDescuentoValor3, 0, 0, CTDec(RapidatxtMontoItem3.Text)) Then
                        Throw New Exception(ErrorMsg)
                    End If
                    If RetPerLinea3Valor1 > 0 Then
                        If Not objIntegration.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, RetPerLinea3Cod1, RetPerLinea3Tasa1, RetPerLinea3Monto1, RetPerLinea3Valor1, "") Then
                            Throw New Exception(ErrorMsg)
                        End If
                    End If
                    If RetPerLinea3Valor2 > 0 Then
                        If Not objIntegration.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, RetPerLinea3Cod2, RetPerLinea3Tasa2, RetPerLinea3Monto2, RetPerLinea3Valor2, "") Then
                            Throw New Exception(ErrorMsg)
                        End If
                    End If
                    If RetPerLinea3Valor3 > 0 Then
                        If Not objIntegration.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, RetPerLinea3Cod3, RetPerLinea3Tasa3, RetPerLinea3Monto3, RetPerLinea3Valor3, "") Then
                            Throw New Exception(ErrorMsg)
                        End If
                    End If
                    If RetPerLinea3Valor4 > 0 Then
                        If Not objIntegration.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, RetPerLinea3Cod4, RetPerLinea3Tasa4, RetPerLinea3Monto4, RetPerLinea3Valor4, "") Then
                            Throw New Exception(ErrorMsg)
                        End If
                    End If
                    If RetPerLinea3Valor5 > 0 Then
                        If Not objIntegration.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, RetPerLinea3Cod5, RetPerLinea3Tasa5, RetPerLinea3Monto5, RetPerLinea3Valor5, "") Then
                            Throw New Exception(ErrorMsg)
                        End If
                    End If
                End If

            End If
            If RapidatxtNombreItem4.Text.Trim <> "" Then
                CantLineas += 1
                AgenteResponsable = ""
                If Not CTDec(TBx_MontoRetenidoPorLinea4.Text) > 0 Then
                    If Not objIntegration.DGIB1addDetalleNuevaLinea(ErrorMsg, ErrorCod, RapidacmbIndicadorFacturacion4.SelectedValue, AgenteResponsable.ToString, RapidatxtNombreItem4.Text, "", CTDec(RapidatxtCantidad4.Text), "N/A", CTDec(RapidatxtPrecioUnitario4.Text), CTDec(RapidatxtSubDescuentoValor4.Text), SubDescuentoValor4, 0, 0, CTDec(RapidatxtMontoItem4.Text)) Then
                        Throw New Exception(ErrorMsg)
                    End If
                Else
                    AgenteResponsable = CBx_AgenteResponsable4.Text
                    If Not objIntegration.DGIB1addDetalleNuevaLinea(ErrorMsg, ErrorCod, RapidacmbIndicadorFacturacion4.SelectedValue, AgenteResponsable.ToString, RapidatxtNombreItem4.Text, "", CTDec(RapidatxtCantidad4.Text), "N/A", CTDec(RapidatxtPrecioUnitario4.Text), CTDec(RapidatxtSubDescuentoValor4.Text), SubDescuentoValor4, 0, 0, CTDec(RapidatxtMontoItem4.Text)) Then
                        Throw New Exception(ErrorMsg)
                    End If
                    If RetPerLinea4Valor1 > 0 Then
                        If Not objIntegration.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, RetPerLinea4Cod1, RetPerLinea4Tasa1, RetPerLinea4Monto1, RetPerLinea4Valor1, "") Then
                            Throw New Exception(ErrorMsg)
                        End If
                    End If
                    If RetPerLinea4Valor2 > 0 Then
                        If Not objIntegration.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, RetPerLinea4Cod2, RetPerLinea4Tasa2, RetPerLinea4Monto2, RetPerLinea4Valor2, "") Then
                            Throw New Exception(ErrorMsg)
                        End If
                    End If
                    If RetPerLinea4Valor3 > 0 Then
                        If Not objIntegration.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, RetPerLinea4Cod3, RetPerLinea4Tasa3, RetPerLinea4Monto3, RetPerLinea4Valor3, "") Then
                            Throw New Exception(ErrorMsg)
                        End If
                    End If
                    If RetPerLinea4Valor4 > 0 Then
                        If Not objIntegration.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, RetPerLinea4Cod4, RetPerLinea4Tasa4, RetPerLinea4Monto4, RetPerLinea4Valor4, "") Then
                            Throw New Exception(ErrorMsg)
                        End If
                    End If
                    If RetPerLinea4Valor5 > 0 Then
                        If Not objIntegration.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, RetPerLinea4Cod5, RetPerLinea4Tasa5, RetPerLinea4Monto5, RetPerLinea4Valor5, "") Then
                            Throw New Exception(ErrorMsg)
                        End If
                    End If

                End If

            End If
            If RapidatxtNombreItem5.Text.Trim <> "" Then
                CantLineas += 1
                AgenteResponsable = ""
                If Not CTDec(TBx_MontoRetenidoPorLinea5.Text) > 0 Then
                    If Not objIntegration.DGIB1addDetalleNuevaLinea(ErrorMsg, ErrorCod, RapidacmbIndicadorFacturacion5.SelectedValue, AgenteResponsable.ToString, RapidatxtNombreItem5.Text, "", CTDec(RapidatxtCantidad5.Text), "N/A", CTDec(RapidatxtPrecioUnitario5.Text), CTDec(RapidatxtSubDescuentoValor5.Text), SubDescuentoValor5, 0, 0, CTDec(RapidatxtMontoItem5.Text)) Then
                        Throw New Exception(ErrorMsg)
                    End If
                Else
                    AgenteResponsable = CBx_AgenteResponsable5.Text
                    If Not objIntegration.DGIB1addDetalleNuevaLinea(ErrorMsg, ErrorCod, RapidacmbIndicadorFacturacion5.SelectedValue, AgenteResponsable.ToString, RapidatxtNombreItem5.Text, "", CTDec(RapidatxtCantidad5.Text), "N/A", CTDec(RapidatxtPrecioUnitario5.Text), CTDec(RapidatxtSubDescuentoValor5.Text), SubDescuentoValor5, 0, 0, CTDec(RapidatxtMontoItem5.Text)) Then
                        Throw New Exception(ErrorMsg)
                    End If
                    If RetPerLinea5Valor1 > 0 Then
                        If Not objIntegration.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, RetPerLinea5Cod1, RetPerLinea5Tasa1, RetPerLinea5Monto1, RetPerLinea5Valor1, "") Then
                            Throw New Exception(ErrorMsg)
                        End If
                    End If
                    If RetPerLinea5Valor2 > 0 Then
                        If Not objIntegration.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, RetPerLinea5Cod2, RetPerLinea5Tasa2, RetPerLinea5Monto2, RetPerLinea5Valor2, "") Then
                            Throw New Exception(ErrorMsg)
                        End If
                    End If
                    If RetPerLinea5Valor3 > 0 Then
                        If Not objIntegration.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, RetPerLinea5Cod3, RetPerLinea5Tasa3, RetPerLinea5Monto3, RetPerLinea5Valor3, "") Then
                            Throw New Exception(ErrorMsg)
                        End If
                    End If
                    If RetPerLinea5Valor4 > 0 Then
                        If Not objIntegration.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, RetPerLinea5Cod4, RetPerLinea5Tasa4, RetPerLinea5Monto4, RetPerLinea5Valor4, "") Then
                            Throw New Exception(ErrorMsg)
                        End If
                    End If
                    If RetPerLinea5Valor5 > 0 Then
                        If Not objIntegration.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, RetPerLinea5Cod5, RetPerLinea5Tasa5, RetPerLinea5Monto5, RetPerLinea5Valor5, "") Then
                            Throw New Exception(ErrorMsg)
                        End If
                    End If

                End If

            End If
            If RapidatxtNombreItem6.Text.Trim <> "" Then
                CantLineas += 1
                AgenteResponsable = ""
                If Not CTDec(TBx_MontoRetenidoPorLinea6.Text) > 0 Then
                    If Not objIntegration.DGIB1addDetalleNuevaLinea(ErrorMsg, ErrorCod, RapidacmbIndicadorFacturacion6.SelectedValue, AgenteResponsable.ToString, RapidatxtNombreItem6.Text, "", CTDec(RapidatxtCantidad6.Text), "N/A", CTDec(RapidatxtPrecioUnitario6.Text), CTDec(RapidatxtSubDescuentoValor6.Text), SubDescuentoValor6, 0, 0, CTDec(RapidatxtMontoItem6.Text)) Then
                        Throw New Exception(ErrorMsg)
                    End If
                Else
                    AgenteResponsable = CBx_AgenteResponsable6.Text
                    If Not objIntegration.DGIB1addDetalleNuevaLinea(ErrorMsg, ErrorCod, RapidacmbIndicadorFacturacion6.SelectedValue, AgenteResponsable.ToString, RapidatxtNombreItem6.Text, "", CTDec(RapidatxtCantidad6.Text), "N/A", CTDec(RapidatxtPrecioUnitario6.Text), CTDec(RapidatxtSubDescuentoValor6.Text), SubDescuentoValor6, 0, 0, CTDec(RapidatxtMontoItem6.Text)) Then
                        Throw New Exception(ErrorMsg)
                    End If
                    If RetPerLinea6Valor1 > 0 Then
                        If Not objIntegration.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, RetPerLinea6Cod1, RetPerLinea6Tasa1, RetPerLinea6Monto1, RetPerLinea6Valor1, "") Then
                            Throw New Exception(ErrorMsg)
                        End If
                    End If
                    If RetPerLinea6Valor2 > 0 Then
                        If Not objIntegration.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, RetPerLinea6Cod2, RetPerLinea6Tasa2, RetPerLinea6Monto2, RetPerLinea6Valor2, "") Then
                            Throw New Exception(ErrorMsg)
                        End If
                    End If
                    If RetPerLinea6Valor3 > 0 Then
                        If Not objIntegration.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, RetPerLinea6Cod3, RetPerLinea6Tasa3, RetPerLinea6Monto3, RetPerLinea6Valor3, "") Then
                            Throw New Exception(ErrorMsg)
                        End If
                    End If
                    If RetPerLinea6Valor4 > 0 Then
                        If Not objIntegration.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, RetPerLinea6Cod4, RetPerLinea6Tasa4, RetPerLinea6Monto4, RetPerLinea6Valor4, "") Then
                            Throw New Exception(ErrorMsg)
                        End If
                    End If
                    If RetPerLinea6Valor5 > 0 Then
                        If Not objIntegration.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, RetPerLinea6Cod5, RetPerLinea6Tasa5, RetPerLinea6Monto5, RetPerLinea6Valor5, "") Then
                            Throw New Exception(ErrorMsg)
                        End If
                    End If

                End If

            End If
            If RapidatxtNombreItem7.Text.Trim <> "" Then
                CantLineas += 1
                AgenteResponsable = ""
                If Not CTDec(TBx_MontoRetenidoPorLinea7.Text) > 0 Then
                    If Not objIntegration.DGIB1addDetalleNuevaLinea(ErrorMsg, ErrorCod, RapidacmbIndicadorFacturacion7.SelectedValue, AgenteResponsable.ToString, RapidatxtNombreItem7.Text, "", CTDec(RapidatxtCantidad7.Text), "N/A", CTDec(RapidatxtPrecioUnitario7.Text), CTDec(RapidatxtSubDescuentoValor7.Text), SubDescuentoValor7, 0, 0, CTDec(RapidatxtMontoItem7.Text)) Then
                        Throw New Exception(ErrorMsg)
                    End If
                Else
                    AgenteResponsable = CBx_AgenteResponsable7.Text
                    If Not objIntegration.DGIB1addDetalleNuevaLinea(ErrorMsg, ErrorCod, RapidacmbIndicadorFacturacion7.SelectedValue, AgenteResponsable.ToString, RapidatxtNombreItem7.Text, "", CTDec(RapidatxtCantidad7.Text), "N/A", CTDec(RapidatxtPrecioUnitario7.Text), CTDec(RapidatxtSubDescuentoValor7.Text), SubDescuentoValor7, 0, 0, CTDec(RapidatxtMontoItem7.Text)) Then
                        Throw New Exception(ErrorMsg)
                    End If
                    If RetPerLinea7Valor1 > 0 Then
                        If Not objIntegration.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, RetPerLinea7Cod1, RetPerLinea7Tasa1, RetPerLinea7Monto1, RetPerLinea7Valor1, "") Then
                            Throw New Exception(ErrorMsg)
                        End If
                    End If
                    If RetPerLinea7Valor2 > 0 Then
                        If Not objIntegration.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, RetPerLinea7Cod2, RetPerLinea7Tasa2, RetPerLinea7Monto2, RetPerLinea7Valor2, "") Then
                            Throw New Exception(ErrorMsg)
                        End If
                    End If
                    If RetPerLinea7Valor3 > 0 Then
                        If Not objIntegration.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, RetPerLinea7Cod3, RetPerLinea7Tasa3, RetPerLinea7Monto3, RetPerLinea7Valor3, "") Then
                            Throw New Exception(ErrorMsg)
                        End If
                    End If
                    If RetPerLinea7Valor4 > 0 Then
                        If Not objIntegration.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, RetPerLinea7Cod4, RetPerLinea7Tasa4, RetPerLinea7Monto4, RetPerLinea7Valor4, "") Then
                            Throw New Exception(ErrorMsg)
                        End If
                    End If
                    If RetPerLinea7Valor5 > 0 Then
                        If Not objIntegration.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, RetPerLinea7Cod5, RetPerLinea7Tasa5, RetPerLinea7Monto5, RetPerLinea7Valor5, "") Then
                            Throw New Exception(ErrorMsg)
                        End If
                    End If

                End If

            End If
            If RapidatxtNombreItem8.Text.Trim <> "" Then
                CantLineas += 1
                AgenteResponsable = ""
                If Not CTDec(TBx_MontoRetenidoPorLinea8.Text) > 0 Then
                    If Not objIntegration.DGIB1addDetalleNuevaLinea(ErrorMsg, ErrorCod, RapidacmbIndicadorFacturacion8.SelectedValue, AgenteResponsable.ToString, RapidatxtNombreItem8.Text, "", CTDec(RapidatxtCantidad8.Text), "N/A", CTDec(RapidatxtPrecioUnitario8.Text), CTDec(RapidatxtSubDescuentoValor8.Text), SubDescuentoValor8, 0, 0, CTDec(RapidatxtMontoItem8.Text)) Then
                        Throw New Exception(ErrorMsg)
                    End If
                Else
                    AgenteResponsable = CBx_AgenteResponsable8.Text
                    If Not objIntegration.DGIB1addDetalleNuevaLinea(ErrorMsg, ErrorCod, RapidacmbIndicadorFacturacion8.SelectedValue, AgenteResponsable.ToString, RapidatxtNombreItem8.Text, "", CTDec(RapidatxtCantidad8.Text), "N/A", CTDec(RapidatxtPrecioUnitario8.Text), CTDec(RapidatxtSubDescuentoValor8.Text), SubDescuentoValor8, 0, 0, CTDec(RapidatxtMontoItem8.Text)) Then
                        Throw New Exception(ErrorMsg)
                    End If
                    If RetPerLinea8Valor1 > 0 Then
                        If Not objIntegration.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, RetPerLinea8Cod1, RetPerLinea8Tasa1, RetPerLinea8Monto1, RetPerLinea8Valor1, "") Then
                            Throw New Exception(ErrorMsg)
                        End If
                    End If
                    If RetPerLinea8Valor2 > 0 Then
                        If Not objIntegration.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, RetPerLinea8Cod2, RetPerLinea8Tasa2, RetPerLinea8Monto2, RetPerLinea8Valor2, "") Then
                            Throw New Exception(ErrorMsg)
                        End If
                    End If
                    If RetPerLinea8Valor3 > 0 Then
                        If Not objIntegration.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, RetPerLinea8Cod3, RetPerLinea8Tasa3, RetPerLinea8Monto3, RetPerLinea8Valor3, "") Then
                            Throw New Exception(ErrorMsg)
                        End If
                    End If
                    If RetPerLinea8Valor4 > 0 Then
                        If Not objIntegration.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, RetPerLinea8Cod4, RetPerLinea8Tasa4, RetPerLinea8Monto4, RetPerLinea8Valor4, "") Then
                            Throw New Exception(ErrorMsg)
                        End If
                    End If
                    If RetPerLinea8Valor5 > 0 Then
                        If Not objIntegration.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, RetPerLinea8Cod5, RetPerLinea8Tasa5, RetPerLinea8Monto5, RetPerLinea8Valor5, "") Then
                            Throw New Exception(ErrorMsg)
                        End If
                    End If

                End If

            End If
            If RapidatxtNombreItem9.Text.Trim <> "" Then
                CantLineas += 1
                AgenteResponsable = ""
                If Not CTDec(TBx_MontoRetenidoPorLinea9.Text) > 0 Then
                    If Not objIntegration.DGIB1addDetalleNuevaLinea(ErrorMsg, ErrorCod, RapidacmbIndicadorFacturacion9.SelectedValue, AgenteResponsable.ToString, RapidatxtNombreItem9.Text, "", CTDec(RapidatxtCantidad9.Text), "N/A", CTDec(RapidatxtPrecioUnitario9.Text), CTDec(RapidatxtSubDescuentoValor9.Text), SubDescuentoValor9, 0, 0, CTDec(RapidatxtMontoItem9.Text)) Then
                        Throw New Exception(ErrorMsg)
                    End If
                Else
                    AgenteResponsable = CBx_AgenteResponsable9.Text
                    If Not objIntegration.DGIB1addDetalleNuevaLinea(ErrorMsg, ErrorCod, RapidacmbIndicadorFacturacion9.SelectedValue, AgenteResponsable.ToString, RapidatxtNombreItem9.Text, "", CTDec(RapidatxtCantidad9.Text), "N/A", CTDec(RapidatxtPrecioUnitario9.Text), CTDec(RapidatxtSubDescuentoValor9.Text), SubDescuentoValor9, 0, 0, CTDec(RapidatxtMontoItem9.Text)) Then
                        Throw New Exception(ErrorMsg)
                    End If
                    If RetPerLinea9Valor1 > 0 Then
                        If Not objIntegration.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, RetPerLinea9Cod1, RetPerLinea9Tasa1, RetPerLinea9Monto1, RetPerLinea9Valor1, "") Then
                            Throw New Exception(ErrorMsg)
                        End If
                    End If
                    If RetPerLinea9Valor2 > 0 Then
                        If Not objIntegration.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, RetPerLinea9Cod2, RetPerLinea9Tasa2, RetPerLinea9Monto2, RetPerLinea9Valor2, "") Then
                            Throw New Exception(ErrorMsg)
                        End If
                    End If
                    If RetPerLinea9Valor3 > 0 Then
                        If Not objIntegration.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, RetPerLinea9Cod3, RetPerLinea9Tasa3, RetPerLinea9Monto3, RetPerLinea9Valor3, "") Then
                            Throw New Exception(ErrorMsg)
                        End If
                    End If
                    If RetPerLinea9Valor4 > 0 Then
                        If Not objIntegration.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, RetPerLinea9Cod4, RetPerLinea9Tasa4, RetPerLinea9Monto4, RetPerLinea9Valor4, "") Then
                            Throw New Exception(ErrorMsg)
                        End If
                    End If
                    If RetPerLinea9Valor5 > 0 Then
                        If Not objIntegration.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, RetPerLinea9Cod5, RetPerLinea9Tasa5, RetPerLinea9Monto5, RetPerLinea9Valor5, "") Then
                            Throw New Exception(ErrorMsg)
                        End If
                    End If

                End If

            End If
            If RapidatxtNombreItem10.Text.Trim <> "" Then
                CantLineas += 1
                AgenteResponsable = ""
                If Not CTDec(TBx_MontoRetenidoPorLinea10.Text) > 0 Then
                    If Not objIntegration.DGIB1addDetalleNuevaLinea(ErrorMsg, ErrorCod, RapidacmbIndicadorFacturacion10.SelectedValue, AgenteResponsable.ToString, RapidatxtNombreItem10.Text, "", CTDec(RapidatxtCantidad10.Text), "N/A", CTDec(RapidatxtPrecioUnitario10.Text), CTDec(RapidatxtSubDescuentoValor10.Text), SubDescuentoValor10, 0, 0, CTDec(RapidatxtMontoItem10.Text)) Then
                        Throw New Exception(ErrorMsg)
                    End If
                Else
                    AgenteResponsable = CBx_AgenteResponsable10.Text
                    If Not objIntegration.DGIB1addDetalleNuevaLinea(ErrorMsg, ErrorCod, RapidacmbIndicadorFacturacion10.SelectedValue, AgenteResponsable.ToString, RapidatxtNombreItem10.Text, "", CTDec(RapidatxtCantidad10.Text), "N/A", CTDec(RapidatxtPrecioUnitario10.Text), CTDec(RapidatxtSubDescuentoValor10.Text), SubDescuentoValor10, 0, 0, CTDec(RapidatxtMontoItem10.Text)) Then
                        Throw New Exception(ErrorMsg)
                    End If
                    If RetPerLinea10Valor1 > 0 Then
                        If Not objIntegration.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, RetPerLinea10Cod1, RetPerLinea10Tasa1, RetPerLinea10Monto1, RetPerLinea10Valor1, "") Then
                            Throw New Exception(ErrorMsg)
                        End If
                    End If
                    If RetPerLinea10Valor2 > 0 Then
                        If Not objIntegration.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, RetPerLinea10Cod2, RetPerLinea10Tasa2, RetPerLinea10Monto2, RetPerLinea10Valor2, "") Then
                            Throw New Exception(ErrorMsg)
                        End If
                    End If
                    If RetPerLinea10Valor3 > 0 Then
                        If Not objIntegration.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, RetPerLinea10Cod3, RetPerLinea10Tasa3, RetPerLinea10Monto3, RetPerLinea10Valor3, "") Then
                            Throw New Exception(ErrorMsg)
                        End If
                    End If
                    If RetPerLinea10Valor4 > 0 Then
                        If Not objIntegration.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, RetPerLinea10Cod4, RetPerLinea10Tasa4, RetPerLinea10Monto4, RetPerLinea10Valor4, "") Then
                            Throw New Exception(ErrorMsg)
                        End If
                    End If
                    If RetPerLinea10Valor5 > 0 Then
                        If Not objIntegration.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, RetPerLinea10Cod5, RetPerLinea10Tasa5, RetPerLinea10Monto5, RetPerLinea10Valor5, "") Then
                            Throw New Exception(ErrorMsg)
                        End If
                    End If

                End If

            End If

            If CTDec(RapidaTxtMontoNoFacturable.Text) <> 0 Then
                Dim IndFacturacion As Integer = 0
                Dim MontoNoFacturable As Decimal = 0
                Dim textoNoFacturable As String
                CantLineas += 1

                If CTDec(RapidaTxtMontoNoFacturable.Text) > 0 Then
                    IndFacturacion = 6
                    MontoNoFacturable = CTDec(RapidaTxtMontoNoFacturable.Text)
                    textoNoFacturable = "Monto No Facturable"
                Else
                    IndFacturacion = 7
                    MontoNoFacturable = CTDec(RapidaTxtMontoNoFacturable.Text) * -1
                    textoNoFacturable = "Monto No Facturable Negativo"
                End If

                If Not objIntegration.DGIB1addDetalleNuevaLinea(ErrorMsg, ErrorCod, IndFacturacion, "", textoNoFacturable, "", 1, "N/A", MontoNoFacturable, 0, 0, 0, 0, MontoNoFacturable) Then
                    Throw New Exception(ErrorMsg)
                End If

            End If

            If CTDec(TBx_ValorSubTotales1.Text) > 0 Then
                If Not objIntegration.DGIC1addSubTotalInformativo(ErrorMsg, ErrorCod, TBx_GlosaSubTotales1.Text, 1, CTDec(TBx_ValorSubTotales1.Text)) Then
                    Throw New Exception(ErrorMsg)
                End If
            End If
            If CTDec(TBx_ValorSubTotales2.Text) > 0 Then
                If Not objIntegration.DGIC1addSubTotalInformativo(ErrorMsg, ErrorCod, TBx_GlosaSubTotales2.Text, 2, CTDec(TBx_ValorSubTotales2.Text)) Then
                    Throw New Exception(ErrorMsg)
                End If
            End If
            If CTDec(TBx_ValorSubTotales3.Text) > 0 Then
                If Not objIntegration.DGIC1addSubTotalInformativo(ErrorMsg, ErrorCod, TBx_GlosaSubTotales3.Text, 3, CTDec(TBx_ValorSubTotales3.Text)) Then
                    Throw New Exception(ErrorMsg)
                End If
            End If
            If CTDec(TBx_ValorSubTotales4.Text) > 0 Then
                If Not objIntegration.DGIC1addSubTotalInformativo(ErrorMsg, ErrorCod, TBx_GlosaSubTotales4.Text, 4, CTDec(TBx_ValorSubTotales4.Text)) Then
                    Throw New Exception(ErrorMsg)
                End If
            End If
            If CTDec(TBx_ValorSubTotales5.Text) > 0 Then
                If Not objIntegration.DGIC1addSubTotalInformativo(ErrorMsg, ErrorCod, TBx_GlosaSubTotales5.Text, 5, CTDec(TBx_ValorSubTotales5.Text)) Then
                    Throw New Exception(ErrorMsg)
                End If
            End If
            If objConfig.USARETENCIONPERCEPCION = 1 And Not IsNothing(CodigosRetPer) Then
                If CodigosRetPer.Count > 0 Then
                    For Each linea As String In CodigosRetPer
                        Dim codigo = linea.Split("-")
                        If Not objIntegration.DGIA6addTotalRetencionOPercepcion(ErrorMsg, ErrorCod, codigo(0).Trim, CTDec(codigo(1).Trim)) Then
                            Throw New Exception(ErrorMsg)
                        End If

                    Next

                End If
            End If

            'Totales
            Dim Moneda As String = "UYU"
            If cmbCfgTipoMonedaTransaccion.SelectedIndex = 1 Then
                Moneda = "USD"
            End If
            If Not objIntegration.DGIA5setTotalesDeEncabezado(ErrorMsg, ErrorCod, Moneda, CTDec(txtCfgTipoDeCambio.Text), CTDec(RapidatxtTotalMontoNoGravado.Text), TotalMontoExportacionAsimiladas, 0, 0, CTDec(RapidatxtTotalMontoNetoIVATasaMinima.Text), CTDec(RapidatxtTotalMontoNetoIVATasaBasica.Text), 0, CTDec(txtCfgTasaMinimaIVA.Text), CTDec(txtCfgTasaBasicaIVA.Text), CTDec(RapidatxtTotalIVATasaMinima.Text), CTDec(RapidatxtTotalIVATasaBasica.Text), 0, TotalMontoTotalTotalesEncabezado, CTDec(TBx_MontoRetenido.Text), CantLineas, CTDec(RapidaTxtMontoNoFacturable.Text), CTDec(RapidatxtMontoTotalPagar.Text), 0) Then
                Throw New Exception(ErrorMsg)
            End If

            'antes de invocar a datos adicionales me cercioro del nro de documento que tengo en el fichero config
            objConfig = New XMLCONFIGFACMANUAL
            objConfig = objConfig.CargarConfiguracion

            If RapidatxtNumeroComprobanteEncabezado.Enabled Then
                objConfig.NROFACTURA = RapidatxtNumeroComprobanteEncabezado.Text
                objConfig.GuardarConfig()
            Else
                RapidatxtNumeroComprobanteEncabezado.Text = objConfig.NROFACTURA
            End If

            If Not objIntegration.DATOSADICIONALESsetDatos(ErrorMsg, ErrorCod, "Facturador Manual", "1.0", txtCfgRazonSocialEmisor.Text, CTDec(txtCfgRUTEmisor.Text), CTDec(txtNroSucursal.Text), txtCfgNombreSucursalEmisor.Text, CTDec(txtCfgCajaNro.Text), CTDec(txtCfgCajeroNro.Text), txtCfgCajeroNombre.Text, cmbTipoVenta.Text, RapidatxtSerieComprobanteEncabezado.Text, CTDec(RapidatxtNumeroComprobanteEncabezado.Text), 0, 0, RapidatxtDocumentoReceptor.Text, RapidatxtNombreReceptor.Text, RapidatxtNombreReceptor.Text, RapidatxtDireccionReceptor.Text, "", "", RapidacmbCodigoPaisReceptor.Text, CTDec(txtCfgVendedorNro.Text), txtCfgVendedorNom.Text, CTDec(txtCfgValorUnidadIndexada.Text)) Then
                Throw New Exception(ErrorMsg)
            End If

            'Adenda
            If Not objIntegration.DGIJ1setAdenda(ErrorMsg, ErrorCod, RapidatxtAdenda.Text, Nothing) Then
                Throw New Exception(ErrorMsg)
            End If

            'Finalizar Factura
            Dim XMLFactura As String = ""
            If Not objIntegration.FinalizarFactura(ErrorMsg, ErrorCod, XMLFactura) Then
                Throw New Exception(ErrorMsg)
            End If

            'Inicio firma de factura
            If Not objIntegration.Inicializar(ErrorMsg, ErrorCod, txturlServidorGateway.Text, CTDec(txtSegundosTimeout.Text), CTDec(txtCfgRUTEmisor.Text), CTDec(txtNroSucursal.Text), CTDec(txtCfgCajaNro.Text), TextBox_CarpetaOperacion.Text) Then
                Throw New Exception(ErrorMsg)
            End If

            'Reservo el CAE para enviar a cobrar la factura primero antes de entrar a facturacion electronica
            Dim CAESerie As String = ""
            Dim CAENroReserv, CAENroAutoriz As Long
            If Not objIntegration.ReservarNroCAE(ErrorCod, ErrorMsg, TipoCFE, CAENroAutoriz, CAESerie, CAENroReserv) Then
                Throw New Exception("Error al reservar Nro CAE: " & ErrorCod & "--- " & ErrorMsg)
            End If

            'Seteo de parametros en TransAct
            If Not objIntegration.CargarConfiguracion(ErrorMsg, False, enumTipoImpresionTransAct.NoImprimir, 0, 0) Then
                Throw New Exception(ErrorMsg)
            End If

            Dim TrnId As Integer
            Dim NroTicket As Integer
            Dim Lote As Integer
            Dim NroAut As Double
            Dim TransaccionConfirmada As Boolean = False
            Dim FacturaConfirmada As Boolean = False
            Dim XMLRespuesta As String = ""

            'Si solo deseo emitir facura electronica no paso por TransAct
            If chkSoloFacturar.Checked Then
                If Not objIntegration.FirmarFactura(ErrorMsg, ErrorCod, XMLFactura, XMLRespuesta) Then
                    Throw New Exception(ErrorMsg)
                Else
                    objIntegration.UnParsingXMLRespuesta(XMLRespuesta, ErrorMsg, ErrorCod, "", True, Now, 0, 0, 0, Now, UltimaSerieComprobante, UltimoNumeroComprobante, "", "", "", "")
                    UltimoTipoComprobante = TipoCFE
                    If chkImprimeApi.Checked And Not Ckb_esContingencia.Checked Then
                        ImprimirFactura(UltimoTipoComprobante, UltimaSerieComprobante, UltimoNumeroComprobante)
                    End If
                End If

                'Cargo Datos en la respuesta
                Dim frmResp As New frmRespuesta(XMLRespuesta, False)
                frmResp.ShowDialog()
            Else
                'Llamada a TransAct
                If objIntegration.NuevaTransaccion(ErrorMsg, txtEmpCod.Text, txtTermCod.Text, IIf(rbVenta.Checked, enumTipoOperacion.OperacionVenta, enumTipoOperacion.OperacionDevolucion), IIf(cmbCfgTipoMonedaTransaccion.SelectedIndex = 1, enumTipoMoneda.Dolares, enumTipoMoneda.Pesos), 0, enumTipoTarjetasAdquirente.TarjetasTODAS, 0, enumTipoTarjeta.TarjetaTodas, enumTipoProcesador.Todos) Then
                    Dim MontoTransaccion, MontoFactura, MontoGravadoFactura, MontoIVAFactura, MontoPropina, MontoCashBack As Double
                    Dim CantidadCuotas, IdDecretoLey As Integer
                    IdDecretoLey = 0
                    MontoTransaccion = CTDec(RapidatxtMontoTotalPagar.Text)
                    MontoFactura = MontoTransaccion
                    MontoGravadoFactura = Math.Round(MontoTransaccion / 1.22, 2)
                    MontoIVAFactura = MontoTransaccion - MontoGravadoFactura
                    MontoPropina = MontoTransaccion * 0.1
                    MontoCashBack = 0
                    CantidadCuotas = 1
                    If objIntegration.CargarDatosTransaccion(ErrorMsg, CAENroReserv, MontoTransaccion, MontoFactura, MontoGravadoFactura, MontoIVAFactura, MontoPropina, MontoCashBack, CantidadCuotas, IIf(chkConsumoFinal.Checked, 1, 0), IdDecretoLey, enumTipoCuenta.CajaAhorroPesos) Then
                        objIntegration.ConfigurarComportamientoTransaccion(chkModificarMoneda.Checked, chkModificarMontos.Checked, chkModificarCuotas.Checked, chkModificarFactura.Checked, chkModificarTarjeta.Checked,
                                                       chkModificarPlan.Checked, chkModificarDecretoLey.Checked, chkModificarTipoCuenta.Checked)
                        If objIntegration.ProcesarTransaccion(ErrorMsg, TrnId, NroTicket, Lote, NroAut) Then
                            'Llamada a TAFACE
                            If Not chkSoloCobrar.Checked Then
                                If Not objIntegration.FirmarFacturaConReserva(ErrorMsg, ErrorCod, XMLFactura, XMLRespuesta, CAENroAutoriz, CAESerie, CAENroReserv) Then
                                    objIntegration.ReversarTransaccion(ErrorMsg, TrnId)
                                    Throw New Exception(ErrorMsg)
                                Else
                                    FacturaConfirmada = True
                                End If
                            End If

                            If Not objIntegration.ConfirmarTransAct(ErrorMsg, TrnId) Then
                                Throw New Exception(ErrorMsg)
                            Else
                                TransaccionConfirmada = True
                            End If
                        Else
                            Throw New Exception(ErrorMsg)
                        End If
                    Else
                        Throw New Exception(ErrorMsg)
                    End If
                Else
                    Throw New Exception(ErrorMsg)
                End If
            End If

            'Incremento numerador antes de entrar a respuesta por si falla
            objConfig.NROFACTURA = Double.Parse(RapidatxtNumeroComprobanteEncabezado.Text) + 1
                RapidatxtNumeroComprobanteEncabezado.Text = objConfig.NROFACTURA
                objConfig.GuardarConfig()
                'Guardo datos del receptor en fichero
                If Not RapidatxtDocumentoReceptor.Text.Trim = "" Then
                    If (Not RapidatxtDepartamentoReceptor.Text.Trim = "") And (Not RapidatxtCiudadReceptor.Text.Trim = "") And (Not RapidatxtDireccionReceptor.Text.Trim = "") And (Not RapidatxtNombreReceptor.Text.Trim = "") Then
                        guardarReceptor()
                    End If
                End If

            If chkSoloFacturar.Checked Then
                'Cargo Datos en la respuesta
                If FacturaConfirmada Then
                    Dim frmResp As New frmRespuesta(XMLRespuesta, chkSoloCobrar.Checked)
                    'frmResp.CargarNroTransAct(TrnId, NroTicket, Lote, NroAut)
                    frmResp.ShowDialog()
                Else
                    Throw New Exception("No se pudo confirmar la factura electrónica")
                End If
            ElseIf chkSoloCobrar.Checked Then
                If TransaccionConfirmada Then
                    Dim frmResp As New frmRespuesta(XMLRespuesta, chkSoloCobrar.Checked)
                    frmResp.CargarNroTransAct(TrnId, NroTicket, Lote, NroAut)
                    frmResp.ShowDialog()
                Else
                    Throw New Exception("No se pudo confirmar la transacción")
                End If
            Else
                If FacturaConfirmada And TransaccionConfirmada Then
                    Dim frmResp As New frmRespuesta(XMLRespuesta, chkSoloCobrar.Checked)
                    frmResp.CargarNroTransAct(TrnId, NroTicket, Lote, NroAut)
                    frmResp.ShowDialog()
                End If
            End If

                If Not chkSoloFacturar.Checked And chkImprimeApi.Checked Then
                'Impresion
                If Not objIntegration.InicializarImpresion(ErrorMsg, ErrorCod, cmbImpresora.Text, cmbTipoImpresora.SelectedValue) Then
                    Throw New Exception(ErrorMsg)
                End If
                If rdButtonImprimeVoucherAdenda.Checked Then
                    If chkSoloCobrar.Checked Then
                        If chkImprimeApi.Checked And Not Ckb_esContingencia.Checked Then
                            objIntegration.Imprimir(ErrorMsg, ErrorCod, False, XMLFactura, XMLRespuesta, objIntegration.GetObjetoTransActTarjetas.Transaccion.Respuesta.Voucher, False, CDec(txtCantCopias.Text))
                        End If
                    Else
                        If chkImprimeApi.Checked And Not Ckb_esContingencia.Checked Then
                            objIntegration.Imprimir(ErrorMsg, ErrorCod, True, XMLFactura, XMLRespuesta, objIntegration.GetObjetoTransActTarjetas.Transaccion.Respuesta.Voucher, False, CDec(txtCantCopias.Text))
                        End If
                    End If
                ElseIf rdButtonImprimeVoucherTransAct.Checked Then
                        objIntegration.Imprimir(ErrorMsg, ErrorCod, False, XMLFactura, XMLRespuesta, objIntegration.GetObjetoTransActTarjetas.Transaccion.Respuesta.Voucher, False, CDec(txtCantCopias.Text))
                End If
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Error")
        Finally
            LimpiarCampos()
            LimpiarCamposFacturacion()
            RapidatxtNombreItem1.Focus()
        End Try

    End Sub

    Private Sub ImprimirFactura(ByVal TipoDeComprobante As Integer, ByVal SerieComprobante As String, ByVal NroComprobante As Integer)
        Dim ErrorMsg As String = ""
        Dim ErrorCod As Integer
        Try
            Dim objTAFEApi As New TAFE2Api.TAFEApi_v0206
            If Not objTAFEApi.Inicializar(ErrorMsg, ErrorCod, txturlServidorGateway.Text, CTDec(txtSegundosTimeout.Text), CLng(txtCfgRUTEmisor.Text), CTDec(txtNroSucursal.Text), CTDec(txtCfgCajaNro.Text), TextBox_CarpetaOperacion.Text) Then
                Throw New Exception(ErrorMsg)
            End If
            If Not objTAFEApi.InicializarImpresion(ErrorMsg, ErrorCod, cmbImpresora.Text, cmbTipoImpresora.SelectedValue) Then
                Throw New Exception(ErrorMsg)
            End If
            For i As Integer = 1 To txtCantCopias.Value
                If Not objTAFEApi.ImprimirFactura(ErrorMsg, ErrorCod, CLng(txtCfgRUTEmisor.Text), CTDec(txtNroSucursal.Text), CTDec(txtCfgCajaNro.Text), TipoDeComprobante, SerieComprobante, NroComprobante) Then
                    Throw New Exception(ErrorMsg)
                End If
            Next
        Catch ex As Exception
            MsgBox(ErrorMsg)
        Finally
        End Try
    End Sub


    Private Sub ReImprimirFactura()
        Dim ErrorMsg As String = ""
        Dim ErrorCod As Integer
        Try
            Dim objTAFEApi As TAFE2Api.TAFEApi_v0206
            Dim frmParametrosReimp As New frmReimpresion(UltimoTipoComprobante, UltimaSerieComprobante, UltimoNumeroComprobante)
            frmParametrosReimp.ShowDialog()
            If Not frmParametrosReimp.ProcesoOk Then
                Exit Sub
            End If
            objTAFEApi = New TAFE2Api.TAFEApi_v0206
            If Not objTAFEApi.Inicializar(ErrorMsg, ErrorCod, txturlServidorGateway.Text, CTDec(txtSegundosTimeout.Text), CLng(txtCfgRUTEmisor.Text), CTDec(txtNroSucursal.Text), CTDec(txtCfgCajaNro.Text), TextBox_CarpetaOperacion.Text) Then
                Throw New Exception(ErrorMsg)
            End If
            If Not objTAFEApi.InicializarImpresion(ErrorMsg, ErrorCod, cmbImpresora.Text, cmbTipoImpresora.SelectedValue) Then
                Throw New Exception(ErrorMsg)
            End If
            For i As Integer = 1 To txtCantCopias.Value
                If Not objTAFEApi.ReimprimirFactura(ErrorMsg, ErrorCod, CLng(txtCfgRUTEmisor.Text), CTDec(txtNroSucursal.Text), CTDec(txtCfgCajaNro.Text), frmParametrosReimp.TipoCFE, frmParametrosReimp.SerieCFE, frmParametrosReimp.NumeroCFE) Then
                    Throw New Exception(ErrorMsg)
                End If
            Next
        Catch ex As Exception
            MsgBox(ErrorMsg)
        Finally
        End Try
    End Sub

    Private Sub LimpiarCamposFacturacion()
        If Not RapidatxtDocumentoReceptor.Text.Trim = "" Then
            If (Not RapidatxtDepartamentoReceptor.Text.Trim = "") And (Not RapidatxtCiudadReceptor.Text.Trim = "") And (Not RapidatxtDireccionReceptor.Text.Trim = "") And (Not RapidatxtNombreReceptor.Text.Trim = "") Then
                guardarReceptor()
            End If
        End If
        If Not RapidatxtNombreItem1.Text.Trim = "" Then
            guardarArticulo(RapidatxtNombreItem1.Text.Trim, Convert.ToDouble(RapidatxtPrecioUnitario1.Text))
        End If
        If Not RapidatxtNombreItem2.Text.Trim = "" Then
            guardarArticulo(RapidatxtNombreItem2.Text.Trim, Convert.ToDouble(RapidatxtPrecioUnitario2.Text))
        End If
        If Not RapidatxtNombreItem3.Text.Trim = "" Then
            guardarArticulo(RapidatxtNombreItem3.Text.Trim, Convert.ToDouble(RapidatxtPrecioUnitario3.Text))
        End If
        If Not RapidatxtNombreItem4.Text.Trim = "" Then
            guardarArticulo(RapidatxtNombreItem4.Text.Trim, Convert.ToDouble(RapidatxtPrecioUnitario4.Text))
        End If
        If Not RapidatxtNombreItem5.Text.Trim = "" Then
            guardarArticulo(RapidatxtNombreItem5.Text.Trim, Convert.ToDouble(RapidatxtPrecioUnitario5.Text))
        End If
        If Not RapidatxtNombreItem6.Text.Trim = "" Then
            guardarArticulo(RapidatxtNombreItem6.Text.Trim, Convert.ToDouble(RapidatxtPrecioUnitario6.Text))
        End If
        If Not RapidatxtNombreItem7.Text.Trim = "" Then
            guardarArticulo(RapidatxtNombreItem7.Text.Trim, Convert.ToDouble(RapidatxtPrecioUnitario7.Text))
        End If
        If Not RapidatxtNombreItem8.Text.Trim = "" Then
            guardarArticulo(RapidatxtNombreItem8.Text.Trim, Convert.ToDouble(RapidatxtPrecioUnitario8.Text))
        End If
        If Not RapidatxtNombreItem9.Text.Trim = "" Then
            guardarArticulo(RapidatxtNombreItem9.Text.Trim, Convert.ToDouble(RapidatxtPrecioUnitario9.Text))
        End If
        If Not RapidatxtNombreItem10.Text.Trim = "" Then
            guardarArticulo(RapidatxtNombreItem10.Text.Trim, Convert.ToDouble(RapidatxtPrecioUnitario10.Text))
        End If
    End Sub

    Private Sub BtnFacturar_Click(sender As System.Object, e As System.EventArgs) Handles BtnFacturar.Click
        Facturar()
    End Sub

    Private Sub TipoCFEActual(ByRef TipoCFE As TAFACE2ApiParsing.ITAParsing_v0206.enumTipoDeComprobanteCFE, ByRef TipoCFEAux As Integer)
        If chkConsumoFinal.Checked Then
            If Ckb_esContingencia.Checked Then
                TipoCFE = TAFACE2ApiParsing.ITAParsing_v0206.enumTipoDeComprobanteCFE.eTicketContingencia
                TipoCFEAux = 201
                If cmbTipoVenta.SelectedValue = TipoDocumento.NotaCredito Then
                    TipoCFE = TAFACE2ApiParsing.ITAParsing_v0206.enumTipoDeComprobanteCFE.eTicketNotaCredContingencia
                    TipoCFEAux = 202
                ElseIf cmbTipoVenta.SelectedValue = TipoDocumento.NotaDebito Then
                    TipoCFE = TAFACE2ApiParsing.ITAParsing_v0206.enumTipoDeComprobanteCFE.eTicketNotaDebContingencia
                    TipoCFEAux = 203
                End If
            Else
                TipoCFE = TAFACE2ApiParsing.ITAParsing_v0206.enumTipoDeComprobanteCFE.eTicket
                TipoCFEAux = 101
                If cmbTipoVenta.SelectedValue = TipoDocumento.NotaCredito Then
                    TipoCFE = TAFACE2ApiParsing.ITAParsing_v0206.enumTipoDeComprobanteCFE.eTicketNotaCred
                    TipoCFEAux = 102
                ElseIf cmbTipoVenta.SelectedValue = TipoDocumento.NotaDebito Then
                    TipoCFE = TAFACE2ApiParsing.ITAParsing_v0206.enumTipoDeComprobanteCFE.eTicketNotaDeb
                    TipoCFEAux = 103
                End If
            End If
        ElseIf Ckb_Exportacion.Checked Then
            If Ckb_esContingencia.Checked Then
                If Rdb_esRemito.Checked Then
                    TipoCFE = TAFACE2ApiParsing.ITAParsing_v0206.enumTipoDeComprobanteCFE.eRemitoExportacionContingencia
                    TipoCFEAux = 224
                Else
                    TipoCFE = TAFACE2ApiParsing.ITAParsing_v0206.enumTipoDeComprobanteCFE.eFacturaExportacionContingencia
                    TipoCFEAux = 221
                    If cmbTipoVenta.SelectedValue = TipoDocumento.NotaCredito Then
                        TipoCFE = TAFACE2ApiParsing.ITAParsing_v0206.enumTipoDeComprobanteCFE.eFacturaNotaCredExportacionContingencia
                        TipoCFEAux = 222
                    ElseIf cmbTipoVenta.SelectedValue = TipoDocumento.NotaDebito Then
                        TipoCFE = TAFACE2ApiParsing.ITAParsing_v0206.enumTipoDeComprobanteCFE.eFacturaNotaDebExportacionContingencia
                        TipoCFEAux = 223
                    End If
                End If
            Else
                If Rdb_esRemito.Checked Then
                    TipoCFE = TAFACE2ApiParsing.ITAParsing_v0206.enumTipoDeComprobanteCFE.eRemitoExportacion
                    TipoCFEAux = 124
                Else
                    TipoCFE = TAFACE2ApiParsing.ITAParsing_v0206.enumTipoDeComprobanteCFE.eFacturaExportacion
                    TipoCFEAux = 121
                    If cmbTipoVenta.SelectedValue = TipoDocumento.NotaCredito Then
                        TipoCFE = TAFACE2ApiParsing.ITAParsing_v0206.enumTipoDeComprobanteCFE.eFacturaNotaCredExportacion
                        TipoCFEAux = 122
                    ElseIf cmbTipoVenta.SelectedValue = TipoDocumento.NotaDebito Then
                        TipoCFE = TAFACE2ApiParsing.ITAParsing_v0206.enumTipoDeComprobanteCFE.eFacturaNotaDebExportacion
                        TipoCFEAux = 123
                    End If
                End If
            End If
        ElseIf Ckb_esContingencia.Checked Then
            If Rdb_esRemito.Checked Then
                TipoCFE = TAFACE2ApiParsing.ITAParsing_v0206.enumTipoDeComprobanteCFE.eRemitoContingencia
                TipoCFEAux = 281
            ElseIf Rdb_esBoleta.Checked Then
                TipoCFE = TAFACE2ApiParsing.ITAParsing_v0206.enumTipoDeComprobanteCFE.eBoletaContingencia
                TipoCFEAux = 251
                If cmbTipoVenta.SelectedValue = TipoDocumento.NotaCredito Then
                    TipoCFE = TAFACE2ApiParsing.ITAParsing_v0206.enumTipoDeComprobanteCFE.eBoletaNotaCredContingencia
                    TipoCFEAux = 252
                ElseIf cmbTipoVenta.SelectedValue = TipoDocumento.NotaDebito Then
                    TipoCFE = TAFACE2ApiParsing.ITAParsing_v0206.enumTipoDeComprobanteCFE.eBoletaNotaDebContingencia
                    TipoCFEAux = 253
                End If
            Else
                TipoCFE = TAFACE2ApiParsing.ITAParsing_v0206.enumTipoDeComprobanteCFE.eFacturaContingencia
                TipoCFEAux = 211
                If cmbTipoVenta.SelectedValue = TipoDocumento.NotaCredito Then
                    TipoCFE = TAFACE2ApiParsing.ITAParsing_v0206.enumTipoDeComprobanteCFE.eFacturaNotaCredContingencia
                    TipoCFEAux = 212
                ElseIf cmbTipoVenta.SelectedValue = TipoDocumento.NotaDebito Then
                    TipoCFE = TAFACE2ApiParsing.ITAParsing_v0206.enumTipoDeComprobanteCFE.eFacturaNotaDebContingencia
                    TipoCFEAux = 213
                End If
            End If
        Else
            If Rdb_esRemito.Checked Then
                TipoCFE = TAFACE2ApiParsing.ITAParsing_v0206.enumTipoDeComprobanteCFE.eRemito
                TipoCFEAux = 181
            ElseIf Rdb_esBoleta.Checked Then
                TipoCFE = TAFACE2ApiParsing.ITAParsing_v0206.enumTipoDeComprobanteCFE.eBoleta
                TipoCFEAux = 151
                If cmbTipoVenta.SelectedValue = TipoDocumento.NotaCredito Then
                    TipoCFE = TAFACE2ApiParsing.ITAParsing_v0206.enumTipoDeComprobanteCFE.eBoletaNotaCred
                    TipoCFEAux = 152
                ElseIf cmbTipoVenta.SelectedValue = TipoDocumento.NotaDebito Then
                    TipoCFE = TAFACE2ApiParsing.ITAParsing_v0206.enumTipoDeComprobanteCFE.eBoletaNotaDeb
                    TipoCFEAux = 153
                End If
            Else
                TipoCFE = TAFACE2ApiParsing.ITAParsing_v0206.enumTipoDeComprobanteCFE.eFactura
                TipoCFEAux = 111
                If cmbTipoVenta.SelectedValue = TipoDocumento.NotaCredito Then
                    TipoCFE = TAFACE2ApiParsing.ITAParsing_v0206.enumTipoDeComprobanteCFE.eFacturaNotaCred
                    TipoCFEAux = 112
                ElseIf cmbTipoVenta.SelectedValue = TipoDocumento.NotaDebito Then
                    TipoCFE = TAFACE2ApiParsing.ITAParsing_v0206.enumTipoDeComprobanteCFE.eFacturaNotaDeb
                    TipoCFEAux = 113
                End If
            End If
        End If
    End Sub

    Private Sub frmFactura_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.F3 Then
            ReImprimirFactura()
        ElseIf e.KeyCode = Keys.F8 Then
            BtnFacturar_Click(Nothing, Nothing)

        ElseIf e.KeyCode = Keys.F12 Then
            LimpiarCampos()
        End If
    End Sub

    Private Sub frmFactura_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'Utilizo para capturar KeyDown en el formulario antes que el control que tenga el foco.
        Me.KeyPreview = True

        'Inicializar Try Menu
        mnuDisplayForm = New ToolStripMenuItem("Facturar")
        mnuDisplayForm.Image = My.Resources.Start1
        mnuSep1 = New ToolStripSeparator()
        mnuExit = New ToolStripMenuItem("Cerrar")
        mnuExit.Image = My.Resources.close
        MainMenu = New ContextMenuStrip
        MainMenu.Items.AddRange(New ToolStripItem() {mnuDisplayForm, mnuSep1, mnuExit})
        NotifyIcon1.ContextMenuStrip = MainMenu

        Try
            CalcularTotales()
            cargarReceptores()
            cargarArticulos()
            cmbTipoNegocio.SelectedIndex = 0
            'Verifico que exista la configuracion
            objConfig = New XMLCONFIGFACMANUAL
            objConfig = objConfig.CargarConfiguracion
            If IsNothing(objConfig) Then
                MsgBox("Antes de realizar una factura debe configurar la aplicacion.", MsgBoxStyle.OkOnly, "Configuracion")
                TabControl1.SelectedTab = Configuracion
                Exit Sub
            End If
            'Cargo combos
            CargarCombos()

            'Arreglo vista pantalla
            ArreglarVistaPantalla()

            'Cargo la configuracion
            txtCfgRazonSocialEmisor.Text = objConfig.RAZONSOCIALENEMISOR
            txtCfgNombreEmisor.Text = objConfig.NOMBREEMISOR
            txtCfgRUTEmisor.Text = objConfig.RUTEMISOR
            txtCfgCodigoSucursalEmisor.Text = objConfig.CODDGISUCURSAL
            txtNroSucursal.Text = objConfig.NROSUCURSAL
            txtCfgNombreSucursalEmisor.Text = objConfig.NOMBRESUCURSAL
            txtCfgCajaNro.Text = objConfig.NROCAJA
            txtCfgCajeroNro.Text = objConfig.NROCAJERO
            txtCfgCajeroNombre.Text = objConfig.NOMBRECAJERO
            txtCfgVendedorNro.Text = objConfig.NROVENDEDOR
            txtCfgVendedorNom.Text = objConfig.NOMBREVENDEDOR
            txtCfgGiroNegocioEmisor.Text = objConfig.GIRONEGOCIOEMISOR
            txtCfgCorreoEmisor.Text = objConfig.CORREOEMISOR
            txtCfgDomicilioFiscalEmisor.Text = objConfig.DOMFISCALEMISOR
            txtCfgCiudadEmisor.Text = objConfig.CIUDADEMISOR
            txtCfgDepartamentoEmisor.Text = objConfig.DEPARTAMENTOEMISOR
            txtCfgValorUnidadIndexada.Text = objConfig.VALORUNIDADINDEXADA
            txtCfgTelefonoEmisor.Text = objConfig.TELEFONOEMISOR
            txtCfgTelefono2Emisor.Text = objConfig.TELEFONO2EMISOR
            cmbCfgTipoMonedaTransaccion.SelectedValue = objConfig.TIPOMONEDA
            txtCfgTipoDeCambio.Text = objConfig.TIPODECAMBIO
            txtCfgTasaBasicaIVA.Text = objConfig.TASABASICAIVA
            txtCfgTasaMinimaIVA.Text = objConfig.TASAMINIMAIVA
            txturlServidorGateway.Text = objConfig.URLSERVIDOR
            txtSegundosTimeout.Text = objConfig.TIMEOUTSERVIDOR
            If objConfig.CARPETAOPERACION = Nothing Or objConfig.CARPETAOPERACION = "" Then
                TextBox_CarpetaOperacion.Text = "C:  \"
            Else
                TextBox_CarpetaOperacion.Text = objConfig.CARPETAOPERACION
            End If
            If objConfig.TIPOMONEDA = "USD" Then
                CambiarMonedaDolares()
            Else
                CambiarMonedaPesos()
            End If

            chkImprimeApi.Checked = objConfig.IMPRIMEAPI
            If chkImprimeApi.Checked Then
                cmbImpresora.Text = objConfig.NOMBREIMPRESORA
                cmbTipoImpresora.SelectedValue = CInt(objConfig.TIPOIMPRESORA)
                txtCantCopias.Value = objConfig.CANTIDADCOPIAS
            End If

            cmbCfgIndicadorFacturacion.Text = objConfig.ImpuestoPorDefecto
            chkConsumoFinalDefault.Checked = objConfig.ConsumoFinal
            chkConsumoFinal.Checked = objConfig.ConsumoFinal
            Chk_UsaRetPerc.Checked = objConfig.USARETENCIONPERCEPCION
            Chk_UsaSubTotales.Checked = objConfig.USASUBTOTALES
            Chk_PreFactura.Checked = objConfig.PREFACTURA
            Chk_usaExportacion.Checked = objConfig.USAEXPORTACION
            Chk_emiteRemitos.Checked = objConfig.EMITEEXTRACFE

            If Not objConfig.ImpuestoPorDefecto = "" Then
                RapidacmbIndicadorFacturacion1.Text = objConfig.ImpuestoPorDefecto
                RapidacmbIndicadorFacturacion2.Text = objConfig.ImpuestoPorDefecto
                RapidacmbIndicadorFacturacion3.Text = objConfig.ImpuestoPorDefecto
                RapidacmbIndicadorFacturacion4.Text = objConfig.ImpuestoPorDefecto
                RapidacmbIndicadorFacturacion5.Text = objConfig.ImpuestoPorDefecto
                RapidacmbIndicadorFacturacion6.Text = objConfig.ImpuestoPorDefecto
                RapidacmbIndicadorFacturacion7.Text = objConfig.ImpuestoPorDefecto
                RapidacmbIndicadorFacturacion8.Text = objConfig.ImpuestoPorDefecto
                RapidacmbIndicadorFacturacion9.Text = objConfig.ImpuestoPorDefecto
                RapidacmbIndicadorFacturacion10.Text = objConfig.ImpuestoPorDefecto
            End If

            If objConfig.BLOQUEARIMPUESTO = 1 Then
                RapidacmbIndicadorFacturacion1.Enabled = False
                RapidacmbIndicadorFacturacion2.Enabled = False
                RapidacmbIndicadorFacturacion3.Enabled = False
                RapidacmbIndicadorFacturacion4.Enabled = False
                RapidacmbIndicadorFacturacion5.Enabled = False
                RapidacmbIndicadorFacturacion6.Enabled = False
                RapidacmbIndicadorFacturacion7.Enabled = False
                RapidacmbIndicadorFacturacion8.Enabled = False
                RapidacmbIndicadorFacturacion9.Enabled = False
                RapidacmbIndicadorFacturacion10.Enabled = False
            Else
                RapidacmbIndicadorFacturacion1.Enabled = True
                RapidacmbIndicadorFacturacion2.Enabled = True
                RapidacmbIndicadorFacturacion3.Enabled = True
                RapidacmbIndicadorFacturacion4.Enabled = True
                RapidacmbIndicadorFacturacion5.Enabled = True
                RapidacmbIndicadorFacturacion6.Enabled = True
                RapidacmbIndicadorFacturacion7.Enabled = True
                RapidacmbIndicadorFacturacion8.Enabled = True
                RapidacmbIndicadorFacturacion9.Enabled = True
                RapidacmbIndicadorFacturacion10.Enabled = True
            End If

            RapidaTbx_tipoCambio.Text = objConfig.TIPODECAMBIO
            If objConfig.DESHABILITARCONFIGURACION = 1 Then
                RapidaTbx_tipoCambio.Visible = True
                RapidaLbl_tipoCambio.Visible = True
                RapidaBtn_guardar.Visible = True
            Else
                RapidaTbx_tipoCambio.Visible = False
                RapidaLbl_tipoCambio.Visible = False
                RapidaBtn_guardar.Visible = False
            End If
            If objConfig.USAEXPORTACION = 1 Then
                Ckb_Exportacion.Visible = False
            End If

            If objConfig.EMITEEXTRACFE = 1 Then
                GroupBox1.Visible = True
            Else
                GroupBox1.Visible = False
            End If

            If objConfig.RAZONSOCIALENEMISOR.ToString <> "" Then
                Me.Text = "Factura - TA-Face (Para: " + objConfig.RAZONSOCIALENEMISOR.ToString + ")"
            End If
            If objConfig.USAEXPORTACION = 1 And Not chkConsumoFinal.Checked Then
                Ckb_Exportacion.Visible = True
            Else
                Ckb_Exportacion.Visible = False
            End If
            Ckb_Exportacion.Checked = False

            RapidatxtNumeroComprobanteEncabezado.Text = objConfig.NROFACTURA

            txtEmpCod.Text = Trim(objConfig.EMPCOD)
            txtTermCod.Text = Trim(objConfig.TERMCOD)
            chkModificarCuotas.Checked = objConfig.MODIFICARCUOTAS
            chkModificarFactura.Checked = objConfig.MODIFICARFACTURA
            chkModificarMoneda.Checked = objConfig.MODIFICARMONEDA
            chkModificarMontos.Checked = objConfig.MODIFICARMONTOS
            chkModificarPlan.Checked = objConfig.MODIFICARPLAN
            chkModificarTarjeta.Checked = objConfig.MODIFICARTARJETA
            chkModificarTipoCuenta.Checked = objConfig.MODIFICARTIPOCUENTA
            chkModificarDecretoLey.Checked = objConfig.MODOFICARDECRETOLEY

            objConfig.IIMPRIMEVOUCHERADENDA = rdButtonImprimeVoucherAdenda.Checked

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        End Try
    End Sub

    Private Sub ArreglarVistaPantalla()
        GBx_Exportaciones.Visible = False
        If objConfig.USARETENCIONPERCEPCION = 1 Then
            GBx_RetencionesPercepciones.Visible = True
        Else
            GBx_RetencionesPercepciones.Visible = False
        End If

        If objConfig.USASUBTOTALES = 1 Then
            GBx_SubTotalesInformativos.Visible = True
        Else
            GBx_SubTotalesInformativos.Visible = False
        End If

        If objConfig.USARETENCIONPERCEPCION = 1 Or objConfig.USASUBTOTALES = 1 Then
            Lbl_Comentarios.Location = New Point(5, 495)
            RapidatxtAdenda.Location = New Point(6, 511)
            GBx_MediosPagos.Location = New Point(8, 646)
            GBx_Totales.Location = New Point(243, 646)
            BtnFacturar.Location = New Point(888, 847)
            btnReimprimir.Location = New Point(5, 839)
            BtnLimpiarCampos.Location = New Point(5, 876)
            panAdvertencia.Location = New Point(135, 844)
            GBx_PreFactura.Location = New Point(6, 925)

            If objConfig.USASUBTOTALES = 1 Then
                RapidatxtAdenda.Size = New System.Drawing.Size(231, 129)
            Else
                RapidatxtAdenda.Size = New System.Drawing.Size(775, 129)
            End If

            If objConfig.PREFACTURA = 1 Then
                Lbl_NewAge.Location = New Point(407, 999)
            Else
                Lbl_NewAge.Location = New Point(407, 901)
            End If
        End If
        If objConfig.USASUBTOTALES = 0 And objConfig.USARETENCIONPERCEPCION = 0 Then
            Lbl_Comentarios.Location = New Point(792, 265)
            RapidatxtAdenda.Location = New Point(787, 282)
            RapidatxtAdenda.Size = New System.Drawing.Size(278, 209)
            GBx_MediosPagos.Location = New Point(8, 497)
            GBx_Totales.Location = New Point(243, 497)
            BtnFacturar.Location = New Point(888, 698)
            btnReimprimir.Location = New Point(5, 690)
            BtnLimpiarCampos.Location = New Point(5, 727)
            panAdvertencia.Location = New Point(135, 694)
            GBx_PreFactura.Location = New Point(6, 776)

            If objConfig.PREFACTURA = 1 Then
                Lbl_NewAge.Location = New Point(407, 850)
            Else
                Lbl_NewAge.Location = New Point(407, 752)
            End If
        End If

        If objConfig.PREFACTURA = 1 Then
            GBx_PreFactura.Visible = True
        Else
            GBx_PreFactura.Visible = False
        End If

    End Sub

    Private Sub CalcularTotales()
        SubDescuentoValor1 = 0
        SubDescuentoValor2 = 0
        SubDescuentoValor3 = 0
        SubDescuentoValor4 = 0
        SubDescuentoValor5 = 0

        SubDescuentoValor6 = 0
        SubDescuentoValor7 = 0
        SubDescuentoValor8 = 0
        SubDescuentoValor9 = 0
        SubDescuentoValor10 = 0

        Dim TotalMontoNoGravado As Decimal = 0
        Dim TotalMontoIVAEnSuspenso As Decimal = 0
        Dim TotalMontoNetoIVATasaMinima As Decimal = 0
        Dim TotalMontoNetoIVATasaBasica As Decimal = 0
        Dim TotalMontoNetoIVAOtraTasa As Decimal = 0
        Dim TotalIVATasaMinima As Decimal = 0
        Dim TotalIVATasaBasica As Decimal = 0
        Dim TotalIVAOtraTasa As Decimal = 0
        Dim TotalMontoTotal As Decimal = 0
        Dim TotalMontoRetenido As Decimal = 0
        Dim MontoNoFacturable As Decimal = 0
        Dim MontoTotalAPagar As Decimal = 0
        Dim TotalMontoProductoServicioNoFacturable As Decimal = 0
        Dim TotalMontoProductoServicioNoFacturableNegativo As Decimal = 0
        Dim IVATasaMinima As Decimal = 0
        Dim IVATasaBasica As Decimal = 0
        TotalMontoExportacionAsimiladas = 0

        IVATasaMinima = CTDec(txtCfgTasaMinimaIVA.Text)
        IVATasaBasica = CTDec(txtCfgTasaBasicaIVA.Text)

        '2-Factura Detalle
        RapidatxtMontoItem1.Text = Math.Round(CTDec(RapidatxtCantidad1.Text) * CTDec(RapidatxtPrecioUnitario1.Text), 2)
        RapidatxtMontoItem2.Text = Math.Round(CTDec(RapidatxtCantidad2.Text) * CTDec(RapidatxtPrecioUnitario2.Text), 2)
        RapidatxtMontoItem3.Text = Math.Round(CTDec(RapidatxtCantidad3.Text) * CTDec(RapidatxtPrecioUnitario3.Text), 2)
        RapidatxtMontoItem4.Text = Math.Round(CTDec(RapidatxtCantidad4.Text) * CTDec(RapidatxtPrecioUnitario4.Text), 2)
        RapidatxtMontoItem5.Text = Math.Round(CTDec(RapidatxtCantidad5.Text) * CTDec(RapidatxtPrecioUnitario5.Text), 2)

        RapidatxtMontoItem6.Text = Math.Round(CTDec(RapidatxtCantidad6.Text) * CTDec(RapidatxtPrecioUnitario6.Text), 2)
        RapidatxtMontoItem7.Text = Math.Round(CTDec(RapidatxtCantidad7.Text) * CTDec(RapidatxtPrecioUnitario7.Text), 2)
        RapidatxtMontoItem8.Text = Math.Round(CTDec(RapidatxtCantidad8.Text) * CTDec(RapidatxtPrecioUnitario8.Text), 2)
        RapidatxtMontoItem9.Text = Math.Round(CTDec(RapidatxtCantidad9.Text) * CTDec(RapidatxtPrecioUnitario9.Text), 2)
        RapidatxtMontoItem10.Text = Math.Round(CTDec(RapidatxtCantidad10.Text) * CTDec(RapidatxtPrecioUnitario10.Text), 2)

        nCantLineas = 0
        If RapidatxtNombreItem1.Text <> "" Then
            nCantLineas += 1
        End If
        If RapidatxtNombreItem2.Text <> "" Then
            nCantLineas += 1
        End If
        If RapidatxtNombreItem3.Text <> "" Then
            nCantLineas += 1
        End If
        If RapidatxtNombreItem4.Text <> "" Then
            nCantLineas += 1
        End If
        If RapidatxtNombreItem5.Text <> "" Then
            nCantLineas += 1
        End If

        If RapidatxtNombreItem6.Text <> "" Then
            nCantLineas += 1
        End If
        If RapidatxtNombreItem7.Text <> "" Then
            nCantLineas += 1
        End If
        If RapidatxtNombreItem8.Text <> "" Then
            nCantLineas += 1
        End If
        If RapidatxtNombreItem9.Text <> "" Then
            nCantLineas += 1
        End If
        If RapidatxtNombreItem10.Text <> "" Then
            nCantLineas += 1
        End If

        Try
            'SUBDESCUENTO LINEA 1
            If CDec(RapidatxtSubDescuentoValor1.Text) > 0 Then
                SubDescuentoValor1 = Math.Round((CTDec(RapidatxtMontoItem1.Text) * CDec(RapidatxtSubDescuentoValor1.Text)) / 100, 2)
            End If
            RapidatxtMontoItem1.Text = CTDec(RapidatxtMontoItem1.Text) - SubDescuentoValor1

            'SUBDESCUENTO LINEA 2
            If CDec(RapidatxtSubDescuentoValor2.Text) > 0 Then
                SubDescuentoValor2 = Math.Round((CTDec(RapidatxtMontoItem2.Text) * CDec(RapidatxtSubDescuentoValor2.Text)) / 100, 2)
            End If
            RapidatxtMontoItem2.Text = CTDec(RapidatxtMontoItem2.Text) - SubDescuentoValor2

            'SUBDESCUENTO LINEA 3
            If CDec(RapidatxtSubDescuentoValor3.Text) > 0 Then
                SubDescuentoValor3 = Math.Round((CTDec(RapidatxtMontoItem3.Text) * CDec(RapidatxtSubDescuentoValor3.Text)) / 100, 2)
            End If
            RapidatxtMontoItem3.Text = CTDec(RapidatxtMontoItem3.Text) - SubDescuentoValor3

            'SUBDESCUENTO LINEA 4
            If CDec(RapidatxtSubDescuentoValor4.Text) > 0 Then
                SubDescuentoValor4 = Math.Round((CTDec(RapidatxtMontoItem4.Text) * CDec(RapidatxtSubDescuentoValor4.Text)) / 100, 2)
            End If
            RapidatxtMontoItem4.Text = CTDec(RapidatxtMontoItem4.Text) - SubDescuentoValor4

            'SUBDESCUENTO LINEA 5
            If CDec(RapidatxtSubDescuentoValor5.Text) > 0 Then
                SubDescuentoValor5 = Math.Round((CTDec(RapidatxtMontoItem5.Text) * CDec(RapidatxtSubDescuentoValor5.Text)) / 100, 2)
            End If
            RapidatxtMontoItem5.Text = CTDec(RapidatxtMontoItem5.Text) - SubDescuentoValor5

            'SUBDESCUENTO LINEA 6
            If CDec(RapidatxtSubDescuentoValor6.Text) > 0 Then
                SubDescuentoValor6 = Math.Round((CTDec(RapidatxtMontoItem6.Text) * CDec(RapidatxtSubDescuentoValor6.Text)) / 100, 2)
            End If
            RapidatxtMontoItem6.Text = CTDec(RapidatxtMontoItem6.Text) - SubDescuentoValor6

            'SUBDESCUENTO LINEA 7
            If CDec(RapidatxtSubDescuentoValor7.Text) > 0 Then
                SubDescuentoValor7 = Math.Round((CTDec(RapidatxtMontoItem7.Text) * CDec(RapidatxtSubDescuentoValor7.Text)) / 100, 2)
            End If
            RapidatxtMontoItem7.Text = CTDec(RapidatxtMontoItem7.Text) - SubDescuentoValor7

            'SUBDESCUENTO LINEA 8
            If CDec(RapidatxtSubDescuentoValor8.Text) > 0 Then
                SubDescuentoValor8 = Math.Round((CTDec(RapidatxtMontoItem8.Text) * CDec(RapidatxtSubDescuentoValor8.Text)) / 100, 2)
            End If
            RapidatxtMontoItem8.Text = CTDec(RapidatxtMontoItem8.Text) - SubDescuentoValor8

            'SUBDESCUENTO LINEA 9
            If CDec(RapidatxtSubDescuentoValor9.Text) > 0 Then
                SubDescuentoValor9 = Math.Round((CTDec(RapidatxtMontoItem9.Text) * CDec(RapidatxtSubDescuentoValor9.Text)) / 100, 2)
            End If
            RapidatxtMontoItem9.Text = CTDec(RapidatxtMontoItem9.Text) - SubDescuentoValor9

            'SUBDESCUENTO LINEA 10
            If CDec(RapidatxtSubDescuentoValor10.Text) > 0 Then
                SubDescuentoValor10 = Math.Round((CTDec(RapidatxtMontoItem10.Text) * CDec(RapidatxtSubDescuentoValor10.Text)) / 100, 2)
            End If
            RapidatxtMontoItem10.Text = CTDec(RapidatxtMontoItem10.Text) - SubDescuentoValor10

        Catch
        End Try

        If CTDec(RapidatxtPrecioUnitario1.Text) > 0 And CTDec(RapidatxtCantidad1.Text) > 0 Then
            'RapidatxtSubDescuentoValor1.Text = (SubDescuentoValor1 * 100) / (CTDec(RapidatxtCantidad1.Text) * CTDec(RapidatxtPrecioUnitario1.Text))
        End If
        If CTDec(RapidatxtPrecioUnitario2.Text) > 0 And CTDec(RapidatxtCantidad2.Text) > 0 Then
            'RapidatxtSubDescuentoValor2.Text = (SubDescuentoValor2 * 100) / (CTDec(RapidatxtCantidad2.Text) * CTDec(RapidatxtPrecioUnitario2.Text))
        End If
        If CTDec(RapidatxtPrecioUnitario3.Text) > 0 And CTDec(RapidatxtCantidad3.Text) > 0 Then
            'RapidatxtSubDescuentoValor3.Text = (SubDescuentoValor3 * 100) / (CTDec(RapidatxtCantidad3.Text) * CTDec(RapidatxtPrecioUnitario3.Text))
        End If
        If CTDec(RapidatxtPrecioUnitario4.Text) > 0 And CTDec(RapidatxtCantidad4.Text) > 0 Then
            'RapidatxtSubDescuentoValor4.Text = (SubDescuentoValor4 * 100) / (CTDec(RapidatxtCantidad4.Text) * CTDec(RapidatxtPrecioUnitario4.Text))
        End If
        If CTDec(RapidatxtPrecioUnitario5.Text) > 0 And CTDec(RapidatxtCantidad5.Text) > 0 Then
            'RapidatxtSubDescuentoValor5.Text = (SubDescuentoValor5 * 100) / (CTDec(RapidatxtCantidad5.Text) * CTDec(RapidatxtPrecioUnitario5.Text))
        End If

        'TOTAL MONTO NO GRAVADO        
        If RapidacmbIndicadorFacturacion1.SelectedValue = IndicadorDeFacturacionDetalle.ExentoIVA Or
            RapidacmbIndicadorFacturacion1.SelectedValue = IndicadorDeFacturacionDetalle.ItemVendidoContribuyenteIMEBA Or
            RapidacmbIndicadorFacturacion1.SelectedValue = IndicadorDeFacturacionDetalle.ItemVendidoContribuyenteMonotributo Or
            RapidacmbIndicadorFacturacion1.SelectedValue = IndicadorDeFacturacionDetalle.ItemVendidoPorNoContribuyente Then
            TotalMontoNoGravado += CTDec(RapidatxtMontoItem1.Text)
        End If

        If RapidacmbIndicadorFacturacion2.SelectedValue = IndicadorDeFacturacionDetalle.ExentoIVA Or
            RapidacmbIndicadorFacturacion2.SelectedValue = IndicadorDeFacturacionDetalle.ItemVendidoContribuyenteIMEBA Or
            RapidacmbIndicadorFacturacion2.SelectedValue = IndicadorDeFacturacionDetalle.ItemVendidoContribuyenteMonotributo Or
            RapidacmbIndicadorFacturacion2.SelectedValue = IndicadorDeFacturacionDetalle.ItemVendidoPorNoContribuyente Then
            TotalMontoNoGravado += CTDec(RapidatxtMontoItem2.Text)
        End If

        If RapidacmbIndicadorFacturacion3.SelectedValue = IndicadorDeFacturacionDetalle.ExentoIVA Or
            RapidacmbIndicadorFacturacion3.SelectedValue = IndicadorDeFacturacionDetalle.ItemVendidoContribuyenteIMEBA Or
            RapidacmbIndicadorFacturacion3.SelectedValue = IndicadorDeFacturacionDetalle.ItemVendidoContribuyenteMonotributo Or
            RapidacmbIndicadorFacturacion3.SelectedValue = IndicadorDeFacturacionDetalle.ItemVendidoPorNoContribuyente Then
            TotalMontoNoGravado += CTDec(RapidatxtMontoItem3.Text)
        End If

        If RapidacmbIndicadorFacturacion4.SelectedValue = IndicadorDeFacturacionDetalle.ExentoIVA Or
            RapidacmbIndicadorFacturacion4.SelectedValue = IndicadorDeFacturacionDetalle.ItemVendidoContribuyenteIMEBA Or
            RapidacmbIndicadorFacturacion4.SelectedValue = IndicadorDeFacturacionDetalle.ItemVendidoContribuyenteMonotributo Or
            RapidacmbIndicadorFacturacion4.SelectedValue = IndicadorDeFacturacionDetalle.ItemVendidoPorNoContribuyente Then
            TotalMontoNoGravado += CTDec(RapidatxtMontoItem4.Text)
        End If

        If RapidacmbIndicadorFacturacion5.SelectedValue = IndicadorDeFacturacionDetalle.ExentoIVA Or
            RapidacmbIndicadorFacturacion5.SelectedValue = IndicadorDeFacturacionDetalle.ItemVendidoContribuyenteIMEBA Or
            RapidacmbIndicadorFacturacion5.SelectedValue = IndicadorDeFacturacionDetalle.ItemVendidoContribuyenteMonotributo Or
            RapidacmbIndicadorFacturacion5.SelectedValue = IndicadorDeFacturacionDetalle.ItemVendidoPorNoContribuyente Then
            TotalMontoNoGravado += CTDec(RapidatxtMontoItem5.Text)
        End If

        If RapidacmbIndicadorFacturacion6.SelectedValue = IndicadorDeFacturacionDetalle.ExentoIVA Or
            RapidacmbIndicadorFacturacion6.SelectedValue = IndicadorDeFacturacionDetalle.ItemVendidoContribuyenteIMEBA Or
            RapidacmbIndicadorFacturacion6.SelectedValue = IndicadorDeFacturacionDetalle.ItemVendidoContribuyenteMonotributo Or
            RapidacmbIndicadorFacturacion7.SelectedValue = IndicadorDeFacturacionDetalle.ItemVendidoPorNoContribuyente Then
            TotalMontoNoGravado += CTDec(RapidatxtMontoItem6.Text)
        End If

        If RapidacmbIndicadorFacturacion7.SelectedValue = IndicadorDeFacturacionDetalle.ExentoIVA Or
            RapidacmbIndicadorFacturacion7.SelectedValue = IndicadorDeFacturacionDetalle.ItemVendidoContribuyenteIMEBA Or
            RapidacmbIndicadorFacturacion7.SelectedValue = IndicadorDeFacturacionDetalle.ItemVendidoContribuyenteMonotributo Or
            RapidacmbIndicadorFacturacion7.SelectedValue = IndicadorDeFacturacionDetalle.ItemVendidoPorNoContribuyente Then
            TotalMontoNoGravado += CTDec(RapidatxtMontoItem7.Text)
        End If

        If RapidacmbIndicadorFacturacion8.SelectedValue = IndicadorDeFacturacionDetalle.ExentoIVA Or
            RapidacmbIndicadorFacturacion8.SelectedValue = IndicadorDeFacturacionDetalle.ItemVendidoContribuyenteIMEBA Or
            RapidacmbIndicadorFacturacion8.SelectedValue = IndicadorDeFacturacionDetalle.ItemVendidoContribuyenteMonotributo Or
            RapidacmbIndicadorFacturacion8.SelectedValue = IndicadorDeFacturacionDetalle.ItemVendidoPorNoContribuyente Then
            TotalMontoNoGravado += CTDec(RapidatxtMontoItem8.Text)
        End If

        If RapidacmbIndicadorFacturacion9.SelectedValue = IndicadorDeFacturacionDetalle.ExentoIVA Or
            RapidacmbIndicadorFacturacion9.SelectedValue = IndicadorDeFacturacionDetalle.ItemVendidoContribuyenteIMEBA Or
            RapidacmbIndicadorFacturacion9.SelectedValue = IndicadorDeFacturacionDetalle.ItemVendidoContribuyenteMonotributo Or
            RapidacmbIndicadorFacturacion9.SelectedValue = IndicadorDeFacturacionDetalle.ItemVendidoPorNoContribuyente Then
            TotalMontoNoGravado += CTDec(RapidatxtMontoItem9.Text)
        End If

        If RapidacmbIndicadorFacturacion10.SelectedValue = IndicadorDeFacturacionDetalle.ExentoIVA Or
            RapidacmbIndicadorFacturacion10.SelectedValue = IndicadorDeFacturacionDetalle.ItemVendidoContribuyenteIMEBA Or
            RapidacmbIndicadorFacturacion10.SelectedValue = IndicadorDeFacturacionDetalle.ItemVendidoContribuyenteMonotributo Or
            RapidacmbIndicadorFacturacion10.SelectedValue = IndicadorDeFacturacionDetalle.ItemVendidoPorNoContribuyente Then
            TotalMontoNoGravado += CTDec(RapidatxtMontoItem10.Text)
        End If

        'PRIMERO APLICO DESCUENTO DE LINEA
        'SEGUNDO APLICO DESCUENTO GLOBAL
        'TERCERO APLICO IVA

        If RapidacmbIndicadorFacturacion1.SelectedValue = IndicadorDeFacturacionDetalle.TasaMinima Then
            TotalMontoNetoIVATasaMinima += CTDec(RapidatxtMontoItem1.Text)
        End If

        If RapidacmbIndicadorFacturacion2.SelectedValue = IndicadorDeFacturacionDetalle.TasaMinima Then
            TotalMontoNetoIVATasaMinima += CTDec(RapidatxtMontoItem2.Text)
        End If

        If RapidacmbIndicadorFacturacion3.SelectedValue = IndicadorDeFacturacionDetalle.TasaMinima Then
            TotalMontoNetoIVATasaMinima += CTDec(RapidatxtMontoItem3.Text)
        End If

        If RapidacmbIndicadorFacturacion4.SelectedValue = IndicadorDeFacturacionDetalle.TasaMinima Then
            TotalMontoNetoIVATasaMinima += CTDec(RapidatxtMontoItem4.Text)
        End If

        If RapidacmbIndicadorFacturacion5.SelectedValue = IndicadorDeFacturacionDetalle.TasaMinima Then
            TotalMontoNetoIVATasaMinima += CTDec(RapidatxtMontoItem5.Text)
        End If

        If RapidacmbIndicadorFacturacion6.SelectedValue = IndicadorDeFacturacionDetalle.TasaMinima Then
            TotalMontoNetoIVATasaMinima += CTDec(RapidatxtMontoItem6.Text)
        End If

        If RapidacmbIndicadorFacturacion7.SelectedValue = IndicadorDeFacturacionDetalle.TasaMinima Then
            TotalMontoNetoIVATasaMinima += CTDec(RapidatxtMontoItem7.Text)
        End If

        If RapidacmbIndicadorFacturacion8.SelectedValue = IndicadorDeFacturacionDetalle.TasaMinima Then
            TotalMontoNetoIVATasaMinima += CTDec(RapidatxtMontoItem8.Text)
        End If

        If RapidacmbIndicadorFacturacion9.SelectedValue = IndicadorDeFacturacionDetalle.TasaMinima Then
            TotalMontoNetoIVATasaMinima += CTDec(RapidatxtMontoItem9.Text)
        End If

        If RapidacmbIndicadorFacturacion10.SelectedValue = IndicadorDeFacturacionDetalle.TasaMinima Then
            TotalMontoNetoIVATasaMinima += CTDec(RapidatxtMontoItem10.Text)
        End If

        'Saco IVA si es monto bruto
        If CmbIndicadorMontoBrutoEncabezado.SelectedValue = 1 Then
            Dim AuxTotalMontoNetoIVATasaMinima As Decimal
            AuxTotalMontoNetoIVATasaMinima = TotalMontoNetoIVATasaMinima
            'TotalMontoNetoIVATasaMinima = TotalMontoNetoIVATasaMinima / 1 + IVATasaMinima
            'TotalMontoNetoIVATasaMinima = Math.Round(TotalMontoNetoIVATasaMinima / IVATasaMinima, 2)
            TotalMontoNetoIVATasaMinima = Math.Round(TotalMontoNetoIVATasaMinima / ((100 + IVATasaMinima) / 100), 2)
            TotalIVATasaMinima = Math.Round(AuxTotalMontoNetoIVATasaMinima - TotalMontoNetoIVATasaMinima, 2)
        Else
            TotalIVATasaMinima = Math.Round(TotalMontoNetoIVATasaMinima * IVATasaMinima / 100, 2)
        End If

        'El monto neto es antes de aplicar el IVA pero luego de aplicar descuentos y recargos
        RapidatxtTotalMontoNetoIVATasaMinima.Text = Math.Round(TotalMontoNetoIVATasaMinima, 2)

        'TOTAL MONTO NETO IVA TASA BASICA

        'PRIMERO APLICO DESCUENTO DE LINEA
        'SEGUNDO APLICO DESCUENTO GLOBAL
        'TERCERO APLICO IVA
        If RapidacmbIndicadorFacturacion1.SelectedValue = IndicadorDeFacturacionDetalle.TasaBasica Then
            TotalMontoNetoIVATasaBasica += CTDec(RapidatxtMontoItem1.Text)
        End If

        If RapidacmbIndicadorFacturacion2.SelectedValue = IndicadorDeFacturacionDetalle.TasaBasica Then
            TotalMontoNetoIVATasaBasica += CTDec(RapidatxtMontoItem2.Text)
        End If

        If RapidacmbIndicadorFacturacion3.SelectedValue = IndicadorDeFacturacionDetalle.TasaBasica Then
            TotalMontoNetoIVATasaBasica += CTDec(RapidatxtMontoItem3.Text)
        End If

        If RapidacmbIndicadorFacturacion4.SelectedValue = IndicadorDeFacturacionDetalle.TasaBasica Then
            TotalMontoNetoIVATasaBasica += CTDec(RapidatxtMontoItem4.Text)
        End If

        If RapidacmbIndicadorFacturacion5.SelectedValue = IndicadorDeFacturacionDetalle.TasaBasica Then
            TotalMontoNetoIVATasaBasica += CTDec(RapidatxtMontoItem5.Text)
        End If

        If RapidacmbIndicadorFacturacion6.SelectedValue = IndicadorDeFacturacionDetalle.TasaBasica Then
            TotalMontoNetoIVATasaBasica += CTDec(RapidatxtMontoItem6.Text)
        End If

        If RapidacmbIndicadorFacturacion7.SelectedValue = IndicadorDeFacturacionDetalle.TasaBasica Then
            TotalMontoNetoIVATasaBasica += CTDec(RapidatxtMontoItem7.Text)
        End If

        If RapidacmbIndicadorFacturacion8.SelectedValue = IndicadorDeFacturacionDetalle.TasaBasica Then
            TotalMontoNetoIVATasaBasica += CTDec(RapidatxtMontoItem8.Text)
        End If

        If RapidacmbIndicadorFacturacion9.SelectedValue = IndicadorDeFacturacionDetalle.TasaBasica Then
            TotalMontoNetoIVATasaBasica += CTDec(RapidatxtMontoItem9.Text)
        End If

        If RapidacmbIndicadorFacturacion10.SelectedValue = IndicadorDeFacturacionDetalle.TasaBasica Then
            TotalMontoNetoIVATasaBasica += CTDec(RapidatxtMontoItem10.Text)
        End If

        'TOTAL EXPORTACION Y ASIMILADAS 
        If RapidacmbIndicadorFacturacion1.SelectedValue = IndicadorDeFacturacionDetalle.ExportacionYAsimiladas Then
            TotalMontoExportacionAsimiladas += CTDec(RapidatxtMontoItem1.Text)
        End If

        If RapidacmbIndicadorFacturacion2.SelectedValue = IndicadorDeFacturacionDetalle.ExportacionYAsimiladas Then
            TotalMontoExportacionAsimiladas += CTDec(RapidatxtMontoItem2.Text)
        End If

        If RapidacmbIndicadorFacturacion3.SelectedValue = IndicadorDeFacturacionDetalle.ExportacionYAsimiladas Then
            TotalMontoExportacionAsimiladas += CTDec(RapidatxtMontoItem3.Text)
        End If

        If RapidacmbIndicadorFacturacion4.SelectedValue = IndicadorDeFacturacionDetalle.ExportacionYAsimiladas Then
            TotalMontoExportacionAsimiladas += CTDec(RapidatxtMontoItem4.Text)
        End If

        If RapidacmbIndicadorFacturacion5.SelectedValue = IndicadorDeFacturacionDetalle.ExportacionYAsimiladas Then
            TotalMontoExportacionAsimiladas += CTDec(RapidatxtMontoItem5.Text)
        End If

        If RapidacmbIndicadorFacturacion6.SelectedValue = IndicadorDeFacturacionDetalle.ExportacionYAsimiladas Then
            TotalMontoExportacionAsimiladas += CTDec(RapidatxtMontoItem6.Text)
        End If

        If RapidacmbIndicadorFacturacion7.SelectedValue = IndicadorDeFacturacionDetalle.ExportacionYAsimiladas Then
            TotalMontoExportacionAsimiladas += CTDec(RapidatxtMontoItem7.Text)
        End If

        If RapidacmbIndicadorFacturacion8.SelectedValue = IndicadorDeFacturacionDetalle.ExportacionYAsimiladas Then
            TotalMontoExportacionAsimiladas += CTDec(RapidatxtMontoItem8.Text)
        End If

        If RapidacmbIndicadorFacturacion9.SelectedValue = IndicadorDeFacturacionDetalle.ExportacionYAsimiladas Then
            TotalMontoExportacionAsimiladas += CTDec(RapidatxtMontoItem9.Text)
        End If

        If RapidacmbIndicadorFacturacion10.SelectedValue = IndicadorDeFacturacionDetalle.ExportacionYAsimiladas Then
            TotalMontoExportacionAsimiladas += CTDec(RapidatxtMontoItem10.Text)
        End If

        'Saco IVA si es monto bruto
        If CmbIndicadorMontoBrutoEncabezado.SelectedValue = 1 Then
            Dim AuxTotalMontoNetoIVATasaBasica As Decimal
            AuxTotalMontoNetoIVATasaBasica = TotalMontoNetoIVATasaBasica
            'TotalMontoNetoIVATasaMinima = TotalMontoNetoIVATasaMinima / 1 + IVATasaMinima
            'TotalMontoNetoIVATasaMinima = Math.Round(TotalMontoNetoIVATasaMinima / IVATasaMinima, 2)
            TotalMontoNetoIVATasaBasica = Math.Round(TotalMontoNetoIVATasaBasica / ((100 + IVATasaBasica) / 100), 2)
            TotalIVATasaBasica = Math.Round(AuxTotalMontoNetoIVATasaBasica - TotalMontoNetoIVATasaBasica, 2)
        Else
            TotalIVATasaBasica = Math.Round(TotalMontoNetoIVATasaBasica * IVATasaBasica / 100, 2)
        End If
        If Not IsNothing(objConfig) Then
            If objConfig.USARETENCIONPERCEPCION = 1 And Not Ckb_Exportacion.Checked Then

                TBx_MontoRetenidoPorLinea1.Text = (RetPerLinea1Valor1 + RetPerLinea1Valor2 + RetPerLinea1Valor3 + RetPerLinea1Valor4 + RetPerLinea1Valor5)
                TBx_MontoRetenidoPorLinea2.Text = (RetPerLinea2Valor1 + RetPerLinea2Valor2 + RetPerLinea2Valor3 + RetPerLinea2Valor4 + RetPerLinea2Valor5)
                TBx_MontoRetenidoPorLinea3.Text = (RetPerLinea3Valor1 + RetPerLinea3Valor2 + RetPerLinea3Valor3 + RetPerLinea3Valor4 + RetPerLinea3Valor5)
                TBx_MontoRetenidoPorLinea4.Text = (RetPerLinea4Valor1 + RetPerLinea4Valor2 + RetPerLinea4Valor3 + RetPerLinea4Valor4 + RetPerLinea4Valor5)
                TBx_MontoRetenidoPorLinea5.Text = (RetPerLinea5Valor1 + RetPerLinea5Valor2 + RetPerLinea5Valor3 + RetPerLinea5Valor4 + RetPerLinea5Valor5)
                TBx_MontoRetenidoPorLinea6.Text = (RetPerLinea6Valor1 + RetPerLinea6Valor2 + RetPerLinea6Valor3 + RetPerLinea6Valor4 + RetPerLinea6Valor5)
                TBx_MontoRetenidoPorLinea7.Text = (RetPerLinea7Valor1 + RetPerLinea7Valor2 + RetPerLinea7Valor3 + RetPerLinea7Valor4 + RetPerLinea7Valor5)
                TBx_MontoRetenidoPorLinea8.Text = (RetPerLinea8Valor1 + RetPerLinea8Valor2 + RetPerLinea8Valor3 + RetPerLinea8Valor4 + RetPerLinea8Valor5)
                TBx_MontoRetenidoPorLinea9.Text = (RetPerLinea9Valor1 + RetPerLinea9Valor2 + RetPerLinea9Valor3 + RetPerLinea9Valor4 + RetPerLinea9Valor5)
                TBx_MontoRetenidoPorLinea10.Text = (RetPerLinea10Valor1 + RetPerLinea10Valor2 + RetPerLinea10Valor3 + RetPerLinea10Valor4 + RetPerLinea10Valor5)
                TBx_MontoRetenido.Text = CTDec(TBx_MontoRetenidoPorLinea1.Text) + CTDec(TBx_MontoRetenidoPorLinea2.Text) + CTDec(TBx_MontoRetenidoPorLinea3.Text) + CTDec(TBx_MontoRetenidoPorLinea4.Text) + CTDec(TBx_MontoRetenidoPorLinea5.Text) + CTDec(TBx_MontoRetenidoPorLinea6.Text) + CTDec(TBx_MontoRetenidoPorLinea7.Text) + CTDec(TBx_MontoRetenidoPorLinea8.Text) + CTDec(TBx_MontoRetenidoPorLinea9.Text) + CTDec(TBx_MontoRetenidoPorLinea10.Text)
                CompletarListaRetenciones()
            Else
                LBx_RetencionesPercepciones.Items.Clear()
                TBx_MontoRetenidoPorLinea1.Text = 0
                TBx_MontoRetenidoPorLinea2.Text = 0
                TBx_MontoRetenidoPorLinea3.Text = 0
                TBx_MontoRetenidoPorLinea4.Text = 0
                TBx_MontoRetenidoPorLinea5.Text = 0
                TBx_MontoRetenidoPorLinea6.Text = 0
                TBx_MontoRetenidoPorLinea7.Text = 0
                TBx_MontoRetenidoPorLinea8.Text = 0
                TBx_MontoRetenidoPorLinea9.Text = 0
                TBx_MontoRetenidoPorLinea10.Text = 0
                TBx_MontoRetenido.Text = 0
            End If
        End If

        TBx_ExportacionYAsimilados.Text = TotalMontoExportacionAsimiladas

        'El monto neto es antes de aplicar el IVA pero luego de aplicar descuentos y recargos
        RapidatxtTotalMontoNetoIVATasaBasica.Text = Math.Round(TotalMontoNetoIVATasaBasica, 2)

        RapidatxtTotalIVATasaMinima.Text = TotalIVATasaMinima

        'TOTAL NO GRAVADO'
        RapidatxtTotalMontoNoGravado.Text = TotalMontoNoGravado

        'TOTAL IVA TASA BASICA
        RapidatxtTotalIVATasaBasica.Text = TotalIVATasaBasica

        'TOTAL MONTO TOTAL
        TotalMontoTotalTotalesEncabezado = Math.Round(TotalMontoExportacionAsimiladas + TotalMontoNetoIVATasaBasica + TotalIVATasaBasica + TotalIVATasaMinima + TotalMontoNetoIVATasaMinima + TotalMontoNoGravado, 2)

        'MONTO TOTAL A PAGAR, SE CALCULA DIFERENTE PARA eBOLETA  
        TipoCFEActual(TipoCFE, TipoCFEAux)
        If TipoCFE = TAFACE2ApiParsing.ITAParsing_v0206.enumTipoDeComprobanteCFE.eBoleta Or
                           TipoCFE = TAFACE2ApiParsing.ITAParsing_v0206.enumTipoDeComprobanteCFE.eBoletaContingencia Or
                           TipoCFE = TAFACE2ApiParsing.ITAParsing_v0206.enumTipoDeComprobanteCFE.eBoletaNotaCred Or
                           TipoCFE = TAFACE2ApiParsing.ITAParsing_v0206.enumTipoDeComprobanteCFE.eBoletaNotaCredContingencia Or
                           TipoCFE = TAFACE2ApiParsing.ITAParsing_v0206.enumTipoDeComprobanteCFE.eBoletaNotaDeb Or
                           TipoCFE = TAFACE2ApiParsing.ITAParsing_v0206.enumTipoDeComprobanteCFE.eBoletaNotaDebContingencia Then
            ActualizarTotal(TotalMontoTotalTotalesEncabezado - CTDec(TBx_MontoRetenido.Text) + CTDec(RapidaTxtMontoNoFacturable.Text))
        Else
            ActualizarTotal(TotalMontoTotalTotalesEncabezado + CTDec(RapidaTxtMontoNoFacturable.Text) + CTDec(TBx_MontoRetenido.Text))
        End If
        'VUELTO
        RapidatxtVuelto.Text = (CTDec(RapidatxtValorMediosDePago1.Text) + CTDec(RapidatxtValorMediosDePago2.Text)) - CTDec(RapidatxtMontoTotalPagar.Text)
    End Sub

    Private Sub ActualizarTotal(ByVal Total As String)
        Dim Fuente As Font
        If Total.Length > 9 Then
            If Total.Length > 12 Then
                If Total.Length > 15 Then
                    Fuente = New Font("Microsoft Sans Serif", 50 - Total.Length - 6)
                Else
                    Fuente = New Font("Microsoft Sans Serif", 50 - Total.Length - 4)
                End If
            Else
                Fuente = New Font("Microsoft Sans Serif", 50 - Total.Length)
            End If
        Else
            Fuente = New Font("Microsoft Sans Serif", 50)
        End If
        RapidatxtMontoTotalPagar.Font = Fuente
        RapidatxtMontoTotalPagar.Text = Total
    End Sub

    Private Sub chkImprimeApi_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkImprimeApi.CheckedChanged
        If chkImprimeApi.Checked Then
            cmbTipoImpresora.Enabled = True
            cmbImpresora.Enabled = True
        Else
            cmbTipoImpresora.Enabled = False
            cmbImpresora.Enabled = False
        End If
    End Sub

    Private Sub CargaImpWIN()
        Dim Impre As New Printing.PrinterSettings
        Dim Impres As Printing.PrinterSettings.StringCollection
        Dim Item, Sel As Integer

        With Me.cmbImpresora
            Me.cmbImpresora.Items.Clear()
            Sel = 0
            Impres = Printing.PrinterSettings.InstalledPrinters()
            For Item = 0 To Impres.Count - 1
                .Items.Add(Impres.Item(Item).ToString)
            Next
            If .Items.Count > 0 Then
                .SelectedIndex = Sel
            End If
        End With
    End Sub

    Private Sub CargarCombos()

        CargaImpWIN()

        Dim dTipoCFE As New Dictionary(Of Integer, String)
        dTipoCFE.Add(TipoDocumento.VentaContado, "Venta Contado")
        dTipoCFE.Add(TipoDocumento.VentaCredito, "Venta Credito")
        dTipoCFE.Add(TipoDocumento.NotaCredito, "Nota de Credito")
        dTipoCFE.Add(TipoDocumento.NotaDebito, "Nota de Debito")

        cmbTipoVenta.DataSource = New BindingSource(dTipoCFE, Nothing)
        cmbTipoVenta.DisplayMember = "Value"
        cmbTipoVenta.ValueMember = "Key"

        Dim dFormaPago As New Dictionary(Of Integer, String)
        dFormaPago.Add(1, "Contado")
        dFormaPago.Add(2, "Credito")

        cmbFormaPago.DataSource = New BindingSource(dFormaPago, Nothing)
        cmbFormaPago.DisplayMember = "Value"
        cmbFormaPago.ValueMember = "Key"
        cmbFormaPago.SelectedIndex = 0

        Dim dTipoCFERef As New Dictionary(Of Integer, String)
        dTipoCFERef.Add(TipoCFE.eTicket, "e-Ticket")
        dTipoCFERef.Add(TipoCFE.eFactura, "e-Factura")

        cmbTipoCFEInformacionReferencia.DataSource = New BindingSource(dTipoCFERef, Nothing)
        cmbTipoCFEInformacionReferencia.DisplayMember = "Value"
        cmbTipoCFEInformacionReferencia.ValueMember = "Key"

        Dim dRapidaIndicadorFacturacion As New Dictionary(Of Integer, String)
        dRapidaIndicadorFacturacion.Add(IndicadorDeFacturacionDetalle.TasaBasica, "22%")
        dRapidaIndicadorFacturacion.Add(IndicadorDeFacturacionDetalle.TasaMinima, "10%")
        dRapidaIndicadorFacturacion.Add(IndicadorDeFacturacionDetalle.ExentoIVA, "0%")
        dRapidaIndicadorFacturacion.Add(IndicadorDeFacturacionDetalle.ExportacionYAsimiladas, "Exp-Asim")
        dRapidaIndicadorFacturacion.Add(IndicadorDeFacturacionDetalle.ItemVendidoPorNoContribuyente, "No contrib")
        dRapidaIndicadorFacturacion.Add(IndicadorDeFacturacionDetalle.ItemVendidoContribuyenteMonotributo, "Monotrib")
        dRapidaIndicadorFacturacion.Add(IndicadorDeFacturacionDetalle.ItemVendidoContribuyenteIMEBA, "IMEBA")

        Dim dModVenta As New Dictionary(Of String, String)
        dModVenta.Add(TAFACE2ApiParsing.ITAParsing_v0206.enumModalidadDeVenta.SinDefinir, "SinDefinir")
        dModVenta.Add(TAFACE2ApiParsing.ITAParsing_v0206.enumModalidadDeVenta.BienesPropiosAExclavesAduaneros, "BienesPropiosAExclavesAduaneros")
        dModVenta.Add(TAFACE2ApiParsing.ITAParsing_v0206.enumModalidadDeVenta.Consignacion, "Consignacion")
        dModVenta.Add(TAFACE2ApiParsing.ITAParsing_v0206.enumModalidadDeVenta.OtrasTransacciones, "OtrasTransacciones")
        dModVenta.Add(TAFACE2ApiParsing.ITAParsing_v0206.enumModalidadDeVenta.PrecioReservable, "PrecioReservable")
        dModVenta.Add(TAFACE2ApiParsing.ITAParsing_v0206.enumModalidadDeVenta.RegimenGeneral, "RegimenGeneral")
        dModVenta.Add(TAFACE2ApiParsing.ITAParsing_v0206.enumModalidadDeVenta.RegimenGeneralEsportacionDeServ, "RegimenGeneralExportacionDeServ")
        CBx_ModalidadVenta.DataSource = New BindingSource(dModVenta, Nothing)
        CBx_ModalidadVenta.DisplayMember = "Value"
        CBx_ModalidadVenta.ValueMember = "Key"

        Dim dTipoTransp As New Dictionary(Of String, String)
        dTipoTransp.Add(TAFACE2ApiParsing.ITAParsing_v0206.enumViaDeTransporte.SinDefinir, "SinDefinir")
        dTipoTransp.Add(TAFACE2ApiParsing.ITAParsing_v0206.enumViaDeTransporte.Aereo, "Aereo")
        dTipoTransp.Add(TAFACE2ApiParsing.ITAParsing_v0206.enumViaDeTransporte.Maritimo, "Maritimo")
        dTipoTransp.Add(TAFACE2ApiParsing.ITAParsing_v0206.enumViaDeTransporte.Terrestre, "Terrestre")
        dTipoTransp.Add(TAFACE2ApiParsing.ITAParsing_v0206.enumViaDeTransporte.NA, "No Aplica")
        dTipoTransp.Add(TAFACE2ApiParsing.ITAParsing_v0206.enumViaDeTransporte.Otro, "Otro")
        CBx_ViaTransporte.DataSource = New BindingSource(dTipoTransp, Nothing)
        CBx_ViaTransporte.DisplayMember = "Value"
        CBx_ViaTransporte.ValueMember = "Key"

        RapidacmbIndicadorFacturacion1.DataSource = New BindingSource(dRapidaIndicadorFacturacion, Nothing)
        RapidacmbIndicadorFacturacion1.DisplayMember = "Value"
        RapidacmbIndicadorFacturacion1.ValueMember = "Key"

        RapidacmbIndicadorFacturacion2.DataSource = New BindingSource(dRapidaIndicadorFacturacion, Nothing)
        RapidacmbIndicadorFacturacion2.DisplayMember = "Value"
        RapidacmbIndicadorFacturacion2.ValueMember = "Key"

        RapidacmbIndicadorFacturacion3.DataSource = New BindingSource(dRapidaIndicadorFacturacion, Nothing)
        RapidacmbIndicadorFacturacion3.DisplayMember = "Value"
        RapidacmbIndicadorFacturacion3.ValueMember = "Key"

        RapidacmbIndicadorFacturacion4.DataSource = New BindingSource(dRapidaIndicadorFacturacion, Nothing)
        RapidacmbIndicadorFacturacion4.DisplayMember = "Value"
        RapidacmbIndicadorFacturacion4.ValueMember = "Key"

        RapidacmbIndicadorFacturacion5.DataSource = New BindingSource(dRapidaIndicadorFacturacion, Nothing)
        RapidacmbIndicadorFacturacion5.DisplayMember = "Value"
        RapidacmbIndicadorFacturacion5.ValueMember = "Key"

        RapidacmbIndicadorFacturacion6.DataSource = New BindingSource(dRapidaIndicadorFacturacion, Nothing)
        RapidacmbIndicadorFacturacion6.DisplayMember = "Value"
        RapidacmbIndicadorFacturacion6.ValueMember = "Key"

        RapidacmbIndicadorFacturacion7.DataSource = New BindingSource(dRapidaIndicadorFacturacion, Nothing)
        RapidacmbIndicadorFacturacion7.DisplayMember = "Value"
        RapidacmbIndicadorFacturacion7.ValueMember = "Key"

        RapidacmbIndicadorFacturacion8.DataSource = New BindingSource(dRapidaIndicadorFacturacion, Nothing)
        RapidacmbIndicadorFacturacion8.DisplayMember = "Value"
        RapidacmbIndicadorFacturacion8.ValueMember = "Key"

        RapidacmbIndicadorFacturacion9.DataSource = New BindingSource(dRapidaIndicadorFacturacion, Nothing)
        RapidacmbIndicadorFacturacion9.DisplayMember = "Value"
        RapidacmbIndicadorFacturacion9.ValueMember = "Key"

        RapidacmbIndicadorFacturacion10.DataSource = New BindingSource(dRapidaIndicadorFacturacion, Nothing)
        RapidacmbIndicadorFacturacion10.DisplayMember = "Value"
        RapidacmbIndicadorFacturacion10.ValueMember = "Key"

        cmbCfgIndicadorFacturacion.DataSource = New BindingSource(dRapidaIndicadorFacturacion, Nothing)
        cmbCfgIndicadorFacturacion.DisplayMember = "Value"
        cmbCfgIndicadorFacturacion.ValueMember = "Key"

        Dim dTipoDocumentos As New Dictionary(Of Integer, String)
        dTipoDocumentos.Add(TipoDocumentoDelReceptor.SinDefinir, "Sin Definir")
        dTipoDocumentos.Add(TipoDocumentoDelReceptor.RUC, "RUT")
        dTipoDocumentos.Add(TipoDocumentoDelReceptor.CI, "C.I")
        dTipoDocumentos.Add(TipoDocumentoDelReceptor.Otros, "Otros")
        dTipoDocumentos.Add(TipoDocumentoDelReceptor.Pasaporte, "Pasaporte")
        dTipoDocumentos.Add(TipoDocumentoDelReceptor.DNI, "DNI")
        dTipoDocumentos.Add(TipoDocumentoDelReceptor.NIFE, "NIFE")

        RapidacmbTipoDocumentoReceptor.DataSource = New BindingSource(dTipoDocumentos, Nothing)
        RapidacmbTipoDocumentoReceptor.DisplayMember = "Value"
        RapidacmbTipoDocumentoReceptor.ValueMember = "Key"
        RapidacmbTipoDocumentoReceptor.SelectedIndex = 0

        Dim dPaises As New Dictionary(Of String, String)
        dPaises.Add("AR", "Argentina")
        dPaises.Add("BO", "Bolivia")
        dPaises.Add("BR", "Brasil")
        dPaises.Add("CL", "Chile")
        dPaises.Add("CO", "Colombia")
        dPaises.Add("EC", "Ecuador")
        dPaises.Add("PY", "Paraguay")
        dPaises.Add("UE", "Union Europea")
        dPaises.Add("US", "EE.UU.")
        dPaises.Add("UY", "Uruguay")
        dPaises.Add("VE", "Venezuela")

        RapidacmbCodigoPaisReceptor.DataSource = New BindingSource(dPaises, Nothing)
        RapidacmbCodigoPaisReceptor.DisplayMember = "Value"
        RapidacmbCodigoPaisReceptor.ValueMember = "Key"
        RapidacmbCodigoPaisReceptor.SelectedIndex = 9

        Dim dTipoImpresora As New Dictionary(Of Integer, String)
        dTipoImpresora.Add(1, "EpsonTicket")
        dTipoImpresora.Add(2, "CitizenTicket")
        dTipoImpresora.Add(3, "LaserA4")
        dTipoImpresora.Add(5, "StarMicronics")
        dTipoImpresora.Add(6, "Rongta RP80")
        dTipoImpresora.Add(7, "Bematech MP-4200 TH")
        dTipoImpresora.Add(8, "Bematech LR2000")
        cmbTipoImpresora.DataSource = New BindingSource(dTipoImpresora, Nothing)
        cmbTipoImpresora.DisplayMember = "Value"
        cmbTipoImpresora.ValueMember = "Key"
        cmbTipoImpresora.SelectedIndex = 0

        Dim dAgenteResponsable As New Dictionary(Of String, String)
        dAgenteResponsable.Add("1", " ")
        dAgenteResponsable.Add("2", "A")
        dAgenteResponsable.Add("3", "R")

        CBx_AgenteResponsable1.DataSource = New BindingSource(dAgenteResponsable, Nothing)
        CBx_AgenteResponsable1.DisplayMember = "Value"
        CBx_AgenteResponsable1.ValueMember = "Key"
        CBx_AgenteResponsable1.SelectedIndex = 0
        CBx_AgenteResponsable2.DataSource = New BindingSource(dAgenteResponsable, Nothing)
        CBx_AgenteResponsable2.DisplayMember = "Value"
        CBx_AgenteResponsable2.ValueMember = "Key"
        CBx_AgenteResponsable2.SelectedIndex = 0
        CBx_AgenteResponsable3.DataSource = New BindingSource(dAgenteResponsable, Nothing)
        CBx_AgenteResponsable3.DisplayMember = "Value"
        CBx_AgenteResponsable3.ValueMember = "Key"
        CBx_AgenteResponsable3.SelectedIndex = 0
        CBx_AgenteResponsable4.DataSource = New BindingSource(dAgenteResponsable, Nothing)
        CBx_AgenteResponsable4.DisplayMember = "Value"
        CBx_AgenteResponsable4.ValueMember = "Key"
        CBx_AgenteResponsable4.SelectedIndex = 0
        CBx_AgenteResponsable5.DataSource = New BindingSource(dAgenteResponsable, Nothing)
        CBx_AgenteResponsable5.DisplayMember = "Value"
        CBx_AgenteResponsable5.ValueMember = "Key"
        CBx_AgenteResponsable5.SelectedIndex = 0
        CBx_AgenteResponsable6.DataSource = New BindingSource(dAgenteResponsable, Nothing)
        CBx_AgenteResponsable6.DisplayMember = "Value"
        CBx_AgenteResponsable6.ValueMember = "Key"
        CBx_AgenteResponsable6.SelectedIndex = 0
        CBx_AgenteResponsable7.DataSource = New BindingSource(dAgenteResponsable, Nothing)
        CBx_AgenteResponsable7.DisplayMember = "Value"
        CBx_AgenteResponsable7.ValueMember = "Key"
        CBx_AgenteResponsable7.SelectedIndex = 0
        CBx_AgenteResponsable8.DataSource = New BindingSource(dAgenteResponsable, Nothing)
        CBx_AgenteResponsable8.DisplayMember = "Value"
        CBx_AgenteResponsable8.ValueMember = "Key"
        CBx_AgenteResponsable8.SelectedIndex = 0
        CBx_AgenteResponsable9.DataSource = New BindingSource(dAgenteResponsable, Nothing)
        CBx_AgenteResponsable9.DisplayMember = "Value"
        CBx_AgenteResponsable9.ValueMember = "Key"
        CBx_AgenteResponsable9.SelectedIndex = 0
        CBx_AgenteResponsable10.DataSource = New BindingSource(dAgenteResponsable, Nothing)
        CBx_AgenteResponsable10.DisplayMember = "Value"
        CBx_AgenteResponsable10.ValueMember = "Key"
        CBx_AgenteResponsable10.SelectedIndex = 0

        'AGREGAR LA FIRMA DE eREMITOS        
        Dim dTipoTrasladoBienes As New Dictionary(Of Integer, String)
        dTipoTrasladoBienes.Add(0, "SinDefinir")
        dTipoTrasladoBienes.Add(1, "Venta")
        dTipoTrasladoBienes.Add(2, "Traslados Internos")
        cmbIndicadorTipoTrasladoBienesEncabezado.DataSource = New BindingSource(dTipoTrasladoBienes, Nothing)
        cmbIndicadorTipoTrasladoBienesEncabezado.DisplayMember = "Value"
        cmbIndicadorTipoTrasladoBienesEncabezado.ValueMember = "Key"

        Dim dPropiedadMercaderiaTransp As New Dictionary(Of Integer, String)
        dPropiedadMercaderiaTransp.Add(0, "SinDefinir")
        dPropiedadMercaderiaTransp.Add(1, "MercaderiaTerceros")
        cmbIndicadorPropMercaderiaTransp.DataSource = New BindingSource(dPropiedadMercaderiaTransp, Nothing)
        cmbIndicadorPropMercaderiaTransp.DisplayMember = "Value"
        cmbIndicadorPropMercaderiaTransp.ValueMember = "Key"

        cmbTipoDocMercaderiaTransp.DataSource = New BindingSource(dTipoDocumentos, Nothing)
        cmbTipoDocMercaderiaTransp.DisplayMember = "Value"
        cmbTipoDocMercaderiaTransp.ValueMember = "Key"
        cmbTipoDocMercaderiaTransp.SelectedIndex = 0

        Dim dPaisesERemito As New Dictionary(Of String, String)
        dPaisesERemito.Add("AF", "Afganistan")
        dPaisesERemito.Add("AX", "Aland")
        dPaisesERemito.Add("AL", "Albania")
        dPaisesERemito.Add("DE", "Alemania")
        dPaisesERemito.Add("AD", "Andorra")
        dPaisesERemito.Add("AO", "Angola")
        dPaisesERemito.Add("AI", "Anguila")
        dPaisesERemito.Add("AQ", "Antartida")
        dPaisesERemito.Add("AG", "Antigua y Barbuda")
        dPaisesERemito.Add("SA", "Arabia Saudita")
        dPaisesERemito.Add("DZ", "Argelia")
        dPaisesERemito.Add("AR", "Argentina")
        dPaisesERemito.Add("AM", "Armenia")
        dPaisesERemito.Add("AW", "Aruba")
        dPaisesERemito.Add("AU", "Australia")
        dPaisesERemito.Add("AT", "Austria")
        dPaisesERemito.Add("AZ", "Azerbaiyan")
        dPaisesERemito.Add("BS", "Bahamas")
        dPaisesERemito.Add("BD", "Banglades")
        dPaisesERemito.Add("BB", "Barbados")
        dPaisesERemito.Add("BH", "Barein")
        dPaisesERemito.Add("BE", "Belgica")
        dPaisesERemito.Add("BZ", "Belice")
        dPaisesERemito.Add("BJ", "Benin")
        dPaisesERemito.Add("BM", "Bermudas")
        dPaisesERemito.Add("BY", "Bielorrusia")
        dPaisesERemito.Add("BO", "Bolivia")
        dPaisesERemito.Add("BQ", "Bonaire San Eustaquio y Saba")
        dPaisesERemito.Add("BA", "Bosnia y Herzegovina")
        dPaisesERemito.Add("BW", "Botsuana")
        dPaisesERemito.Add("BR", "Brasil")
        dPaisesERemito.Add("BN", "Brunei")
        dPaisesERemito.Add("BG", "Bulgaria")
        dPaisesERemito.Add("BF", "Burkina Faso")
        dPaisesERemito.Add("BI", "Burundi")
        dPaisesERemito.Add("BT", "Butan")
        dPaisesERemito.Add("CV", "Cabo Verde")
        dPaisesERemito.Add("KH", "Camboya")
        dPaisesERemito.Add("CM", "Camerun")
        dPaisesERemito.Add("CA", "Canada")
        dPaisesERemito.Add("QA", "Catar")
        dPaisesERemito.Add("TD", "Chad")
        dPaisesERemito.Add("CL", "Chile")
        dPaisesERemito.Add("CN", "China")
        dPaisesERemito.Add("CY", "Chipre")
        dPaisesERemito.Add("CO", "Colombia")
        dPaisesERemito.Add("KM", "Comoras")
        dPaisesERemito.Add("KP", "Corea del Norte")
        dPaisesERemito.Add("KR", "Corea del Sur")
        dPaisesERemito.Add("CI", "Costa de Marfil")
        dPaisesERemito.Add("CR", "Costa Rica")
        dPaisesERemito.Add("HR", "Croacia")
        dPaisesERemito.Add("CU", "Cuba")
        dPaisesERemito.Add("CW", "Curazao")
        dPaisesERemito.Add("DK", "Dinamarca")
        dPaisesERemito.Add("DM", "Dominica")
        dPaisesERemito.Add("EC", "Ecuador")
        dPaisesERemito.Add("EG", "Egipto")
        dPaisesERemito.Add("SV", "El Salvador")
        dPaisesERemito.Add("AE", "Emiratos Arabes Unidos")
        dPaisesERemito.Add("ER", "Eritrea")
        dPaisesERemito.Add("SK", "Eslovaquia")
        dPaisesERemito.Add("SI", "Eslovenia")
        dPaisesERemito.Add("ES", "Espana")
        dPaisesERemito.Add("US", "Estados Unidos")
        dPaisesERemito.Add("EE", "Estonia")
        dPaisesERemito.Add("ET", "Etiopia")
        dPaisesERemito.Add("PH", "Filipinas")
        dPaisesERemito.Add("FI", "Finlandia")
        dPaisesERemito.Add("FJ", "Fiyi")
        dPaisesERemito.Add("FR", "Francia")
        dPaisesERemito.Add("GA", "Gabon")
        dPaisesERemito.Add("GM", "Gambia")
        dPaisesERemito.Add("GE", "Georgia")
        dPaisesERemito.Add("GH", "Ghana")
        dPaisesERemito.Add("GI", "Gibraltar")
        dPaisesERemito.Add("GD", "Granada")
        dPaisesERemito.Add("GR", "Grecia")
        dPaisesERemito.Add("GL", "Groenlandia")
        dPaisesERemito.Add("GP", "Guadalupe")
        dPaisesERemito.Add("GU", "Guam")
        dPaisesERemito.Add("GT", "Guatemala")
        dPaisesERemito.Add("GF", "Guayana Francesa")
        dPaisesERemito.Add("GG", "Guernsey")
        dPaisesERemito.Add("GN", "Guinea")
        dPaisesERemito.Add("GW", "Guinea Bisau")
        dPaisesERemito.Add("GQ", "Guinea Ecuatorial")
        dPaisesERemito.Add("GY", "Guyana")
        dPaisesERemito.Add("HT", "Haiti")
        dPaisesERemito.Add("HN", "Honduras")
        dPaisesERemito.Add("HK", "Hong Kong")
        dPaisesERemito.Add("HU", "Hungria")
        dPaisesERemito.Add("[IN]", "India")
        dPaisesERemito.Add("ID", "Indonesia")
        dPaisesERemito.Add("IQ", "Irak")
        dPaisesERemito.Add("IR", "Iran")
        dPaisesERemito.Add("IE", "Irlanda")
        dPaisesERemito.Add("BV", "Isla Bouvet")
        dPaisesERemito.Add("IM", "Isla de Man")
        dPaisesERemito.Add("CX", "Isla de Navidad")
        dPaisesERemito.Add("[IS]", "Islandia")
        dPaisesERemito.Add("KY", "Islas Caiman")
        dPaisesERemito.Add("CC", "Islas Cocos")
        dPaisesERemito.Add("CK", "Islas Cook")
        dPaisesERemito.Add("FO", "Islas Feroe")
        dPaisesERemito.Add("GS", "Islas Georgias del Sur y Sandwich del Sur")
        dPaisesERemito.Add("HM", "Islas Heard y McDonald")
        dPaisesERemito.Add("FK", "Islas Malvinas")
        dPaisesERemito.Add("MP", "Islas Marianas del Norte")
        dPaisesERemito.Add("MH", "Islas Marshall")
        dPaisesERemito.Add("PN", "Islas Pitcairn")
        dPaisesERemito.Add("SB", "Islas Salomon")
        dPaisesERemito.Add("TC", "Islas Turcas y Caicos")
        dPaisesERemito.Add("UM", "Islas Ultramarinas de Estados Unidos")
        dPaisesERemito.Add("VG", "Islas Virgenes Britanicas")
        dPaisesERemito.Add("VI", "Islas Virgenes de los Estados Unidos")
        dPaisesERemito.Add("IL", "Israel")
        dPaisesERemito.Add("IT", "Italia")
        dPaisesERemito.Add("JM", "Jamaica")
        dPaisesERemito.Add("JP", "Japon")
        dPaisesERemito.Add("JE", "Jersey")
        dPaisesERemito.Add("JO", "Jordania")
        dPaisesERemito.Add("KZ", "Kazajistan")
        dPaisesERemito.Add("KE", "Kenia")
        dPaisesERemito.Add("KG", "Kirguistan")
        dPaisesERemito.Add("KI", "Kiribati")
        dPaisesERemito.Add("KW", "Kuwait")
        dPaisesERemito.Add("LA", "Laos")
        dPaisesERemito.Add("LS", "Lesoto")
        dPaisesERemito.Add("LV", "Letonia")
        dPaisesERemito.Add("LB", "Libano")
        dPaisesERemito.Add("LR", "Liberia")
        dPaisesERemito.Add("LY", "Libia")
        dPaisesERemito.Add("LI", "Liechtenstein")
        dPaisesERemito.Add("LT", "Lituania")
        dPaisesERemito.Add("LU", "Luxemburgo")
        dPaisesERemito.Add("MO", "Macao")
        dPaisesERemito.Add("MK", "Macedonia")
        dPaisesERemito.Add("MG", "Madagascar")
        dPaisesERemito.Add("MY", "Malasia")
        dPaisesERemito.Add("MW", "Malaui")
        dPaisesERemito.Add("MV", "Maldivas")
        dPaisesERemito.Add("ML", "Mali")
        dPaisesERemito.Add("MT", "Malta")
        dPaisesERemito.Add("MA", "Marruecos")
        dPaisesERemito.Add("MQ", "Martinica")
        dPaisesERemito.Add("MU", "Mauricio")
        dPaisesERemito.Add("MR", "Mauritania")
        dPaisesERemito.Add("YT", "Mayotte")
        dPaisesERemito.Add("MX", "Mexico")
        dPaisesERemito.Add("FM", "Micronesia")
        dPaisesERemito.Add("MD", "Moldavia")
        dPaisesERemito.Add("MC", "Monaco")
        dPaisesERemito.Add("MN", "Mongolia")
        dPaisesERemito.Add("[ME]", "Montenegro")
        dPaisesERemito.Add("MS", "Montserrat")
        dPaisesERemito.Add("MZ", "Mozambique")
        dPaisesERemito.Add("MM", "Myanmar")
        dPaisesERemito.Add("NA", "Namibia")
        dPaisesERemito.Add("NR", "Nauru")
        dPaisesERemito.Add("NP", "Nepal")
        dPaisesERemito.Add("NI", "Nicaragua")
        dPaisesERemito.Add("NE", "Niger")
        dPaisesERemito.Add("NG", "Nigeria")
        dPaisesERemito.Add("NU", "Niue")
        dPaisesERemito.Add("NF", "Norfolk")
        dPaisesERemito.Add("NO", "Noruega")
        dPaisesERemito.Add("NC", "Nueva Caledonia")
        dPaisesERemito.Add("NZ", "Nueva Zelanda")
        dPaisesERemito.Add("OM", "Oman")
        dPaisesERemito.Add("NL", "Paises Bajos")
        dPaisesERemito.Add("PK", "Pakistan")
        dPaisesERemito.Add("PW", "Palaos")
        dPaisesERemito.Add("PS", "Palestina")
        dPaisesERemito.Add("PA", "Panama")
        dPaisesERemito.Add("PG", "Papua Nueva Guinea")
        dPaisesERemito.Add("PY", "Paraguay")
        dPaisesERemito.Add("PE", "Peru")
        dPaisesERemito.Add("PF", "Polinesia Francesa")
        dPaisesERemito.Add("PL", "Polonia")
        dPaisesERemito.Add("PT", "Portugal")
        dPaisesERemito.Add("PR", "Puerto Rico")
        dPaisesERemito.Add("GB", "Reino Unido")
        dPaisesERemito.Add("EH", "Republica Arabe Saharaui Democratica")
        dPaisesERemito.Add("CF", "Republica Centroafricana")
        dPaisesERemito.Add("CZ", "Republica Checa")
        dPaisesERemito.Add("CG", "Republica del Congo")
        dPaisesERemito.Add("CD", "Republica Democratica del Congo")
        dPaisesERemito.Add("[DO]", "Republica Dominicana")
        dPaisesERemito.Add("RE", "Reunion")
        dPaisesERemito.Add("RW", "Ruanda")
        dPaisesERemito.Add("RO", "Rumania")
        dPaisesERemito.Add("RU", "Rusia")
        dPaisesERemito.Add("WS", "Samoa")
        dPaisesERemito.Add("[AS]", "Samoa Americana")
        dPaisesERemito.Add("BL", "San Bartolome")
        dPaisesERemito.Add("KN", "San Cristobal y Nieves")
        dPaisesERemito.Add("SM", "San Marino")
        dPaisesERemito.Add("MF", "San Martin")
        dPaisesERemito.Add("PM", "San Pedro y Miquelon")
        dPaisesERemito.Add("VC", "San Vicente y las Granadinas")
        dPaisesERemito.Add("SH", "Santa Elena Ascension y Tristan de Acuna")
        dPaisesERemito.Add("LC", "Santa Lucia")
        dPaisesERemito.Add("ST", "Santo Tome y Principe")
        dPaisesERemito.Add("SN", "Senegal")
        dPaisesERemito.Add("RS", "Serbia")
        dPaisesERemito.Add("SC", "Seychelles")
        dPaisesERemito.Add("SL", "Sierra Leona")
        dPaisesERemito.Add("SG", "Singapur")
        dPaisesERemito.Add("SX", "Sint Maarten")
        dPaisesERemito.Add("SY", "Siria")
        dPaisesERemito.Add("SO", "Somalia")
        dPaisesERemito.Add("LK", "Sri Lanka")
        dPaisesERemito.Add("SZ", "Suazilandia")
        dPaisesERemito.Add("ZA", "Sudafrica")
        dPaisesERemito.Add("SD", "Sudan")
        dPaisesERemito.Add("SS", "Sudan del Sur")
        dPaisesERemito.Add("SE", "Suecia")
        dPaisesERemito.Add("CH", "Suiza")
        dPaisesERemito.Add("SR", "Surinam")
        dPaisesERemito.Add("SJ", "Svalbard y Jan Mayen")
        dPaisesERemito.Add("TH", "Tailandia")
        dPaisesERemito.Add("TW", "Taiwan Republica de China")
        dPaisesERemito.Add("TZ", "Tanzania")
        dPaisesERemito.Add("TJ", "Tayikistan")
        dPaisesERemito.Add("IO", "Territorio Britanico del Oceano Indico")
        dPaisesERemito.Add("TF", "Tierras Australes y Antarticas Francesas")
        dPaisesERemito.Add("TL", "Timor Oriental")
        dPaisesERemito.Add("TG", "Togo")
        dPaisesERemito.Add("TK", "Tokelau")
        dPaisesERemito.Add("[TO]", "Tonga")
        dPaisesERemito.Add("TT", "Trinidad y Tobago")
        dPaisesERemito.Add("TN", "Tunez")
        dPaisesERemito.Add("TM", "Turkmenistan")
        dPaisesERemito.Add("TR", "Turquia")
        dPaisesERemito.Add("TV", "Tuvalu")
        dPaisesERemito.Add("UA", "Ucrania")
        dPaisesERemito.Add("UG", "Uganda")
        dPaisesERemito.Add("UY", "Uruguay")
        dPaisesERemito.Add("UZ", "Uzbekistan")
        dPaisesERemito.Add("VU", "Vanuatu")
        dPaisesERemito.Add("VA", "Ciudad del Vaticano")
        dPaisesERemito.Add("VE", "Venezuela")
        dPaisesERemito.Add("VN", "Vietnam")
        dPaisesERemito.Add("WF", "Wallis y Futuna")
        dPaisesERemito.Add("YE", "Yemen")
        dPaisesERemito.Add("DJ", "Yibuti")
        dPaisesERemito.Add("ZM", "Zambia")
        dPaisesERemito.Add("ZW", "Zimbabue")
        dPaisesERemito.Add("99", "NoExisteCodigo")

        cmbCodPaisPropMercaderiaTransp.DataSource = New BindingSource(dPaisesERemito, Nothing)
        cmbCodPaisPropMercaderiaTransp.DisplayMember = "Value"
        cmbCodPaisPropMercaderiaTransp.ValueMember = "Key"
        cmbCodPaisPropMercaderiaTransp.SelectedIndex = 238

        Dim dIndicadorMontosBrutos As New Dictionary(Of Integer, String)
        dIndicadorMontosBrutos.Add(0, "NoIncluyenIVA")
        dIndicadorMontosBrutos.Add(1, "IVAIncluido")
        dIndicadorMontosBrutos.Add(2, "IMEBAyAdicionalesIncluido")
        CmbIndicadorMontoBrutoEncabezado.DataSource = New BindingSource(dIndicadorMontosBrutos, Nothing)
        CmbIndicadorMontoBrutoEncabezado.DisplayMember = "Value"
        CmbIndicadorMontoBrutoEncabezado.ValueMember = "Key"
        CmbIndicadorMontoBrutoEncabezado.SelectedIndex = 1

        Dim dIdProcesador As New Dictionary(Of Integer, String)
        dIdProcesador.Add(0, "Todos")
        dIdProcesador.Add(1, "FIRSTDATA")
        dIdProcesador.Add(2, "VISANET")
        dIdProcesador.Add(4, "AMEX")
        dIdProcesador.Add(5, "TARJETAD")
        dIdProcesador.Add(6, "OCA")
        dIdProcesador.Add(8, "CABAL")
        dIdProcesador.Add(9, "ANDA")
        dIdProcesador.Add(12, "CREDITEL")
        dIdProcesador.Add(14, "PASSCARD")
        dIdProcesador.Add(16, "CLUBDELESTE")
        dIdProcesador.Add(19, "MIDES")
        cmbIdProcesadorTransAct.DataSource = New BindingSource(dIdProcesador, Nothing)
        cmbIdProcesadorTransAct.DisplayMember = "Value"
        cmbIdProcesadorTransAct.ValueMember = "Key"
        cmbIdProcesadorTransAct.SelectedIndex = 0

    End Sub

    Private Sub RapidatxtCantidad1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RapidatxtCantidad1.TextChanged
        CalcularTotales()
    End Sub

    Private Sub RapidatxtPrecioUnitario1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RapidatxtPrecioUnitario1.TextChanged
        CalcularTotales()
    End Sub

    Private Sub RapidatxtCantidad2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RapidatxtCantidad2.TextChanged
        CalcularTotales()
    End Sub

    Private Sub RapidatxtPrecioUnitario2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RapidatxtPrecioUnitario2.TextChanged
        CalcularTotales()
    End Sub

    Private Sub RapidatxtCantidad3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RapidatxtCantidad3.TextChanged
        CalcularTotales()
    End Sub

    Private Sub RapidatxtPrecioUnitario3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RapidatxtPrecioUnitario3.TextChanged
        CalcularTotales()
    End Sub

    Private Sub RapidatxtCantidad4_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RapidatxtCantidad4.TextChanged
        CalcularTotales()
    End Sub

    Private Sub RapidatxtPrecioUnitario4_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RapidatxtPrecioUnitario4.TextChanged
        CalcularTotales()
    End Sub

    Private Sub RapidatxtCantidad5_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RapidatxtCantidad5.TextChanged
        CalcularTotales()
    End Sub

    Private Sub RapidatxtPrecioUnitario5_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RapidatxtPrecioUnitario5.TextChanged
        CalcularTotales()
    End Sub

    Private Sub RapidatxtValorMediosDePago1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RapidatxtValorMediosDePago1.TextChanged
        CalcularTotales()
    End Sub

    Private Sub RapidatxtValorMediosDePago2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RapidatxtValorMediosDePago2.TextChanged
        CalcularTotales()
    End Sub

    Private Sub RapidatxtCantidad6_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RapidatxtCantidad6.TextChanged
        CalcularTotales()
    End Sub

    Private Sub RapidatxtPrecioUnitario6_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RapidatxtPrecioUnitario6.TextChanged
        CalcularTotales()
    End Sub

    Private Sub RapidatxtCantidad7_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RapidatxtCantidad7.TextChanged
        CalcularTotales()
    End Sub

    Private Sub RapidatxtPrecioUnitario7_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RapidatxtPrecioUnitario7.TextChanged
        CalcularTotales()
    End Sub

    Private Sub RapidatxtCantidad8_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RapidatxtCantidad8.TextChanged
        CalcularTotales()
    End Sub

    Private Sub RapidatxtPrecioUnitario8_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RapidatxtPrecioUnitario8.TextChanged
        CalcularTotales()
    End Sub

    Private Sub RapidatxtCantidad9_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RapidatxtCantidad9.TextChanged
        CalcularTotales()
    End Sub

    Private Sub RapidatxtPrecioUnitario9_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RapidatxtPrecioUnitario9.TextChanged
        CalcularTotales()
    End Sub

    Private Sub RapidatxtCantidad10_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RapidatxtCantidad10.TextChanged
        CalcularTotales()
    End Sub

    Private Sub RapidatxtPrecioUnitario10_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RapidatxtPrecioUnitario10.TextChanged
        CalcularTotales()
    End Sub

    Private Sub HabilitarDatosReceptor(ByVal Habilitar As Boolean)
        RapidacmbTipoDocumentoReceptor.Enabled = Habilitar
        RapidatxtDocumentoReceptor.Enabled = Habilitar
        RapidatxtNombreReceptor.Enabled = Habilitar
        RapidacmbCodigoPaisReceptor.Enabled = Habilitar
        RapidatxtDepartamentoReceptor.Enabled = Habilitar
        RapidatxtCiudadReceptor.Enabled = Habilitar
        RapidatxtDireccionReceptor.Enabled = Habilitar
    End Sub

    Private Sub HabilitarDatosNotaDeb_Cred(ByVal Habilitar As Boolean)
        chkIndicadorRefGlobal.Visible = Habilitar
        cmbTipoCFEInformacionReferencia.Visible = Habilitar
        lblReferencia.Visible = Habilitar
        lblTipoCFEInformacionReferencia.Visible = Habilitar
        lblSerieCFEInformacionReferencia.Visible = Habilitar
        txtSerieCFEInformacionReferencia.Visible = Habilitar
        lblNumeroCFEInformacionReferencia.Visible = Habilitar
        txtNumeroCFEInformacionReferencia.Visible = Habilitar
        lblRazonInformacionReferencia.Visible = Habilitar
        txtRazonInformacionReferencia.Visible = Habilitar
        lblFechaCFEInformacionReferencia.Visible = Habilitar
        dtpFechaCFEInformacionReferencia.Visible = Habilitar
        If Habilitar Then
            chkIndicadorRefGlobal_CheckedChanged(Nothing, Nothing)
        End If
    End Sub

    Public Function CTDec(ByVal Texto As Object) As Decimal
        If IsNumeric(Texto) Then
            CTDec = CType(CStr(Texto), Decimal)
            If CTDec = Decimal.MinValue Then
                CTDec = 0
            End If
        Else
            CTDec = 0
        End If
    End Function

    Private Sub LimpiarCampos()
        chkConsumoFinal_CheckedChanged(Nothing, Nothing)
        chkConsumoFinal.Checked = chkConsumoFinalDefault.Checked
        RapidacmbTipoDocumentoReceptor.SelectedIndex = 0
        RapidatxtDocumentoReceptor.Text = ""
        RapidatxtNombreReceptor.Text = ""
        RapidacmbCodigoPaisReceptor.SelectedIndex = 9
        RapidatxtDepartamentoReceptor.Text = ""
        RapidatxtCiudadReceptor.Text = ""
        RapidatxtDireccionReceptor.Text = ""
        RapidacmbIndicadorFacturacion1.SelectedIndex = cmbCfgIndicadorFacturacion.SelectedIndex
        RapidacmbIndicadorFacturacion2.SelectedIndex = cmbCfgIndicadorFacturacion.SelectedIndex
        RapidacmbIndicadorFacturacion3.SelectedIndex = cmbCfgIndicadorFacturacion.SelectedIndex
        RapidacmbIndicadorFacturacion4.SelectedIndex = cmbCfgIndicadorFacturacion.SelectedIndex
        RapidacmbIndicadorFacturacion5.SelectedIndex = cmbCfgIndicadorFacturacion.SelectedIndex
        RapidacmbIndicadorFacturacion6.SelectedIndex = cmbCfgIndicadorFacturacion.SelectedIndex
        RapidacmbIndicadorFacturacion7.SelectedIndex = cmbCfgIndicadorFacturacion.SelectedIndex
        RapidacmbIndicadorFacturacion8.SelectedIndex = cmbCfgIndicadorFacturacion.SelectedIndex
        RapidacmbIndicadorFacturacion9.SelectedIndex = cmbCfgIndicadorFacturacion.SelectedIndex
        RapidacmbIndicadorFacturacion10.SelectedIndex = cmbCfgIndicadorFacturacion.SelectedIndex

        RapidatxtNombreItem1.Text = ""
        RapidatxtNombreItem2.Text = ""
        RapidatxtNombreItem3.Text = ""
        RapidatxtNombreItem4.Text = ""
        RapidatxtNombreItem5.Text = ""
        RapidatxtNombreItem6.Text = ""
        RapidatxtNombreItem7.Text = ""
        RapidatxtNombreItem8.Text = ""
        RapidatxtNombreItem9.Text = ""
        RapidatxtNombreItem10.Text = ""
        RapidatxtCantidad1.Text = "1"
        RapidatxtCantidad2.Text = "1"
        RapidatxtCantidad3.Text = "1"
        RapidatxtCantidad4.Text = "1"
        RapidatxtCantidad5.Text = "1"
        RapidatxtCantidad6.Text = "1"
        RapidatxtCantidad7.Text = "1"
        RapidatxtCantidad8.Text = "1"
        RapidatxtCantidad9.Text = "1"
        RapidatxtCantidad10.Text = "1"
        RapidatxtPrecioUnitario1.Text = "0"
        RapidatxtPrecioUnitario2.Text = "0"
        RapidatxtPrecioUnitario3.Text = "0"
        RapidatxtPrecioUnitario4.Text = "0"
        RapidatxtPrecioUnitario5.Text = "0"
        RapidatxtPrecioUnitario6.Text = "0"
        RapidatxtPrecioUnitario7.Text = "0"
        RapidatxtPrecioUnitario8.Text = "0"
        RapidatxtPrecioUnitario9.Text = "0"
        RapidatxtPrecioUnitario10.Text = "0"
        RapidatxtSubDescuentoValor1.Text = "0"
        RapidatxtSubDescuentoValor2.Text = "0"
        RapidatxtSubDescuentoValor3.Text = "0"
        RapidatxtSubDescuentoValor4.Text = "0"
        RapidatxtSubDescuentoValor5.Text = "0"
        RapidatxtSubDescuentoValor6.Text = "0"
        RapidatxtSubDescuentoValor7.Text = "0"
        RapidatxtSubDescuentoValor8.Text = "0"
        RapidatxtSubDescuentoValor9.Text = "0"
        RapidatxtSubDescuentoValor10.Text = "0"
        RapidatxtMontoItem1.Text = "0"
        RapidatxtMontoItem2.Text = "0"
        RapidatxtMontoItem3.Text = "0"
        RapidatxtMontoItem4.Text = "0"
        RapidatxtMontoItem5.Text = "0"
        RapidatxtMontoItem6.Text = "0"
        RapidatxtMontoItem7.Text = "0"
        RapidatxtMontoItem8.Text = "0"
        RapidatxtMontoItem9.Text = "0"
        RapidatxtMontoItem10.Text = "0"
        RapidatxtAdenda.Text = ""
        RapidatxtGlosaMediosDePago1.Text = ""
        RapidatxtValorMediosDePago1.Text = "0"
        RapidatxtGlosaMediosDePago2.Text = ""
        RapidatxtValorMediosDePago2.Text = "0"
        RapidatxtVuelto.Text = "0"
        RapidatxtTotalMontoNetoIVATasaMinima.Text = "0"
        RapidatxtTotalIVATasaMinima.Text = "0"
        RapidatxtTotalMontoNetoIVATasaBasica.Text = "0"
        RapidatxtTotalIVATasaBasica.Text = "0"
        RapidatxtMontoTotalPagar.Text = "0"
        CmbIndicadorMontoBrutoEncabezado.SelectedValue = 1
        RapidaTxtMontoNoFacturable.Text = "0"
        cmbFormaPago.SelectedIndex = 0
        If Not IsNothing(objConfig) AndAlso objConfig.TIPOMONEDA = "USD" Then
            CambiarMonedaDolares()
        Else
            CambiarMonedaPesos()
        End If

        SubDescuentoValor1 = 0
        SubDescuentoValor2 = 0
        SubDescuentoValor3 = 0
        SubDescuentoValor4 = 0
        SubDescuentoValor5 = 0
        SubDescuentoValor6 = 0
        SubDescuentoValor7 = 0
        SubDescuentoValor8 = 0
        SubDescuentoValor9 = 0
        SubDescuentoValor10 = 0

        TBx_GlosaSubTotales1.Text = ""
        TBx_GlosaSubTotales2.Text = ""
        TBx_GlosaSubTotales3.Text = ""
        TBx_GlosaSubTotales4.Text = ""
        TBx_GlosaSubTotales5.Text = ""
        TBx_ValorSubTotales1.Text = "0"
        TBx_ValorSubTotales2.Text = "0"
        TBx_ValorSubTotales3.Text = "0"
        TBx_ValorSubTotales4.Text = "0"
        TBx_ValorSubTotales5.Text = "0"

        CBx_ViaTransporte.SelectedIndex = 0
        CBx_ModalidadVenta.SelectedIndex = 0

        TBx_ExportacionYAsimilados.Text = "0"
        LineaSeleccionada = 0

        Ckb_Exportacion.Checked = False

        txtSerieCFEInformacionReferencia.Text = ""
        txtNumeroCFEInformacionReferencia.Text = ""
        txtRazonInformacionReferencia.Text = ""

        LimpiarRetencionPercepcion()

        LimpiarRemitos()

    End Sub

    Private Sub lblCerrar_Click(sender As System.Object, e As System.EventArgs) Handles lblCerrar.Click
        panAdvertencia.Visible = False
    End Sub

    Private Sub chkConsumoFinal_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkConsumoFinal.CheckedChanged
        If chkConsumoFinal.Checked Then
            Ckb_Exportacion.Visible = False
            HabilitarDatosReceptor(True)
        Else
            If Chk_usaExportacion.Checked Then
                Ckb_Exportacion.Visible = True
            End If
            HabilitarDatosReceptor(True)
            RapidatxtDocumentoReceptor.Focus()
        End If
    End Sub

    Private Sub cmbTipoVenta_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cmbTipoVenta.SelectedIndexChanged
        If cmbTipoVenta.SelectedIndex = 2 Or cmbTipoVenta.SelectedIndex = 3 Then
            HabilitarDatosNotaDeb_Cred(True)
        Else
            HabilitarDatosNotaDeb_Cred(False)
        End If
    End Sub

    Private Sub chkIndicadorRefGlobal_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkIndicadorRefGlobal.CheckedChanged
        If chkIndicadorRefGlobal.Checked Then
            cmbTipoCFEInformacionReferencia.Visible = False
            txtSerieCFEInformacionReferencia.Visible = False
            txtNumeroCFEInformacionReferencia.Visible = False
            dtpFechaCFEInformacionReferencia.Visible = False
            lblSerieCFEInformacionReferencia.Visible = False
            lblFechaCFEInformacionReferencia.Visible = False
            lblTipoCFEInformacionReferencia.Visible = False
            lblNumeroCFEInformacionReferencia.Visible = False
        Else
            cmbTipoCFEInformacionReferencia.Visible = True
            txtSerieCFEInformacionReferencia.Visible = True
            txtNumeroCFEInformacionReferencia.Visible = True
            dtpFechaCFEInformacionReferencia.Visible = True
            lblSerieCFEInformacionReferencia.Visible = True
            lblFechaCFEInformacionReferencia.Visible = True
            lblTipoCFEInformacionReferencia.Visible = True
            lblNumeroCFEInformacionReferencia.Visible = True
        End If
    End Sub

    Sub BtnGuardarConfig_Click(sender As System.Object, e As System.EventArgs) Handles BtnGuardarConfig.Click
        Try
            If IsNothing(objConfig) Then
                objConfig = New XMLCONFIGFACMANUAL
            End If
            'Configuracion Servidor
            objConfig.URLSERVIDOR = txturlServidorGateway.Text
            objConfig.TIMEOUTSERVIDOR = CTDec(txtSegundosTimeout.Text)
            'Configuracion Facturador
            objConfig.NOMBREEMISOR = txtCfgNombreEmisor.Text
            objConfig.RAZONSOCIALENEMISOR = txtCfgRazonSocialEmisor.Text
            objConfig.RUTEMISOR = txtCfgRUTEmisor.Text
            objConfig.CODDGISUCURSAL = CTDec(txtCfgCodigoSucursalEmisor.Text)
            objConfig.NROSUCURSAL = CTDec(txtNroSucursal.Text)
            objConfig.NOMBRESUCURSAL = txtCfgNombreSucursalEmisor.Text
            objConfig.NROCAJA = CTDec(txtCfgCajaNro.Text)
            objConfig.NROCAJERO = CTDec(txtCfgCajeroNro.Text)
            objConfig.NOMBRECAJERO = txtCfgCajeroNombre.Text
            objConfig.NROVENDEDOR = CTDec(txtCfgVendedorNro.Text)
            objConfig.NOMBREVENDEDOR = txtCfgVendedorNom.Text
            objConfig.GIRONEGOCIOEMISOR = txtCfgGiroNegocioEmisor.Text
            objConfig.CORREOEMISOR = txtCfgCorreoEmisor.Text
            objConfig.DOMFISCALEMISOR = txtCfgDomicilioFiscalEmisor.Text
            objConfig.CIUDADEMISOR = txtCfgCiudadEmisor.Text
            objConfig.DEPARTAMENTOEMISOR = txtCfgDepartamentoEmisor.Text
            objConfig.VALORUNIDADINDEXADA = CTDec(txtCfgValorUnidadIndexada.Text)
            objConfig.TELEFONOEMISOR = txtCfgTelefonoEmisor.Text
            objConfig.TELEFONO2EMISOR = txtCfgTelefono2Emisor.Text
            If cmbCfgTipoMonedaTransaccion.SelectedIndex = 1 Then
                objConfig.TIPOMONEDA = "USD"
            Else
                objConfig.TIPOMONEDA = "UYU"
            End If

            objConfig.TIPODECAMBIO = CTDec(txtCfgTipoDeCambio.Text)
            objConfig.TASABASICAIVA = CTDec(txtCfgTasaBasicaIVA.Text)
            objConfig.TASAMINIMAIVA = CTDec(txtCfgTasaMinimaIVA.Text)

            objConfig.TIPONEGOCIO = cmbTipoNegocio.SelectedIndex + 1
            objConfig.ImpuestoPorDefecto = cmbCfgIndicadorFacturacion.Text

            If Chk_Bloqueado.Checked Then
                objConfig.BLOQUEARIMPUESTO = 1
            Else
                objConfig.BLOQUEARIMPUESTO = 0
            End If
            If Chk_UsaSubTotales.Checked Then
                objConfig.USASUBTOTALES = 1
            Else
                objConfig.USASUBTOTALES = 0
            End If
            If Chk_UsaRetPerc.Checked Then
                objConfig.USARETENCIONPERCEPCION = 1
            Else
                objConfig.USARETENCIONPERCEPCION = 0
            End If
            If Chk_PreFactura.Checked Then
                objConfig.PREFACTURA = 1
            Else
                objConfig.PREFACTURA = 0
            End If

            If Chk_usaExportacion.Checked Then
                objConfig.USAEXPORTACION = 1
            Else
                objConfig.USAEXPORTACION = 0
            End If

            If Chk_emiteRemitos.Checked Then
                objConfig.EMITEEXTRACFE = 1
            Else
                objConfig.EMITEEXTRACFE = 0
                Rdb_esTckFact.Checked = True 'para el caso de que desmarque la opcion y se haya quedado seleccionado Boleta o Remito
            End If

            objConfig.CARPETAOPERACION = TextBox_CarpetaOperacion.Text

            objConfig.ConsumoFinal = chkConsumoFinalDefault.Checked

            'Configuracion impresora
            If chkImprimeApi.Checked Then
                objConfig.IMPRIMEAPI = 1
                objConfig.NOMBREIMPRESORA = cmbImpresora.Text
                objConfig.TIPOIMPRESORA = cmbTipoImpresora.SelectedValue
                objConfig.CANTIDADCOPIAS = txtCantCopias.Value
            Else
                objConfig.IMPRIMEAPI = 0
                objConfig.NOMBREIMPRESORA = ""
                objConfig.TIPOIMPRESORA = 0
                objConfig.CANTIDADCOPIAS = 1
            End If

            objConfig.EMPCOD = Trim(txtEmpCod.Text)
            objConfig.TERMCOD = Trim(txtTermCod.Text)
            objConfig.MODIFICARCUOTAS = chkModificarCuotas.Checked
            objConfig.MODIFICARFACTURA = chkModificarFactura.Checked
            objConfig.MODIFICARMONEDA = chkModificarMoneda.Checked
            objConfig.MODIFICARMONTOS = chkModificarMontos.Checked
            objConfig.MODIFICARPLAN = chkModificarPlan.Checked
            objConfig.MODIFICARTARJETA = chkModificarTarjeta.Checked
            objConfig.MODIFICARTIPOCUENTA = chkModificarTipoCuenta.Checked
            objConfig.MODOFICARDECRETOLEY = chkModificarDecretoLey.Checked

            objConfig.IIMPRIMEVOUCHERADENDA = rdButtonImprimeVoucherAdenda.Checked

            objConfig.GuardarConfig()
            MsgBox("Configuracion guardada correctamente.", MsgBoxStyle.Information, "Configuracion")
            TabControl1.SelectedTab = FacturaRapida
            frmFactura_Load(Nothing, Nothing)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Configuracion")
        End Try
    End Sub

    Private Sub BtnLimpiarCampos_Click(sender As System.Object, e As System.EventArgs) Handles BtnLimpiarCampos.Click
        LimpiarCampos()
    End Sub

    Private Sub chkIndicadorMontoBrutoEncabezado_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        CalcularTotales()
    End Sub

    Private Sub btnImprimirPrueba_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimirPrueba.Click
        Dim ErrorMsg As String = ""
        Dim ErrorCod As Integer
        Try
            Dim imp As New TAFACE2ApiImpresion.TAFirmoImprimir
            imp.InicializarImpresion(ErrorMsg, ErrorCod, cmbTipoImpresora.SelectedValue, cmbImpresora.Text, 1, 0, IIf(chkAbrirCajon.Checked, 1, 0))
            imp.ImprimrPruebaImpresion(ErrorMsg, ErrorCod)
        Catch ex As Exception
            MsgBox(ErrorMsg)
        End Try
    End Sub

    Private Sub RapidatxtSubDescuentoValor1_TextChanged(sender As System.Object, e As System.EventArgs) Handles RapidatxtSubDescuentoValor1.TextChanged, RapidatxtSubDescuentoValor2.TextChanged, RapidatxtSubDescuentoValor3.TextChanged, RapidatxtSubDescuentoValor4.TextChanged, RapidatxtSubDescuentoValor5.TextChanged, RapidatxtSubDescuentoValor6.TextChanged, RapidatxtSubDescuentoValor7.TextChanged, RapidatxtSubDescuentoValor8.TextChanged, RapidatxtSubDescuentoValor9.TextChanged, RapidatxtSubDescuentoValor10.TextChanged
        CalcularTotales()
    End Sub

    Private Sub RapidaTxtMontoNoFacturable_TextChanged(sender As System.Object, e As System.EventArgs) Handles RapidaTxtMontoNoFacturable.TextChanged
        CalcularTotales()
    End Sub

    Private Sub lblDolares_Click(sender As System.Object, e As System.EventArgs) Handles lblDolares.Click
        CambiarMonedaDolares()
    End Sub

    Private Sub lblPesos_Click(sender As System.Object, e As System.EventArgs) Handles lblPesos.Click
        CambiarMonedaPesos()
    End Sub

    Private Sub CambiarMonedaPesos()
        lblPesos.BackColor = System.Drawing.SystemColors.ActiveCaption
        lblPesos.ForeColor = Color.WhiteSmoke
        lblDolares.BackColor = Color.Transparent
        lblDolares.ForeColor = Color.Black
        lblMonedaSimbolo.Text = "$"
        cmbCfgTipoMonedaTransaccion.SelectedIndex = 0
    End Sub

    Private Sub CambiarMonedaDolares()
        lblDolares.BackColor = System.Drawing.SystemColors.ActiveCaption
        lblDolares.ForeColor = Color.WhiteSmoke
        lblPesos.BackColor = Color.Transparent
        lblPesos.ForeColor = Color.Black
        lblMonedaSimbolo.Text = "US$"
        cmbCfgTipoMonedaTransaccion.SelectedIndex = 1
    End Sub

    Private Sub cmbFormaPago_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cmbFormaPago.SelectedIndexChanged
        If cmbFormaPago.SelectedIndex = 0 Then
            lblVencimiento.Visible = False
            dtpFechaVencimiento.Visible = False
            chkVencimientoCredito.Visible = False
        ElseIf cmbFormaPago.SelectedIndex = 1 Then
            lblVencimiento.Visible = True
            dtpFechaVencimiento.Visible = True
            chkVencimientoCredito.Visible = True
        End If
    End Sub

    Private Sub chkVencimientoCredito_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkVencimientoCredito.CheckedChanged
        dtpFechaVencimiento.Enabled = chkVencimientoCredito.Checked
    End Sub

    Private Sub btnReimprimir_Click(sender As System.Object, e As System.EventArgs) Handles btnReimprimir.Click
        ReImprimirFactura()
    End Sub

    Private Sub RapidaBtn_guardar_Click(sender As Object, e As EventArgs) Handles RapidaBtn_guardar.Click
        txtCfgTipoDeCambio.Text = RapidaTbx_tipoCambio.Text

        BtnGuardarConfig_Click(sender, EventArgs.Empty)
    End Sub

    Private Sub TabControl1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabControl1.SelectedIndexChanged

        If IsNothing(objConfig) Then

        ElseIf objConfig.DESHABILITARCONFIGURACION = 1 And TabControl1.SelectedTab Is Configuracion Then
            TabControl1.SelectedTab = FacturaRapida
            MessageBox.Show("La configuración se encuentra deshabilitada.", "Configuración", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Function MostrarPopupContingencia() As Boolean
        Dim frmDatConting As New frmFacturaContingencia
        Dim CodForm As Windows.Forms.DialogResult
        CodForm = frmDatConting.ShowDialog
        If CodForm = Windows.Forms.DialogResult.Retry Then
            Return True
        ElseIf CodForm = Windows.Forms.DialogResult.OK Then
            CFCSerie = frmDatConting.txtSerie.Text
            CFCNumero = frmDatConting.txtNumero.Text
            CFCCAENro = frmDatConting.txtCAENro.Text
            CFCCAENroDesde = frmDatConting.txtCAENroDesde.Text
            CFCCAENroHasta = frmDatConting.txtCAENroHasta.Text
            CFCCAEFchVencimiento = frmDatConting.dtpCAEFechaVenc.Value
            CFCFecha = frmDatConting.Dtp_CFCFecha.Value
            Return True
        End If
        Return False
    End Function

    Private Function MostrarPopupRetencionesPercepciones() As Boolean
        Dim frmRetPer As New frmFacturaRetencionesPercepciones
        Dim CodForm As Windows.Forms.DialogResult
        CodForm = frmRetPer.ShowDialog
        If CodForm = Windows.Forms.DialogResult.Retry Then
            CalcularTotales()
            Return True
        ElseIf CodForm = Windows.Forms.DialogResult.OK Then
            If LineaSeleccionada = 1 Then
                RetPerLinea1Cod1 = CTDec(frmRetPer.TBx_Codigo1.Text)
                RetPerLinea1Cod2 = CTDec(frmRetPer.TBx_Codigo2.Text)
                RetPerLinea1Cod3 = CTDec(frmRetPer.TBx_Codigo3.Text)
                RetPerLinea1Cod4 = CTDec(frmRetPer.TBx_Codigo4.Text)
                RetPerLinea1Cod5 = CTDec(frmRetPer.TBx_Codigo5.Text)
                RetPerLinea1Tasa1 = CTDec(frmRetPer.TBx_Tasa1.Text)
                RetPerLinea1Tasa2 = CTDec(frmRetPer.TBx_Tasa2.Text)
                RetPerLinea1Tasa3 = CTDec(frmRetPer.TBx_Tasa3.Text)
                RetPerLinea1Tasa4 = CTDec(frmRetPer.TBx_Tasa4.Text)
                RetPerLinea1Tasa5 = CTDec(frmRetPer.TBx_Tasa5.Text)
                RetPerLinea1Monto1 = CTDec(frmRetPer.TBx_MontoSujeto1.Text)
                RetPerLinea1Monto2 = CTDec(frmRetPer.TBx_MontoSujeto2.Text)
                RetPerLinea1Monto3 = CTDec(frmRetPer.TBx_MontoSujeto3.Text)
                RetPerLinea1Monto4 = CTDec(frmRetPer.TBx_MontoSujeto4.Text)
                RetPerLinea1Monto5 = CTDec(frmRetPer.TBx_MontoSujeto5.Text)
                RetPerLinea1Valor1 = CTDec(frmRetPer.TBx_TotalRetenido1.Text)
                RetPerLinea1Valor2 = CTDec(frmRetPer.TBx_TotalRetenido2.Text)
                RetPerLinea1Valor3 = CTDec(frmRetPer.TBx_TotalRetenido3.Text)
                RetPerLinea1Valor4 = CTDec(frmRetPer.TBx_TotalRetenido4.Text)
                RetPerLinea1Valor5 = CTDec(frmRetPer.TBx_TotalRetenido5.Text)
            End If
            If LineaSeleccionada = 2 Then
                RetPerLinea2Cod1 = CTDec(frmRetPer.TBx_Codigo1.Text)
                RetPerLinea2Cod2 = CTDec(frmRetPer.TBx_Codigo2.Text)
                RetPerLinea2Cod3 = CTDec(frmRetPer.TBx_Codigo3.Text)
                RetPerLinea2Cod4 = CTDec(frmRetPer.TBx_Codigo4.Text)
                RetPerLinea2Cod5 = CTDec(frmRetPer.TBx_Codigo5.Text)
                RetPerLinea2Tasa1 = CTDec(frmRetPer.TBx_Tasa1.Text)
                RetPerLinea2Tasa2 = CTDec(frmRetPer.TBx_Tasa2.Text)
                RetPerLinea2Tasa3 = CTDec(frmRetPer.TBx_Tasa3.Text)
                RetPerLinea2Tasa4 = CTDec(frmRetPer.TBx_Tasa4.Text)
                RetPerLinea2Tasa5 = CTDec(frmRetPer.TBx_Tasa5.Text)
                RetPerLinea2Monto1 = CTDec(frmRetPer.TBx_MontoSujeto1.Text)
                RetPerLinea2Monto2 = CTDec(frmRetPer.TBx_MontoSujeto2.Text)
                RetPerLinea2Monto3 = CTDec(frmRetPer.TBx_MontoSujeto3.Text)
                RetPerLinea2Monto4 = CTDec(frmRetPer.TBx_MontoSujeto4.Text)
                RetPerLinea2Monto5 = CTDec(frmRetPer.TBx_MontoSujeto5.Text)
                RetPerLinea2Valor1 = CTDec(frmRetPer.TBx_TotalRetenido1.Text)
                RetPerLinea2Valor2 = CTDec(frmRetPer.TBx_TotalRetenido2.Text)
                RetPerLinea2Valor3 = CTDec(frmRetPer.TBx_TotalRetenido3.Text)
                RetPerLinea2Valor4 = CTDec(frmRetPer.TBx_TotalRetenido4.Text)
                RetPerLinea2Valor5 = CTDec(frmRetPer.TBx_TotalRetenido5.Text)
            End If
            If LineaSeleccionada = 3 Then
                RetPerLinea3Cod1 = CTDec(frmRetPer.TBx_Codigo1.Text)
                RetPerLinea3Cod2 = CTDec(frmRetPer.TBx_Codigo2.Text)
                RetPerLinea3Cod3 = CTDec(frmRetPer.TBx_Codigo3.Text)
                RetPerLinea3Cod4 = CTDec(frmRetPer.TBx_Codigo4.Text)
                RetPerLinea3Cod5 = CTDec(frmRetPer.TBx_Codigo5.Text)
                RetPerLinea3Tasa1 = CTDec(frmRetPer.TBx_Tasa1.Text)
                RetPerLinea3Tasa2 = CTDec(frmRetPer.TBx_Tasa2.Text)
                RetPerLinea3Tasa3 = CTDec(frmRetPer.TBx_Tasa3.Text)
                RetPerLinea3Tasa4 = CTDec(frmRetPer.TBx_Tasa4.Text)
                RetPerLinea3Tasa5 = CTDec(frmRetPer.TBx_Tasa5.Text)
                RetPerLinea3Monto1 = CTDec(frmRetPer.TBx_MontoSujeto1.Text)
                RetPerLinea3Monto2 = CTDec(frmRetPer.TBx_MontoSujeto2.Text)
                RetPerLinea3Monto3 = CTDec(frmRetPer.TBx_MontoSujeto3.Text)
                RetPerLinea3Monto4 = CTDec(frmRetPer.TBx_MontoSujeto4.Text)
                RetPerLinea3Monto5 = CTDec(frmRetPer.TBx_MontoSujeto5.Text)
                RetPerLinea3Valor1 = CTDec(frmRetPer.TBx_TotalRetenido1.Text)
                RetPerLinea3Valor2 = CTDec(frmRetPer.TBx_TotalRetenido2.Text)
                RetPerLinea3Valor3 = CTDec(frmRetPer.TBx_TotalRetenido3.Text)
                RetPerLinea3Valor4 = CTDec(frmRetPer.TBx_TotalRetenido4.Text)
                RetPerLinea3Valor5 = CTDec(frmRetPer.TBx_TotalRetenido5.Text)
            End If
            If LineaSeleccionada = 4 Then
                RetPerLinea4Cod1 = CTDec(frmRetPer.TBx_Codigo1.Text)
                RetPerLinea4Cod2 = CTDec(frmRetPer.TBx_Codigo2.Text)
                RetPerLinea4Cod3 = CTDec(frmRetPer.TBx_Codigo3.Text)
                RetPerLinea4Cod4 = CTDec(frmRetPer.TBx_Codigo4.Text)
                RetPerLinea4Cod5 = CTDec(frmRetPer.TBx_Codigo5.Text)
                RetPerLinea4Tasa1 = CTDec(frmRetPer.TBx_Tasa1.Text)
                RetPerLinea4Tasa2 = CTDec(frmRetPer.TBx_Tasa2.Text)
                RetPerLinea4Tasa3 = CTDec(frmRetPer.TBx_Tasa3.Text)
                RetPerLinea4Tasa4 = CTDec(frmRetPer.TBx_Tasa4.Text)
                RetPerLinea4Tasa5 = CTDec(frmRetPer.TBx_Tasa5.Text)
                RetPerLinea4Monto1 = CTDec(frmRetPer.TBx_MontoSujeto1.Text)
                RetPerLinea4Monto2 = CTDec(frmRetPer.TBx_MontoSujeto2.Text)
                RetPerLinea4Monto3 = CTDec(frmRetPer.TBx_MontoSujeto3.Text)
                RetPerLinea4Monto4 = CTDec(frmRetPer.TBx_MontoSujeto4.Text)
                RetPerLinea4Monto5 = CTDec(frmRetPer.TBx_MontoSujeto5.Text)
                RetPerLinea4Valor1 = CTDec(frmRetPer.TBx_TotalRetenido1.Text)
                RetPerLinea4Valor2 = CTDec(frmRetPer.TBx_TotalRetenido2.Text)
                RetPerLinea4Valor3 = CTDec(frmRetPer.TBx_TotalRetenido3.Text)
                RetPerLinea4Valor4 = CTDec(frmRetPer.TBx_TotalRetenido4.Text)
                RetPerLinea4Valor5 = CTDec(frmRetPer.TBx_TotalRetenido5.Text)
            End If
            If LineaSeleccionada = 5 Then
                RetPerLinea5Cod1 = CTDec(frmRetPer.TBx_Codigo1.Text)
                RetPerLinea5Cod2 = CTDec(frmRetPer.TBx_Codigo2.Text)
                RetPerLinea5Cod3 = CTDec(frmRetPer.TBx_Codigo3.Text)
                RetPerLinea5Cod4 = CTDec(frmRetPer.TBx_Codigo4.Text)
                RetPerLinea5Cod5 = CTDec(frmRetPer.TBx_Codigo5.Text)
                RetPerLinea5Tasa1 = CTDec(frmRetPer.TBx_Tasa1.Text)
                RetPerLinea5Tasa2 = CTDec(frmRetPer.TBx_Tasa2.Text)
                RetPerLinea5Tasa3 = CTDec(frmRetPer.TBx_Tasa3.Text)
                RetPerLinea5Tasa4 = CTDec(frmRetPer.TBx_Tasa4.Text)
                RetPerLinea5Tasa5 = CTDec(frmRetPer.TBx_Tasa5.Text)
                RetPerLinea5Monto1 = CTDec(frmRetPer.TBx_MontoSujeto1.Text)
                RetPerLinea5Monto2 = CTDec(frmRetPer.TBx_MontoSujeto2.Text)
                RetPerLinea5Monto3 = CTDec(frmRetPer.TBx_MontoSujeto3.Text)
                RetPerLinea5Monto4 = CTDec(frmRetPer.TBx_MontoSujeto4.Text)
                RetPerLinea5Monto5 = CTDec(frmRetPer.TBx_MontoSujeto5.Text)
                RetPerLinea5Valor1 = CTDec(frmRetPer.TBx_TotalRetenido1.Text)
                RetPerLinea5Valor2 = CTDec(frmRetPer.TBx_TotalRetenido2.Text)
                RetPerLinea5Valor3 = CTDec(frmRetPer.TBx_TotalRetenido3.Text)
                RetPerLinea5Valor4 = CTDec(frmRetPer.TBx_TotalRetenido4.Text)
                RetPerLinea5Valor5 = CTDec(frmRetPer.TBx_TotalRetenido5.Text)
            End If
            If LineaSeleccionada = 6 Then
                RetPerLinea6Cod1 = CTDec(frmRetPer.TBx_Codigo1.Text)
                RetPerLinea6Cod2 = CTDec(frmRetPer.TBx_Codigo2.Text)
                RetPerLinea6Cod3 = CTDec(frmRetPer.TBx_Codigo3.Text)
                RetPerLinea6Cod4 = CTDec(frmRetPer.TBx_Codigo4.Text)
                RetPerLinea6Cod5 = CTDec(frmRetPer.TBx_Codigo5.Text)
                RetPerLinea6Tasa1 = CTDec(frmRetPer.TBx_Tasa1.Text)
                RetPerLinea6Tasa2 = CTDec(frmRetPer.TBx_Tasa2.Text)
                RetPerLinea6Tasa3 = CTDec(frmRetPer.TBx_Tasa3.Text)
                RetPerLinea6Tasa4 = CTDec(frmRetPer.TBx_Tasa4.Text)
                RetPerLinea6Tasa5 = CTDec(frmRetPer.TBx_Tasa5.Text)
                RetPerLinea6Monto1 = CTDec(frmRetPer.TBx_MontoSujeto1.Text)
                RetPerLinea6Monto2 = CTDec(frmRetPer.TBx_MontoSujeto2.Text)
                RetPerLinea6Monto3 = CTDec(frmRetPer.TBx_MontoSujeto3.Text)
                RetPerLinea6Monto4 = CTDec(frmRetPer.TBx_MontoSujeto4.Text)
                RetPerLinea6Monto5 = CTDec(frmRetPer.TBx_MontoSujeto5.Text)
                RetPerLinea6Valor1 = CTDec(frmRetPer.TBx_TotalRetenido1.Text)
                RetPerLinea6Valor2 = CTDec(frmRetPer.TBx_TotalRetenido2.Text)
                RetPerLinea6Valor3 = CTDec(frmRetPer.TBx_TotalRetenido3.Text)
                RetPerLinea6Valor4 = CTDec(frmRetPer.TBx_TotalRetenido4.Text)
                RetPerLinea6Valor5 = CTDec(frmRetPer.TBx_TotalRetenido5.Text)
            End If
            If LineaSeleccionada = 7 Then
                RetPerLinea7Cod1 = CTDec(frmRetPer.TBx_Codigo1.Text)
                RetPerLinea7Cod2 = CTDec(frmRetPer.TBx_Codigo2.Text)
                RetPerLinea7Cod3 = CTDec(frmRetPer.TBx_Codigo3.Text)
                RetPerLinea7Cod4 = CTDec(frmRetPer.TBx_Codigo4.Text)
                RetPerLinea7Cod5 = CTDec(frmRetPer.TBx_Codigo5.Text)
                RetPerLinea7Tasa1 = CTDec(frmRetPer.TBx_Tasa1.Text)
                RetPerLinea7Tasa2 = CTDec(frmRetPer.TBx_Tasa2.Text)
                RetPerLinea7Tasa3 = CTDec(frmRetPer.TBx_Tasa3.Text)
                RetPerLinea7Tasa4 = CTDec(frmRetPer.TBx_Tasa4.Text)
                RetPerLinea7Tasa5 = CTDec(frmRetPer.TBx_Tasa5.Text)
                RetPerLinea7Monto1 = CTDec(frmRetPer.TBx_MontoSujeto1.Text)
                RetPerLinea7Monto2 = CTDec(frmRetPer.TBx_MontoSujeto2.Text)
                RetPerLinea7Monto3 = CTDec(frmRetPer.TBx_MontoSujeto3.Text)
                RetPerLinea7Monto4 = CTDec(frmRetPer.TBx_MontoSujeto4.Text)
                RetPerLinea7Monto5 = CTDec(frmRetPer.TBx_MontoSujeto5.Text)
                RetPerLinea7Valor1 = CTDec(frmRetPer.TBx_TotalRetenido1.Text)
                RetPerLinea7Valor2 = CTDec(frmRetPer.TBx_TotalRetenido2.Text)
                RetPerLinea7Valor3 = CTDec(frmRetPer.TBx_TotalRetenido3.Text)
                RetPerLinea7Valor4 = CTDec(frmRetPer.TBx_TotalRetenido4.Text)
                RetPerLinea7Valor5 = CTDec(frmRetPer.TBx_TotalRetenido5.Text)
            End If
            If LineaSeleccionada = 8 Then
                RetPerLinea8Cod1 = CTDec(frmRetPer.TBx_Codigo1.Text)
                RetPerLinea8Cod2 = CTDec(frmRetPer.TBx_Codigo2.Text)
                RetPerLinea8Cod3 = CTDec(frmRetPer.TBx_Codigo3.Text)
                RetPerLinea8Cod4 = CTDec(frmRetPer.TBx_Codigo4.Text)
                RetPerLinea8Cod5 = CTDec(frmRetPer.TBx_Codigo5.Text)
                RetPerLinea8Tasa1 = CTDec(frmRetPer.TBx_Tasa1.Text)
                RetPerLinea8Tasa2 = CTDec(frmRetPer.TBx_Tasa2.Text)
                RetPerLinea8Tasa3 = CTDec(frmRetPer.TBx_Tasa3.Text)
                RetPerLinea8Tasa4 = CTDec(frmRetPer.TBx_Tasa4.Text)
                RetPerLinea8Tasa5 = CTDec(frmRetPer.TBx_Tasa5.Text)
                RetPerLinea8Monto1 = CTDec(frmRetPer.TBx_MontoSujeto1.Text)
                RetPerLinea8Monto2 = CTDec(frmRetPer.TBx_MontoSujeto2.Text)
                RetPerLinea8Monto3 = CTDec(frmRetPer.TBx_MontoSujeto3.Text)
                RetPerLinea8Monto4 = CTDec(frmRetPer.TBx_MontoSujeto4.Text)
                RetPerLinea8Monto5 = CTDec(frmRetPer.TBx_MontoSujeto5.Text)
                RetPerLinea8Valor1 = CTDec(frmRetPer.TBx_TotalRetenido1.Text)
                RetPerLinea8Valor2 = CTDec(frmRetPer.TBx_TotalRetenido2.Text)
                RetPerLinea8Valor3 = CTDec(frmRetPer.TBx_TotalRetenido3.Text)
                RetPerLinea8Valor4 = CTDec(frmRetPer.TBx_TotalRetenido4.Text)
                RetPerLinea8Valor5 = CTDec(frmRetPer.TBx_TotalRetenido5.Text)
            End If
            If LineaSeleccionada = 9 Then
                RetPerLinea9Cod1 = CTDec(frmRetPer.TBx_Codigo1.Text)
                RetPerLinea9Cod2 = CTDec(frmRetPer.TBx_Codigo2.Text)
                RetPerLinea9Cod3 = CTDec(frmRetPer.TBx_Codigo3.Text)
                RetPerLinea9Cod4 = CTDec(frmRetPer.TBx_Codigo4.Text)
                RetPerLinea9Cod5 = CTDec(frmRetPer.TBx_Codigo5.Text)
                RetPerLinea9Tasa1 = CTDec(frmRetPer.TBx_Tasa1.Text)
                RetPerLinea9Tasa2 = CTDec(frmRetPer.TBx_Tasa2.Text)
                RetPerLinea9Tasa3 = CTDec(frmRetPer.TBx_Tasa3.Text)
                RetPerLinea9Tasa4 = CTDec(frmRetPer.TBx_Tasa4.Text)
                RetPerLinea9Tasa5 = CTDec(frmRetPer.TBx_Tasa5.Text)
                RetPerLinea9Monto1 = CTDec(frmRetPer.TBx_MontoSujeto1.Text)
                RetPerLinea9Monto2 = CTDec(frmRetPer.TBx_MontoSujeto2.Text)
                RetPerLinea9Monto3 = CTDec(frmRetPer.TBx_MontoSujeto3.Text)
                RetPerLinea9Monto4 = CTDec(frmRetPer.TBx_MontoSujeto4.Text)
                RetPerLinea9Monto5 = CTDec(frmRetPer.TBx_MontoSujeto5.Text)
                RetPerLinea9Valor1 = CTDec(frmRetPer.TBx_TotalRetenido1.Text)
                RetPerLinea9Valor2 = CTDec(frmRetPer.TBx_TotalRetenido2.Text)
                RetPerLinea9Valor3 = CTDec(frmRetPer.TBx_TotalRetenido3.Text)
                RetPerLinea9Valor4 = CTDec(frmRetPer.TBx_TotalRetenido4.Text)
                RetPerLinea9Valor5 = CTDec(frmRetPer.TBx_TotalRetenido5.Text)
            End If
            If LineaSeleccionada = 10 Then
                RetPerLinea10Cod1 = CTDec(frmRetPer.TBx_Codigo1.Text)
                RetPerLinea10Cod2 = CTDec(frmRetPer.TBx_Codigo2.Text)
                RetPerLinea10Cod3 = CTDec(frmRetPer.TBx_Codigo3.Text)
                RetPerLinea10Cod4 = CTDec(frmRetPer.TBx_Codigo4.Text)
                RetPerLinea10Cod5 = CTDec(frmRetPer.TBx_Codigo5.Text)
                RetPerLinea10Tasa1 = CTDec(frmRetPer.TBx_Tasa1.Text)
                RetPerLinea10Tasa2 = CTDec(frmRetPer.TBx_Tasa2.Text)
                RetPerLinea10Tasa3 = CTDec(frmRetPer.TBx_Tasa3.Text)
                RetPerLinea10Tasa4 = CTDec(frmRetPer.TBx_Tasa4.Text)
                RetPerLinea10Tasa5 = CTDec(frmRetPer.TBx_Tasa5.Text)
                RetPerLinea10Monto1 = CTDec(frmRetPer.TBx_MontoSujeto1.Text)
                RetPerLinea10Monto2 = CTDec(frmRetPer.TBx_MontoSujeto2.Text)
                RetPerLinea10Monto3 = CTDec(frmRetPer.TBx_MontoSujeto3.Text)
                RetPerLinea10Monto4 = CTDec(frmRetPer.TBx_MontoSujeto4.Text)
                RetPerLinea10Monto5 = CTDec(frmRetPer.TBx_MontoSujeto5.Text)
                RetPerLinea10Valor1 = CTDec(frmRetPer.TBx_TotalRetenido1.Text)
                RetPerLinea10Valor2 = CTDec(frmRetPer.TBx_TotalRetenido2.Text)
                RetPerLinea10Valor3 = CTDec(frmRetPer.TBx_TotalRetenido3.Text)
                RetPerLinea10Valor4 = CTDec(frmRetPer.TBx_TotalRetenido4.Text)
                RetPerLinea10Valor5 = CTDec(frmRetPer.TBx_TotalRetenido5.Text)
            End If
            CalcularTotales()
            Return True
        End If
        Return False
    End Function

    Private Sub Btn_PreFacturaGuardar_Click(sender As Object, e As EventArgs) Handles Btn_PreFacturaGuardar.Click
        'prefactura.CargarDatos()
        'prefactura.Guardar()
        'LimpiarCampos()
    End Sub

    Private Sub Btn_RetencionesPercepciones1_Click(sender As Object, e As EventArgs) Handles Btn_RetencionesPercepciones1.Click
        LineaSeleccionada = 1
        MostrarPopupRetencionesPercepciones()
    End Sub

    Private Sub Btn_RetencionesPercepciones2_Click(sender As Object, e As EventArgs) Handles Btn_RetencionesPercepciones2.Click
        LineaSeleccionada = 2
        MostrarPopupRetencionesPercepciones()
    End Sub

    Private Sub Btn_RetencionesPercepciones3_Click(sender As Object, e As EventArgs) Handles Btn_RetencionesPercepciones3.Click
        LineaSeleccionada = 3
        MostrarPopupRetencionesPercepciones()
    End Sub

    Private Sub Btn_RetencionesPercepciones4_Click(sender As Object, e As EventArgs) Handles Btn_RetencionesPercepciones4.Click
        LineaSeleccionada = 4
        MostrarPopupRetencionesPercepciones()
    End Sub

    Private Sub Btn_RetencionesPercepciones5_Click(sender As Object, e As EventArgs) Handles Btn_RetencionesPercepciones5.Click
        LineaSeleccionada = 5
        MostrarPopupRetencionesPercepciones()
    End Sub

    Private Sub Btn_RetencionesPercepciones6_Click(sender As Object, e As EventArgs) Handles Btn_RetencionesPercepciones6.Click
        LineaSeleccionada = 6
        MostrarPopupRetencionesPercepciones()
    End Sub

    Private Sub Btn_RetencionesPercepciones7_Click(sender As Object, e As EventArgs) Handles Btn_RetencionesPercepciones7.Click
        LineaSeleccionada = 7
        MostrarPopupRetencionesPercepciones()
    End Sub

    Private Sub Btn_RetencionesPercepciones8_Click(sender As Object, e As EventArgs) Handles Btn_RetencionesPercepciones8.Click
        LineaSeleccionada = 8
        MostrarPopupRetencionesPercepciones()
    End Sub

    Private Sub Btn_RetencionesPercepciones9_Click(sender As Object, e As EventArgs) Handles Btn_RetencionesPercepciones9.Click
        LineaSeleccionada = 9
        MostrarPopupRetencionesPercepciones()
    End Sub

    Private Sub Btn_RetencionesPercepciones10_Click(sender As Object, e As EventArgs) Handles Btn_RetencionesPercepciones10.Click
        LineaSeleccionada = 10
        MostrarPopupRetencionesPercepciones()
    End Sub

    Private Sub CompletarListaRetenciones()
        If objConfig.USARETENCIONPERCEPCION = 1 Then
            CodigosRetPer = New List(Of String)
            CodigosRetPer.Clear()
            LBx_RetencionesPercepciones.Items.Clear()

            If CTDec(TBx_MontoRetenidoPorLinea1.Text) > 0 Then
                If RetPerLinea1Valor1 > 0 Then
                    If CodigosRetPer.Count > 0 Then
                        Dim ubicacion As Integer = 0
                        Dim encontro As Boolean = False
                        For Each s As String In CodigosRetPer
                            Dim lineaSeparada = s.Split("-")
                            If CTDec(lineaSeparada(0)) = RetPerLinea1Cod1 Then
                                Dim monto As Double
                                monto = RetPerLinea1Valor1 + CTDec(lineaSeparada(1))
                                CodigosRetPer(ubicacion) = String.Concat(RetPerLinea1Cod1, " - ", monto)
                                encontro = True
                                Exit For
                            End If
                            ubicacion = ubicacion + 1
                        Next
                        If Not encontro Then
                            Dim linea As String = String.Concat(RetPerLinea1Cod1, " - ", RetPerLinea1Valor1)
                            CodigosRetPer.Add(linea)
                        End If
                    Else
                        Dim linea As String = String.Concat(RetPerLinea1Cod1, " - ", RetPerLinea1Valor1)
                        CodigosRetPer.Add(linea)
                    End If
                End If
                If RetPerLinea1Valor2 > 0 Then
                    If CodigosRetPer.Count > 0 Then
                        Dim ubicacion As Integer = 0
                        Dim encontro As Boolean = False
                        For Each s As String In CodigosRetPer
                            Dim lineaSeparada = s.Split("-")
                            If lineaSeparada(0) = RetPerLinea1Cod2 Then
                                Dim monto As Double
                                monto = RetPerLinea1Valor2 + CTDec(lineaSeparada(1))
                                CodigosRetPer(ubicacion) = String.Concat(RetPerLinea1Cod2, " - ", monto)
                                encontro = True
                                Exit For
                            End If
                            ubicacion = ubicacion + 1
                        Next
                        If Not encontro Then
                            Dim linea As String = String.Concat(RetPerLinea1Cod2, " - ", RetPerLinea1Valor2)
                            CodigosRetPer.Add(linea)
                        End If
                    Else
                        Dim linea As String = String.Concat(RetPerLinea1Cod2, " - ", RetPerLinea1Valor2)
                        CodigosRetPer.Add(linea)
                    End If
                End If
                If RetPerLinea1Valor3 > 0 Then
                    If CodigosRetPer.Count > 0 Then
                        Dim ubicacion As Integer = 0
                        Dim encontro As Boolean = False
                        For Each s As String In CodigosRetPer
                            Dim lineaSeparada = s.Split("-")
                            If lineaSeparada(0) = RetPerLinea1Cod3 Then
                                Dim monto As Double
                                monto = RetPerLinea1Valor3 + CTDec(lineaSeparada(1))
                                CodigosRetPer(ubicacion) = String.Concat(RetPerLinea1Cod3, " - ", monto)
                                encontro = True
                                Exit For
                            End If
                            ubicacion = ubicacion + 1
                        Next
                        If Not encontro Then
                            Dim linea As String = String.Concat(RetPerLinea1Cod3, " - ", RetPerLinea1Valor3)
                            CodigosRetPer.Add(linea)
                        End If
                    Else
                        Dim linea As String = String.Concat(RetPerLinea1Cod3, " - ", RetPerLinea1Valor3)
                        CodigosRetPer.Add(linea)
                    End If
                End If
                If RetPerLinea1Valor4 > 0 Then
                    If CodigosRetPer.Count > 0 Then
                        Dim ubicacion As Integer = 0
                        Dim encontro As Boolean = False
                        For Each s As String In CodigosRetPer
                            Dim lineaSeparada = s.Split("-")
                            If lineaSeparada(0) = RetPerLinea1Cod4 Then
                                Dim monto As Double
                                monto = RetPerLinea1Valor4 + CTDec(lineaSeparada(1))
                                CodigosRetPer(ubicacion) = String.Concat(RetPerLinea1Cod4, " - ", monto)
                                encontro = True
                                Exit For
                            End If
                            ubicacion = ubicacion + 1
                        Next
                        If Not encontro Then
                            Dim linea As String = String.Concat(RetPerLinea1Cod4, " - ", RetPerLinea1Valor4)
                            CodigosRetPer.Add(linea)
                        End If
                    Else
                        Dim linea As String = String.Concat(RetPerLinea1Cod4, " - ", RetPerLinea1Valor4)
                        CodigosRetPer.Add(linea)
                    End If
                End If
                If RetPerLinea1Valor5 > 0 Then
                    If CodigosRetPer.Count > 0 Then
                        Dim ubicacion As Integer = 0
                        Dim encontro As Boolean = False
                        For Each s As String In CodigosRetPer
                            Dim lineaSeparada = s.Split("-")
                            If lineaSeparada(0) = RetPerLinea1Cod5 Then
                                Dim monto As Double
                                monto = RetPerLinea1Valor5 + CTDec(lineaSeparada(1))
                                CodigosRetPer(ubicacion) = String.Concat(RetPerLinea1Cod5, " - ", monto)
                                encontro = True
                                Exit For
                            End If
                            ubicacion = ubicacion + 1
                        Next
                        If Not encontro Then
                            Dim linea As String = String.Concat(RetPerLinea1Cod5, " - ", RetPerLinea1Valor5)
                            CodigosRetPer.Add(linea)
                        End If
                    Else
                        Dim linea As String = String.Concat(RetPerLinea1Cod5, " - ", RetPerLinea1Valor5)
                        CodigosRetPer.Add(linea)
                    End If
                End If
            End If

            If CTDec(TBx_MontoRetenidoPorLinea2.Text) > 0 Then
                If RetPerLinea2Valor1 > 0 Then
                    If CodigosRetPer.Count > 0 Then
                        Dim ubicacion As Integer = 0
                        Dim encontro As Boolean = False
                        For Each s As String In CodigosRetPer
                            Dim lineaSeparada = s.Split("-")
                            If lineaSeparada(0) = RetPerLinea2Cod1 Then
                                Dim monto As Double
                                monto = RetPerLinea2Valor1 + CTDec(lineaSeparada(1))
                                CodigosRetPer(ubicacion) = String.Concat(RetPerLinea2Cod1, " - ", monto)
                                encontro = True
                                Exit For
                            End If
                            ubicacion = ubicacion + 1
                        Next
                        If Not encontro Then
                            Dim linea As String = String.Concat(RetPerLinea2Cod1, " - ", RetPerLinea2Valor1)
                            CodigosRetPer.Add(linea)
                        End If
                    Else
                        Dim linea As String = String.Concat(RetPerLinea2Cod1, " - ", RetPerLinea2Valor1)
                        CodigosRetPer.Add(linea)
                    End If
                End If
                If RetPerLinea2Valor2 > 0 Then
                    If CodigosRetPer.Count > 0 Then
                        Dim ubicacion As Integer = 0
                        Dim encontro As Boolean = False
                        For Each s As String In CodigosRetPer
                            Dim lineaSeparada = s.Split("-")
                            If lineaSeparada(0) = RetPerLinea2Cod2 Then
                                Dim monto As Double
                                monto = RetPerLinea2Valor2 + CTDec(lineaSeparada(1))
                                CodigosRetPer(ubicacion) = String.Concat(RetPerLinea2Cod2, " - ", monto)
                                encontro = True
                                Exit For
                            End If
                            ubicacion = ubicacion + 1
                        Next
                        If Not encontro Then
                            Dim linea As String = String.Concat(RetPerLinea2Cod2, " - ", RetPerLinea2Valor2)
                            CodigosRetPer.Add(linea)
                        End If
                    Else
                        Dim linea As String = String.Concat(RetPerLinea2Cod2, " - ", RetPerLinea2Valor2)
                        CodigosRetPer.Add(linea)
                    End If
                End If
                If RetPerLinea2Valor3 > 0 Then
                    If CodigosRetPer.Count > 0 Then
                        Dim ubicacion As Integer = 0
                        Dim encontro As Boolean = False
                        For Each s As String In CodigosRetPer
                            Dim lineaSeparada = s.Split("-")
                            If lineaSeparada(0) = RetPerLinea2Cod3 Then
                                Dim monto As Double
                                monto = RetPerLinea2Valor3 + CTDec(lineaSeparada(1))
                                CodigosRetPer(ubicacion) = String.Concat(RetPerLinea2Cod3, " - ", monto)
                                encontro = True
                                Exit For
                            End If
                            ubicacion = ubicacion + 1
                        Next
                        If Not encontro Then
                            Dim linea As String = String.Concat(RetPerLinea2Cod3, " - ", RetPerLinea2Valor3)
                            CodigosRetPer.Add(linea)
                        End If
                    Else
                        Dim linea As String = String.Concat(RetPerLinea2Cod3, " - ", RetPerLinea2Valor3)
                        CodigosRetPer.Add(linea)
                    End If
                End If
                If RetPerLinea2Valor4 > 0 Then
                    If CodigosRetPer.Count > 0 Then
                        Dim ubicacion As Integer = 0
                        Dim encontro As Boolean = False
                        For Each s As String In CodigosRetPer
                            Dim lineaSeparada = s.Split("-")
                            If lineaSeparada(0) = RetPerLinea2Cod4 Then
                                Dim monto As Double
                                monto = RetPerLinea2Valor4 + CTDec(lineaSeparada(1))
                                CodigosRetPer(ubicacion) = String.Concat(RetPerLinea2Cod4, " - ", monto)
                                encontro = True
                                Exit For
                            End If
                            ubicacion = ubicacion + 1
                        Next
                        If Not encontro Then
                            Dim linea As String = String.Concat(RetPerLinea2Cod4, " - ", RetPerLinea2Valor4)
                            CodigosRetPer.Add(linea)
                        End If
                    Else
                        Dim linea As String = String.Concat(RetPerLinea2Cod4, " - ", RetPerLinea2Valor4)
                        CodigosRetPer.Add(linea)
                    End If
                End If
                If RetPerLinea2Valor5 > 0 Then
                    If CodigosRetPer.Count > 0 Then
                        Dim ubicacion As Integer = 0
                        Dim encontro As Boolean = False
                        For Each s As String In CodigosRetPer
                            Dim lineaSeparada = s.Split("-")
                            If lineaSeparada(0) = RetPerLinea2Cod5 Then
                                Dim monto As Double
                                monto = RetPerLinea2Valor5 + CTDec(lineaSeparada(1))
                                CodigosRetPer(ubicacion) = String.Concat(RetPerLinea2Cod5, " - ", monto)
                                encontro = True
                                Exit For
                            End If
                            ubicacion = ubicacion + 1
                        Next
                        If Not encontro Then
                            Dim linea As String = String.Concat(RetPerLinea2Cod5, " - ", RetPerLinea2Valor5)
                            CodigosRetPer.Add(linea)
                        End If
                    Else
                        Dim linea As String = String.Concat(RetPerLinea2Cod5, " - ", RetPerLinea2Valor5)
                        CodigosRetPer.Add(linea)
                    End If
                End If
            End If
            If CTDec(TBx_MontoRetenidoPorLinea3.Text) > 0 Then
                If RetPerLinea3Valor1 > 0 Then
                    If CodigosRetPer.Count > 0 Then
                        Dim ubicacion As Integer = 0
                        Dim encontro As Boolean = False
                        For Each s As String In CodigosRetPer
                            Dim lineaSeparada = s.Split("-")
                            If lineaSeparada(0) = RetPerLinea3Cod1 Then
                                Dim monto As Double
                                monto = RetPerLinea3Valor1 + CTDec(lineaSeparada(1))
                                CodigosRetPer(ubicacion) = String.Concat(RetPerLinea3Cod1, " - ", monto)
                                encontro = True
                                Exit For
                            End If
                            ubicacion = ubicacion + 1
                        Next
                        If Not encontro Then
                            Dim linea As String = String.Concat(RetPerLinea3Cod1, " - ", RetPerLinea3Valor1)
                            CodigosRetPer.Add(linea)
                        End If
                    Else
                        Dim linea As String = String.Concat(RetPerLinea3Cod1, " - ", RetPerLinea3Valor1)
                        CodigosRetPer.Add(linea)
                    End If
                End If
                If RetPerLinea3Valor2 > 0 Then
                    If CodigosRetPer.Count > 0 Then
                        Dim ubicacion As Integer = 0
                        Dim encontro As Boolean = False
                        For Each s As String In CodigosRetPer
                            Dim lineaSeparada = s.Split("-")
                            If lineaSeparada(0) = RetPerLinea3Cod2 Then
                                Dim monto As Double
                                monto = RetPerLinea3Valor2 + CTDec(lineaSeparada(1))
                                CodigosRetPer(ubicacion) = String.Concat(RetPerLinea3Cod2, " - ", monto)
                                encontro = True
                                Exit For
                            End If
                            ubicacion = ubicacion + 1
                        Next
                        If Not encontro Then
                            Dim linea As String = String.Concat(RetPerLinea3Cod2, " - ", RetPerLinea3Valor2)
                            CodigosRetPer.Add(linea)
                        End If
                    Else
                        Dim linea As String = String.Concat(RetPerLinea3Cod2, " - ", RetPerLinea3Valor2)
                        CodigosRetPer.Add(linea)
                    End If
                End If
                If RetPerLinea3Valor3 > 0 Then
                    If CodigosRetPer.Count > 0 Then
                        Dim ubicacion As Integer = 0
                        Dim encontro As Boolean = False
                        For Each s As String In CodigosRetPer
                            Dim lineaSeparada = s.Split("-")
                            If lineaSeparada(0) = RetPerLinea3Cod3 Then
                                Dim monto As Double
                                monto = RetPerLinea3Valor3 + CTDec(lineaSeparada(1))
                                CodigosRetPer(ubicacion) = String.Concat(RetPerLinea3Cod3, " - ", monto)
                                encontro = True
                                Exit For
                            End If
                            ubicacion = ubicacion + 1
                        Next
                        If Not encontro Then
                            Dim linea As String = String.Concat(RetPerLinea3Cod3, " - ", RetPerLinea3Valor3)
                            CodigosRetPer.Add(linea)
                        End If
                    Else
                        Dim linea As String = String.Concat(RetPerLinea3Cod3, " - ", RetPerLinea3Valor3)
                        CodigosRetPer.Add(linea)
                    End If
                End If
                If RetPerLinea3Valor4 > 0 Then
                    If CodigosRetPer.Count > 0 Then
                        Dim ubicacion As Integer = 0
                        Dim encontro As Boolean = False
                        For Each s As String In CodigosRetPer
                            Dim lineaSeparada = s.Split("-")
                            If lineaSeparada(0) = RetPerLinea3Cod4 Then
                                Dim monto As Double
                                monto = RetPerLinea3Valor4 + CTDec(lineaSeparada(1))
                                CodigosRetPer(ubicacion) = String.Concat(RetPerLinea3Cod4, " - ", monto)
                                encontro = True
                                Exit For
                            End If
                            ubicacion = ubicacion + 1
                        Next
                        If Not encontro Then
                            Dim linea As String = String.Concat(RetPerLinea3Cod4, " - ", RetPerLinea3Valor4)
                            CodigosRetPer.Add(linea)
                        End If
                    Else
                        Dim linea As String = String.Concat(RetPerLinea3Cod4, " - ", RetPerLinea3Valor4)
                        CodigosRetPer.Add(linea)
                    End If
                End If
                If RetPerLinea3Valor5 > 0 Then
                    If CodigosRetPer.Count > 0 Then
                        Dim ubicacion As Integer = 0
                        Dim encontro As Boolean = False

                        For Each s As String In CodigosRetPer
                            Dim lineaSeparada = s.Split("-")
                            If lineaSeparada(0) = RetPerLinea3Cod5 Then
                                Dim monto As Double
                                monto = RetPerLinea3Valor5 + CTDec(lineaSeparada(1))
                                CodigosRetPer(ubicacion) = String.Concat(RetPerLinea3Cod5, " - ", monto)
                                encontro = True
                                Exit For
                            End If
                            ubicacion = ubicacion + 1
                        Next
                        If Not encontro Then
                            Dim linea As String = String.Concat(RetPerLinea3Cod5, " - ", RetPerLinea3Valor5)
                            CodigosRetPer.Add(linea)
                        End If
                    Else
                        Dim linea As String = String.Concat(RetPerLinea3Cod5, " - ", RetPerLinea3Valor5)
                        CodigosRetPer.Add(linea)
                    End If
                End If
            End If
            If CTDec(TBx_MontoRetenidoPorLinea4.Text) > 0 Then
                If RetPerLinea4Valor1 > 0 Then
                    If CodigosRetPer.Count > 0 Then
                        Dim ubicacion As Integer = 0
                        Dim encontro As Boolean = False
                        For Each s As String In CodigosRetPer
                            Dim lineaSeparada = s.Split("-")
                            If lineaSeparada(0) = RetPerLinea4Cod1 Then
                                Dim monto As Double
                                monto = RetPerLinea4Valor1 + CTDec(lineaSeparada(1))
                                CodigosRetPer(ubicacion) = String.Concat(RetPerLinea4Cod1, " - ", monto)
                                encontro = True
                                Exit For
                            End If
                            ubicacion = ubicacion + 1
                        Next
                        If Not encontro Then
                            Dim linea As String = String.Concat(RetPerLinea4Cod1, " - ", RetPerLinea4Valor1)
                            CodigosRetPer.Add(linea)
                        End If
                    Else
                        Dim linea As String = String.Concat(RetPerLinea4Cod1, " - ", RetPerLinea4Valor1)
                        CodigosRetPer.Add(linea)
                    End If
                End If
                If RetPerLinea4Valor2 > 0 Then
                    If CodigosRetPer.Count > 0 Then
                        Dim ubicacion As Integer = 0
                        Dim encontro As Boolean = False
                        For Each s As String In CodigosRetPer
                            Dim lineaSeparada = s.Split("-")
                            If lineaSeparada(0) = RetPerLinea4Cod2 Then
                                Dim monto As Double
                                monto = RetPerLinea4Valor2 + CTDec(lineaSeparada(1))
                                CodigosRetPer(ubicacion) = String.Concat(RetPerLinea4Cod2, " - ", monto)
                                encontro = True
                                Exit For
                            End If
                            ubicacion = ubicacion + 1
                        Next
                        If Not encontro Then
                            Dim linea As String = String.Concat(RetPerLinea4Cod2, " - ", RetPerLinea4Valor2)
                            CodigosRetPer.Add(linea)
                        End If
                    Else
                        Dim linea As String = String.Concat(RetPerLinea4Cod2, " - ", RetPerLinea4Valor2)
                        CodigosRetPer.Add(linea)
                    End If
                End If
                If RetPerLinea4Valor3 > 0 Then
                    If CodigosRetPer.Count > 0 Then
                        Dim ubicacion As Integer = 0
                        Dim encontro As Boolean = False
                        For Each s As String In CodigosRetPer
                            Dim lineaSeparada = s.Split("-")
                            If lineaSeparada(0) = RetPerLinea4Cod3 Then
                                Dim monto As Double
                                monto = RetPerLinea4Valor3 + CTDec(lineaSeparada(1))
                                CodigosRetPer(ubicacion) = String.Concat(RetPerLinea4Cod3, " - ", monto)
                                encontro = True
                                Exit For
                            End If
                            ubicacion = ubicacion + 1
                        Next
                        If Not encontro Then
                            Dim linea As String = String.Concat(RetPerLinea4Cod3, " - ", RetPerLinea4Valor3)
                            CodigosRetPer.Add(linea)
                        End If
                    Else
                        Dim linea As String = String.Concat(RetPerLinea4Cod3, " - ", RetPerLinea4Valor3)
                        CodigosRetPer.Add(linea)
                    End If
                End If
                If RetPerLinea4Valor4 > 0 Then
                    If CodigosRetPer.Count > 0 Then
                        Dim ubicacion As Integer = 0
                        Dim encontro As Boolean = False
                        For Each s As String In CodigosRetPer
                            Dim lineaSeparada = s.Split("-")
                            If lineaSeparada(0) = RetPerLinea4Cod4 Then
                                Dim monto As Double
                                monto = RetPerLinea4Valor4 + CTDec(lineaSeparada(1))
                                CodigosRetPer(ubicacion) = String.Concat(RetPerLinea4Cod4, " - ", monto)
                                encontro = True
                                Exit For
                            End If
                            ubicacion = ubicacion + 1
                        Next
                        If Not encontro Then
                            Dim linea As String = String.Concat(RetPerLinea4Cod4, " - ", RetPerLinea4Valor4)
                            CodigosRetPer.Add(linea)
                        End If
                    Else
                        Dim linea As String = String.Concat(RetPerLinea4Cod4, " - ", RetPerLinea4Valor4)
                        CodigosRetPer.Add(linea)
                    End If
                End If
                If RetPerLinea4Valor5 > 0 Then
                    If CodigosRetPer.Count > 0 Then
                        Dim ubicacion As Integer = 0
                        Dim encontro As Boolean = False
                        For Each s As String In CodigosRetPer
                            Dim lineaSeparada = s.Split("-")
                            If lineaSeparada(0) = RetPerLinea4Cod5 Then
                                Dim monto As Double
                                monto = RetPerLinea4Valor5 + CTDec(lineaSeparada(1))
                                CodigosRetPer(ubicacion) = String.Concat(RetPerLinea4Cod5, " - ", monto)
                                encontro = True
                                Exit For
                            End If
                            ubicacion = ubicacion + 1
                        Next
                        If Not encontro Then
                            Dim linea As String = String.Concat(RetPerLinea4Cod5, " - ", RetPerLinea4Valor5)
                            CodigosRetPer.Add(linea)
                        End If
                    Else
                        Dim linea As String = String.Concat(RetPerLinea4Cod5, " - ", RetPerLinea4Valor5)
                        CodigosRetPer.Add(linea)
                    End If
                End If
            End If
            If CTDec(TBx_MontoRetenidoPorLinea5.Text) > 0 Then
                If RetPerLinea5Valor1 > 0 Then
                    If CodigosRetPer.Count > 0 Then
                        Dim ubicacion As Integer = 0
                        Dim encontro As Boolean = False
                        For Each s As String In CodigosRetPer
                            Dim lineaSeparada = s.Split("-")
                            If lineaSeparada(0) = RetPerLinea5Cod1 Then
                                Dim monto As Double
                                monto = RetPerLinea5Valor1 + CTDec(lineaSeparada(1))
                                CodigosRetPer(ubicacion) = String.Concat(RetPerLinea5Cod1, " - ", monto)
                                encontro = True
                                Exit For
                            End If
                            ubicacion = ubicacion + 1
                        Next
                        If Not encontro Then
                            Dim linea As String = String.Concat(RetPerLinea5Cod1, " - ", RetPerLinea5Valor1)
                            CodigosRetPer.Add(linea)
                        End If
                    Else
                        Dim linea As String = String.Concat(RetPerLinea5Cod1, " - ", RetPerLinea5Valor1)
                        CodigosRetPer.Add(linea)
                    End If
                End If
                If RetPerLinea5Valor2 > 0 Then
                    If CodigosRetPer.Count > 0 Then
                        Dim ubicacion As Integer = 0
                        Dim encontro As Boolean = False
                        For Each s As String In CodigosRetPer
                            Dim lineaSeparada = s.Split("-")
                            If lineaSeparada(0) = RetPerLinea5Cod2 Then
                                Dim monto As Double
                                monto = RetPerLinea5Valor2 + CTDec(lineaSeparada(1))
                                CodigosRetPer(ubicacion) = String.Concat(RetPerLinea5Cod2, " - ", monto)
                                encontro = True
                                Exit For
                            End If
                            ubicacion = ubicacion + 1
                        Next
                        If Not encontro Then
                            Dim linea As String = String.Concat(RetPerLinea5Cod2, " - ", RetPerLinea5Valor2)
                            CodigosRetPer.Add(linea)
                        End If
                    Else
                        Dim linea As String = String.Concat(RetPerLinea5Cod2, " - ", RetPerLinea5Valor2)
                        CodigosRetPer.Add(linea)
                    End If
                End If
                If RetPerLinea5Valor3 > 0 Then
                    If CodigosRetPer.Count > 0 Then
                        Dim ubicacion As Integer = 0
                        Dim encontro As Boolean = False
                        For Each s As String In CodigosRetPer
                            Dim lineaSeparada = s.Split("-")
                            If lineaSeparada(0) = RetPerLinea5Cod3 Then
                                Dim monto As Double
                                monto = RetPerLinea5Valor3 + CTDec(lineaSeparada(1))
                                CodigosRetPer(ubicacion) = String.Concat(RetPerLinea5Cod3, " - ", monto)
                                encontro = True
                                Exit For
                            End If
                            ubicacion = ubicacion + 1
                        Next
                        If Not encontro Then
                            Dim linea As String = String.Concat(RetPerLinea5Cod3, " - ", RetPerLinea5Valor3)
                            CodigosRetPer.Add(linea)
                        End If
                    Else
                        Dim linea As String = String.Concat(RetPerLinea5Cod3, " - ", RetPerLinea5Valor3)
                        CodigosRetPer.Add(linea)
                    End If
                End If
                If RetPerLinea5Valor4 > 0 Then
                    If CodigosRetPer.Count > 0 Then
                        Dim ubicacion As Integer = 0
                        Dim encontro As Boolean = False
                        For Each s As String In CodigosRetPer
                            Dim lineaSeparada = s.Split("-")
                            If lineaSeparada(0) = RetPerLinea5Cod4 Then
                                Dim monto As Double
                                monto = RetPerLinea5Valor4 + CTDec(lineaSeparada(1))
                                CodigosRetPer(ubicacion) = String.Concat(RetPerLinea5Cod4, " - ", monto)
                                encontro = True
                                Exit For
                            End If
                            ubicacion = ubicacion + 1
                        Next
                        If Not encontro Then
                            Dim linea As String = String.Concat(RetPerLinea5Cod4, " - ", RetPerLinea5Valor4)
                            CodigosRetPer.Add(linea)
                        End If
                    Else
                        Dim linea As String = String.Concat(RetPerLinea5Cod4, " - ", RetPerLinea5Valor4)
                        CodigosRetPer.Add(linea)
                    End If
                End If
                If RetPerLinea5Valor5 > 0 Then
                    If CodigosRetPer.Count > 0 Then
                        Dim ubicacion As Integer = 0
                        Dim encontro As Boolean = False
                        For Each s As String In CodigosRetPer
                            Dim lineaSeparada = s.Split("-")
                            If lineaSeparada(0) = RetPerLinea5Cod5 Then
                                Dim monto As Double
                                monto = RetPerLinea5Valor5 + CTDec(lineaSeparada(1))
                                CodigosRetPer(ubicacion) = String.Concat(RetPerLinea5Cod5, " - ", monto)
                                encontro = True
                                Exit For
                            End If
                            ubicacion = ubicacion + 1
                        Next
                        If Not encontro Then
                            Dim linea As String = String.Concat(RetPerLinea5Cod5, " - ", RetPerLinea5Valor5)
                            CodigosRetPer.Add(linea)
                        End If
                    Else
                        Dim linea As String = String.Concat(RetPerLinea5Cod5, " - ", RetPerLinea5Valor5)
                        CodigosRetPer.Add(linea)
                    End If
                End If
            End If
            If CTDec(TBx_MontoRetenidoPorLinea6.Text) > 0 Then
                If RetPerLinea6Valor1 > 0 Then
                    If CodigosRetPer.Count > 0 Then
                        Dim ubicacion As Integer = 0
                        Dim encontro As Boolean = False
                        For Each s As String In CodigosRetPer
                            Dim lineaSeparada = s.Split("-")
                            If lineaSeparada(0) = RetPerLinea6Cod1 Then
                                Dim monto As Double
                                monto = RetPerLinea6Valor1 + CTDec(lineaSeparada(1))
                                CodigosRetPer(ubicacion) = String.Concat(RetPerLinea6Cod1, " - ", monto)
                                encontro = True
                                Exit For
                            End If
                            ubicacion = ubicacion + 1
                        Next
                        If Not encontro Then
                            Dim linea As String = String.Concat(RetPerLinea6Cod1, " - ", RetPerLinea6Valor1)
                            CodigosRetPer.Add(linea)
                        End If
                    Else
                        Dim linea As String = String.Concat(RetPerLinea6Cod1, " - ", RetPerLinea6Valor1)
                        CodigosRetPer.Add(linea)
                    End If
                End If
                If RetPerLinea6Valor2 > 0 Then
                    If CodigosRetPer.Count > 0 Then
                        Dim ubicacion As Integer = 0
                        Dim encontro As Boolean = False
                        For Each s As String In CodigosRetPer
                            Dim lineaSeparada = s.Split("-")
                            If lineaSeparada(0) = RetPerLinea6Cod2 Then
                                Dim monto As Double
                                monto = RetPerLinea6Valor2 + CTDec(lineaSeparada(1))
                                CodigosRetPer(ubicacion) = String.Concat(RetPerLinea6Cod2, " - ", monto)
                                encontro = True
                                Exit For
                            End If
                            ubicacion = ubicacion + 1
                        Next
                        If Not encontro Then
                            Dim linea As String = String.Concat(RetPerLinea6Cod2, " - ", RetPerLinea6Valor2)
                            CodigosRetPer.Add(linea)
                        End If
                    Else
                        Dim linea As String = String.Concat(RetPerLinea6Cod2, " - ", RetPerLinea6Valor2)
                        CodigosRetPer.Add(linea)
                    End If
                End If
                If RetPerLinea6Valor3 > 0 Then
                    If CodigosRetPer.Count > 0 Then
                        Dim ubicacion As Integer = 0
                        Dim encontro As Boolean = False
                        For Each s As String In CodigosRetPer
                            Dim lineaSeparada = s.Split("-")
                            If lineaSeparada(0) = RetPerLinea6Cod3 Then
                                Dim monto As Double
                                monto = RetPerLinea6Valor3 + CTDec(lineaSeparada(1))
                                CodigosRetPer(ubicacion) = String.Concat(RetPerLinea6Cod3, " - ", monto)
                                encontro = True
                                Exit For
                            End If
                            ubicacion = ubicacion + 1
                        Next
                        If Not encontro Then
                            Dim linea As String = String.Concat(RetPerLinea6Cod3, " - ", RetPerLinea6Valor3)
                            CodigosRetPer.Add(linea)
                        End If
                    Else
                        Dim linea As String = String.Concat(RetPerLinea6Cod3, " - ", RetPerLinea6Valor3)
                        CodigosRetPer.Add(linea)
                    End If
                End If
                If RetPerLinea6Valor4 > 0 Then
                    If CodigosRetPer.Count > 0 Then
                        Dim ubicacion As Integer = 0
                        Dim encontro As Boolean = False
                        For Each s As String In CodigosRetPer
                            Dim lineaSeparada = s.Split("-")
                            If lineaSeparada(0) = RetPerLinea6Cod4 Then
                                Dim monto As Double
                                monto = RetPerLinea6Valor4 + CTDec(lineaSeparada(1))
                                CodigosRetPer(ubicacion) = String.Concat(RetPerLinea6Cod4, " - ", monto)
                                encontro = True

                                Exit For
                            End If
                            ubicacion = ubicacion + 1
                        Next
                        If Not encontro Then
                            Dim linea As String = String.Concat(RetPerLinea6Cod4, " - ", RetPerLinea6Valor4)
                            CodigosRetPer.Add(linea)
                        End If
                    Else
                        Dim linea As String = String.Concat(RetPerLinea6Cod4, " - ", RetPerLinea6Valor4)
                        CodigosRetPer.Add(linea)
                    End If
                End If
                If RetPerLinea6Valor5 > 0 Then
                    If CodigosRetPer.Count > 0 Then
                        Dim ubicacion As Integer = 0
                        Dim encontro As Boolean = False
                        For Each s As String In CodigosRetPer
                            Dim lineaSeparada = s.Split("-")
                            If lineaSeparada(0) = RetPerLinea6Cod5 Then
                                Dim monto As Double
                                monto = RetPerLinea6Valor5 + CTDec(lineaSeparada(1))

                                CodigosRetPer(ubicacion) = String.Concat(RetPerLinea6Cod5, " - ", monto)
                                encontro = True
                                Exit For
                            End If
                            ubicacion = ubicacion + 1
                        Next
                        If Not encontro Then
                            Dim linea As String = String.Concat(RetPerLinea6Cod5, " - ", RetPerLinea6Valor5)
                            CodigosRetPer.Add(linea)
                        End If
                    Else
                        Dim linea As String = String.Concat(RetPerLinea6Cod5, " - ", RetPerLinea6Valor5)
                        CodigosRetPer.Add(linea)
                    End If
                End If
            End If
            If CTDec(TBx_MontoRetenidoPorLinea7.Text) > 0 Then
                If RetPerLinea7Valor1 > 0 Then
                    If CodigosRetPer.Count > 0 Then
                        Dim ubicacion As Integer = 0
                        Dim encontro As Boolean = False
                        For Each s As String In CodigosRetPer
                            Dim lineaSeparada = s.Split("-")
                            If lineaSeparada(0) = RetPerLinea7Cod1 Then
                                Dim monto As Double
                                monto = RetPerLinea7Valor1 + CTDec(lineaSeparada(1))
                                CodigosRetPer(ubicacion) = String.Concat(RetPerLinea7Cod1, " - ", monto)
                                encontro = True
                                Exit For
                            End If
                            ubicacion = ubicacion + 1
                        Next
                        If Not encontro Then
                            Dim linea As String = String.Concat(RetPerLinea7Cod1, " - ", RetPerLinea7Valor1)
                            CodigosRetPer.Add(linea)
                        End If
                    Else
                        Dim linea As String = String.Concat(RetPerLinea7Cod1, " - ", RetPerLinea7Valor1)
                        CodigosRetPer.Add(linea)
                    End If
                End If
                If RetPerLinea7Valor2 > 0 Then
                    If CodigosRetPer.Count > 0 Then
                        Dim ubicacion As Integer = 0
                        Dim encontro As Boolean = False
                        For Each s As String In CodigosRetPer
                            Dim lineaSeparada = s.Split("-")
                            If lineaSeparada(0) = RetPerLinea7Cod2 Then
                                Dim monto As Double
                                monto = RetPerLinea7Valor2 + CTDec(lineaSeparada(1))
                                CodigosRetPer(ubicacion) = String.Concat(RetPerLinea7Cod2, " - ", monto)
                                encontro = True
                                Exit For
                            End If
                            ubicacion = ubicacion + 1
                        Next
                        If Not encontro Then
                            Dim linea As String = String.Concat(RetPerLinea7Cod2, " - ", RetPerLinea7Valor2)
                            CodigosRetPer.Add(linea)
                        End If
                    Else
                        Dim linea As String = String.Concat(RetPerLinea7Cod2, " - ", RetPerLinea7Valor2)
                        CodigosRetPer.Add(linea)
                    End If
                End If
                If RetPerLinea7Valor3 > 0 Then
                    If CodigosRetPer.Count > 0 Then
                        Dim ubicacion As Integer = 0
                        Dim encontro As Boolean = False
                        For Each s As String In CodigosRetPer
                            Dim lineaSeparada = s.Split("-")
                            If lineaSeparada(0) = RetPerLinea7Cod3 Then
                                Dim monto As Double
                                monto = RetPerLinea7Valor3 + CTDec(lineaSeparada(1))
                                CodigosRetPer(ubicacion) = String.Concat(RetPerLinea7Cod3, " - ", monto)
                                encontro = True
                                Exit For
                            End If
                            ubicacion = ubicacion + 1
                        Next
                        If Not encontro Then
                            Dim linea As String = String.Concat(RetPerLinea7Cod3, " - ", RetPerLinea7Valor3)
                            CodigosRetPer.Add(linea)
                        End If
                    Else
                        Dim linea As String = String.Concat(RetPerLinea7Cod3, " - ", RetPerLinea7Valor3)
                        CodigosRetPer.Add(linea)
                    End If
                End If
                If RetPerLinea7Valor4 > 0 Then
                    If CodigosRetPer.Count > 0 Then
                        Dim ubicacion As Integer = 0
                        Dim encontro As Boolean = False
                        For Each s As String In CodigosRetPer
                            Dim lineaSeparada = s.Split("-")
                            If lineaSeparada(0) = RetPerLinea7Cod4 Then
                                Dim monto As Double
                                monto = RetPerLinea7Valor4 + CTDec(lineaSeparada(1))
                                CodigosRetPer(ubicacion) = String.Concat(RetPerLinea7Cod4, " - ", monto)
                                encontro = True
                                Exit For
                            End If
                            ubicacion = ubicacion + 1
                        Next
                        If Not encontro Then
                            Dim linea As String = String.Concat(RetPerLinea7Cod4, " - ", RetPerLinea7Valor4)
                            CodigosRetPer.Add(linea)
                        End If
                    Else
                        Dim linea As String = String.Concat(RetPerLinea7Cod4, " - ", RetPerLinea7Valor4)
                        CodigosRetPer.Add(linea)
                    End If
                End If
                If RetPerLinea7Valor5 > 0 Then
                    If CodigosRetPer.Count > 0 Then
                        Dim encontro As Boolean = False
                        Dim ubicacion As Integer = 0
                        For Each s As String In CodigosRetPer
                            Dim lineaSeparada = s.Split("-")
                            If lineaSeparada(0) = RetPerLinea7Cod5 Then
                                Dim monto As Double
                                monto = RetPerLinea7Valor5 + CTDec(lineaSeparada(1))
                                CodigosRetPer(ubicacion) = String.Concat(RetPerLinea7Cod5, " - ", monto)
                                encontro = True
                                Exit For
                            End If
                            ubicacion = ubicacion + 1
                        Next
                        If Not encontro Then
                            Dim linea As String = String.Concat(RetPerLinea7Cod5, " - ", RetPerLinea7Valor5)
                            CodigosRetPer.Add(linea)
                        End If
                    Else
                        Dim linea As String = String.Concat(RetPerLinea7Cod5, " - ", RetPerLinea7Valor5)
                        CodigosRetPer.Add(linea)
                    End If
                End If
            End If
            If CTDec(TBx_MontoRetenidoPorLinea8.Text) > 0 Then
                If RetPerLinea8Valor1 > 0 Then
                    If CodigosRetPer.Count > 0 Then
                        Dim ubicacion As Integer = 0
                        Dim encontro As Boolean = False
                        For Each s As String In CodigosRetPer
                            Dim lineaSeparada = s.Split("-")
                            If lineaSeparada(0) = RetPerLinea8Cod1 Then
                                Dim monto As Double
                                monto = RetPerLinea8Valor1 + CTDec(lineaSeparada(1))
                                CodigosRetPer(ubicacion) = String.Concat(RetPerLinea8Cod1, " - ", monto)
                                encontro = True
                                Exit For
                            End If
                            ubicacion = ubicacion + 1
                        Next
                        If Not encontro Then
                            Dim linea As String = String.Concat(RetPerLinea8Cod1, " - ", RetPerLinea8Valor1)
                            CodigosRetPer.Add(linea)
                        End If
                    Else
                        Dim linea As String = String.Concat(RetPerLinea8Cod1, " - ", RetPerLinea8Valor1)
                        CodigosRetPer.Add(linea)
                    End If
                End If
                If RetPerLinea8Valor2 > 0 Then
                    If CodigosRetPer.Count > 0 Then
                        Dim ubicacion As Integer = 0
                        Dim encontro As Boolean = False
                        For Each s As String In CodigosRetPer
                            Dim lineaSeparada = s.Split("-")
                            If lineaSeparada(0) = RetPerLinea8Cod2 Then
                                Dim monto As Double
                                monto = RetPerLinea8Valor2 + CTDec(lineaSeparada(1))
                                CodigosRetPer(ubicacion) = String.Concat(RetPerLinea8Cod2, " - ", monto)
                                encontro = True
                                Exit For
                            End If
                            ubicacion = ubicacion + 1
                        Next
                        If Not encontro Then
                            Dim linea As String = String.Concat(RetPerLinea8Cod2, " - ", RetPerLinea8Valor2)
                            CodigosRetPer.Add(linea)
                        End If
                    Else
                        Dim linea As String = String.Concat(RetPerLinea8Cod2, " - ", RetPerLinea8Valor2)
                        CodigosRetPer.Add(linea)
                    End If
                End If
                If RetPerLinea8Valor3 > 0 Then
                    If CodigosRetPer.Count > 0 Then
                        Dim ubicacion As Integer = 0
                        Dim encontro As Boolean = False
                        For Each s As String In CodigosRetPer
                            Dim lineaSeparada = s.Split("-")
                            If lineaSeparada(0) = RetPerLinea8Cod3 Then
                                Dim monto As Double
                                monto = RetPerLinea8Valor3 + CTDec(lineaSeparada(1))
                                CodigosRetPer(ubicacion) = String.Concat(RetPerLinea8Cod3, " - ", monto)
                                encontro = True
                                Exit For
                            End If
                            ubicacion = ubicacion + 1
                        Next
                        If Not encontro Then
                            Dim linea As String = String.Concat(RetPerLinea8Cod3, " - ", RetPerLinea8Valor3)
                            CodigosRetPer.Add(linea)
                        End If
                    Else
                        Dim linea As String = String.Concat(RetPerLinea8Cod3, " - ", RetPerLinea8Valor3)
                        CodigosRetPer.Add(linea)
                    End If
                End If
                If RetPerLinea8Valor4 > 0 Then
                    If CodigosRetPer.Count > 0 Then
                        Dim ubicacion As Integer = 0

                        Dim encontro As Boolean = False
                        For Each s As String In CodigosRetPer
                            Dim lineaSeparada = s.Split("-")
                            If lineaSeparada(0) = RetPerLinea8Cod4 Then
                                Dim monto As Double
                                monto = RetPerLinea8Valor4 + CTDec(lineaSeparada(1))
                                CodigosRetPer(ubicacion) = String.Concat(RetPerLinea8Cod4, " - ", monto)
                                encontro = True
                                Exit For
                            End If
                            ubicacion = ubicacion + 1
                        Next
                        If Not encontro Then
                            Dim linea As String = String.Concat(RetPerLinea8Cod4, " - ", RetPerLinea8Valor4)
                            CodigosRetPer.Add(linea)
                        End If
                    Else
                        Dim linea As String = String.Concat(RetPerLinea8Cod4, " - ", RetPerLinea8Valor4)
                        CodigosRetPer.Add(linea)
                    End If
                End If
                If RetPerLinea8Valor5 > 0 Then
                    If CodigosRetPer.Count > 0 Then
                        Dim ubicacion As Integer = 0
                        Dim encontro As Boolean = False
                        For Each s As String In CodigosRetPer
                            Dim lineaSeparada = s.Split("-")
                            If lineaSeparada(0) = RetPerLinea8Cod5 Then
                                Dim monto As Double
                                monto = RetPerLinea8Valor5 + CTDec(lineaSeparada(1))
                                CodigosRetPer(ubicacion) = String.Concat(RetPerLinea8Cod5, " - ", monto)
                                encontro = True
                                Exit For
                            End If
                            ubicacion = ubicacion + 1
                        Next
                        If Not encontro Then
                            Dim linea As String = String.Concat(RetPerLinea8Cod5, " - ", RetPerLinea8Valor5)
                            CodigosRetPer.Add(linea)
                        End If
                    Else
                        Dim linea As String = String.Concat(RetPerLinea8Cod5, " - ", RetPerLinea8Valor5)
                        CodigosRetPer.Add(linea)
                    End If
                End If
            End If
            If CTDec(TBx_MontoRetenidoPorLinea9.Text) > 0 Then
                If RetPerLinea9Valor1 > 0 Then
                    If CodigosRetPer.Count > 0 Then
                        Dim ubicacion As Integer = 0
                        Dim encontro As Boolean = False
                        For Each s As String In CodigosRetPer
                            Dim lineaSeparada = s.Split("-")
                            If lineaSeparada(0) = RetPerLinea9Cod1 Then
                                Dim monto As Double
                                monto = RetPerLinea9Valor1 + CTDec(lineaSeparada(1))
                                CodigosRetPer(ubicacion) = String.Concat(RetPerLinea9Cod1, " - ", monto)
                                encontro = True
                                Exit For
                            End If
                            ubicacion = ubicacion + 1
                        Next
                        If Not encontro Then
                            Dim linea As String = String.Concat(RetPerLinea9Cod1, " - ", RetPerLinea9Valor1)
                            CodigosRetPer.Add(linea)
                        End If
                    Else
                        Dim linea As String = String.Concat(RetPerLinea9Cod1, " - ", RetPerLinea9Valor1)
                        CodigosRetPer.Add(linea)
                    End If
                End If
                If RetPerLinea9Valor2 > 0 Then
                    If CodigosRetPer.Count > 0 Then
                        Dim ubicacion As Integer = 0
                        Dim encontro As Boolean = False
                        For Each s As String In CodigosRetPer
                            Dim lineaSeparada = s.Split("-")
                            If lineaSeparada(0) = RetPerLinea9Cod2 Then
                                Dim monto As Double
                                monto = RetPerLinea9Valor2 + CTDec(lineaSeparada(1))
                                CodigosRetPer(ubicacion) = String.Concat(RetPerLinea9Cod2, " - ", monto)
                                encontro = True
                                Exit For
                            End If
                            ubicacion = ubicacion + 1
                        Next
                        If Not encontro Then
                            Dim linea As String = String.Concat(RetPerLinea9Cod2, " - ", RetPerLinea9Valor2)
                            CodigosRetPer.Add(linea)
                        End If
                    Else
                        Dim linea As String = String.Concat(RetPerLinea9Cod2, " - ", RetPerLinea9Valor2)
                        CodigosRetPer.Add(linea)
                    End If
                End If
                If RetPerLinea9Valor3 > 0 Then
                    If CodigosRetPer.Count > 0 Then
                        Dim encontro As Boolean = False
                        Dim ubicacion As Integer = 0
                        For Each s As String In CodigosRetPer
                            Dim lineaSeparada = s.Split("-")
                            If lineaSeparada(0) = RetPerLinea9Cod3 Then
                                Dim monto As Double
                                monto = RetPerLinea9Valor3 + CTDec(lineaSeparada(1))
                                CodigosRetPer(ubicacion) = String.Concat(RetPerLinea9Cod3, " - ", monto)
                                encontro = True
                                Exit For
                            End If
                            ubicacion = ubicacion + 1
                        Next
                        If Not encontro Then
                            Dim linea As String = String.Concat(RetPerLinea9Cod3, " - ", RetPerLinea9Valor3)
                            CodigosRetPer.Add(linea)
                        End If
                    Else
                        Dim linea As String = String.Concat(RetPerLinea9Cod3, " - ", RetPerLinea9Valor3)
                        CodigosRetPer.Add(linea)
                    End If
                End If
                If RetPerLinea9Valor4 > 0 Then
                    If CodigosRetPer.Count > 0 Then
                        Dim ubicacion As Integer = 0
                        Dim encontro As Boolean = False
                        For Each s As String In CodigosRetPer
                            Dim lineaSeparada = s.Split("-")
                            If lineaSeparada(0) = RetPerLinea9Cod4 Then
                                Dim monto As Double
                                monto = RetPerLinea9Valor4 + CTDec(lineaSeparada(1))
                                CodigosRetPer(ubicacion) = String.Concat(RetPerLinea9Cod4, " - ", monto)
                                encontro = True
                                Exit For
                            End If
                            ubicacion = ubicacion + 1
                        Next
                        If Not encontro Then
                            Dim linea As String = String.Concat(RetPerLinea9Cod4, " - ", RetPerLinea9Valor4)
                            CodigosRetPer.Add(linea)
                        End If
                    Else
                        Dim linea As String = String.Concat(RetPerLinea9Cod4, " - ", RetPerLinea9Valor4)
                        CodigosRetPer.Add(linea)
                    End If
                End If
                If RetPerLinea9Valor5 > 0 Then
                    If CodigosRetPer.Count > 0 Then
                        Dim ubicacion As Integer = 0
                        Dim encontro As Boolean = False
                        For Each s As String In CodigosRetPer
                            Dim lineaSeparada = s.Split("-")
                            If lineaSeparada(0) = RetPerLinea9Cod5 Then
                                Dim monto As Double
                                monto = RetPerLinea9Valor5 + CTDec(lineaSeparada(1))
                                CodigosRetPer(ubicacion) = String.Concat(RetPerLinea9Cod5, " - ", monto)
                                encontro = True
                                Exit For
                            End If
                            ubicacion = ubicacion + 1
                        Next
                        If Not encontro Then
                            Dim linea As String = String.Concat(RetPerLinea9Cod5, " - ", RetPerLinea9Valor5)
                            CodigosRetPer.Add(linea)
                        End If
                    Else
                        Dim linea As String = String.Concat(RetPerLinea9Cod5, " - ", RetPerLinea9Valor5)
                        CodigosRetPer.Add(linea)
                    End If
                End If
            End If
            If CTDec(TBx_MontoRetenidoPorLinea10.Text) > 0 Then
                If RetPerLinea10Valor1 > 0 Then
                    If CodigosRetPer.Count > 0 Then
                        Dim ubicacion As Integer = 0
                        Dim encontro As Boolean = False
                        For Each s As String In CodigosRetPer
                            Dim lineaSeparada = s.Split("-")
                            If lineaSeparada(0) = RetPerLinea10Cod1 Then
                                Dim monto As Double
                                monto = RetPerLinea10Valor1 + CTDec(lineaSeparada(1))
                                CodigosRetPer(ubicacion) = String.Concat(RetPerLinea10Cod1, " - ", monto)
                                encontro = True
                                Exit For
                            End If
                            ubicacion = ubicacion + 1
                        Next
                        If Not encontro Then
                            Dim linea As String = String.Concat(RetPerLinea10Cod1, " - ", RetPerLinea10Valor1)
                            CodigosRetPer.Add(linea)
                        End If
                    Else
                        Dim linea As String = String.Concat(RetPerLinea10Cod1, " - ", RetPerLinea10Valor1)
                        CodigosRetPer.Add(linea)
                    End If
                End If
                If RetPerLinea10Valor2 > 0 Then
                    If CodigosRetPer.Count > 0 Then
                        Dim ubicacion As Integer = 0
                        Dim encontro As Boolean = False
                        For Each s As String In CodigosRetPer
                            Dim lineaSeparada = s.Split("-")
                            If lineaSeparada(0) = RetPerLinea10Cod2 Then
                                Dim monto As Double
                                monto = RetPerLinea10Valor2 + CTDec(lineaSeparada(1))
                                CodigosRetPer(ubicacion) = String.Concat(RetPerLinea10Cod2, " - ", monto)
                                encontro = True
                                Exit For
                            End If
                            ubicacion = ubicacion + 1
                        Next
                        If Not encontro Then
                            Dim linea As String = String.Concat(RetPerLinea10Cod2, " - ", RetPerLinea10Valor2)
                            CodigosRetPer.Add(linea)
                        End If
                    Else
                        Dim linea As String = String.Concat(RetPerLinea10Cod2, " - ", RetPerLinea10Valor2)
                        CodigosRetPer.Add(linea)
                    End If
                End If
                If RetPerLinea10Valor3 > 0 Then
                    If CodigosRetPer.Count > 0 Then
                        Dim ubicacion As Integer = 0
                        Dim encontro As Boolean = False
                        For Each s As String In CodigosRetPer
                            Dim lineaSeparada = s.Split("-")
                            If lineaSeparada(0) = RetPerLinea10Cod3 Then
                                Dim monto As Double
                                monto = RetPerLinea10Valor3 + CTDec(lineaSeparada(1))
                                CodigosRetPer(ubicacion) = String.Concat(RetPerLinea10Cod3, " - ", monto)
                                encontro = True
                                Exit For
                            End If
                            ubicacion = ubicacion + 1
                        Next
                        If Not encontro Then
                            Dim linea As String = String.Concat(RetPerLinea10Cod3, " - ", RetPerLinea10Valor3)
                            CodigosRetPer.Add(linea)
                        End If
                    Else
                        Dim linea As String = String.Concat(RetPerLinea10Cod3, " - ", RetPerLinea10Valor3)
                        CodigosRetPer.Add(linea)
                    End If
                End If
                If RetPerLinea10Valor4 > 0 Then
                    If CodigosRetPer.Count > 0 Then
                        Dim ubicacion As Integer = 0
                        Dim encontro As Boolean = False
                        For Each s As String In CodigosRetPer
                            Dim lineaSeparada = s.Split("-")
                            If lineaSeparada(0) = RetPerLinea10Cod4 Then
                                Dim monto As Double
                                monto = RetPerLinea10Valor4 + CTDec(lineaSeparada(1))
                                CodigosRetPer(ubicacion) = String.Concat(RetPerLinea10Cod4, " - ", monto)
                                encontro = True
                                Exit For
                            End If
                            ubicacion = ubicacion + 1
                        Next
                        If Not encontro Then
                            Dim linea As String = String.Concat(RetPerLinea10Cod4, " - ", RetPerLinea10Valor4)
                            CodigosRetPer.Add(linea)
                        End If
                    Else
                        Dim linea As String = String.Concat(RetPerLinea10Cod4, " - ", RetPerLinea10Valor4)
                        CodigosRetPer.Add(linea)
                    End If
                End If
                If RetPerLinea10Valor5 > 0 Then
                    If CodigosRetPer.Count > 0 Then
                        Dim ubicacion As Integer = 0
                        Dim encontro As Boolean = False
                        For Each s As String In CodigosRetPer
                            Dim lineaSeparada = s.Split("-")
                            If lineaSeparada(0) = RetPerLinea10Cod5 Then
                                Dim monto As Double
                                monto = RetPerLinea10Valor5 + CTDec(lineaSeparada(1))
                                CodigosRetPer(ubicacion) = String.Concat(RetPerLinea10Cod5, " - ", monto)
                                encontro = True
                                Exit For
                            End If
                            ubicacion = ubicacion + 1
                        Next
                        If Not encontro Then
                            Dim linea As String = String.Concat(RetPerLinea10Cod5, " - ", RetPerLinea10Valor5)
                            CodigosRetPer.Add(linea)
                        End If
                    Else
                        Dim linea As String = String.Concat(RetPerLinea10Cod5, " - ", RetPerLinea10Valor5)
                        CodigosRetPer.Add(linea)
                    End If
                End If
            End If
            For Each s As String In CodigosRetPer
                LBx_RetencionesPercepciones.Items.Add(s)
            Next

        End If

    End Sub

    Private Sub Ckb_Exportacion_CheckedChanged(sender As Object, e As EventArgs) Handles Ckb_Exportacion.CheckedChanged
        If Ckb_Exportacion.Checked Then
            CalcularTotales()
            RapidacmbIndicadorFacturacion1.Text = "Exp-Asim"
            RapidacmbIndicadorFacturacion2.Text = "Exp-Asim"
            RapidacmbIndicadorFacturacion3.Text = "Exp-Asim"
            RapidacmbIndicadorFacturacion4.Text = "Exp-Asim"
            RapidacmbIndicadorFacturacion5.Text = "Exp-Asim"
            RapidacmbIndicadorFacturacion6.Text = "Exp-Asim"
            RapidacmbIndicadorFacturacion7.Text = "Exp-Asim"
            RapidacmbIndicadorFacturacion8.Text = "Exp-Asim"
            RapidacmbIndicadorFacturacion9.Text = "Exp-Asim"
            RapidacmbIndicadorFacturacion10.Text = "Exp-Asim"
            GBx_RetencionesPercepciones.Visible = False
            GBx_Exportaciones.Visible = True
            GBx_Exportaciones.Location = New Point(787, 267)
            If objConfig.USASUBTOTALES = 0 And objConfig.USARETENCIONPERCEPCION = 0 Then
                Lbl_Comentarios.Location = New Point(787, 373)
                RapidatxtAdenda.Location = New Point(787, 389)
                RapidatxtAdenda.Size = New System.Drawing.Size(288, 102)
            End If
        Else
            CalcularTotales()
            ArreglarVistaPantalla()
            If Not objConfig.ImpuestoPorDefecto = "" Then
                RapidacmbIndicadorFacturacion1.Text = objConfig.ImpuestoPorDefecto
                RapidacmbIndicadorFacturacion2.Text = objConfig.ImpuestoPorDefecto
                RapidacmbIndicadorFacturacion3.Text = objConfig.ImpuestoPorDefecto
                RapidacmbIndicadorFacturacion4.Text = objConfig.ImpuestoPorDefecto
                RapidacmbIndicadorFacturacion5.Text = objConfig.ImpuestoPorDefecto
                RapidacmbIndicadorFacturacion6.Text = objConfig.ImpuestoPorDefecto
                RapidacmbIndicadorFacturacion7.Text = objConfig.ImpuestoPorDefecto
                RapidacmbIndicadorFacturacion8.Text = objConfig.ImpuestoPorDefecto
                RapidacmbIndicadorFacturacion9.Text = objConfig.ImpuestoPorDefecto
                RapidacmbIndicadorFacturacion10.Text = objConfig.ImpuestoPorDefecto
            End If
        End If
    End Sub

    Private Sub LimpiarRetencionPercepcion()

        RetPerLinea1Cod1 = 0
        RetPerLinea1Cod2 = 0
        RetPerLinea1Cod3 = 0
        RetPerLinea1Cod4 = 0
        RetPerLinea1Cod5 = 0

        RetPerLinea2Cod1 = 0
        RetPerLinea2Cod2 = 0
        RetPerLinea2Cod3 = 0
        RetPerLinea2Cod4 = 0
        RetPerLinea2Cod5 = 0

        RetPerLinea3Cod1 = 0
        RetPerLinea3Cod2 = 0
        RetPerLinea3Cod3 = 0
        RetPerLinea3Cod4 = 0
        RetPerLinea3Cod5 = 0

        RetPerLinea4Cod1 = 0
        RetPerLinea4Cod2 = 0
        RetPerLinea4Cod3 = 0
        RetPerLinea4Cod4 = 0
        RetPerLinea4Cod5 = 0

        RetPerLinea5Cod1 = 0
        RetPerLinea5Cod2 = 0
        RetPerLinea5Cod3 = 0
        RetPerLinea5Cod4 = 0
        RetPerLinea5Cod5 = 0

        RetPerLinea6Cod1 = 0
        RetPerLinea6Cod2 = 0
        RetPerLinea6Cod3 = 0
        RetPerLinea6Cod4 = 0
        RetPerLinea6Cod5 = 0

        RetPerLinea7Cod1 = 0
        RetPerLinea7Cod2 = 0
        RetPerLinea7Cod3 = 0
        RetPerLinea7Cod4 = 0
        RetPerLinea7Cod5 = 0

        RetPerLinea8Cod1 = 0
        RetPerLinea8Cod2 = 0
        RetPerLinea8Cod3 = 0
        RetPerLinea8Cod4 = 0
        RetPerLinea8Cod5 = 0

        RetPerLinea9Cod1 = 0
        RetPerLinea9Cod2 = 0
        RetPerLinea9Cod3 = 0
        RetPerLinea9Cod4 = 0
        RetPerLinea9Cod5 = 0

        RetPerLinea10Cod1 = 0
        RetPerLinea10Cod2 = 0
        RetPerLinea10Cod3 = 0
        RetPerLinea10Cod4 = 0
        RetPerLinea10Cod5 = 0

        RetPerLinea1Tasa1 = 0
        RetPerLinea1Tasa2 = 0
        RetPerLinea1Tasa3 = 0
        RetPerLinea1Tasa4 = 0
        RetPerLinea1Tasa5 = 0

        RetPerLinea2Tasa1 = 0
        RetPerLinea2Tasa2 = 0
        RetPerLinea2Tasa3 = 0
        RetPerLinea2Tasa4 = 0
        RetPerLinea2Tasa5 = 0

        RetPerLinea3Tasa1 = 0
        RetPerLinea3Tasa2 = 0
        RetPerLinea3Tasa3 = 0
        RetPerLinea3Tasa4 = 0
        RetPerLinea3Tasa5 = 0

        RetPerLinea4Tasa1 = 0
        RetPerLinea4Tasa2 = 0
        RetPerLinea4Tasa3 = 0
        RetPerLinea4Tasa4 = 0
        RetPerLinea4Tasa5 = 0

        RetPerLinea5Tasa1 = 0
        RetPerLinea5Tasa2 = 0
        RetPerLinea5Tasa3 = 0
        RetPerLinea5Tasa4 = 0
        RetPerLinea5Tasa5 = 0

        RetPerLinea6Tasa1 = 0
        RetPerLinea6Tasa2 = 0
        RetPerLinea6Tasa3 = 0
        RetPerLinea6Tasa4 = 0
        RetPerLinea6Tasa5 = 0

        RetPerLinea7Tasa1 = 0
        RetPerLinea7Tasa2 = 0
        RetPerLinea7Tasa3 = 0
        RetPerLinea7Tasa4 = 0
        RetPerLinea7Tasa5 = 0

        RetPerLinea8Tasa1 = 0
        RetPerLinea8Tasa2 = 0
        RetPerLinea8Tasa3 = 0
        RetPerLinea8Tasa4 = 0
        RetPerLinea8Tasa5 = 0

        RetPerLinea9Tasa1 = 0
        RetPerLinea9Tasa2 = 0
        RetPerLinea9Tasa3 = 0
        RetPerLinea9Tasa4 = 0
        RetPerLinea9Tasa5 = 0

        RetPerLinea10Tasa1 = 0
        RetPerLinea10Tasa2 = 0
        RetPerLinea10Tasa3 = 0
        RetPerLinea10Tasa4 = 0
        RetPerLinea10Tasa5 = 0

        RetPerLinea1Monto1 = 0
        RetPerLinea1Monto2 = 0
        RetPerLinea1Monto3 = 0
        RetPerLinea1Monto4 = 0
        RetPerLinea1Monto5 = 0

        RetPerLinea2Monto1 = 0
        RetPerLinea2Monto2 = 0
        RetPerLinea2Monto3 = 0
        RetPerLinea2Monto4 = 0
        RetPerLinea2Monto5 = 0

        RetPerLinea3Monto1 = 0
        RetPerLinea3Monto2 = 0
        RetPerLinea3Monto3 = 0
        RetPerLinea3Monto4 = 0
        RetPerLinea3Monto5 = 0

        RetPerLinea4Monto1 = 0
        RetPerLinea4Monto2 = 0
        RetPerLinea4Monto3 = 0
        RetPerLinea4Monto4 = 0
        RetPerLinea4Monto5 = 0

        RetPerLinea5Monto1 = 0
        RetPerLinea5Monto2 = 0
        RetPerLinea5Monto3 = 0
        RetPerLinea5Monto4 = 0
        RetPerLinea5Monto5 = 0

        RetPerLinea6Monto1 = 0
        RetPerLinea6Monto2 = 0
        RetPerLinea6Monto3 = 0
        RetPerLinea6Monto4 = 0
        RetPerLinea6Monto5 = 0

        RetPerLinea7Monto1 = 0
        RetPerLinea7Monto2 = 0
        RetPerLinea7Monto3 = 0
        RetPerLinea7Monto4 = 0
        RetPerLinea7Monto5 = 0

        RetPerLinea8Monto1 = 0
        RetPerLinea8Monto2 = 0
        RetPerLinea8Monto3 = 0
        RetPerLinea8Monto4 = 0
        RetPerLinea8Monto5 = 0

        RetPerLinea9Monto1 = 0
        RetPerLinea9Monto2 = 0
        RetPerLinea9Monto3 = 0
        RetPerLinea9Monto4 = 0
        RetPerLinea9Monto5 = 0

        RetPerLinea10Monto1 = 0
        RetPerLinea10Monto2 = 0
        RetPerLinea10Monto3 = 0
        RetPerLinea10Monto4 = 0
        RetPerLinea10Monto5 = 0

        RetPerLinea1Valor1 = 0
        RetPerLinea1Valor2 = 0
        RetPerLinea1Valor3 = 0
        RetPerLinea1Valor4 = 0
        RetPerLinea1Valor5 = 0

        RetPerLinea2Valor1 = 0
        RetPerLinea2Valor2 = 0
        RetPerLinea2Valor3 = 0
        RetPerLinea2Valor4 = 0
        RetPerLinea2Valor5 = 0

        RetPerLinea3Valor1 = 0
        RetPerLinea3Valor2 = 0
        RetPerLinea3Valor3 = 0
        RetPerLinea3Valor4 = 0
        RetPerLinea3Valor5 = 0

        RetPerLinea4Valor1 = 0
        RetPerLinea4Valor2 = 0
        RetPerLinea4Valor3 = 0
        RetPerLinea4Valor4 = 0
        RetPerLinea4Valor5 = 0

        RetPerLinea5Valor1 = 0
        RetPerLinea5Valor2 = 0
        RetPerLinea5Valor3 = 0
        RetPerLinea5Valor4 = 0
        RetPerLinea5Valor5 = 0

        RetPerLinea6Valor1 = 0
        RetPerLinea6Valor2 = 0
        RetPerLinea6Valor3 = 0
        RetPerLinea6Valor4 = 0
        RetPerLinea6Valor5 = 0

        RetPerLinea7Valor1 = 0
        RetPerLinea7Valor2 = 0
        RetPerLinea7Valor3 = 0
        RetPerLinea7Valor4 = 0
        RetPerLinea7Valor5 = 0

        RetPerLinea8Valor1 = 0
        RetPerLinea8Valor2 = 0
        RetPerLinea8Valor3 = 0
        RetPerLinea8Valor4 = 0
        RetPerLinea8Valor5 = 0

        RetPerLinea9Valor1 = 0
        RetPerLinea9Valor2 = 0
        RetPerLinea9Valor3 = 0
        RetPerLinea9Valor4 = 0
        RetPerLinea9Valor5 = 0

        RetPerLinea10Valor1 = 0
        RetPerLinea10Valor2 = 0
        RetPerLinea10Valor3 = 0
        RetPerLinea10Valor4 = 0
        RetPerLinea10Valor5 = 0

        LBx_RetencionesPercepciones.Items.Clear()
        TBx_MontoRetenidoPorLinea1.Text = 0
        TBx_MontoRetenidoPorLinea2.Text = 0
        TBx_MontoRetenidoPorLinea3.Text = 0
        TBx_MontoRetenidoPorLinea4.Text = 0
        TBx_MontoRetenidoPorLinea5.Text = 0
        TBx_MontoRetenidoPorLinea6.Text = 0
        TBx_MontoRetenidoPorLinea7.Text = 0
        TBx_MontoRetenidoPorLinea8.Text = 0
        TBx_MontoRetenidoPorLinea9.Text = 0
        TBx_MontoRetenidoPorLinea10.Text = 0
    End Sub

    Private Sub RapidatxtDocumentoReceptor_Leave(sender As Object, e As System.EventArgs) Handles RapidatxtDocumentoReceptor.Leave
        Dim cli As cliente
        For Each c As cliente In Clientes
            If c.Documento.Trim = RapidatxtDocumentoReceptor.Text.Trim Then
                cli = c
                Exit For
            End If
        Next

        If Not IsNothing(cli) Then
            RapidatxtNombreReceptor.Text = cli.Nombre
            RapidatxtDireccionReceptor.Text = cli.Direccion
            RapidatxtCiudadReceptor.Text = cli.Ciudad
            RapidatxtDepartamentoReceptor.Text = cli.Departamento
            RapidacmbCodigoPaisReceptor.SelectedIndex = cli.Pais
            RapidacmbTipoDocumentoReceptor.SelectedIndex = cli.TipoDocumento
        Else
            RapidatxtNombreReceptor.Text = ""
            RapidatxtDireccionReceptor.Text = ""
            RapidatxtCiudadReceptor.Text = ""
            RapidatxtDepartamentoReceptor.Text = ""
            RapidacmbCodigoPaisReceptor.SelectedIndex = 9
        End If
    End Sub

    Private Sub guardarReceptor()
        Dim existe As Boolean = False
        Dim cli As cliente = New cliente
        For Each c As cliente In Clientes
            If c.Documento.Trim = RapidatxtDocumentoReceptor.Text.Trim Then
                existe = True
                cli = c
                Exit For
            End If
        Next
        If Not existe Then
            cli.TipoDocumento = RapidacmbTipoDocumentoReceptor.SelectedIndex
            cli.Documento = RapidatxtDocumentoReceptor.Text.Trim
            cli.Nombre = RapidatxtNombreReceptor.Text.Trim
            cli.Pais = RapidacmbCodigoPaisReceptor.SelectedIndex
            cli.Departamento = RapidatxtDepartamentoReceptor.Text.Trim
            cli.Ciudad = RapidatxtCiudadReceptor.Text.Trim
            cli.Direccion = RapidatxtDireccionReceptor.Text.Trim
            Clientes.Add(cli)
            actualizarReceptores()
            'Else
            '    Dim msg As String = "Se han ingresado datos diferentes a los ya guardados para este cliente:" & vbCrLf & "Guardado            En este documento" & vbCrLf
            '    Dim cambio As Boolean
            '    If Not cli.Nombre.Trim = RapidatxtNombreReceptor.Text.Trim Then
            '        cambio = True
            '        msg = msg & cli.Nombre.Trim & "            " & RapidatxtNombreReceptor.Text.Trim & vbCrLf
            '    End If
        End If
    End Sub

    Private Sub guardarArticulo(nombre As String, precio As Double)
        Dim existe As Boolean = False
        Dim art As articulo = New articulo
        Dim actualizar As Boolean = False
        For Each a As articulo In Articulos
            If a.Nombre.Trim.ToUpper = nombre.ToUpper Then
                existe = True
                If a.GuardaPrecio Then
                    If Not Convert.ToDouble(a.Precio) = precio Then
                        a.Precio = precio
                        actualizar = True
                    End If
                End If
                Exit For
            End If
        Next
        If Not existe Then
            art.Nombre = nombre.ToUpper
            art.Precio = precio
            art.GuardaPrecio = True
            Articulos.Add(art)
            actualizar = True
        End If
        If actualizar Then
            actualizarArticulos()
        End If
    End Sub

    Private Sub actualizarReceptores()
        Dim sw As StreamWriter
        Dim listaOrdenada = From item In Clientes Order By item.Nombre Select item
        Dim sRutaArchivoClientes As String = System.AppDomain.CurrentDomain.BaseDirectory & "Clientes.TAFACE"
        Try
            sw = New StreamWriter(sRutaArchivoClientes, False, System.Text.Encoding.Default)
            For Each c As cliente In listaOrdenada
                sw.WriteLine(c.Documento & "|" & c.TipoDocumento & "|" & c.Nombre & "|" & c.Pais & "|" & c.Departamento & "|" & c.Ciudad & "|" & c.Direccion)
            Next
        Catch ex As Exception
            Throw
        Finally
            If Not IsNothing(sw) Then
                sw.Close()
            End If
        End Try
    End Sub

    Private Sub actualizarArticulos()
        Dim sw As StreamWriter
        Dim listaOrdenada = From item In Articulos Order By item.Nombre Select item
        Dim sRutaArchivoArticulos As String = System.AppDomain.CurrentDomain.BaseDirectory & "Articulos.TAFACE"
        Try
            sw = New StreamWriter(sRutaArchivoArticulos, False, System.Text.Encoding.Default)
            For Each a As articulo In listaOrdenada
                If a.GuardaPrecio Then
                    sw.WriteLine(a.Nombre & "|" & a.Precio.ToString() & "| ")
                Else
                    sw.WriteLine(a.Nombre & "|0|X")
                End If
            Next
        Catch ex As Exception
            Throw
        Finally
            If Not IsNothing(sw) Then
                sw.Close()
            End If
        End Try
    End Sub

    Private Sub cargarReceptores()
        Dim sr As StreamReader = Nothing
        Dim sRutaArchivoClientes As String = System.AppDomain.CurrentDomain.BaseDirectory & "Clientes.TAFACE"
        Clientes.Clear()
        Try
            If Not File.Exists(sRutaArchivoClientes) Then
                Exit Sub
            End If
            sr = New StreamReader(sRutaArchivoClientes, System.Text.Encoding.Default)
            Dim cliente As String = ""
            cliente = sr.ReadLine()
            While Not cliente Is Nothing
                Dim datos As String() = cliente.Split("|")
                Dim cli As cliente = New cliente
                cli.Documento = datos(0).Trim
                cli.TipoDocumento = datos(1).Trim
                cli.Nombre = datos(2).Trim
                cli.Pais = datos(3).Trim
                cli.Departamento = datos(4).Trim
                cli.Ciudad = datos(5).Trim
                cli.Direccion = datos(6).Trim
                Clientes.Add(cli)
                cliente = sr.ReadLine()
            End While
        Catch ex As Exception
            Return
        Finally
            If Not IsNothing(sr) Then
                sr.Close()
            End If
        End Try
    End Sub

    Private Sub cargarArticulos()
        Dim sr As StreamReader = Nothing
        Dim sRutaArchivoArticulos As String = System.AppDomain.CurrentDomain.BaseDirectory & "Articulos.TAFACE"
        Articulos.Clear()
        Try
            If Not File.Exists(sRutaArchivoArticulos) Then
                Exit Sub
            End If
            sr = New StreamReader(sRutaArchivoArticulos, System.Text.Encoding.Default)
            Dim articulo As String = ""
            articulo = sr.ReadLine()
            While Not articulo Is Nothing
                Dim datos As String() = articulo.Split("|")
                Dim art As articulo = New articulo
                art.Nombre = datos(0).Trim
                If Not datos(1).Trim = "" Then
                    art.Precio = Convert.ToDouble(datos(1).Trim)
                Else
                    art.Precio = 0
                End If
                If datos(2).Trim.ToUpper = "X".ToUpper Then
                    art.GuardaPrecio = False
                Else
                    art.GuardaPrecio = True
                End If
                Articulos.Add(art)
                articulo = sr.ReadLine()
            End While
        Catch ex As Exception
            Return
        Finally
            If Not IsNothing(sr) Then
                sr.Close()
            End If
        End Try
    End Sub

    Private Sub Btn_Buscar_Click(sender As Object, e As EventArgs) Handles Btn_Buscar.Click
        If Buscar("Clientes", 0) Then
            RapidatxtDocumentoReceptor_Leave(sender, e)
        End If
    End Sub

    Private Function Buscar(queBusca As String, item As Integer)
        Dim frmBuscar As New frmBuscar
        frmBuscar.QueBusca = queBusca
        frmBuscar.Item = item
        Dim CodForm As Windows.Forms.DialogResult
        CodForm = frmBuscar.ShowDialog
        If CodForm = Windows.Forms.DialogResult.OK Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub btn_buscarArticulo1_Click(sender As Object, e As EventArgs) Handles btn_buscarArticulo1.Click
        Buscar("Articulos", 1)
    End Sub

    Private Sub btn_buscarArticulo2_Click(sender As Object, e As EventArgs) Handles btn_buscarArticulo2.Click
        Buscar("Articulos", 2)
    End Sub

    Private Sub btn_buscarArticulo3_Click(sender As Object, e As EventArgs) Handles btn_buscarArticulo3.Click
        Buscar("Articulos", 3)
    End Sub

    Private Sub btn_buscarArticulo4_Click(sender As Object, e As EventArgs) Handles btn_buscarArticulo4.Click
        Buscar("Articulos", 4)
    End Sub

    Private Sub btn_buscarArticulo5_Click(sender As Object, e As EventArgs) Handles btn_buscarArticulo5.Click
        Buscar("Articulos", 5)
    End Sub

    Private Sub btn_buscarArticulo6_Click(sender As Object, e As EventArgs) Handles btn_buscarArticulo6.Click
        Buscar("Articulos", 6)
    End Sub

    Private Sub btn_buscarArticulo7_Click(sender As Object, e As EventArgs) Handles btn_buscarArticulo7.Click
        Buscar("Articulos", 7)
    End Sub

    Private Sub btn_buscarArticulo8_Click(sender As Object, e As EventArgs) Handles btn_buscarArticulo8.Click
        Buscar("Articulos", 8)
    End Sub

    Private Sub btn_buscarArticulo9_Click(sender As Object, e As EventArgs) Handles btn_buscarArticulo9.Click
        Buscar("Articulos", 9)
    End Sub

    Private Sub btn_buscarArticulo10_Click(sender As Object, e As EventArgs) Handles btn_buscarArticulo10.Click
        Buscar("Articulos", 10)
    End Sub

    Private Sub RapidatxtNombreItem1_Leave(sender As Object, e As System.EventArgs) Handles RapidatxtNombreItem1.Leave
        For Each a As articulo In Articulos
            If a.Nombre.Trim.ToUpper = RapidatxtNombreItem1.Text.Trim.ToUpper Then
                RapidatxtPrecioUnitario1.Text = a.Precio.ToString()
                Exit For
            End If
        Next
    End Sub

    Private Sub RapidatxtNombreItem2_Leave(sender As Object, e As System.EventArgs) Handles RapidatxtNombreItem2.Leave
        For Each a As articulo In Articulos
            If a.Nombre.Trim.ToUpper = RapidatxtNombreItem2.Text.Trim.ToUpper Then
                RapidatxtPrecioUnitario2.Text = a.Precio.ToString()
                Exit For
            End If
        Next
    End Sub

    Private Sub RapidatxtNombreItem3_Leave(sender As Object, e As System.EventArgs) Handles RapidatxtNombreItem3.Leave
        For Each a As articulo In Articulos
            If a.Nombre.Trim.ToUpper = RapidatxtNombreItem3.Text.Trim.ToUpper Then
                RapidatxtPrecioUnitario3.Text = a.Precio.ToString()
                Exit For
            End If
        Next
    End Sub

    Private Sub RapidatxtNombreItem4_Leave(sender As Object, e As System.EventArgs) Handles RapidatxtNombreItem4.Leave
        For Each a As articulo In Articulos
            If a.Nombre.Trim.ToUpper = RapidatxtNombreItem4.Text.Trim.ToUpper Then
                RapidatxtPrecioUnitario4.Text = a.Precio.ToString()
                Exit For
            End If
        Next
    End Sub

    Private Sub RapidatxtNombreItem5_Leave(sender As Object, e As System.EventArgs) Handles RapidatxtNombreItem5.Leave
        For Each a As articulo In Articulos
            If a.Nombre.Trim.ToUpper = RapidatxtNombreItem5.Text.Trim.ToUpper Then
                RapidatxtPrecioUnitario5.Text = a.Precio.ToString()
                Exit For
            End If
        Next
    End Sub

    Private Sub RapidatxtNombreItem6_Leave(sender As Object, e As System.EventArgs) Handles RapidatxtNombreItem6.Leave
        For Each a As articulo In Articulos
            If a.Nombre.Trim.ToUpper = RapidatxtNombreItem6.Text.Trim.ToUpper Then
                RapidatxtPrecioUnitario6.Text = a.Precio.ToString()
                Exit For
            End If
        Next
    End Sub

    Private Sub RapidatxtNombreItem7_Leave(sender As Object, e As System.EventArgs) Handles RapidatxtNombreItem7.Leave
        For Each a As articulo In Articulos
            If a.Nombre.Trim.ToUpper = RapidatxtNombreItem7.Text.Trim.ToUpper Then
                RapidatxtPrecioUnitario7.Text = a.Precio.ToString()
                Exit For
            End If
        Next
    End Sub

    Private Sub RapidatxtNombreItem8_Leave(sender As Object, e As System.EventArgs) Handles RapidatxtNombreItem8.Leave
        For Each a As articulo In Articulos
            If a.Nombre.Trim.ToUpper = RapidatxtNombreItem8.Text.Trim.ToUpper Then
                RapidatxtPrecioUnitario8.Text = a.Precio.ToString()
                Exit For
            End If
        Next
    End Sub

    Private Sub RapidatxtNombreItem9_Leave(sender As Object, e As System.EventArgs) Handles RapidatxtNombreItem9.Leave
        For Each a As articulo In Articulos
            If a.Nombre.Trim.ToUpper = RapidatxtNombreItem9.Text.Trim.ToUpper Then
                RapidatxtPrecioUnitario9.Text = a.Precio.ToString()
                Exit For
            End If
        Next
    End Sub

    Private Sub RapidatxtNombreItem10_Leave(sender As Object, e As System.EventArgs) Handles RapidatxtNombreItem10.Leave
        For Each a As articulo In Articulos
            If a.Nombre.Trim.ToUpper = RapidatxtNombreItem10.Text.Trim.ToUpper Then
                RapidatxtPrecioUnitario10.Text = a.Precio.ToString()
                Exit For
            End If
        Next
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        If FolderBrowserDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            Try
                TextBox_CarpetaOperacion.Text = FolderBrowserDialog1.SelectedPath
            Catch Ex As Exception
                MessageBox.Show("Error al seleccionar carpeta operacion: " & Ex.Message)
            Finally

            End Try
        End If
    End Sub

    'ajustar que en el caso de Brasil permita la entrada de hasta 15 caracteres el tipo documento receptor de acuerdo a solicitud hecha por el cliente
    Private Sub RapidatxtDocumentoReceptor_KeyPress(sender As Object, e As KeyPressEventArgs) Handles RapidatxtDocumentoReceptor.KeyPress
        If RapidacmbTipoDocumentoReceptor.Text = "Otros" And RapidacmbCodigoPaisReceptor.Text = "Brasil" Then
            RapidatxtDocumentoReceptor.MaxLength = 15
        Else
            RapidatxtDocumentoReceptor.MaxLength = 12
        End If
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button_Sincronizar.Click
        Dim objTAFEApi As New TAFE2Api.TAFEApi_v0206
        Dim ErrorMsg As String = ""
        Dim ErrorCod As Integer
        If objTAFEApi.Inicializar(ErrorMsg, ErrorCod, txturlServidorGateway.Text, CTDec(txtSegundosTimeout.Text), CTDec(txtCfgRUTEmisor.Text), CTDec(txtNroSucursal.Text), CTDec(txtCfgCajaNro.Text), TextBox_CarpetaOperacion.Text) Then
            If objTAFEApi.Sincronizar(ErrorMsg, ErrorCod) Then
                MessageBox.Show("Sincronizacion completada", "Proceso de sincronizacion", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                MessageBox.Show(ErrorMsg, "Proceso de sincronizacion", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        Else
            MessageBox.Show(ErrorMsg, "Proceso de sincronizacion", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

    Private Sub Button_CerrarCaja_Click(sender As Object, e As EventArgs)
        Dim objTAFEApi As New TAFE2Api.TAFEApi_v0206
        Dim ErrorMsg As String = ""
        Dim ErrorCod As Integer
        If objTAFEApi.Inicializar(ErrorMsg, ErrorCod, txturlServidorGateway.Text, CTDec(txtSegundosTimeout.Text), CTDec(txtCfgRUTEmisor.Text), CTDec(txtNroSucursal.Text), CTDec(txtCfgCajaNro.Text), TextBox_CarpetaOperacion.Text) Then
            If objTAFEApi.CerrarCaja(ErrorMsg, ErrorCod, txturlServidorGateway.Text, CTDec(txtCfgRUTEmisor.Text), CTDec(txtNroSucursal.Text), CTDec(txtCfgCajaNro.Text), TextBox_CarpetaOperacion.Text) Then
                MessageBox.Show("Caja cerrada correctamente", "Cerrar caja", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                MessageBox.Show(ErrorMsg, "Cerrar caja", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        Else
            MessageBox.Show(ErrorMsg, "Cerrar caja", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

    Private Sub Chk_emiteRemitos_CheckedChanged(sender As Object, e As EventArgs) Handles Chk_emiteRemitos.CheckedChanged
        If Chk_emiteRemitos.Checked Then
            GroupBox1.Visible = True
        Else
            GroupBox1.Visible = True
        End If
    End Sub

    Private Sub TextBox_NombreRznSocPropMercaderiaTransp_Enter(sender As Object, e As EventArgs) Handles TextBox_NombreRznSocPropMercaderiaTransp.Enter
        If TextBox_NombreRznSocPropMercaderiaTransp.Text = "Nombre o denominacion propietario mercaderia transportada..." Then
            TextBox_NombreRznSocPropMercaderiaTransp.Text = ""
        End If
    End Sub

    Private Sub TextBox_NombreRznSocPropMercaderiaTransp_Leave(sender As Object, e As EventArgs) Handles TextBox_NombreRznSocPropMercaderiaTransp.Leave
        If TextBox_NombreRznSocPropMercaderiaTransp.Text = "" Then
            TextBox_NombreRznSocPropMercaderiaTransp.Text = "Nombre o denominacion propietario mercaderia transportada..."
        End If
    End Sub

    Private Sub cmbIndicadorPropMercaderiaTransp_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbIndicadorPropMercaderiaTransp.SelectedIndexChanged
        If cmbIndicadorPropMercaderiaTransp.SelectedIndex <> 0 Then
            Panel1.Visible = True
        Else
            Panel1.Visible = False
        End If
    End Sub

    Private Sub cmbIndicadorTipoTrasladoBienesEncabezado_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbIndicadorTipoTrasladoBienesEncabezado.SelectedIndexChanged
        If cmbIndicadorTipoTrasladoBienesEncabezado.SelectedIndex <> 0 Then
            cmbIndicadorPropMercaderiaTransp.Visible = True
            Label38.Visible = True
        Else
            cmbIndicadorPropMercaderiaTransp.Visible = False
            Panel1.Visible = False
            Label38.Visible = False
        End If
    End Sub

    Private Sub Rdb_esRemito_CheckedChanged(sender As Object, e As EventArgs) Handles Rdb_esRemito.CheckedChanged
        If Rdb_esRemito.Checked Then
            chkConsumoFinal.Enabled = False
            GroupBox11.Visible = False
            GroupBox_Remito.Visible = True
            Ckb_Exportacion.Enabled = True
        Else
            chkConsumoFinal.Enabled = True
            GroupBox11.Visible = True
            GroupBox_Remito.Visible = False
        End If
    End Sub

    Private Sub LimpiarRemitos()
        If Rdb_esRemito.Checked Then
            GroupBox11.Visible = True
            GroupBox_Remito.Visible = False
            Panel1.Visible = False
            cmbIndicadorPropMercaderiaTransp.SelectedIndex = 0
            textBox_NroDocPropMercaderiaTransp.Clear()
            TextBox_NombreRznSocPropMercaderiaTransp.Text = "Nombre o denominacion propietario mercaderia transportada..."
            cmbTipoDocMercaderiaTransp.SelectedIndex = 0
            cmbCodPaisPropMercaderiaTransp.SelectedIndex = 238
            cmbIndicadorTipoTrasladoBienesEncabezado.SelectedIndex = 0
            Rdb_esRemito.Checked = False
        End If
    End Sub

    Private Sub Rdb_esBoleta_CheckedChanged(sender As Object, e As EventArgs) Handles Rdb_esBoleta.CheckedChanged
        If Rdb_esRemito.Checked Then
            chkConsumoFinal.Enabled = False
            GroupBox11.Visible = True
            GroupBox_Remito.Visible = False
        End If
        chkConsumoFinal.Enabled = False
        Ckb_Exportacion.Enabled = False
        Dim dTipoCFERef As New Dictionary(Of Integer, String)
        dTipoCFERef.Add(TipoCFE.eBoleta, "e-Boleta")

        cmbTipoCFEInformacionReferencia.DataSource = New BindingSource(dTipoCFERef, Nothing)
        cmbTipoCFEInformacionReferencia.DisplayMember = "Value"
        cmbTipoCFEInformacionReferencia.ValueMember = "Key"

        CalcularTotales()
        ArreglarVistaPantalla()
        If Not objConfig.ImpuestoPorDefecto = "" Then
            RapidacmbIndicadorFacturacion1.Text = objConfig.ImpuestoPorDefecto
            RapidacmbIndicadorFacturacion2.Text = objConfig.ImpuestoPorDefecto
            RapidacmbIndicadorFacturacion3.Text = objConfig.ImpuestoPorDefecto
            RapidacmbIndicadorFacturacion4.Text = objConfig.ImpuestoPorDefecto
            RapidacmbIndicadorFacturacion5.Text = objConfig.ImpuestoPorDefecto
            RapidacmbIndicadorFacturacion6.Text = objConfig.ImpuestoPorDefecto
            RapidacmbIndicadorFacturacion7.Text = objConfig.ImpuestoPorDefecto
            RapidacmbIndicadorFacturacion8.Text = objConfig.ImpuestoPorDefecto
            RapidacmbIndicadorFacturacion9.Text = objConfig.ImpuestoPorDefecto
            RapidacmbIndicadorFacturacion10.Text = objConfig.ImpuestoPorDefecto
        End If
    End Sub

    Private Sub Rdb_esTckFact_CheckedChanged(sender As Object, e As EventArgs) Handles Rdb_esTckFact.CheckedChanged
        If Rdb_esTckFact.Checked Then
            chkConsumoFinal.Enabled = True
            GroupBox11.Visible = True
            GroupBox_Remito.Visible = False
            Ckb_Exportacion.Enabled = True
        End If
        Dim dTipoCFERef As New Dictionary(Of Integer, String)
        dTipoCFERef.Add(TipoCFE.eTicket, "e-Ticket")
        dTipoCFERef.Add(TipoCFE.eFactura, "e-Factura")

        cmbTipoCFEInformacionReferencia.DataSource = New BindingSource(dTipoCFERef, Nothing)
        cmbTipoCFEInformacionReferencia.DisplayMember = "Value"
        cmbTipoCFEInformacionReferencia.ValueMember = "Key"
    End Sub

    Private Sub Label164_MouseClick(sender As Object, e As MouseEventArgs) Handles Label164.MouseClick
        'si da un click actualizo el valor con el nro que tengo en el fichero config
        If RapidatxtNumeroComprobanteEncabezado.Enabled = True Then
            RapidatxtNumeroComprobanteEncabezado.Enabled = False
        End If
        objConfig = New XMLCONFIGFACMANUAL
        objConfig = objConfig.CargarConfiguracion
        RapidatxtNumeroComprobanteEncabezado.Text = objConfig.NROFACTURA
    End Sub

    Private Sub Label164_DoubleClick(sender As Object, e As EventArgs) Handles Label164.DoubleClick
        'si le doy doble click dejo editar el nro de documento original dejo setear manualmente
        If RapidatxtNumeroComprobanteEncabezado.Enabled = False Then
            RapidatxtNumeroComprobanteEncabezado.Enabled = True
        Else
            RapidatxtNumeroComprobanteEncabezado.Enabled = False
        End If
    End Sub

    Private Sub RapidatxtNumeroComprobanteEncabezado_Click(sender As Object, e As EventArgs) Handles RapidatxtNumeroComprobanteEncabezado.Click
        'si da un click actualizo el valor con el nro que tengo en el fichero config
        If RapidatxtNumeroComprobanteEncabezado.Enabled = False Then
            objConfig = New XMLCONFIGFACMANUAL
            objConfig = objConfig.CargarConfiguracion
            RapidatxtNumeroComprobanteEncabezado.Text = objConfig.NROFACTURA
        End If
    End Sub

    'Ensamblar voucher de impresion de transact para guardarlo en la adenda, sacar los datos de copia cliente
    Private Function ObtenerCopiaComercio(ByVal voucherRenderizado As String()) As String
        Dim voucher As String = ""
        Dim copiaComercio As New List(Of String)
        Dim finCopiaComercio As Boolean = False
        For Each linea As String In voucherRenderizado
            If linea.Contains("COPIA COMERCIO") Then
                finCopiaComercio = True
                copiaComercio.Add(linea)
            End If
            If Not finCopiaComercio Then
                copiaComercio.Add(linea)
            End If
        Next
        For Each linea As String In copiaComercio
            voucher &= linea & vbCrLf
        Next
        Return vbCrLf & voucher & vbCrLf
    End Function

    Private Function ObtenerVoucher(ByVal voucherRenderizado As String()) As String
        Dim voucher As String = ""
        Dim counter As Integer = 1
        Dim finCopiaComercio As Boolean = False
        For Each linea As String In voucherRenderizado
            voucher &= linea & vbCrLf
        Next
        Return voucher
    End Function

    Private Sub btnCerrarLote_Click(sender As Object, e As EventArgs) Handles btnCerrarLote.Click
        Dim objIntegration As New NADIntegration.NADIntegration
        Dim ErrorMsg As String = ""
        If chkConsultarUltimoCierre.Checked Then
            If objIntegration.ConsultarUltimoCierreLote(ErrorMsg, Trim(txtEmpCod.Text), Trim(txtTermCod.Text)) Then
                For Each Cierre In objIntegration.GetObjetoTransActTarjetas.CierreLote.Respuesta.DatosCierre
                    MessageBox.Show("Lote " & Cierre.Lote & " de Procesador " & Cierre.ProcesadorId & " " & IIf(Cierre.Aprobado, "APROBADO", "DENEGADO"), "Cierre lote", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Next
            Else
                MessageBox.Show(ErrorMsg, "Cierre lote", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        Else
            If objIntegration.CerrarLote(ErrorMsg, Trim(txtEmpCod.Text), Trim(txtTermCod.Text), cmbIdProcesadorTransAct.SelectedValue, chkCierreCentralizado.Checked) Then
                For Each Cierre In objIntegration.GetObjetoTransActTarjetas.CierreLote.Respuesta.DatosCierre
                    MessageBox.Show("Lote " & Cierre.Lote & " de Procesador " & Cierre.ProcesadorId & " " & IIf(Cierre.Aprobado, "APROBADO", "DENEGADO") & " .Fecha: " & Cierre.Extendida.CierreFechaHora, "Cierre lote", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Next
            Else
                MessageBox.Show(ErrorMsg, "Cierre lote", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        End If
    End Sub

    Private Sub chkSoloFacturar_CheckedChanged(sender As Object, e As EventArgs) Handles chkSoloFacturar.CheckedChanged
        If chkSoloCobrar.Checked Then
            chkSoloCobrar.Checked = False
        End If
        chkSoloFacturar.Checked = Not chkSoloCobrar.Checked
    End Sub

    Private Sub chkSoloCobrar_CheckedChanged(sender As Object, e As EventArgs) Handles chkSoloCobrar.CheckedChanged
        If chkSoloFacturar.Checked Then
            chkSoloFacturar.Checked = False
        End If
        chkSoloCobrar.Checked = Not chkSoloFacturar.Checked
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim objIntegration As New NADIntegration.NADIntegration
        Dim ErrorMsg = "", Respuesta = "", Track1 = "", Track2 = "", Track3 As String = ""
        If objIntegration.LeerTarjeta(ErrorMsg, Trim(txtEmpCod.Text), Trim(txtTermCod.Text), Respuesta, Track1, Track2, Track3) Then
            MessageBox.Show(Respuesta & vbCrLf & Track1 & vbCrLf & Track2 & vbCrLf & Track3, "Leer tarjeta", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            MessageBox.Show(ErrorMsg, "Leer tarjeta", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If

    End Sub
End Class
