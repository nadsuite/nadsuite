﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReimpresion
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmReimpresion))
        Me.gbxCAE = New System.Windows.Forms.GroupBox()
        Me.cmbTipoComprobante = New System.Windows.Forms.ComboBox()
        Me.lblTipo = New System.Windows.Forms.Label()
        Me.txtNumeroCFE = New System.Windows.Forms.MaskedTextBox()
        Me.lblSerie = New System.Windows.Forms.Label()
        Me.txtSeriCFE = New System.Windows.Forms.TextBox()
        Me.lblNumero = New System.Windows.Forms.Label()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.gbxCAE.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbxCAE
        '
        Me.gbxCAE.Controls.Add(Me.cmbTipoComprobante)
        Me.gbxCAE.Controls.Add(Me.lblTipo)
        Me.gbxCAE.Controls.Add(Me.txtNumeroCFE)
        Me.gbxCAE.Controls.Add(Me.lblSerie)
        Me.gbxCAE.Controls.Add(Me.txtSeriCFE)
        Me.gbxCAE.Controls.Add(Me.lblNumero)
        Me.gbxCAE.Location = New System.Drawing.Point(13, 10)
        Me.gbxCAE.Name = "gbxCAE"
        Me.gbxCAE.Size = New System.Drawing.Size(230, 86)
        Me.gbxCAE.TabIndex = 11
        Me.gbxCAE.TabStop = False
        Me.gbxCAE.Text = "Datos CAE"
        '
        'cmbTipoComprobante
        '
        Me.cmbTipoComprobante.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTipoComprobante.FormattingEnabled = True
        Me.cmbTipoComprobante.Location = New System.Drawing.Point(59, 23)
        Me.cmbTipoComprobante.Name = "cmbTipoComprobante"
        Me.cmbTipoComprobante.Size = New System.Drawing.Size(156, 21)
        Me.cmbTipoComprobante.TabIndex = 0
        '
        'lblTipo
        '
        Me.lblTipo.AutoSize = True
        Me.lblTipo.Location = New System.Drawing.Point(22, 26)
        Me.lblTipo.Name = "lblTipo"
        Me.lblTipo.Size = New System.Drawing.Size(31, 13)
        Me.lblTipo.TabIndex = 0
        Me.lblTipo.Text = "Tipo:"
        '
        'txtNumeroCFE
        '
        Me.txtNumeroCFE.Location = New System.Drawing.Point(140, 50)
        Me.txtNumeroCFE.Mask = "99999999999"
        Me.txtNumeroCFE.Name = "txtNumeroCFE"
        Me.txtNumeroCFE.Size = New System.Drawing.Size(75, 20)
        Me.txtNumeroCFE.TabIndex = 2
        '
        'lblSerie
        '
        Me.lblSerie.AutoSize = True
        Me.lblSerie.Location = New System.Drawing.Point(22, 53)
        Me.lblSerie.Name = "lblSerie"
        Me.lblSerie.Size = New System.Drawing.Size(34, 13)
        Me.lblSerie.TabIndex = 2
        Me.lblSerie.Text = "Serie:"
        '
        'txtSeriCFE
        '
        Me.txtSeriCFE.Location = New System.Drawing.Point(59, 50)
        Me.txtSeriCFE.MaxLength = 2
        Me.txtSeriCFE.Name = "txtSeriCFE"
        Me.txtSeriCFE.Size = New System.Drawing.Size(22, 20)
        Me.txtSeriCFE.TabIndex = 1
        Me.txtSeriCFE.Text = "AA"
        '
        'lblNumero
        '
        Me.lblNumero.AutoSize = True
        Me.lblNumero.Location = New System.Drawing.Point(87, 53)
        Me.lblNumero.Name = "lblNumero"
        Me.lblNumero.Size = New System.Drawing.Size(47, 13)
        Me.lblNumero.TabIndex = 4
        Me.lblNumero.Text = "Numero:"
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(168, 102)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 26)
        Me.btnAceptar.TabIndex = 10
        Me.btnAceptar.Text = "Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'frmReimpresion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(257, 138)
        Me.Controls.Add(Me.gbxCAE)
        Me.Controls.Add(Me.btnAceptar)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmReimpresion"
        Me.Text = "Reimpresion"
        Me.gbxCAE.ResumeLayout(False)
        Me.gbxCAE.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents gbxCAE As GroupBox
    Friend WithEvents cmbTipoComprobante As ComboBox
    Friend WithEvents lblTipo As Label
    Friend WithEvents txtNumeroCFE As MaskedTextBox
    Friend WithEvents lblSerie As Label
    Friend WithEvents txtSeriCFE As TextBox
    Friend WithEvents lblNumero As Label
    Friend WithEvents btnAceptar As Button
End Class
