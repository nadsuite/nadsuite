﻿Public Class cliente
    Private tipoDocumentoField As Integer
    Private documentoField As String
    Private nombreField As String
    Private paisField As Integer
    Private departamentoField As String
    Private ciudadField As String
    Private direccionField As String



    Public Property TipoDocumento As Integer
        Get
            Return tipoDocumentoField
        End Get
        Set(value As Integer)
            tipoDocumentoField = value
        End Set
    End Property

    Public Property Documento As String
        Get
            Return documentoField
        End Get
        Set(value As String)
            documentoField = value
        End Set
    End Property

    Public Property Nombre As String
        Get
            Return nombreField
        End Get
        Set(value As String)
            nombreField = value
        End Set
    End Property

    Public Property Pais As Integer
        Get
            Return paisField
        End Get
        Set(value As Integer)
            paisField = value
        End Set
    End Property

    Public Property Departamento As String
        Get
            Return departamentoField
        End Get
        Set(value As String)
            departamentoField = value
        End Set
    End Property

    Public Property Ciudad As String
        Get
            Return ciudadField
        End Get
        Set(value As String)
            ciudadField = value
        End Set
    End Property

    Public Property Direccion As String
        Get
            Return direccionField
        End Get
        Set(value As String)
            direccionField = value
        End Set
    End Property
End Class
