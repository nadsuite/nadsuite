﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmFactura
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFactura))
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.FacturaRapida = New System.Windows.Forms.TabPage()
        Me.chkSoloFacturar = New System.Windows.Forms.CheckBox()
        Me.chkSoloCobrar = New System.Windows.Forms.CheckBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.rbDevolucion = New System.Windows.Forms.RadioButton()
        Me.rbVenta = New System.Windows.Forms.RadioButton()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Rdb_esTckFact = New System.Windows.Forms.RadioButton()
        Me.Rdb_esRemito = New System.Windows.Forms.RadioButton()
        Me.Rdb_esBoleta = New System.Windows.Forms.RadioButton()
        Me.Ckb_esSecretoProfesional = New System.Windows.Forms.CheckBox()
        Me.btn_buscarArticulo10 = New System.Windows.Forms.Button()
        Me.btn_buscarArticulo9 = New System.Windows.Forms.Button()
        Me.btn_buscarArticulo8 = New System.Windows.Forms.Button()
        Me.btn_buscarArticulo7 = New System.Windows.Forms.Button()
        Me.btn_buscarArticulo6 = New System.Windows.Forms.Button()
        Me.btn_buscarArticulo5 = New System.Windows.Forms.Button()
        Me.btn_buscarArticulo4 = New System.Windows.Forms.Button()
        Me.btn_buscarArticulo3 = New System.Windows.Forms.Button()
        Me.btn_buscarArticulo2 = New System.Windows.Forms.Button()
        Me.btn_buscarArticulo1 = New System.Windows.Forms.Button()
        Me.GBx_Exportaciones = New System.Windows.Forms.GroupBox()
        Me.CBx_ViaTransporte = New System.Windows.Forms.ComboBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.TBx_ClausulaDeVenta = New System.Windows.Forms.TextBox()
        Me.CBx_ModalidadVenta = New System.Windows.Forms.ComboBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Lbl_Comentarios = New System.Windows.Forms.Label()
        Me.GBx_RetencionesPercepciones = New System.Windows.Forms.GroupBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.LBx_RetencionesPercepciones = New System.Windows.Forms.ListBox()
        Me.Btn_RetencionesPercepciones10 = New System.Windows.Forms.Button()
        Me.Btn_RetencionesPercepciones9 = New System.Windows.Forms.Button()
        Me.Btn_RetencionesPercepciones8 = New System.Windows.Forms.Button()
        Me.Btn_RetencionesPercepciones7 = New System.Windows.Forms.Button()
        Me.Btn_RetencionesPercepciones6 = New System.Windows.Forms.Button()
        Me.Btn_RetencionesPercepciones5 = New System.Windows.Forms.Button()
        Me.Btn_RetencionesPercepciones4 = New System.Windows.Forms.Button()
        Me.Btn_RetencionesPercepciones3 = New System.Windows.Forms.Button()
        Me.Btn_RetencionesPercepciones2 = New System.Windows.Forms.Button()
        Me.Btn_RetencionesPercepciones1 = New System.Windows.Forms.Button()
        Me.CBx_AgenteResponsable10 = New System.Windows.Forms.ComboBox()
        Me.CBx_AgenteResponsable9 = New System.Windows.Forms.ComboBox()
        Me.CBx_AgenteResponsable8 = New System.Windows.Forms.ComboBox()
        Me.CBx_AgenteResponsable7 = New System.Windows.Forms.ComboBox()
        Me.CBx_AgenteResponsable6 = New System.Windows.Forms.ComboBox()
        Me.CBx_AgenteResponsable5 = New System.Windows.Forms.ComboBox()
        Me.CBx_AgenteResponsable4 = New System.Windows.Forms.ComboBox()
        Me.CBx_AgenteResponsable3 = New System.Windows.Forms.ComboBox()
        Me.CBx_AgenteResponsable2 = New System.Windows.Forms.ComboBox()
        Me.CBx_AgenteResponsable1 = New System.Windows.Forms.ComboBox()
        Me.TBx_MontoRetenidoPorLinea10 = New System.Windows.Forms.TextBox()
        Me.TBx_MontoRetenidoPorLinea9 = New System.Windows.Forms.TextBox()
        Me.TBx_MontoRetenidoPorLinea8 = New System.Windows.Forms.TextBox()
        Me.TBx_MontoRetenidoPorLinea7 = New System.Windows.Forms.TextBox()
        Me.TBx_MontoRetenidoPorLinea6 = New System.Windows.Forms.TextBox()
        Me.TBx_MontoRetenidoPorLinea5 = New System.Windows.Forms.TextBox()
        Me.TBx_MontoRetenidoPorLinea4 = New System.Windows.Forms.TextBox()
        Me.TBx_MontoRetenidoPorLinea3 = New System.Windows.Forms.TextBox()
        Me.TBx_MontoRetenidoPorLinea2 = New System.Windows.Forms.TextBox()
        Me.TBx_MontoRetenidoPorLinea1 = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.GBx_PreFactura = New System.Windows.Forms.GroupBox()
        Me.Btn_PreFacturaImprimir = New System.Windows.Forms.Button()
        Me.Btn_PreFacturaAbrir = New System.Windows.Forms.Button()
        Me.Btn_PreFacturaGuardar = New System.Windows.Forms.Button()
        Me.GBx_SubTotalesInformativos = New System.Windows.Forms.GroupBox()
        Me.TBx_ValorSubTotales5 = New System.Windows.Forms.TextBox()
        Me.TBx_ValorSubTotales4 = New System.Windows.Forms.TextBox()
        Me.TBx_ValorSubTotales3 = New System.Windows.Forms.TextBox()
        Me.TBx_ValorSubTotales2 = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.TBx_ValorSubTotales1 = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.TBx_GlosaSubTotales5 = New System.Windows.Forms.TextBox()
        Me.TBx_GlosaSubTotales4 = New System.Windows.Forms.TextBox()
        Me.TBx_GlosaSubTotales3 = New System.Windows.Forms.TextBox()
        Me.TBx_GlosaSubTotales2 = New System.Windows.Forms.TextBox()
        Me.TBx_GlosaSubTotales1 = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Ckb_esContingencia = New System.Windows.Forms.CheckBox()
        Me.btnReimprimir = New System.Windows.Forms.Button()
        Me.RapidatxtSubDescuentoValor10 = New System.Windows.Forms.TextBox()
        Me.RapidatxtMontoItem10 = New System.Windows.Forms.TextBox()
        Me.RapidacmbIndicadorFacturacion10 = New System.Windows.Forms.ComboBox()
        Me.RapidatxtNombreItem10 = New System.Windows.Forms.TextBox()
        Me.RapidatxtCantidad10 = New System.Windows.Forms.TextBox()
        Me.RapidatxtPrecioUnitario10 = New System.Windows.Forms.TextBox()
        Me.RapidatxtSubDescuentoValor9 = New System.Windows.Forms.TextBox()
        Me.RapidatxtMontoItem9 = New System.Windows.Forms.TextBox()
        Me.RapidacmbIndicadorFacturacion9 = New System.Windows.Forms.ComboBox()
        Me.RapidatxtNombreItem9 = New System.Windows.Forms.TextBox()
        Me.RapidatxtCantidad9 = New System.Windows.Forms.TextBox()
        Me.RapidatxtPrecioUnitario9 = New System.Windows.Forms.TextBox()
        Me.RapidatxtSubDescuentoValor8 = New System.Windows.Forms.TextBox()
        Me.RapidatxtMontoItem8 = New System.Windows.Forms.TextBox()
        Me.RapidacmbIndicadorFacturacion8 = New System.Windows.Forms.ComboBox()
        Me.RapidatxtNombreItem8 = New System.Windows.Forms.TextBox()
        Me.RapidatxtCantidad8 = New System.Windows.Forms.TextBox()
        Me.RapidatxtPrecioUnitario8 = New System.Windows.Forms.TextBox()
        Me.RapidatxtSubDescuentoValor7 = New System.Windows.Forms.TextBox()
        Me.RapidatxtMontoItem7 = New System.Windows.Forms.TextBox()
        Me.RapidacmbIndicadorFacturacion7 = New System.Windows.Forms.ComboBox()
        Me.RapidatxtNombreItem7 = New System.Windows.Forms.TextBox()
        Me.RapidatxtCantidad7 = New System.Windows.Forms.TextBox()
        Me.RapidatxtPrecioUnitario7 = New System.Windows.Forms.TextBox()
        Me.RapidatxtSubDescuentoValor6 = New System.Windows.Forms.TextBox()
        Me.RapidatxtMontoItem6 = New System.Windows.Forms.TextBox()
        Me.RapidacmbIndicadorFacturacion6 = New System.Windows.Forms.ComboBox()
        Me.RapidatxtNombreItem6 = New System.Windows.Forms.TextBox()
        Me.RapidatxtCantidad6 = New System.Windows.Forms.TextBox()
        Me.RapidatxtPrecioUnitario6 = New System.Windows.Forms.TextBox()
        Me.panAdvertencia = New System.Windows.Forms.Panel()
        Me.lblCerrar = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.lblAdvertencia = New System.Windows.Forms.Label()
        Me.Lbl_NewAge = New System.Windows.Forms.Label()
        Me.RapidatxtAdenda = New System.Windows.Forms.TextBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.RapidatxtSubDescuentoValor5 = New System.Windows.Forms.TextBox()
        Me.RapidatxtMontoItem5 = New System.Windows.Forms.TextBox()
        Me.RapidacmbIndicadorFacturacion5 = New System.Windows.Forms.ComboBox()
        Me.RapidatxtNombreItem5 = New System.Windows.Forms.TextBox()
        Me.RapidatxtCantidad5 = New System.Windows.Forms.TextBox()
        Me.RapidatxtPrecioUnitario5 = New System.Windows.Forms.TextBox()
        Me.RapidatxtSubDescuentoValor4 = New System.Windows.Forms.TextBox()
        Me.RapidatxtMontoItem4 = New System.Windows.Forms.TextBox()
        Me.RapidacmbIndicadorFacturacion4 = New System.Windows.Forms.ComboBox()
        Me.RapidatxtNombreItem4 = New System.Windows.Forms.TextBox()
        Me.RapidatxtCantidad4 = New System.Windows.Forms.TextBox()
        Me.RapidatxtPrecioUnitario4 = New System.Windows.Forms.TextBox()
        Me.RapidatxtSubDescuentoValor3 = New System.Windows.Forms.TextBox()
        Me.RapidatxtMontoItem3 = New System.Windows.Forms.TextBox()
        Me.RapidacmbIndicadorFacturacion3 = New System.Windows.Forms.ComboBox()
        Me.RapidatxtNombreItem3 = New System.Windows.Forms.TextBox()
        Me.RapidatxtCantidad3 = New System.Windows.Forms.TextBox()
        Me.RapidatxtPrecioUnitario3 = New System.Windows.Forms.TextBox()
        Me.RapidatxtSubDescuentoValor2 = New System.Windows.Forms.TextBox()
        Me.RapidatxtMontoItem2 = New System.Windows.Forms.TextBox()
        Me.RapidacmbIndicadorFacturacion2 = New System.Windows.Forms.ComboBox()
        Me.RapidatxtNombreItem2 = New System.Windows.Forms.TextBox()
        Me.RapidatxtCantidad2 = New System.Windows.Forms.TextBox()
        Me.RapidatxtPrecioUnitario2 = New System.Windows.Forms.TextBox()
        Me.RapidatxtSubDescuentoValor1 = New System.Windows.Forms.TextBox()
        Me.Label179 = New System.Windows.Forms.Label()
        Me.RapidatxtMontoItem1 = New System.Windows.Forms.TextBox()
        Me.Label177 = New System.Windows.Forms.Label()
        Me.RapidacmbIndicadorFacturacion1 = New System.Windows.Forms.ComboBox()
        Me.BtnLimpiarCampos = New System.Windows.Forms.Button()
        Me.RapidatxtNombreItem1 = New System.Windows.Forms.TextBox()
        Me.BtnFacturar = New System.Windows.Forms.Button()
        Me.GBx_Totales = New System.Windows.Forms.GroupBox()
        Me.TBx_ExportacionYAsimilados = New System.Windows.Forms.TextBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.TBx_MontoRetenido = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.RapidatxtTotalMontoNoGravado = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.RapidaBtn_guardar = New System.Windows.Forms.Button()
        Me.lblDolares = New System.Windows.Forms.Label()
        Me.lblPesos = New System.Windows.Forms.Label()
        Me.RapidaLbl_tipoCambio = New System.Windows.Forms.Label()
        Me.RapidaTxtMontoNoFacturable = New System.Windows.Forms.TextBox()
        Me.RapidaTbx_tipoCambio = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.RapidatxtMontoTotalPagar = New System.Windows.Forms.Label()
        Me.lblMonedaSimbolo = New System.Windows.Forms.Label()
        Me.RapidatxtTotalIVATasaBasica = New System.Windows.Forms.TextBox()
        Me.Label186 = New System.Windows.Forms.Label()
        Me.RapidatxtTotalIVATasaMinima = New System.Windows.Forms.TextBox()
        Me.Label187 = New System.Windows.Forms.Label()
        Me.RapidatxtTotalMontoNetoIVATasaBasica = New System.Windows.Forms.TextBox()
        Me.Label172 = New System.Windows.Forms.Label()
        Me.RapidatxtTotalMontoNetoIVATasaMinima = New System.Windows.Forms.TextBox()
        Me.Label185 = New System.Windows.Forms.Label()
        Me.RapidatxtCantidad1 = New System.Windows.Forms.TextBox()
        Me.GBx_MediosPagos = New System.Windows.Forms.GroupBox()
        Me.RapidatxtVuelto = New System.Windows.Forms.TextBox()
        Me.Label169 = New System.Windows.Forms.Label()
        Me.RapidatxtValorMediosDePago2 = New System.Windows.Forms.TextBox()
        Me.RapidatxtValorMediosDePago1 = New System.Windows.Forms.TextBox()
        Me.Label167 = New System.Windows.Forms.Label()
        Me.RapidatxtGlosaMediosDePago2 = New System.Windows.Forms.TextBox()
        Me.RapidatxtGlosaMediosDePago1 = New System.Windows.Forms.TextBox()
        Me.Label168 = New System.Windows.Forms.Label()
        Me.Label173 = New System.Windows.Forms.Label()
        Me.Label174 = New System.Windows.Forms.Label()
        Me.Label181 = New System.Windows.Forms.Label()
        Me.RapidatxtPrecioUnitario1 = New System.Windows.Forms.TextBox()
        Me.Label176 = New System.Windows.Forms.Label()
        Me.Label178 = New System.Windows.Forms.Label()
        Me.Label180 = New System.Windows.Forms.Label()
        Me.Label182 = New System.Windows.Forms.Label()
        Me.GroupBox8 = New System.Windows.Forms.GroupBox()
        Me.Btn_Buscar = New System.Windows.Forms.Button()
        Me.Ckb_Exportacion = New System.Windows.Forms.CheckBox()
        Me.chkConsumoFinal = New System.Windows.Forms.CheckBox()
        Me.RapidatxtDepartamentoReceptor = New System.Windows.Forms.TextBox()
        Me.Label210 = New System.Windows.Forms.Label()
        Me.RapidatxtCiudadReceptor = New System.Windows.Forms.TextBox()
        Me.Label197 = New System.Windows.Forms.Label()
        Me.RapidatxtDireccionReceptor = New System.Windows.Forms.TextBox()
        Me.Label85 = New System.Windows.Forms.Label()
        Me.RapidatxtNombreReceptor = New System.Windows.Forms.TextBox()
        Me.Label86 = New System.Windows.Forms.Label()
        Me.RapidatxtDocumentoReceptor = New System.Windows.Forms.TextBox()
        Me.Label163 = New System.Windows.Forms.Label()
        Me.RapidacmbCodigoPaisReceptor = New System.Windows.Forms.ComboBox()
        Me.RapidacmbTipoDocumentoReceptor = New System.Windows.Forms.ComboBox()
        Me.Label75 = New System.Windows.Forms.Label()
        Me.Label84 = New System.Windows.Forms.Label()
        Me.GroupBox11 = New System.Windows.Forms.GroupBox()
        Me.CmbIndicadorMontoBrutoEncabezado = New System.Windows.Forms.ComboBox()
        Me.chkVencimientoCredito = New System.Windows.Forms.CheckBox()
        Me.lblVencimiento = New System.Windows.Forms.Label()
        Me.dtpFechaVencimiento = New System.Windows.Forms.DateTimePicker()
        Me.lblFormaDePago = New System.Windows.Forms.Label()
        Me.cmbFormaPago = New System.Windows.Forms.ComboBox()
        Me.chkIndicadorRefGlobal = New System.Windows.Forms.CheckBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblReferencia = New System.Windows.Forms.Label()
        Me.lblFechaCFEInformacionReferencia = New System.Windows.Forms.Label()
        Me.Label164 = New System.Windows.Forms.Label()
        Me.dtpFechaCFEInformacionReferencia = New System.Windows.Forms.DateTimePicker()
        Me.RapidatxtNumeroComprobanteEncabezado = New System.Windows.Forms.TextBox()
        Me.RapidatxtSerieComprobanteEncabezado = New System.Windows.Forms.TextBox()
        Me.Label165 = New System.Windows.Forms.Label()
        Me.txtRazonInformacionReferencia = New System.Windows.Forms.TextBox()
        Me.txtNumeroCFEInformacionReferencia = New System.Windows.Forms.TextBox()
        Me.Label166 = New System.Windows.Forms.Label()
        Me.lblRazonInformacionReferencia = New System.Windows.Forms.Label()
        Me.lblNumeroCFEInformacionReferencia = New System.Windows.Forms.Label()
        Me.cmbTipoVenta = New System.Windows.Forms.ComboBox()
        Me.txtSerieCFEInformacionReferencia = New System.Windows.Forms.TextBox()
        Me.cmbTipoCFEInformacionReferencia = New System.Windows.Forms.ComboBox()
        Me.lblSerieCFEInformacionReferencia = New System.Windows.Forms.Label()
        Me.lblTipoCFEInformacionReferencia = New System.Windows.Forms.Label()
        Me.GroupBox_Remito = New System.Windows.Forms.GroupBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.textBox_NroDocPropMercaderiaTransp = New System.Windows.Forms.TextBox()
        Me.cmbTipoDocMercaderiaTransp = New System.Windows.Forms.ComboBox()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.cmbCodPaisPropMercaderiaTransp = New System.Windows.Forms.ComboBox()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.TextBox_NombreRznSocPropMercaderiaTransp = New System.Windows.Forms.TextBox()
        Me.cmbIndicadorPropMercaderiaTransp = New System.Windows.Forms.ComboBox()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.cmbIndicadorTipoTrasladoBienesEncabezado = New System.Windows.Forms.ComboBox()
        Me.Label158 = New System.Windows.Forms.Label()
        Me.Configuracion = New System.Windows.Forms.TabPage()
        Me.GroupBox10 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.chkModificarTipoCuenta = New System.Windows.Forms.CheckBox()
        Me.chkModificarDecretoLey = New System.Windows.Forms.CheckBox()
        Me.chkModificarPlan = New System.Windows.Forms.CheckBox()
        Me.chkModificarTarjeta = New System.Windows.Forms.CheckBox()
        Me.chkModificarFactura = New System.Windows.Forms.CheckBox()
        Me.chkModificarCuotas = New System.Windows.Forms.CheckBox()
        Me.chkModificarMontos = New System.Windows.Forms.CheckBox()
        Me.chkModificarMoneda = New System.Windows.Forms.CheckBox()
        Me.txtTermCod = New System.Windows.Forms.TextBox()
        Me.txtEmpCod = New System.Windows.Forms.TextBox()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.GroupBox9 = New System.Windows.Forms.GroupBox()
        Me.chkCierreCentralizado = New System.Windows.Forms.CheckBox()
        Me.chkConsultarUltimoCierre = New System.Windows.Forms.CheckBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.cmbIdProcesadorTransAct = New System.Windows.Forms.ComboBox()
        Me.btnCerrarLote = New System.Windows.Forms.Button()
        Me.Chk_IVAalDia = New System.Windows.Forms.CheckBox()
        Me.Chk_emiteRemitos = New System.Windows.Forms.CheckBox()
        Me.Chk_usaExportacion = New System.Windows.Forms.CheckBox()
        Me.Chk_PreFactura = New System.Windows.Forms.CheckBox()
        Me.Chk_UsaRetPerc = New System.Windows.Forms.CheckBox()
        Me.Chk_UsaSubTotales = New System.Windows.Forms.CheckBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtCfgNombreEmisor = New System.Windows.Forms.TextBox()
        Me.Chk_Bloqueado = New System.Windows.Forms.CheckBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtCfgTelefono2Emisor = New System.Windows.Forms.TextBox()
        Me.Button_Sincronizar = New System.Windows.Forms.Button()
        Me.chkConsumoFinalDefault = New System.Windows.Forms.CheckBox()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cmbCfgIndicadorFacturacion = New System.Windows.Forms.ComboBox()
        Me.Label211 = New System.Windows.Forms.Label()
        Me.txtNroSucursal = New System.Windows.Forms.TextBox()
        Me.gbxImpresion = New System.Windows.Forms.GroupBox()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.rdButtonImprimeVoucherAdenda = New System.Windows.Forms.RadioButton()
        Me.rdButtonImprimeVoucherTransAct = New System.Windows.Forms.RadioButton()
        Me.chkAbrirCajon = New System.Windows.Forms.CheckBox()
        Me.txtCantCopias = New System.Windows.Forms.NumericUpDown()
        Me.lblCopias = New System.Windows.Forms.Label()
        Me.btnImprimirPrueba = New System.Windows.Forms.Button()
        Me.chkImprimeApi = New System.Windows.Forms.CheckBox()
        Me.cmbImpresora = New System.Windows.Forms.ComboBox()
        Me.cmbTipoImpresora = New System.Windows.Forms.ComboBox()
        Me.lblTipoImpresora = New System.Windows.Forms.Label()
        Me.lblNomImpresora = New System.Windows.Forms.Label()
        Me.grpDatosServidor = New System.Windows.Forms.GroupBox()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.txturlServidorGateway = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TextBox_CarpetaOperacion = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.txtSegundosTimeout = New System.Windows.Forms.TextBox()
        Me.BtnGuardarConfig = New System.Windows.Forms.Button()
        Me.cmbTipoNegocio = New System.Windows.Forms.ComboBox()
        Me.cmbCfgTipoMonedaTransaccion = New System.Windows.Forms.ComboBox()
        Me.Label209 = New System.Windows.Forms.Label()
        Me.Label202 = New System.Windows.Forms.Label()
        Me.txtCfgVendedorNom = New System.Windows.Forms.TextBox()
        Me.Label203 = New System.Windows.Forms.Label()
        Me.txtCfgVendedorNro = New System.Windows.Forms.TextBox()
        Me.Label205 = New System.Windows.Forms.Label()
        Me.txtCfgValorUnidadIndexada = New System.Windows.Forms.TextBox()
        Me.Label206 = New System.Windows.Forms.Label()
        Me.txtCfgTasaMinimaIVA = New System.Windows.Forms.TextBox()
        Me.Label207 = New System.Windows.Forms.Label()
        Me.txtCfgTasaBasicaIVA = New System.Windows.Forms.TextBox()
        Me.Label208 = New System.Windows.Forms.Label()
        Me.Label175 = New System.Windows.Forms.Label()
        Me.txtCfgTipoDeCambio = New System.Windows.Forms.TextBox()
        Me.Label194 = New System.Windows.Forms.Label()
        Me.Label195 = New System.Windows.Forms.Label()
        Me.txtCfgCajeroNombre = New System.Windows.Forms.TextBox()
        Me.Label196 = New System.Windows.Forms.Label()
        Me.txtCfgCajeroNro = New System.Windows.Forms.TextBox()
        Me.txtCfgCajaNro = New System.Windows.Forms.TextBox()
        Me.Label198 = New System.Windows.Forms.Label()
        Me.txtCfgNombreSucursalEmisor = New System.Windows.Forms.TextBox()
        Me.Label199 = New System.Windows.Forms.Label()
        Me.txtCfgCodigoSucursalEmisor = New System.Windows.Forms.TextBox()
        Me.Label200 = New System.Windows.Forms.Label()
        Me.txtCfgTelefonoEmisor = New System.Windows.Forms.TextBox()
        Me.Label201 = New System.Windows.Forms.Label()
        Me.txtCfgCiudadEmisor = New System.Windows.Forms.TextBox()
        Me.Label193 = New System.Windows.Forms.Label()
        Me.txtCfgDepartamentoEmisor = New System.Windows.Forms.TextBox()
        Me.Label192 = New System.Windows.Forms.Label()
        Me.txtCfgDomicilioFiscalEmisor = New System.Windows.Forms.TextBox()
        Me.Label191 = New System.Windows.Forms.Label()
        Me.txtCfgCorreoEmisor = New System.Windows.Forms.TextBox()
        Me.Label190 = New System.Windows.Forms.Label()
        Me.txtCfgGiroNegocioEmisor = New System.Windows.Forms.TextBox()
        Me.Label188 = New System.Windows.Forms.Label()
        Me.txtCfgRUTEmisor = New System.Windows.Forms.TextBox()
        Me.Label184 = New System.Windows.Forms.Label()
        Me.txtCfgRazonSocialEmisor = New System.Windows.Forms.TextBox()
        Me.NotifyIcon1 = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.TabControl1.SuspendLayout()
        Me.FacturaRapida.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GBx_Exportaciones.SuspendLayout()
        Me.GBx_RetencionesPercepciones.SuspendLayout()
        Me.GBx_PreFactura.SuspendLayout()
        Me.GBx_SubTotalesInformativos.SuspendLayout()
        Me.panAdvertencia.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GBx_Totales.SuspendLayout()
        Me.GBx_MediosPagos.SuspendLayout()
        Me.GroupBox8.SuspendLayout()
        Me.GroupBox11.SuspendLayout()
        Me.GroupBox_Remito.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Configuracion.SuspendLayout()
        Me.GroupBox10.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox9.SuspendLayout()
        Me.gbxImpresion.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        CType(Me.txtCantCopias, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpDatosServidor.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.FacturaRapida)
        Me.TabControl1.Controls.Add(Me.Configuracion)
        Me.TabControl1.Location = New System.Drawing.Point(12, 3)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1093, 1056)
        Me.TabControl1.TabIndex = 56
        '
        'FacturaRapida
        '
        Me.FacturaRapida.Controls.Add(Me.chkSoloFacturar)
        Me.FacturaRapida.Controls.Add(Me.chkSoloCobrar)
        Me.FacturaRapida.Controls.Add(Me.GroupBox3)
        Me.FacturaRapida.Controls.Add(Me.GroupBox1)
        Me.FacturaRapida.Controls.Add(Me.Ckb_esSecretoProfesional)
        Me.FacturaRapida.Controls.Add(Me.btn_buscarArticulo10)
        Me.FacturaRapida.Controls.Add(Me.btn_buscarArticulo9)
        Me.FacturaRapida.Controls.Add(Me.btn_buscarArticulo8)
        Me.FacturaRapida.Controls.Add(Me.btn_buscarArticulo7)
        Me.FacturaRapida.Controls.Add(Me.btn_buscarArticulo6)
        Me.FacturaRapida.Controls.Add(Me.btn_buscarArticulo5)
        Me.FacturaRapida.Controls.Add(Me.btn_buscarArticulo4)
        Me.FacturaRapida.Controls.Add(Me.btn_buscarArticulo3)
        Me.FacturaRapida.Controls.Add(Me.btn_buscarArticulo2)
        Me.FacturaRapida.Controls.Add(Me.btn_buscarArticulo1)
        Me.FacturaRapida.Controls.Add(Me.GBx_Exportaciones)
        Me.FacturaRapida.Controls.Add(Me.Lbl_Comentarios)
        Me.FacturaRapida.Controls.Add(Me.GBx_RetencionesPercepciones)
        Me.FacturaRapida.Controls.Add(Me.GBx_PreFactura)
        Me.FacturaRapida.Controls.Add(Me.GBx_SubTotalesInformativos)
        Me.FacturaRapida.Controls.Add(Me.Ckb_esContingencia)
        Me.FacturaRapida.Controls.Add(Me.btnReimprimir)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtSubDescuentoValor10)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtMontoItem10)
        Me.FacturaRapida.Controls.Add(Me.RapidacmbIndicadorFacturacion10)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtNombreItem10)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtCantidad10)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtPrecioUnitario10)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtSubDescuentoValor9)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtMontoItem9)
        Me.FacturaRapida.Controls.Add(Me.RapidacmbIndicadorFacturacion9)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtNombreItem9)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtCantidad9)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtPrecioUnitario9)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtSubDescuentoValor8)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtMontoItem8)
        Me.FacturaRapida.Controls.Add(Me.RapidacmbIndicadorFacturacion8)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtNombreItem8)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtCantidad8)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtPrecioUnitario8)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtSubDescuentoValor7)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtMontoItem7)
        Me.FacturaRapida.Controls.Add(Me.RapidacmbIndicadorFacturacion7)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtNombreItem7)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtCantidad7)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtPrecioUnitario7)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtSubDescuentoValor6)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtMontoItem6)
        Me.FacturaRapida.Controls.Add(Me.RapidacmbIndicadorFacturacion6)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtNombreItem6)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtCantidad6)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtPrecioUnitario6)
        Me.FacturaRapida.Controls.Add(Me.panAdvertencia)
        Me.FacturaRapida.Controls.Add(Me.Lbl_NewAge)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtAdenda)
        Me.FacturaRapida.Controls.Add(Me.PictureBox2)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtSubDescuentoValor5)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtMontoItem5)
        Me.FacturaRapida.Controls.Add(Me.RapidacmbIndicadorFacturacion5)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtNombreItem5)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtCantidad5)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtPrecioUnitario5)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtSubDescuentoValor4)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtMontoItem4)
        Me.FacturaRapida.Controls.Add(Me.RapidacmbIndicadorFacturacion4)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtNombreItem4)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtCantidad4)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtPrecioUnitario4)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtSubDescuentoValor3)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtMontoItem3)
        Me.FacturaRapida.Controls.Add(Me.RapidacmbIndicadorFacturacion3)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtNombreItem3)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtCantidad3)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtPrecioUnitario3)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtSubDescuentoValor2)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtMontoItem2)
        Me.FacturaRapida.Controls.Add(Me.RapidacmbIndicadorFacturacion2)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtNombreItem2)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtCantidad2)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtPrecioUnitario2)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtSubDescuentoValor1)
        Me.FacturaRapida.Controls.Add(Me.Label179)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtMontoItem1)
        Me.FacturaRapida.Controls.Add(Me.Label177)
        Me.FacturaRapida.Controls.Add(Me.RapidacmbIndicadorFacturacion1)
        Me.FacturaRapida.Controls.Add(Me.BtnLimpiarCampos)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtNombreItem1)
        Me.FacturaRapida.Controls.Add(Me.BtnFacturar)
        Me.FacturaRapida.Controls.Add(Me.GBx_Totales)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtCantidad1)
        Me.FacturaRapida.Controls.Add(Me.GBx_MediosPagos)
        Me.FacturaRapida.Controls.Add(Me.RapidatxtPrecioUnitario1)
        Me.FacturaRapida.Controls.Add(Me.Label176)
        Me.FacturaRapida.Controls.Add(Me.Label178)
        Me.FacturaRapida.Controls.Add(Me.Label180)
        Me.FacturaRapida.Controls.Add(Me.Label182)
        Me.FacturaRapida.Controls.Add(Me.GroupBox8)
        Me.FacturaRapida.Controls.Add(Me.GroupBox11)
        Me.FacturaRapida.Controls.Add(Me.GroupBox_Remito)
        Me.FacturaRapida.Location = New System.Drawing.Point(4, 22)
        Me.FacturaRapida.Name = "FacturaRapida"
        Me.FacturaRapida.Size = New System.Drawing.Size(1085, 1030)
        Me.FacturaRapida.TabIndex = 5
        Me.FacturaRapida.Text = "Factura Rapida"
        Me.FacturaRapida.UseVisualStyleBackColor = True
        '
        'chkSoloFacturar
        '
        Me.chkSoloFacturar.AutoSize = True
        Me.chkSoloFacturar.Location = New System.Drawing.Point(876, 29)
        Me.chkSoloFacturar.Name = "chkSoloFacturar"
        Me.chkSoloFacturar.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.chkSoloFacturar.Size = New System.Drawing.Size(86, 17)
        Me.chkSoloFacturar.TabIndex = 317
        Me.chkSoloFacturar.Text = "Solo facturar"
        Me.chkSoloFacturar.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.chkSoloFacturar.UseVisualStyleBackColor = True
        '
        'chkSoloCobrar
        '
        Me.chkSoloCobrar.AutoSize = True
        Me.chkSoloCobrar.Location = New System.Drawing.Point(882, 6)
        Me.chkSoloCobrar.Name = "chkSoloCobrar"
        Me.chkSoloCobrar.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.chkSoloCobrar.Size = New System.Drawing.Size(80, 17)
        Me.chkSoloCobrar.TabIndex = 316
        Me.chkSoloCobrar.Text = "Solo cobrar"
        Me.chkSoloCobrar.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.chkSoloCobrar.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.rbDevolucion)
        Me.GroupBox3.Controls.Add(Me.rbVenta)
        Me.GroupBox3.Location = New System.Drawing.Point(713, 59)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(166, 47)
        Me.GroupBox3.TabIndex = 303
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Operación"
        '
        'rbDevolucion
        '
        Me.rbDevolucion.AutoSize = True
        Me.rbDevolucion.Location = New System.Drawing.Point(80, 19)
        Me.rbDevolucion.Name = "rbDevolucion"
        Me.rbDevolucion.Size = New System.Drawing.Size(79, 17)
        Me.rbDevolucion.TabIndex = 8
        Me.rbDevolucion.Text = "Devolución"
        Me.rbDevolucion.UseVisualStyleBackColor = True
        '
        'rbVenta
        '
        Me.rbVenta.AutoSize = True
        Me.rbVenta.Checked = True
        Me.rbVenta.Location = New System.Drawing.Point(12, 19)
        Me.rbVenta.Name = "rbVenta"
        Me.rbVenta.Size = New System.Drawing.Size(53, 17)
        Me.rbVenta.TabIndex = 6
        Me.rbVenta.TabStop = True
        Me.rbVenta.Text = "Venta"
        Me.rbVenta.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Rdb_esTckFact)
        Me.GroupBox1.Controls.Add(Me.Rdb_esRemito)
        Me.GroupBox1.Controls.Add(Me.Rdb_esBoleta)
        Me.GroupBox1.Location = New System.Drawing.Point(883, 59)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(190, 47)
        Me.GroupBox1.TabIndex = 302
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Tipos CFE"
        '
        'Rdb_esTckFact
        '
        Me.Rdb_esTckFact.AutoSize = True
        Me.Rdb_esTckFact.Checked = True
        Me.Rdb_esTckFact.Location = New System.Drawing.Point(7, 19)
        Me.Rdb_esTckFact.Name = "Rdb_esTckFact"
        Me.Rdb_esTckFact.Size = New System.Drawing.Size(61, 17)
        Me.Rdb_esTckFact.TabIndex = 305
        Me.Rdb_esTckFact.TabStop = True
        Me.Rdb_esTckFact.Text = "Factura"
        Me.Rdb_esTckFact.UseVisualStyleBackColor = True
        '
        'Rdb_esRemito
        '
        Me.Rdb_esRemito.AutoSize = True
        Me.Rdb_esRemito.Location = New System.Drawing.Point(127, 19)
        Me.Rdb_esRemito.Name = "Rdb_esRemito"
        Me.Rdb_esRemito.Size = New System.Drawing.Size(58, 17)
        Me.Rdb_esRemito.TabIndex = 303
        Me.Rdb_esRemito.Text = "Remito"
        Me.Rdb_esRemito.UseVisualStyleBackColor = True
        '
        'Rdb_esBoleta
        '
        Me.Rdb_esBoleta.AutoSize = True
        Me.Rdb_esBoleta.Location = New System.Drawing.Point(70, 19)
        Me.Rdb_esBoleta.Name = "Rdb_esBoleta"
        Me.Rdb_esBoleta.Size = New System.Drawing.Size(55, 17)
        Me.Rdb_esBoleta.TabIndex = 304
        Me.Rdb_esBoleta.Text = "Boleta"
        Me.Rdb_esBoleta.UseVisualStyleBackColor = True
        '
        'Ckb_esSecretoProfesional
        '
        Me.Ckb_esSecretoProfesional.AutoSize = True
        Me.Ckb_esSecretoProfesional.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Ckb_esSecretoProfesional.Location = New System.Drawing.Point(961, 31)
        Me.Ckb_esSecretoProfesional.Name = "Ckb_esSecretoProfesional"
        Me.Ckb_esSecretoProfesional.Size = New System.Drawing.Size(117, 17)
        Me.Ckb_esSecretoProfesional.TabIndex = 300
        Me.Ckb_esSecretoProfesional.Text = "Secreto profesional"
        Me.Ckb_esSecretoProfesional.UseVisualStyleBackColor = True
        '
        'btn_buscarArticulo10
        '
        Me.btn_buscarArticulo10.Image = CType(resources.GetObject("btn_buscarArticulo10.Image"), System.Drawing.Image)
        Me.btn_buscarArticulo10.Location = New System.Drawing.Point(479, 470)
        Me.btn_buscarArticulo10.Name = "btn_buscarArticulo10"
        Me.btn_buscarArticulo10.Size = New System.Drawing.Size(24, 22)
        Me.btn_buscarArticulo10.TabIndex = 295
        Me.btn_buscarArticulo10.UseVisualStyleBackColor = True
        '
        'btn_buscarArticulo9
        '
        Me.btn_buscarArticulo9.Image = CType(resources.GetObject("btn_buscarArticulo9.Image"), System.Drawing.Image)
        Me.btn_buscarArticulo9.Location = New System.Drawing.Point(479, 449)
        Me.btn_buscarArticulo9.Name = "btn_buscarArticulo9"
        Me.btn_buscarArticulo9.Size = New System.Drawing.Size(24, 22)
        Me.btn_buscarArticulo9.TabIndex = 298
        Me.btn_buscarArticulo9.UseVisualStyleBackColor = True
        '
        'btn_buscarArticulo8
        '
        Me.btn_buscarArticulo8.Image = CType(resources.GetObject("btn_buscarArticulo8.Image"), System.Drawing.Image)
        Me.btn_buscarArticulo8.Location = New System.Drawing.Point(479, 428)
        Me.btn_buscarArticulo8.Name = "btn_buscarArticulo8"
        Me.btn_buscarArticulo8.Size = New System.Drawing.Size(24, 22)
        Me.btn_buscarArticulo8.TabIndex = 297
        Me.btn_buscarArticulo8.UseVisualStyleBackColor = True
        '
        'btn_buscarArticulo7
        '
        Me.btn_buscarArticulo7.Image = CType(resources.GetObject("btn_buscarArticulo7.Image"), System.Drawing.Image)
        Me.btn_buscarArticulo7.Location = New System.Drawing.Point(479, 407)
        Me.btn_buscarArticulo7.Name = "btn_buscarArticulo7"
        Me.btn_buscarArticulo7.Size = New System.Drawing.Size(24, 22)
        Me.btn_buscarArticulo7.TabIndex = 296
        Me.btn_buscarArticulo7.UseVisualStyleBackColor = True
        '
        'btn_buscarArticulo6
        '
        Me.btn_buscarArticulo6.Image = CType(resources.GetObject("btn_buscarArticulo6.Image"), System.Drawing.Image)
        Me.btn_buscarArticulo6.Location = New System.Drawing.Point(479, 386)
        Me.btn_buscarArticulo6.Name = "btn_buscarArticulo6"
        Me.btn_buscarArticulo6.Size = New System.Drawing.Size(24, 22)
        Me.btn_buscarArticulo6.TabIndex = 295
        Me.btn_buscarArticulo6.UseVisualStyleBackColor = True
        '
        'btn_buscarArticulo5
        '
        Me.btn_buscarArticulo5.Image = CType(resources.GetObject("btn_buscarArticulo5.Image"), System.Drawing.Image)
        Me.btn_buscarArticulo5.Location = New System.Drawing.Point(479, 365)
        Me.btn_buscarArticulo5.Name = "btn_buscarArticulo5"
        Me.btn_buscarArticulo5.Size = New System.Drawing.Size(24, 22)
        Me.btn_buscarArticulo5.TabIndex = 294
        Me.btn_buscarArticulo5.UseVisualStyleBackColor = True
        '
        'btn_buscarArticulo4
        '
        Me.btn_buscarArticulo4.Image = CType(resources.GetObject("btn_buscarArticulo4.Image"), System.Drawing.Image)
        Me.btn_buscarArticulo4.Location = New System.Drawing.Point(479, 344)
        Me.btn_buscarArticulo4.Name = "btn_buscarArticulo4"
        Me.btn_buscarArticulo4.Size = New System.Drawing.Size(24, 22)
        Me.btn_buscarArticulo4.TabIndex = 293
        Me.btn_buscarArticulo4.UseVisualStyleBackColor = True
        '
        'btn_buscarArticulo3
        '
        Me.btn_buscarArticulo3.Image = CType(resources.GetObject("btn_buscarArticulo3.Image"), System.Drawing.Image)
        Me.btn_buscarArticulo3.Location = New System.Drawing.Point(479, 323)
        Me.btn_buscarArticulo3.Name = "btn_buscarArticulo3"
        Me.btn_buscarArticulo3.Size = New System.Drawing.Size(24, 22)
        Me.btn_buscarArticulo3.TabIndex = 292
        Me.btn_buscarArticulo3.UseVisualStyleBackColor = True
        '
        'btn_buscarArticulo2
        '
        Me.btn_buscarArticulo2.Image = CType(resources.GetObject("btn_buscarArticulo2.Image"), System.Drawing.Image)
        Me.btn_buscarArticulo2.Location = New System.Drawing.Point(479, 302)
        Me.btn_buscarArticulo2.Name = "btn_buscarArticulo2"
        Me.btn_buscarArticulo2.Size = New System.Drawing.Size(24, 22)
        Me.btn_buscarArticulo2.TabIndex = 291
        Me.btn_buscarArticulo2.UseVisualStyleBackColor = True
        '
        'btn_buscarArticulo1
        '
        Me.btn_buscarArticulo1.Image = CType(resources.GetObject("btn_buscarArticulo1.Image"), System.Drawing.Image)
        Me.btn_buscarArticulo1.Location = New System.Drawing.Point(479, 281)
        Me.btn_buscarArticulo1.Name = "btn_buscarArticulo1"
        Me.btn_buscarArticulo1.Size = New System.Drawing.Size(24, 22)
        Me.btn_buscarArticulo1.TabIndex = 290
        Me.btn_buscarArticulo1.UseVisualStyleBackColor = True
        '
        'GBx_Exportaciones
        '
        Me.GBx_Exportaciones.Controls.Add(Me.CBx_ViaTransporte)
        Me.GBx_Exportaciones.Controls.Add(Me.Label30)
        Me.GBx_Exportaciones.Controls.Add(Me.Label29)
        Me.GBx_Exportaciones.Controls.Add(Me.TBx_ClausulaDeVenta)
        Me.GBx_Exportaciones.Controls.Add(Me.CBx_ModalidadVenta)
        Me.GBx_Exportaciones.Controls.Add(Me.Label28)
        Me.GBx_Exportaciones.Location = New System.Drawing.Point(785, 538)
        Me.GBx_Exportaciones.Name = "GBx_Exportaciones"
        Me.GBx_Exportaciones.Size = New System.Drawing.Size(288, 102)
        Me.GBx_Exportaciones.TabIndex = 289
        Me.GBx_Exportaciones.TabStop = False
        Me.GBx_Exportaciones.Text = "Exportaciones"
        '
        'CBx_ViaTransporte
        '
        Me.CBx_ViaTransporte.FormattingEnabled = True
        Me.CBx_ViaTransporte.Location = New System.Drawing.Point(115, 70)
        Me.CBx_ViaTransporte.Name = "CBx_ViaTransporte"
        Me.CBx_ViaTransporte.Size = New System.Drawing.Size(167, 21)
        Me.CBx_ViaTransporte.TabIndex = 5
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(7, 75)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(87, 13)
        Me.Label30.TabIndex = 4
        Me.Label30.Text = "Via de transporte"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(7, 48)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(101, 13)
        Me.Label29.TabIndex = 3
        Me.Label29.Text = "Modalidad de venta"
        '
        'TBx_ClausulaDeVenta
        '
        Me.TBx_ClausulaDeVenta.Location = New System.Drawing.Point(115, 17)
        Me.TBx_ClausulaDeVenta.Name = "TBx_ClausulaDeVenta"
        Me.TBx_ClausulaDeVenta.Size = New System.Drawing.Size(167, 20)
        Me.TBx_ClausulaDeVenta.TabIndex = 2
        '
        'CBx_ModalidadVenta
        '
        Me.CBx_ModalidadVenta.FormattingEnabled = True
        Me.CBx_ModalidadVenta.Location = New System.Drawing.Point(115, 43)
        Me.CBx_ModalidadVenta.Name = "CBx_ModalidadVenta"
        Me.CBx_ModalidadVenta.Size = New System.Drawing.Size(167, 21)
        Me.CBx_ModalidadVenta.TabIndex = 1
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(7, 20)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(92, 13)
        Me.Label28.TabIndex = 0
        Me.Label28.Text = "Cláusula de venta"
        '
        'Lbl_Comentarios
        '
        Me.Lbl_Comentarios.AutoSize = True
        Me.Lbl_Comentarios.Location = New System.Drawing.Point(5, 497)
        Me.Lbl_Comentarios.Name = "Lbl_Comentarios"
        Me.Lbl_Comentarios.Size = New System.Drawing.Size(47, 13)
        Me.Lbl_Comentarios.TabIndex = 288
        Me.Lbl_Comentarios.Text = "Adenda:"
        '
        'GBx_RetencionesPercepciones
        '
        Me.GBx_RetencionesPercepciones.Controls.Add(Me.Label12)
        Me.GBx_RetencionesPercepciones.Controls.Add(Me.LBx_RetencionesPercepciones)
        Me.GBx_RetencionesPercepciones.Controls.Add(Me.Btn_RetencionesPercepciones10)
        Me.GBx_RetencionesPercepciones.Controls.Add(Me.Btn_RetencionesPercepciones9)
        Me.GBx_RetencionesPercepciones.Controls.Add(Me.Btn_RetencionesPercepciones8)
        Me.GBx_RetencionesPercepciones.Controls.Add(Me.Btn_RetencionesPercepciones7)
        Me.GBx_RetencionesPercepciones.Controls.Add(Me.Btn_RetencionesPercepciones6)
        Me.GBx_RetencionesPercepciones.Controls.Add(Me.Btn_RetencionesPercepciones5)
        Me.GBx_RetencionesPercepciones.Controls.Add(Me.Btn_RetencionesPercepciones4)
        Me.GBx_RetencionesPercepciones.Controls.Add(Me.Btn_RetencionesPercepciones3)
        Me.GBx_RetencionesPercepciones.Controls.Add(Me.Btn_RetencionesPercepciones2)
        Me.GBx_RetencionesPercepciones.Controls.Add(Me.Btn_RetencionesPercepciones1)
        Me.GBx_RetencionesPercepciones.Controls.Add(Me.CBx_AgenteResponsable10)
        Me.GBx_RetencionesPercepciones.Controls.Add(Me.CBx_AgenteResponsable9)
        Me.GBx_RetencionesPercepciones.Controls.Add(Me.CBx_AgenteResponsable8)
        Me.GBx_RetencionesPercepciones.Controls.Add(Me.CBx_AgenteResponsable7)
        Me.GBx_RetencionesPercepciones.Controls.Add(Me.CBx_AgenteResponsable6)
        Me.GBx_RetencionesPercepciones.Controls.Add(Me.CBx_AgenteResponsable5)
        Me.GBx_RetencionesPercepciones.Controls.Add(Me.CBx_AgenteResponsable4)
        Me.GBx_RetencionesPercepciones.Controls.Add(Me.CBx_AgenteResponsable3)
        Me.GBx_RetencionesPercepciones.Controls.Add(Me.CBx_AgenteResponsable2)
        Me.GBx_RetencionesPercepciones.Controls.Add(Me.CBx_AgenteResponsable1)
        Me.GBx_RetencionesPercepciones.Controls.Add(Me.TBx_MontoRetenidoPorLinea10)
        Me.GBx_RetencionesPercepciones.Controls.Add(Me.TBx_MontoRetenidoPorLinea9)
        Me.GBx_RetencionesPercepciones.Controls.Add(Me.TBx_MontoRetenidoPorLinea8)
        Me.GBx_RetencionesPercepciones.Controls.Add(Me.TBx_MontoRetenidoPorLinea7)
        Me.GBx_RetencionesPercepciones.Controls.Add(Me.TBx_MontoRetenidoPorLinea6)
        Me.GBx_RetencionesPercepciones.Controls.Add(Me.TBx_MontoRetenidoPorLinea5)
        Me.GBx_RetencionesPercepciones.Controls.Add(Me.TBx_MontoRetenidoPorLinea4)
        Me.GBx_RetencionesPercepciones.Controls.Add(Me.TBx_MontoRetenidoPorLinea3)
        Me.GBx_RetencionesPercepciones.Controls.Add(Me.TBx_MontoRetenidoPorLinea2)
        Me.GBx_RetencionesPercepciones.Controls.Add(Me.TBx_MontoRetenidoPorLinea1)
        Me.GBx_RetencionesPercepciones.Controls.Add(Me.Label10)
        Me.GBx_RetencionesPercepciones.Controls.Add(Me.Label9)
        Me.GBx_RetencionesPercepciones.Location = New System.Drawing.Point(787, 250)
        Me.GBx_RetencionesPercepciones.Name = "GBx_RetencionesPercepciones"
        Me.GBx_RetencionesPercepciones.Size = New System.Drawing.Size(276, 390)
        Me.GBx_RetencionesPercepciones.TabIndex = 287
        Me.GBx_RetencionesPercepciones.TabStop = False
        Me.GBx_RetencionesPercepciones.Text = "Retenciones/Percepciones"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(7, 245)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(93, 13)
        Me.Label12.TabIndex = 305
        Me.Label12.Text = "Totales Retenidos"
        '
        'LBx_RetencionesPercepciones
        '
        Me.LBx_RetencionesPercepciones.FormattingEnabled = True
        Me.LBx_RetencionesPercepciones.Location = New System.Drawing.Point(7, 262)
        Me.LBx_RetencionesPercepciones.Name = "LBx_RetencionesPercepciones"
        Me.LBx_RetencionesPercepciones.Size = New System.Drawing.Size(263, 121)
        Me.LBx_RetencionesPercepciones.TabIndex = 304
        '
        'Btn_RetencionesPercepciones10
        '
        Me.Btn_RetencionesPercepciones10.Location = New System.Drawing.Point(5, 222)
        Me.Btn_RetencionesPercepciones10.Name = "Btn_RetencionesPercepciones10"
        Me.Btn_RetencionesPercepciones10.Size = New System.Drawing.Size(75, 20)
        Me.Btn_RetencionesPercepciones10.TabIndex = 303
        Me.Btn_RetencionesPercepciones10.Text = "Ret/Perc"
        Me.Btn_RetencionesPercepciones10.UseVisualStyleBackColor = True
        '
        'Btn_RetencionesPercepciones9
        '
        Me.Btn_RetencionesPercepciones9.Location = New System.Drawing.Point(5, 201)
        Me.Btn_RetencionesPercepciones9.Name = "Btn_RetencionesPercepciones9"
        Me.Btn_RetencionesPercepciones9.Size = New System.Drawing.Size(75, 20)
        Me.Btn_RetencionesPercepciones9.TabIndex = 302
        Me.Btn_RetencionesPercepciones9.Text = "Ret/Perc"
        Me.Btn_RetencionesPercepciones9.UseVisualStyleBackColor = True
        '
        'Btn_RetencionesPercepciones8
        '
        Me.Btn_RetencionesPercepciones8.Location = New System.Drawing.Point(5, 180)
        Me.Btn_RetencionesPercepciones8.Name = "Btn_RetencionesPercepciones8"
        Me.Btn_RetencionesPercepciones8.Size = New System.Drawing.Size(75, 20)
        Me.Btn_RetencionesPercepciones8.TabIndex = 301
        Me.Btn_RetencionesPercepciones8.Text = "Ret/Perc"
        Me.Btn_RetencionesPercepciones8.UseVisualStyleBackColor = True
        '
        'Btn_RetencionesPercepciones7
        '
        Me.Btn_RetencionesPercepciones7.Location = New System.Drawing.Point(5, 159)
        Me.Btn_RetencionesPercepciones7.Name = "Btn_RetencionesPercepciones7"
        Me.Btn_RetencionesPercepciones7.Size = New System.Drawing.Size(75, 20)
        Me.Btn_RetencionesPercepciones7.TabIndex = 300
        Me.Btn_RetencionesPercepciones7.Text = "Ret/Perc"
        Me.Btn_RetencionesPercepciones7.UseVisualStyleBackColor = True
        '
        'Btn_RetencionesPercepciones6
        '
        Me.Btn_RetencionesPercepciones6.Location = New System.Drawing.Point(5, 138)
        Me.Btn_RetencionesPercepciones6.Name = "Btn_RetencionesPercepciones6"
        Me.Btn_RetencionesPercepciones6.Size = New System.Drawing.Size(75, 20)
        Me.Btn_RetencionesPercepciones6.TabIndex = 299
        Me.Btn_RetencionesPercepciones6.Text = "Ret/Perc"
        Me.Btn_RetencionesPercepciones6.UseVisualStyleBackColor = True
        '
        'Btn_RetencionesPercepciones5
        '
        Me.Btn_RetencionesPercepciones5.Location = New System.Drawing.Point(5, 117)
        Me.Btn_RetencionesPercepciones5.Name = "Btn_RetencionesPercepciones5"
        Me.Btn_RetencionesPercepciones5.Size = New System.Drawing.Size(75, 20)
        Me.Btn_RetencionesPercepciones5.TabIndex = 298
        Me.Btn_RetencionesPercepciones5.Text = "Ret/Perc"
        Me.Btn_RetencionesPercepciones5.UseVisualStyleBackColor = True
        '
        'Btn_RetencionesPercepciones4
        '
        Me.Btn_RetencionesPercepciones4.Location = New System.Drawing.Point(5, 96)
        Me.Btn_RetencionesPercepciones4.Name = "Btn_RetencionesPercepciones4"
        Me.Btn_RetencionesPercepciones4.Size = New System.Drawing.Size(75, 20)
        Me.Btn_RetencionesPercepciones4.TabIndex = 297
        Me.Btn_RetencionesPercepciones4.Text = "Ret/Perc"
        Me.Btn_RetencionesPercepciones4.UseVisualStyleBackColor = True
        '
        'Btn_RetencionesPercepciones3
        '
        Me.Btn_RetencionesPercepciones3.Location = New System.Drawing.Point(5, 75)
        Me.Btn_RetencionesPercepciones3.Name = "Btn_RetencionesPercepciones3"
        Me.Btn_RetencionesPercepciones3.Size = New System.Drawing.Size(75, 20)
        Me.Btn_RetencionesPercepciones3.TabIndex = 296
        Me.Btn_RetencionesPercepciones3.Text = "Ret/Perc"
        Me.Btn_RetencionesPercepciones3.UseVisualStyleBackColor = True
        '
        'Btn_RetencionesPercepciones2
        '
        Me.Btn_RetencionesPercepciones2.Location = New System.Drawing.Point(5, 54)
        Me.Btn_RetencionesPercepciones2.Name = "Btn_RetencionesPercepciones2"
        Me.Btn_RetencionesPercepciones2.Size = New System.Drawing.Size(75, 20)
        Me.Btn_RetencionesPercepciones2.TabIndex = 295
        Me.Btn_RetencionesPercepciones2.Text = "Ret/Perc"
        Me.Btn_RetencionesPercepciones2.UseVisualStyleBackColor = True
        '
        'Btn_RetencionesPercepciones1
        '
        Me.Btn_RetencionesPercepciones1.Location = New System.Drawing.Point(5, 32)
        Me.Btn_RetencionesPercepciones1.Name = "Btn_RetencionesPercepciones1"
        Me.Btn_RetencionesPercepciones1.Size = New System.Drawing.Size(75, 20)
        Me.Btn_RetencionesPercepciones1.TabIndex = 294
        Me.Btn_RetencionesPercepciones1.Text = "Ret/Perc"
        Me.Btn_RetencionesPercepciones1.UseVisualStyleBackColor = True
        '
        'CBx_AgenteResponsable10
        '
        Me.CBx_AgenteResponsable10.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBx_AgenteResponsable10.FormattingEnabled = True
        Me.CBx_AgenteResponsable10.Location = New System.Drawing.Point(88, 221)
        Me.CBx_AgenteResponsable10.Name = "CBx_AgenteResponsable10"
        Me.CBx_AgenteResponsable10.Size = New System.Drawing.Size(38, 21)
        Me.CBx_AgenteResponsable10.TabIndex = 293
        '
        'CBx_AgenteResponsable9
        '
        Me.CBx_AgenteResponsable9.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBx_AgenteResponsable9.FormattingEnabled = True
        Me.CBx_AgenteResponsable9.Location = New System.Drawing.Point(88, 200)
        Me.CBx_AgenteResponsable9.Name = "CBx_AgenteResponsable9"
        Me.CBx_AgenteResponsable9.Size = New System.Drawing.Size(38, 21)
        Me.CBx_AgenteResponsable9.TabIndex = 292
        '
        'CBx_AgenteResponsable8
        '
        Me.CBx_AgenteResponsable8.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBx_AgenteResponsable8.FormattingEnabled = True
        Me.CBx_AgenteResponsable8.Location = New System.Drawing.Point(88, 179)
        Me.CBx_AgenteResponsable8.Name = "CBx_AgenteResponsable8"
        Me.CBx_AgenteResponsable8.Size = New System.Drawing.Size(38, 21)
        Me.CBx_AgenteResponsable8.TabIndex = 291
        '
        'CBx_AgenteResponsable7
        '
        Me.CBx_AgenteResponsable7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBx_AgenteResponsable7.FormattingEnabled = True
        Me.CBx_AgenteResponsable7.Location = New System.Drawing.Point(88, 158)
        Me.CBx_AgenteResponsable7.Name = "CBx_AgenteResponsable7"
        Me.CBx_AgenteResponsable7.Size = New System.Drawing.Size(38, 21)
        Me.CBx_AgenteResponsable7.TabIndex = 290
        '
        'CBx_AgenteResponsable6
        '
        Me.CBx_AgenteResponsable6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBx_AgenteResponsable6.FormattingEnabled = True
        Me.CBx_AgenteResponsable6.Location = New System.Drawing.Point(88, 137)
        Me.CBx_AgenteResponsable6.Name = "CBx_AgenteResponsable6"
        Me.CBx_AgenteResponsable6.Size = New System.Drawing.Size(38, 21)
        Me.CBx_AgenteResponsable6.TabIndex = 289
        '
        'CBx_AgenteResponsable5
        '
        Me.CBx_AgenteResponsable5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBx_AgenteResponsable5.FormattingEnabled = True
        Me.CBx_AgenteResponsable5.Location = New System.Drawing.Point(88, 116)
        Me.CBx_AgenteResponsable5.Name = "CBx_AgenteResponsable5"
        Me.CBx_AgenteResponsable5.Size = New System.Drawing.Size(38, 21)
        Me.CBx_AgenteResponsable5.TabIndex = 288
        '
        'CBx_AgenteResponsable4
        '
        Me.CBx_AgenteResponsable4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBx_AgenteResponsable4.FormattingEnabled = True
        Me.CBx_AgenteResponsable4.Location = New System.Drawing.Point(88, 95)
        Me.CBx_AgenteResponsable4.Name = "CBx_AgenteResponsable4"
        Me.CBx_AgenteResponsable4.Size = New System.Drawing.Size(38, 21)
        Me.CBx_AgenteResponsable4.TabIndex = 287
        '
        'CBx_AgenteResponsable3
        '
        Me.CBx_AgenteResponsable3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBx_AgenteResponsable3.FormattingEnabled = True
        Me.CBx_AgenteResponsable3.Location = New System.Drawing.Point(88, 74)
        Me.CBx_AgenteResponsable3.Name = "CBx_AgenteResponsable3"
        Me.CBx_AgenteResponsable3.Size = New System.Drawing.Size(38, 21)
        Me.CBx_AgenteResponsable3.TabIndex = 286
        '
        'CBx_AgenteResponsable2
        '
        Me.CBx_AgenteResponsable2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBx_AgenteResponsable2.FormattingEnabled = True
        Me.CBx_AgenteResponsable2.Location = New System.Drawing.Point(88, 53)
        Me.CBx_AgenteResponsable2.Name = "CBx_AgenteResponsable2"
        Me.CBx_AgenteResponsable2.Size = New System.Drawing.Size(38, 21)
        Me.CBx_AgenteResponsable2.TabIndex = 285
        '
        'CBx_AgenteResponsable1
        '
        Me.CBx_AgenteResponsable1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBx_AgenteResponsable1.FormattingEnabled = True
        Me.CBx_AgenteResponsable1.Items.AddRange(New Object() {"A", "R"})
        Me.CBx_AgenteResponsable1.Location = New System.Drawing.Point(88, 32)
        Me.CBx_AgenteResponsable1.Name = "CBx_AgenteResponsable1"
        Me.CBx_AgenteResponsable1.Size = New System.Drawing.Size(38, 21)
        Me.CBx_AgenteResponsable1.TabIndex = 284
        '
        'TBx_MontoRetenidoPorLinea10
        '
        Me.TBx_MontoRetenidoPorLinea10.Enabled = False
        Me.TBx_MontoRetenidoPorLinea10.Location = New System.Drawing.Point(132, 222)
        Me.TBx_MontoRetenidoPorLinea10.MaxLength = 17
        Me.TBx_MontoRetenidoPorLinea10.Name = "TBx_MontoRetenidoPorLinea10"
        Me.TBx_MontoRetenidoPorLinea10.Size = New System.Drawing.Size(138, 20)
        Me.TBx_MontoRetenidoPorLinea10.TabIndex = 234
        Me.TBx_MontoRetenidoPorLinea10.Text = "0"
        '
        'TBx_MontoRetenidoPorLinea9
        '
        Me.TBx_MontoRetenidoPorLinea9.Enabled = False
        Me.TBx_MontoRetenidoPorLinea9.Location = New System.Drawing.Point(132, 201)
        Me.TBx_MontoRetenidoPorLinea9.MaxLength = 17
        Me.TBx_MontoRetenidoPorLinea9.Name = "TBx_MontoRetenidoPorLinea9"
        Me.TBx_MontoRetenidoPorLinea9.Size = New System.Drawing.Size(138, 20)
        Me.TBx_MontoRetenidoPorLinea9.TabIndex = 233
        Me.TBx_MontoRetenidoPorLinea9.Text = "0"
        '
        'TBx_MontoRetenidoPorLinea8
        '
        Me.TBx_MontoRetenidoPorLinea8.Enabled = False
        Me.TBx_MontoRetenidoPorLinea8.Location = New System.Drawing.Point(132, 180)
        Me.TBx_MontoRetenidoPorLinea8.MaxLength = 17
        Me.TBx_MontoRetenidoPorLinea8.Name = "TBx_MontoRetenidoPorLinea8"
        Me.TBx_MontoRetenidoPorLinea8.Size = New System.Drawing.Size(138, 20)
        Me.TBx_MontoRetenidoPorLinea8.TabIndex = 232
        Me.TBx_MontoRetenidoPorLinea8.Text = "0"
        '
        'TBx_MontoRetenidoPorLinea7
        '
        Me.TBx_MontoRetenidoPorLinea7.Enabled = False
        Me.TBx_MontoRetenidoPorLinea7.Location = New System.Drawing.Point(132, 159)
        Me.TBx_MontoRetenidoPorLinea7.MaxLength = 17
        Me.TBx_MontoRetenidoPorLinea7.Name = "TBx_MontoRetenidoPorLinea7"
        Me.TBx_MontoRetenidoPorLinea7.Size = New System.Drawing.Size(138, 20)
        Me.TBx_MontoRetenidoPorLinea7.TabIndex = 231
        Me.TBx_MontoRetenidoPorLinea7.Text = "0"
        '
        'TBx_MontoRetenidoPorLinea6
        '
        Me.TBx_MontoRetenidoPorLinea6.Enabled = False
        Me.TBx_MontoRetenidoPorLinea6.Location = New System.Drawing.Point(132, 138)
        Me.TBx_MontoRetenidoPorLinea6.MaxLength = 17
        Me.TBx_MontoRetenidoPorLinea6.Name = "TBx_MontoRetenidoPorLinea6"
        Me.TBx_MontoRetenidoPorLinea6.Size = New System.Drawing.Size(138, 20)
        Me.TBx_MontoRetenidoPorLinea6.TabIndex = 230
        Me.TBx_MontoRetenidoPorLinea6.Text = "0"
        '
        'TBx_MontoRetenidoPorLinea5
        '
        Me.TBx_MontoRetenidoPorLinea5.Enabled = False
        Me.TBx_MontoRetenidoPorLinea5.Location = New System.Drawing.Point(132, 117)
        Me.TBx_MontoRetenidoPorLinea5.MaxLength = 17
        Me.TBx_MontoRetenidoPorLinea5.Name = "TBx_MontoRetenidoPorLinea5"
        Me.TBx_MontoRetenidoPorLinea5.Size = New System.Drawing.Size(138, 20)
        Me.TBx_MontoRetenidoPorLinea5.TabIndex = 229
        Me.TBx_MontoRetenidoPorLinea5.Text = "0"
        '
        'TBx_MontoRetenidoPorLinea4
        '
        Me.TBx_MontoRetenidoPorLinea4.Enabled = False
        Me.TBx_MontoRetenidoPorLinea4.Location = New System.Drawing.Point(132, 96)
        Me.TBx_MontoRetenidoPorLinea4.MaxLength = 17
        Me.TBx_MontoRetenidoPorLinea4.Name = "TBx_MontoRetenidoPorLinea4"
        Me.TBx_MontoRetenidoPorLinea4.Size = New System.Drawing.Size(138, 20)
        Me.TBx_MontoRetenidoPorLinea4.TabIndex = 228
        Me.TBx_MontoRetenidoPorLinea4.Text = "0"
        '
        'TBx_MontoRetenidoPorLinea3
        '
        Me.TBx_MontoRetenidoPorLinea3.Enabled = False
        Me.TBx_MontoRetenidoPorLinea3.Location = New System.Drawing.Point(132, 75)
        Me.TBx_MontoRetenidoPorLinea3.MaxLength = 17
        Me.TBx_MontoRetenidoPorLinea3.Name = "TBx_MontoRetenidoPorLinea3"
        Me.TBx_MontoRetenidoPorLinea3.Size = New System.Drawing.Size(138, 20)
        Me.TBx_MontoRetenidoPorLinea3.TabIndex = 227
        Me.TBx_MontoRetenidoPorLinea3.Text = "0"
        '
        'TBx_MontoRetenidoPorLinea2
        '
        Me.TBx_MontoRetenidoPorLinea2.Enabled = False
        Me.TBx_MontoRetenidoPorLinea2.Location = New System.Drawing.Point(132, 54)
        Me.TBx_MontoRetenidoPorLinea2.MaxLength = 17
        Me.TBx_MontoRetenidoPorLinea2.Name = "TBx_MontoRetenidoPorLinea2"
        Me.TBx_MontoRetenidoPorLinea2.Size = New System.Drawing.Size(138, 20)
        Me.TBx_MontoRetenidoPorLinea2.TabIndex = 226
        Me.TBx_MontoRetenidoPorLinea2.Text = "0"
        '
        'TBx_MontoRetenidoPorLinea1
        '
        Me.TBx_MontoRetenidoPorLinea1.Enabled = False
        Me.TBx_MontoRetenidoPorLinea1.Location = New System.Drawing.Point(132, 33)
        Me.TBx_MontoRetenidoPorLinea1.MaxLength = 17
        Me.TBx_MontoRetenidoPorLinea1.Name = "TBx_MontoRetenidoPorLinea1"
        Me.TBx_MontoRetenidoPorLinea1.Size = New System.Drawing.Size(138, 20)
        Me.TBx_MontoRetenidoPorLinea1.TabIndex = 225
        Me.TBx_MontoRetenidoPorLinea1.Text = "0"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(155, 15)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(78, 13)
        Me.Label10.TabIndex = 1
        Me.Label10.Text = "Monto retenido"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(92, 17)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(27, 13)
        Me.Label9.TabIndex = 0
        Me.Label9.Text = "A/R"
        '
        'GBx_PreFactura
        '
        Me.GBx_PreFactura.Controls.Add(Me.Btn_PreFacturaImprimir)
        Me.GBx_PreFactura.Controls.Add(Me.Btn_PreFacturaAbrir)
        Me.GBx_PreFactura.Controls.Add(Me.Btn_PreFacturaGuardar)
        Me.GBx_PreFactura.Location = New System.Drawing.Point(6, 936)
        Me.GBx_PreFactura.Name = "GBx_PreFactura"
        Me.GBx_PreFactura.Size = New System.Drawing.Size(1059, 63)
        Me.GBx_PreFactura.TabIndex = 286
        Me.GBx_PreFactura.TabStop = False
        Me.GBx_PreFactura.Text = "Pre factura"
        '
        'Btn_PreFacturaImprimir
        '
        Me.Btn_PreFacturaImprimir.Location = New System.Drawing.Point(707, 16)
        Me.Btn_PreFacturaImprimir.Name = "Btn_PreFacturaImprimir"
        Me.Btn_PreFacturaImprimir.Size = New System.Drawing.Size(342, 41)
        Me.Btn_PreFacturaImprimir.TabIndex = 2
        Me.Btn_PreFacturaImprimir.Text = "Preview"
        Me.Btn_PreFacturaImprimir.UseVisualStyleBackColor = True
        '
        'Btn_PreFacturaAbrir
        '
        Me.Btn_PreFacturaAbrir.Location = New System.Drawing.Point(11, 16)
        Me.Btn_PreFacturaAbrir.Name = "Btn_PreFacturaAbrir"
        Me.Btn_PreFacturaAbrir.Size = New System.Drawing.Size(342, 41)
        Me.Btn_PreFacturaAbrir.TabIndex = 0
        Me.Btn_PreFacturaAbrir.Text = "Abrir"
        Me.Btn_PreFacturaAbrir.UseVisualStyleBackColor = True
        '
        'Btn_PreFacturaGuardar
        '
        Me.Btn_PreFacturaGuardar.Location = New System.Drawing.Point(359, 16)
        Me.Btn_PreFacturaGuardar.Name = "Btn_PreFacturaGuardar"
        Me.Btn_PreFacturaGuardar.Size = New System.Drawing.Size(342, 41)
        Me.Btn_PreFacturaGuardar.TabIndex = 1
        Me.Btn_PreFacturaGuardar.Text = "Guardar"
        Me.Btn_PreFacturaGuardar.UseVisualStyleBackColor = True
        '
        'GBx_SubTotalesInformativos
        '
        Me.GBx_SubTotalesInformativos.Controls.Add(Me.TBx_ValorSubTotales5)
        Me.GBx_SubTotalesInformativos.Controls.Add(Me.TBx_ValorSubTotales4)
        Me.GBx_SubTotalesInformativos.Controls.Add(Me.TBx_ValorSubTotales3)
        Me.GBx_SubTotalesInformativos.Controls.Add(Me.TBx_ValorSubTotales2)
        Me.GBx_SubTotalesInformativos.Controls.Add(Me.Label25)
        Me.GBx_SubTotalesInformativos.Controls.Add(Me.TBx_ValorSubTotales1)
        Me.GBx_SubTotalesInformativos.Controls.Add(Me.Label20)
        Me.GBx_SubTotalesInformativos.Controls.Add(Me.Label21)
        Me.GBx_SubTotalesInformativos.Controls.Add(Me.Label22)
        Me.GBx_SubTotalesInformativos.Controls.Add(Me.Label23)
        Me.GBx_SubTotalesInformativos.Controls.Add(Me.Label24)
        Me.GBx_SubTotalesInformativos.Controls.Add(Me.TBx_GlosaSubTotales5)
        Me.GBx_SubTotalesInformativos.Controls.Add(Me.TBx_GlosaSubTotales4)
        Me.GBx_SubTotalesInformativos.Controls.Add(Me.TBx_GlosaSubTotales3)
        Me.GBx_SubTotalesInformativos.Controls.Add(Me.TBx_GlosaSubTotales2)
        Me.GBx_SubTotalesInformativos.Controls.Add(Me.TBx_GlosaSubTotales1)
        Me.GBx_SubTotalesInformativos.Controls.Add(Me.Label19)
        Me.GBx_SubTotalesInformativos.Controls.Add(Me.Label18)
        Me.GBx_SubTotalesInformativos.Controls.Add(Me.Label17)
        Me.GBx_SubTotalesInformativos.Controls.Add(Me.Label16)
        Me.GBx_SubTotalesInformativos.Controls.Add(Me.Label15)
        Me.GBx_SubTotalesInformativos.Controls.Add(Me.Label14)
        Me.GBx_SubTotalesInformativos.Controls.Add(Me.Label13)
        Me.GBx_SubTotalesInformativos.Location = New System.Drawing.Point(244, 497)
        Me.GBx_SubTotalesInformativos.Name = "GBx_SubTotalesInformativos"
        Me.GBx_SubTotalesInformativos.Size = New System.Drawing.Size(537, 143)
        Me.GBx_SubTotalesInformativos.TabIndex = 284
        Me.GBx_SubTotalesInformativos.TabStop = False
        Me.GBx_SubTotalesInformativos.Text = "Subtotales informativos"
        '
        'TBx_ValorSubTotales5
        '
        Me.TBx_ValorSubTotales5.Location = New System.Drawing.Point(440, 118)
        Me.TBx_ValorSubTotales5.MaxLength = 17
        Me.TBx_ValorSubTotales5.Name = "TBx_ValorSubTotales5"
        Me.TBx_ValorSubTotales5.Size = New System.Drawing.Size(88, 20)
        Me.TBx_ValorSubTotales5.TabIndex = 89
        Me.TBx_ValorSubTotales5.Text = "0"
        '
        'TBx_ValorSubTotales4
        '
        Me.TBx_ValorSubTotales4.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.TBx_ValorSubTotales4.Location = New System.Drawing.Point(440, 96)
        Me.TBx_ValorSubTotales4.MaxLength = 17
        Me.TBx_ValorSubTotales4.Name = "TBx_ValorSubTotales4"
        Me.TBx_ValorSubTotales4.Size = New System.Drawing.Size(88, 20)
        Me.TBx_ValorSubTotales4.TabIndex = 87
        Me.TBx_ValorSubTotales4.Text = "0"
        '
        'TBx_ValorSubTotales3
        '
        Me.TBx_ValorSubTotales3.Location = New System.Drawing.Point(440, 74)
        Me.TBx_ValorSubTotales3.MaxLength = 17
        Me.TBx_ValorSubTotales3.Name = "TBx_ValorSubTotales3"
        Me.TBx_ValorSubTotales3.Size = New System.Drawing.Size(88, 20)
        Me.TBx_ValorSubTotales3.TabIndex = 85
        Me.TBx_ValorSubTotales3.Text = "0"
        '
        'TBx_ValorSubTotales2
        '
        Me.TBx_ValorSubTotales2.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.TBx_ValorSubTotales2.Location = New System.Drawing.Point(440, 52)
        Me.TBx_ValorSubTotales2.MaxLength = 17
        Me.TBx_ValorSubTotales2.Name = "TBx_ValorSubTotales2"
        Me.TBx_ValorSubTotales2.Size = New System.Drawing.Size(88, 20)
        Me.TBx_ValorSubTotales2.TabIndex = 83
        Me.TBx_ValorSubTotales2.Text = "0"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(399, 14)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(36, 13)
        Me.Label25.TabIndex = 294
        Me.Label25.Text = "Orden"
        '
        'TBx_ValorSubTotales1
        '
        Me.TBx_ValorSubTotales1.Location = New System.Drawing.Point(440, 30)
        Me.TBx_ValorSubTotales1.MaxLength = 17
        Me.TBx_ValorSubTotales1.Name = "TBx_ValorSubTotales1"
        Me.TBx_ValorSubTotales1.Size = New System.Drawing.Size(88, 20)
        Me.TBx_ValorSubTotales1.TabIndex = 81
        Me.TBx_ValorSubTotales1.Text = "0"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(408, 121)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(13, 13)
        Me.Label20.TabIndex = 293
        Me.Label20.Text = "5"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(408, 99)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(13, 13)
        Me.Label21.TabIndex = 292
        Me.Label21.Text = "4"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(408, 77)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(13, 13)
        Me.Label22.TabIndex = 291
        Me.Label22.Text = "3"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(408, 55)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(13, 13)
        Me.Label23.TabIndex = 290
        Me.Label23.Text = "2"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(408, 33)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(13, 13)
        Me.Label24.TabIndex = 289
        Me.Label24.Text = "1"
        '
        'TBx_GlosaSubTotales5
        '
        Me.TBx_GlosaSubTotales5.Location = New System.Drawing.Point(23, 118)
        Me.TBx_GlosaSubTotales5.MaxLength = 40
        Me.TBx_GlosaSubTotales5.Name = "TBx_GlosaSubTotales5"
        Me.TBx_GlosaSubTotales5.Size = New System.Drawing.Size(370, 20)
        Me.TBx_GlosaSubTotales5.TabIndex = 88
        '
        'TBx_GlosaSubTotales4
        '
        Me.TBx_GlosaSubTotales4.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.TBx_GlosaSubTotales4.Location = New System.Drawing.Point(23, 96)
        Me.TBx_GlosaSubTotales4.MaxLength = 40
        Me.TBx_GlosaSubTotales4.Name = "TBx_GlosaSubTotales4"
        Me.TBx_GlosaSubTotales4.Size = New System.Drawing.Size(370, 20)
        Me.TBx_GlosaSubTotales4.TabIndex = 86
        '
        'TBx_GlosaSubTotales3
        '
        Me.TBx_GlosaSubTotales3.Location = New System.Drawing.Point(23, 74)
        Me.TBx_GlosaSubTotales3.MaxLength = 40
        Me.TBx_GlosaSubTotales3.Name = "TBx_GlosaSubTotales3"
        Me.TBx_GlosaSubTotales3.Size = New System.Drawing.Size(370, 20)
        Me.TBx_GlosaSubTotales3.TabIndex = 84
        '
        'TBx_GlosaSubTotales2
        '
        Me.TBx_GlosaSubTotales2.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.TBx_GlosaSubTotales2.Location = New System.Drawing.Point(23, 52)
        Me.TBx_GlosaSubTotales2.MaxLength = 40
        Me.TBx_GlosaSubTotales2.Name = "TBx_GlosaSubTotales2"
        Me.TBx_GlosaSubTotales2.Size = New System.Drawing.Size(370, 20)
        Me.TBx_GlosaSubTotales2.TabIndex = 82
        '
        'TBx_GlosaSubTotales1
        '
        Me.TBx_GlosaSubTotales1.Location = New System.Drawing.Point(23, 30)
        Me.TBx_GlosaSubTotales1.MaxLength = 40
        Me.TBx_GlosaSubTotales1.Name = "TBx_GlosaSubTotales1"
        Me.TBx_GlosaSubTotales1.Size = New System.Drawing.Size(370, 20)
        Me.TBx_GlosaSubTotales1.TabIndex = 285
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(437, 14)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(88, 13)
        Me.Label19.TabIndex = 6
        Me.Label19.Text = "Valor del subtotal"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(181, 14)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(34, 13)
        Me.Label18.TabIndex = 5
        Me.Label18.Text = "Glosa"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(4, 121)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(13, 13)
        Me.Label17.TabIndex = 4
        Me.Label17.Text = "5"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(4, 99)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(13, 13)
        Me.Label16.TabIndex = 3
        Me.Label16.Text = "4"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(4, 77)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(13, 13)
        Me.Label15.TabIndex = 2
        Me.Label15.Text = "3"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(4, 55)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(13, 13)
        Me.Label14.TabIndex = 1
        Me.Label14.Text = "2"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(4, 33)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(13, 13)
        Me.Label13.TabIndex = 0
        Me.Label13.Text = "1"
        '
        'Ckb_esContingencia
        '
        Me.Ckb_esContingencia.AutoSize = True
        Me.Ckb_esContingencia.Location = New System.Drawing.Point(976, 6)
        Me.Ckb_esContingencia.Name = "Ckb_esContingencia"
        Me.Ckb_esContingencia.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Ckb_esContingencia.Size = New System.Drawing.Size(102, 17)
        Me.Ckb_esContingencia.TabIndex = 282
        Me.Ckb_esContingencia.Text = "Es contingencia"
        Me.Ckb_esContingencia.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.Ckb_esContingencia.UseVisualStyleBackColor = True
        '
        'btnReimprimir
        '
        Me.btnReimprimir.Location = New System.Drawing.Point(5, 844)
        Me.btnReimprimir.Name = "btnReimprimir"
        Me.btnReimprimir.Size = New System.Drawing.Size(123, 37)
        Me.btnReimprimir.TabIndex = 99
        Me.btnReimprimir.Text = "Reimprimir (F3)"
        Me.btnReimprimir.UseVisualStyleBackColor = True
        '
        'RapidatxtSubDescuentoValor10
        '
        Me.RapidatxtSubDescuentoValor10.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.RapidatxtSubDescuentoValor10.Location = New System.Drawing.Point(635, 471)
        Me.RapidatxtSubDescuentoValor10.MaxLength = 17
        Me.RapidatxtSubDescuentoValor10.Name = "RapidatxtSubDescuentoValor10"
        Me.RapidatxtSubDescuentoValor10.Size = New System.Drawing.Size(67, 20)
        Me.RapidatxtSubDescuentoValor10.TabIndex = 64
        Me.RapidatxtSubDescuentoValor10.TabStop = False
        Me.RapidatxtSubDescuentoValor10.Text = "0"
        '
        'RapidatxtMontoItem10
        '
        Me.RapidatxtMontoItem10.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.RapidatxtMontoItem10.Enabled = False
        Me.RapidatxtMontoItem10.Location = New System.Drawing.Point(708, 471)
        Me.RapidatxtMontoItem10.MaxLength = 17
        Me.RapidatxtMontoItem10.Name = "RapidatxtMontoItem10"
        Me.RapidatxtMontoItem10.Size = New System.Drawing.Size(73, 20)
        Me.RapidatxtMontoItem10.TabIndex = 65
        Me.RapidatxtMontoItem10.Text = "0"
        '
        'RapidacmbIndicadorFacturacion10
        '
        Me.RapidacmbIndicadorFacturacion10.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.RapidacmbIndicadorFacturacion10.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.RapidacmbIndicadorFacturacion10.FormattingEnabled = True
        Me.RapidacmbIndicadorFacturacion10.Location = New System.Drawing.Point(3, 471)
        Me.RapidacmbIndicadorFacturacion10.Name = "RapidacmbIndicadorFacturacion10"
        Me.RapidacmbIndicadorFacturacion10.Size = New System.Drawing.Size(68, 21)
        Me.RapidacmbIndicadorFacturacion10.TabIndex = 278
        Me.RapidacmbIndicadorFacturacion10.TabStop = False
        '
        'RapidatxtNombreItem10
        '
        Me.RapidatxtNombreItem10.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.RapidatxtNombreItem10.Location = New System.Drawing.Point(75, 471)
        Me.RapidatxtNombreItem10.MaxLength = 80
        Me.RapidatxtNombreItem10.Name = "RapidatxtNombreItem10"
        Me.RapidatxtNombreItem10.Size = New System.Drawing.Size(399, 20)
        Me.RapidatxtNombreItem10.TabIndex = 61
        '
        'RapidatxtCantidad10
        '
        Me.RapidatxtCantidad10.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.RapidatxtCantidad10.Location = New System.Drawing.Point(508, 471)
        Me.RapidatxtCantidad10.MaxLength = 17
        Me.RapidatxtCantidad10.Name = "RapidatxtCantidad10"
        Me.RapidatxtCantidad10.Size = New System.Drawing.Size(46, 20)
        Me.RapidatxtCantidad10.TabIndex = 62
        Me.RapidatxtCantidad10.Text = "1"
        '
        'RapidatxtPrecioUnitario10
        '
        Me.RapidatxtPrecioUnitario10.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.RapidatxtPrecioUnitario10.Location = New System.Drawing.Point(560, 471)
        Me.RapidatxtPrecioUnitario10.MaxLength = 17
        Me.RapidatxtPrecioUnitario10.Name = "RapidatxtPrecioUnitario10"
        Me.RapidatxtPrecioUnitario10.Size = New System.Drawing.Size(69, 20)
        Me.RapidatxtPrecioUnitario10.TabIndex = 63
        Me.RapidatxtPrecioUnitario10.Text = "0"
        '
        'RapidatxtSubDescuentoValor9
        '
        Me.RapidatxtSubDescuentoValor9.Location = New System.Drawing.Point(635, 450)
        Me.RapidatxtSubDescuentoValor9.MaxLength = 17
        Me.RapidatxtSubDescuentoValor9.Name = "RapidatxtSubDescuentoValor9"
        Me.RapidatxtSubDescuentoValor9.Size = New System.Drawing.Size(67, 20)
        Me.RapidatxtSubDescuentoValor9.TabIndex = 59
        Me.RapidatxtSubDescuentoValor9.TabStop = False
        Me.RapidatxtSubDescuentoValor9.Text = "0"
        '
        'RapidatxtMontoItem9
        '
        Me.RapidatxtMontoItem9.Enabled = False
        Me.RapidatxtMontoItem9.Location = New System.Drawing.Point(708, 450)
        Me.RapidatxtMontoItem9.MaxLength = 17
        Me.RapidatxtMontoItem9.Name = "RapidatxtMontoItem9"
        Me.RapidatxtMontoItem9.Size = New System.Drawing.Size(73, 20)
        Me.RapidatxtMontoItem9.TabIndex = 60
        Me.RapidatxtMontoItem9.Text = "0"
        '
        'RapidacmbIndicadorFacturacion9
        '
        Me.RapidacmbIndicadorFacturacion9.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.RapidacmbIndicadorFacturacion9.FormattingEnabled = True
        Me.RapidacmbIndicadorFacturacion9.Location = New System.Drawing.Point(3, 450)
        Me.RapidacmbIndicadorFacturacion9.Name = "RapidacmbIndicadorFacturacion9"
        Me.RapidacmbIndicadorFacturacion9.Size = New System.Drawing.Size(68, 21)
        Me.RapidacmbIndicadorFacturacion9.TabIndex = 275
        Me.RapidacmbIndicadorFacturacion9.TabStop = False
        '
        'RapidatxtNombreItem9
        '
        Me.RapidatxtNombreItem9.Location = New System.Drawing.Point(75, 450)
        Me.RapidatxtNombreItem9.MaxLength = 80
        Me.RapidatxtNombreItem9.Name = "RapidatxtNombreItem9"
        Me.RapidatxtNombreItem9.Size = New System.Drawing.Size(399, 20)
        Me.RapidatxtNombreItem9.TabIndex = 56
        '
        'RapidatxtCantidad9
        '
        Me.RapidatxtCantidad9.Location = New System.Drawing.Point(508, 450)
        Me.RapidatxtCantidad9.MaxLength = 17
        Me.RapidatxtCantidad9.Name = "RapidatxtCantidad9"
        Me.RapidatxtCantidad9.Size = New System.Drawing.Size(46, 20)
        Me.RapidatxtCantidad9.TabIndex = 57
        Me.RapidatxtCantidad9.Text = "1"
        '
        'RapidatxtPrecioUnitario9
        '
        Me.RapidatxtPrecioUnitario9.Location = New System.Drawing.Point(560, 450)
        Me.RapidatxtPrecioUnitario9.MaxLength = 17
        Me.RapidatxtPrecioUnitario9.Name = "RapidatxtPrecioUnitario9"
        Me.RapidatxtPrecioUnitario9.Size = New System.Drawing.Size(69, 20)
        Me.RapidatxtPrecioUnitario9.TabIndex = 58
        Me.RapidatxtPrecioUnitario9.Text = "0"
        '
        'RapidatxtSubDescuentoValor8
        '
        Me.RapidatxtSubDescuentoValor8.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.RapidatxtSubDescuentoValor8.Location = New System.Drawing.Point(635, 429)
        Me.RapidatxtSubDescuentoValor8.MaxLength = 17
        Me.RapidatxtSubDescuentoValor8.Name = "RapidatxtSubDescuentoValor8"
        Me.RapidatxtSubDescuentoValor8.Size = New System.Drawing.Size(67, 20)
        Me.RapidatxtSubDescuentoValor8.TabIndex = 54
        Me.RapidatxtSubDescuentoValor8.TabStop = False
        Me.RapidatxtSubDescuentoValor8.Text = "0"
        '
        'RapidatxtMontoItem8
        '
        Me.RapidatxtMontoItem8.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.RapidatxtMontoItem8.Enabled = False
        Me.RapidatxtMontoItem8.Location = New System.Drawing.Point(708, 429)
        Me.RapidatxtMontoItem8.MaxLength = 17
        Me.RapidatxtMontoItem8.Name = "RapidatxtMontoItem8"
        Me.RapidatxtMontoItem8.Size = New System.Drawing.Size(73, 20)
        Me.RapidatxtMontoItem8.TabIndex = 55
        Me.RapidatxtMontoItem8.Text = "0"
        '
        'RapidacmbIndicadorFacturacion8
        '
        Me.RapidacmbIndicadorFacturacion8.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.RapidacmbIndicadorFacturacion8.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.RapidacmbIndicadorFacturacion8.FormattingEnabled = True
        Me.RapidacmbIndicadorFacturacion8.Location = New System.Drawing.Point(3, 429)
        Me.RapidacmbIndicadorFacturacion8.Name = "RapidacmbIndicadorFacturacion8"
        Me.RapidacmbIndicadorFacturacion8.Size = New System.Drawing.Size(68, 21)
        Me.RapidacmbIndicadorFacturacion8.TabIndex = 272
        Me.RapidacmbIndicadorFacturacion8.TabStop = False
        '
        'RapidatxtNombreItem8
        '
        Me.RapidatxtNombreItem8.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.RapidatxtNombreItem8.Location = New System.Drawing.Point(75, 429)
        Me.RapidatxtNombreItem8.MaxLength = 80
        Me.RapidatxtNombreItem8.Name = "RapidatxtNombreItem8"
        Me.RapidatxtNombreItem8.Size = New System.Drawing.Size(399, 20)
        Me.RapidatxtNombreItem8.TabIndex = 51
        '
        'RapidatxtCantidad8
        '
        Me.RapidatxtCantidad8.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.RapidatxtCantidad8.Location = New System.Drawing.Point(508, 429)
        Me.RapidatxtCantidad8.MaxLength = 17
        Me.RapidatxtCantidad8.Name = "RapidatxtCantidad8"
        Me.RapidatxtCantidad8.Size = New System.Drawing.Size(46, 20)
        Me.RapidatxtCantidad8.TabIndex = 52
        Me.RapidatxtCantidad8.Text = "1"
        '
        'RapidatxtPrecioUnitario8
        '
        Me.RapidatxtPrecioUnitario8.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.RapidatxtPrecioUnitario8.Location = New System.Drawing.Point(560, 429)
        Me.RapidatxtPrecioUnitario8.MaxLength = 17
        Me.RapidatxtPrecioUnitario8.Name = "RapidatxtPrecioUnitario8"
        Me.RapidatxtPrecioUnitario8.Size = New System.Drawing.Size(69, 20)
        Me.RapidatxtPrecioUnitario8.TabIndex = 53
        Me.RapidatxtPrecioUnitario8.Text = "0"
        '
        'RapidatxtSubDescuentoValor7
        '
        Me.RapidatxtSubDescuentoValor7.Location = New System.Drawing.Point(635, 408)
        Me.RapidatxtSubDescuentoValor7.MaxLength = 17
        Me.RapidatxtSubDescuentoValor7.Name = "RapidatxtSubDescuentoValor7"
        Me.RapidatxtSubDescuentoValor7.Size = New System.Drawing.Size(67, 20)
        Me.RapidatxtSubDescuentoValor7.TabIndex = 48
        Me.RapidatxtSubDescuentoValor7.TabStop = False
        Me.RapidatxtSubDescuentoValor7.Text = "0"
        '
        'RapidatxtMontoItem7
        '
        Me.RapidatxtMontoItem7.Enabled = False
        Me.RapidatxtMontoItem7.Location = New System.Drawing.Point(708, 408)
        Me.RapidatxtMontoItem7.MaxLength = 17
        Me.RapidatxtMontoItem7.Name = "RapidatxtMontoItem7"
        Me.RapidatxtMontoItem7.Size = New System.Drawing.Size(73, 20)
        Me.RapidatxtMontoItem7.TabIndex = 49
        Me.RapidatxtMontoItem7.Text = "0"
        '
        'RapidacmbIndicadorFacturacion7
        '
        Me.RapidacmbIndicadorFacturacion7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.RapidacmbIndicadorFacturacion7.FormattingEnabled = True
        Me.RapidacmbIndicadorFacturacion7.Location = New System.Drawing.Point(3, 408)
        Me.RapidacmbIndicadorFacturacion7.Name = "RapidacmbIndicadorFacturacion7"
        Me.RapidacmbIndicadorFacturacion7.Size = New System.Drawing.Size(68, 21)
        Me.RapidacmbIndicadorFacturacion7.TabIndex = 269
        Me.RapidacmbIndicadorFacturacion7.TabStop = False
        '
        'RapidatxtNombreItem7
        '
        Me.RapidatxtNombreItem7.Location = New System.Drawing.Point(75, 408)
        Me.RapidatxtNombreItem7.MaxLength = 80
        Me.RapidatxtNombreItem7.Name = "RapidatxtNombreItem7"
        Me.RapidatxtNombreItem7.Size = New System.Drawing.Size(399, 20)
        Me.RapidatxtNombreItem7.TabIndex = 45
        '
        'RapidatxtCantidad7
        '
        Me.RapidatxtCantidad7.Location = New System.Drawing.Point(508, 408)
        Me.RapidatxtCantidad7.MaxLength = 17
        Me.RapidatxtCantidad7.Name = "RapidatxtCantidad7"
        Me.RapidatxtCantidad7.Size = New System.Drawing.Size(46, 20)
        Me.RapidatxtCantidad7.TabIndex = 46
        Me.RapidatxtCantidad7.Text = "1"
        '
        'RapidatxtPrecioUnitario7
        '
        Me.RapidatxtPrecioUnitario7.Location = New System.Drawing.Point(560, 408)
        Me.RapidatxtPrecioUnitario7.MaxLength = 17
        Me.RapidatxtPrecioUnitario7.Name = "RapidatxtPrecioUnitario7"
        Me.RapidatxtPrecioUnitario7.Size = New System.Drawing.Size(69, 20)
        Me.RapidatxtPrecioUnitario7.TabIndex = 47
        Me.RapidatxtPrecioUnitario7.Text = "0"
        '
        'RapidatxtSubDescuentoValor6
        '
        Me.RapidatxtSubDescuentoValor6.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.RapidatxtSubDescuentoValor6.Location = New System.Drawing.Point(635, 387)
        Me.RapidatxtSubDescuentoValor6.MaxLength = 17
        Me.RapidatxtSubDescuentoValor6.Name = "RapidatxtSubDescuentoValor6"
        Me.RapidatxtSubDescuentoValor6.Size = New System.Drawing.Size(67, 20)
        Me.RapidatxtSubDescuentoValor6.TabIndex = 43
        Me.RapidatxtSubDescuentoValor6.TabStop = False
        Me.RapidatxtSubDescuentoValor6.Text = "0"
        '
        'RapidatxtMontoItem6
        '
        Me.RapidatxtMontoItem6.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.RapidatxtMontoItem6.Enabled = False
        Me.RapidatxtMontoItem6.Location = New System.Drawing.Point(708, 387)
        Me.RapidatxtMontoItem6.MaxLength = 17
        Me.RapidatxtMontoItem6.Name = "RapidatxtMontoItem6"
        Me.RapidatxtMontoItem6.Size = New System.Drawing.Size(73, 20)
        Me.RapidatxtMontoItem6.TabIndex = 44
        Me.RapidatxtMontoItem6.Text = "0"
        '
        'RapidacmbIndicadorFacturacion6
        '
        Me.RapidacmbIndicadorFacturacion6.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.RapidacmbIndicadorFacturacion6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.RapidacmbIndicadorFacturacion6.FormattingEnabled = True
        Me.RapidacmbIndicadorFacturacion6.Location = New System.Drawing.Point(3, 387)
        Me.RapidacmbIndicadorFacturacion6.Name = "RapidacmbIndicadorFacturacion6"
        Me.RapidacmbIndicadorFacturacion6.Size = New System.Drawing.Size(68, 21)
        Me.RapidacmbIndicadorFacturacion6.TabIndex = 266
        Me.RapidacmbIndicadorFacturacion6.TabStop = False
        '
        'RapidatxtNombreItem6
        '
        Me.RapidatxtNombreItem6.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.RapidatxtNombreItem6.Location = New System.Drawing.Point(75, 387)
        Me.RapidatxtNombreItem6.MaxLength = 34
        Me.RapidatxtNombreItem6.Name = "RapidatxtNombreItem6"
        Me.RapidatxtNombreItem6.Size = New System.Drawing.Size(399, 20)
        Me.RapidatxtNombreItem6.TabIndex = 40
        '
        'RapidatxtCantidad6
        '
        Me.RapidatxtCantidad6.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.RapidatxtCantidad6.Location = New System.Drawing.Point(508, 387)
        Me.RapidatxtCantidad6.MaxLength = 35
        Me.RapidatxtCantidad6.Name = "RapidatxtCantidad6"
        Me.RapidatxtCantidad6.Size = New System.Drawing.Size(46, 20)
        Me.RapidatxtCantidad6.TabIndex = 41
        Me.RapidatxtCantidad6.Text = "1"
        '
        'RapidatxtPrecioUnitario6
        '
        Me.RapidatxtPrecioUnitario6.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.RapidatxtPrecioUnitario6.Location = New System.Drawing.Point(560, 387)
        Me.RapidatxtPrecioUnitario6.MaxLength = 17
        Me.RapidatxtPrecioUnitario6.Name = "RapidatxtPrecioUnitario6"
        Me.RapidatxtPrecioUnitario6.Size = New System.Drawing.Size(69, 20)
        Me.RapidatxtPrecioUnitario6.TabIndex = 42
        Me.RapidatxtPrecioUnitario6.Text = "0"
        '
        'panAdvertencia
        '
        Me.panAdvertencia.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.panAdvertencia.Controls.Add(Me.lblCerrar)
        Me.panAdvertencia.Controls.Add(Me.Label27)
        Me.panAdvertencia.Controls.Add(Me.lblAdvertencia)
        Me.panAdvertencia.Location = New System.Drawing.Point(135, 851)
        Me.panAdvertencia.Name = "panAdvertencia"
        Me.panAdvertencia.Size = New System.Drawing.Size(753, 61)
        Me.panAdvertencia.TabIndex = 210
        Me.panAdvertencia.Visible = False
        '
        'lblCerrar
        '
        Me.lblCerrar.AutoSize = True
        Me.lblCerrar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCerrar.ForeColor = System.Drawing.Color.Red
        Me.lblCerrar.Location = New System.Drawing.Point(738, 0)
        Me.lblCerrar.Name = "lblCerrar"
        Me.lblCerrar.Size = New System.Drawing.Size(15, 16)
        Me.lblCerrar.TabIndex = 1
        Me.lblCerrar.Text = "x"
        '
        'Label27
        '
        Me.Label27.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.ForeColor = System.Drawing.Color.Red
        Me.Label27.Location = New System.Drawing.Point(3, -11)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(750, 61)
        Me.Label27.TabIndex = 0
        Me.Label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Label27.Visible = False
        '
        'lblAdvertencia
        '
        Me.lblAdvertencia.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAdvertencia.ForeColor = System.Drawing.Color.Red
        Me.lblAdvertencia.Location = New System.Drawing.Point(2, -6)
        Me.lblAdvertencia.Name = "lblAdvertencia"
        Me.lblAdvertencia.Size = New System.Drawing.Size(750, 61)
        Me.lblAdvertencia.TabIndex = 0
        Me.lblAdvertencia.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblAdvertencia.Visible = False
        '
        'Lbl_NewAge
        '
        Me.Lbl_NewAge.AutoSize = True
        Me.Lbl_NewAge.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl_NewAge.Location = New System.Drawing.Point(407, 912)
        Me.Lbl_NewAge.Name = "Lbl_NewAge"
        Me.Lbl_NewAge.Size = New System.Drawing.Size(228, 15)
        Me.Lbl_NewAge.TabIndex = 207
        Me.Lbl_NewAge.Text = "New Age Data - www.new-age-data.com"
        '
        'RapidatxtAdenda
        '
        Me.RapidatxtAdenda.Location = New System.Drawing.Point(8, 513)
        Me.RapidatxtAdenda.MaxLength = 320000
        Me.RapidatxtAdenda.Multiline = True
        Me.RapidatxtAdenda.Name = "RapidatxtAdenda"
        Me.RapidatxtAdenda.Size = New System.Drawing.Size(229, 122)
        Me.RapidatxtAdenda.TabIndex = 79
        Me.RapidatxtAdenda.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(3, 3)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(248, 92)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 203
        Me.PictureBox2.TabStop = False
        '
        'RapidatxtSubDescuentoValor5
        '
        Me.RapidatxtSubDescuentoValor5.Location = New System.Drawing.Point(635, 366)
        Me.RapidatxtSubDescuentoValor5.MaxLength = 17
        Me.RapidatxtSubDescuentoValor5.Name = "RapidatxtSubDescuentoValor5"
        Me.RapidatxtSubDescuentoValor5.Size = New System.Drawing.Size(67, 20)
        Me.RapidatxtSubDescuentoValor5.TabIndex = 38
        Me.RapidatxtSubDescuentoValor5.TabStop = False
        Me.RapidatxtSubDescuentoValor5.Text = "0"
        '
        'RapidatxtMontoItem5
        '
        Me.RapidatxtMontoItem5.Enabled = False
        Me.RapidatxtMontoItem5.Location = New System.Drawing.Point(708, 366)
        Me.RapidatxtMontoItem5.MaxLength = 17
        Me.RapidatxtMontoItem5.Name = "RapidatxtMontoItem5"
        Me.RapidatxtMontoItem5.Size = New System.Drawing.Size(73, 20)
        Me.RapidatxtMontoItem5.TabIndex = 39
        Me.RapidatxtMontoItem5.Text = "0"
        '
        'RapidacmbIndicadorFacturacion5
        '
        Me.RapidacmbIndicadorFacturacion5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.RapidacmbIndicadorFacturacion5.FormattingEnabled = True
        Me.RapidacmbIndicadorFacturacion5.Location = New System.Drawing.Point(3, 366)
        Me.RapidacmbIndicadorFacturacion5.Name = "RapidacmbIndicadorFacturacion5"
        Me.RapidacmbIndicadorFacturacion5.Size = New System.Drawing.Size(68, 21)
        Me.RapidacmbIndicadorFacturacion5.TabIndex = 197
        Me.RapidacmbIndicadorFacturacion5.TabStop = False
        '
        'RapidatxtNombreItem5
        '
        Me.RapidatxtNombreItem5.Location = New System.Drawing.Point(75, 366)
        Me.RapidatxtNombreItem5.MaxLength = 80
        Me.RapidatxtNombreItem5.Name = "RapidatxtNombreItem5"
        Me.RapidatxtNombreItem5.Size = New System.Drawing.Size(399, 20)
        Me.RapidatxtNombreItem5.TabIndex = 35
        '
        'RapidatxtCantidad5
        '
        Me.RapidatxtCantidad5.Location = New System.Drawing.Point(508, 366)
        Me.RapidatxtCantidad5.MaxLength = 17
        Me.RapidatxtCantidad5.Name = "RapidatxtCantidad5"
        Me.RapidatxtCantidad5.Size = New System.Drawing.Size(46, 20)
        Me.RapidatxtCantidad5.TabIndex = 36
        Me.RapidatxtCantidad5.Text = "1"
        '
        'RapidatxtPrecioUnitario5
        '
        Me.RapidatxtPrecioUnitario5.Location = New System.Drawing.Point(560, 366)
        Me.RapidatxtPrecioUnitario5.MaxLength = 17
        Me.RapidatxtPrecioUnitario5.Name = "RapidatxtPrecioUnitario5"
        Me.RapidatxtPrecioUnitario5.Size = New System.Drawing.Size(69, 20)
        Me.RapidatxtPrecioUnitario5.TabIndex = 37
        Me.RapidatxtPrecioUnitario5.Text = "0"
        '
        'RapidatxtSubDescuentoValor4
        '
        Me.RapidatxtSubDescuentoValor4.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.RapidatxtSubDescuentoValor4.Location = New System.Drawing.Point(635, 345)
        Me.RapidatxtSubDescuentoValor4.MaxLength = 17
        Me.RapidatxtSubDescuentoValor4.Name = "RapidatxtSubDescuentoValor4"
        Me.RapidatxtSubDescuentoValor4.Size = New System.Drawing.Size(67, 20)
        Me.RapidatxtSubDescuentoValor4.TabIndex = 33
        Me.RapidatxtSubDescuentoValor4.TabStop = False
        Me.RapidatxtSubDescuentoValor4.Text = "0"
        '
        'RapidatxtMontoItem4
        '
        Me.RapidatxtMontoItem4.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.RapidatxtMontoItem4.Enabled = False
        Me.RapidatxtMontoItem4.Location = New System.Drawing.Point(708, 345)
        Me.RapidatxtMontoItem4.MaxLength = 17
        Me.RapidatxtMontoItem4.Name = "RapidatxtMontoItem4"
        Me.RapidatxtMontoItem4.Size = New System.Drawing.Size(73, 20)
        Me.RapidatxtMontoItem4.TabIndex = 34
        Me.RapidatxtMontoItem4.Text = "0"
        '
        'RapidacmbIndicadorFacturacion4
        '
        Me.RapidacmbIndicadorFacturacion4.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.RapidacmbIndicadorFacturacion4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.RapidacmbIndicadorFacturacion4.FormattingEnabled = True
        Me.RapidacmbIndicadorFacturacion4.Location = New System.Drawing.Point(3, 345)
        Me.RapidacmbIndicadorFacturacion4.Name = "RapidacmbIndicadorFacturacion4"
        Me.RapidacmbIndicadorFacturacion4.Size = New System.Drawing.Size(68, 21)
        Me.RapidacmbIndicadorFacturacion4.TabIndex = 190
        Me.RapidacmbIndicadorFacturacion4.TabStop = False
        '
        'RapidatxtNombreItem4
        '
        Me.RapidatxtNombreItem4.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.RapidatxtNombreItem4.Location = New System.Drawing.Point(75, 345)
        Me.RapidatxtNombreItem4.MaxLength = 80
        Me.RapidatxtNombreItem4.Name = "RapidatxtNombreItem4"
        Me.RapidatxtNombreItem4.Size = New System.Drawing.Size(399, 20)
        Me.RapidatxtNombreItem4.TabIndex = 30
        '
        'RapidatxtCantidad4
        '
        Me.RapidatxtCantidad4.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.RapidatxtCantidad4.Location = New System.Drawing.Point(508, 345)
        Me.RapidatxtCantidad4.MaxLength = 17
        Me.RapidatxtCantidad4.Name = "RapidatxtCantidad4"
        Me.RapidatxtCantidad4.Size = New System.Drawing.Size(46, 20)
        Me.RapidatxtCantidad4.TabIndex = 31
        Me.RapidatxtCantidad4.Text = "1"
        '
        'RapidatxtPrecioUnitario4
        '
        Me.RapidatxtPrecioUnitario4.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.RapidatxtPrecioUnitario4.Location = New System.Drawing.Point(560, 345)
        Me.RapidatxtPrecioUnitario4.MaxLength = 17
        Me.RapidatxtPrecioUnitario4.Name = "RapidatxtPrecioUnitario4"
        Me.RapidatxtPrecioUnitario4.Size = New System.Drawing.Size(69, 20)
        Me.RapidatxtPrecioUnitario4.TabIndex = 32
        Me.RapidatxtPrecioUnitario4.Text = "0"
        '
        'RapidatxtSubDescuentoValor3
        '
        Me.RapidatxtSubDescuentoValor3.Location = New System.Drawing.Point(635, 324)
        Me.RapidatxtSubDescuentoValor3.MaxLength = 17
        Me.RapidatxtSubDescuentoValor3.Name = "RapidatxtSubDescuentoValor3"
        Me.RapidatxtSubDescuentoValor3.Size = New System.Drawing.Size(67, 20)
        Me.RapidatxtSubDescuentoValor3.TabIndex = 28
        Me.RapidatxtSubDescuentoValor3.TabStop = False
        Me.RapidatxtSubDescuentoValor3.Text = "0"
        '
        'RapidatxtMontoItem3
        '
        Me.RapidatxtMontoItem3.Enabled = False
        Me.RapidatxtMontoItem3.Location = New System.Drawing.Point(708, 324)
        Me.RapidatxtMontoItem3.MaxLength = 17
        Me.RapidatxtMontoItem3.Name = "RapidatxtMontoItem3"
        Me.RapidatxtMontoItem3.Size = New System.Drawing.Size(73, 20)
        Me.RapidatxtMontoItem3.TabIndex = 29
        Me.RapidatxtMontoItem3.Text = "0"
        '
        'RapidacmbIndicadorFacturacion3
        '
        Me.RapidacmbIndicadorFacturacion3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.RapidacmbIndicadorFacturacion3.FormattingEnabled = True
        Me.RapidacmbIndicadorFacturacion3.Location = New System.Drawing.Point(3, 324)
        Me.RapidacmbIndicadorFacturacion3.Name = "RapidacmbIndicadorFacturacion3"
        Me.RapidacmbIndicadorFacturacion3.Size = New System.Drawing.Size(68, 21)
        Me.RapidacmbIndicadorFacturacion3.TabIndex = 183
        Me.RapidacmbIndicadorFacturacion3.TabStop = False
        '
        'RapidatxtNombreItem3
        '
        Me.RapidatxtNombreItem3.Location = New System.Drawing.Point(75, 324)
        Me.RapidatxtNombreItem3.MaxLength = 80
        Me.RapidatxtNombreItem3.Name = "RapidatxtNombreItem3"
        Me.RapidatxtNombreItem3.Size = New System.Drawing.Size(399, 20)
        Me.RapidatxtNombreItem3.TabIndex = 25
        '
        'RapidatxtCantidad3
        '
        Me.RapidatxtCantidad3.Location = New System.Drawing.Point(508, 324)
        Me.RapidatxtCantidad3.MaxLength = 17
        Me.RapidatxtCantidad3.Name = "RapidatxtCantidad3"
        Me.RapidatxtCantidad3.Size = New System.Drawing.Size(46, 20)
        Me.RapidatxtCantidad3.TabIndex = 26
        Me.RapidatxtCantidad3.Text = "1"
        '
        'RapidatxtPrecioUnitario3
        '
        Me.RapidatxtPrecioUnitario3.Location = New System.Drawing.Point(560, 324)
        Me.RapidatxtPrecioUnitario3.MaxLength = 17
        Me.RapidatxtPrecioUnitario3.Name = "RapidatxtPrecioUnitario3"
        Me.RapidatxtPrecioUnitario3.Size = New System.Drawing.Size(69, 20)
        Me.RapidatxtPrecioUnitario3.TabIndex = 27
        Me.RapidatxtPrecioUnitario3.Text = "0"
        '
        'RapidatxtSubDescuentoValor2
        '
        Me.RapidatxtSubDescuentoValor2.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.RapidatxtSubDescuentoValor2.Location = New System.Drawing.Point(635, 303)
        Me.RapidatxtSubDescuentoValor2.MaxLength = 17
        Me.RapidatxtSubDescuentoValor2.Name = "RapidatxtSubDescuentoValor2"
        Me.RapidatxtSubDescuentoValor2.Size = New System.Drawing.Size(67, 20)
        Me.RapidatxtSubDescuentoValor2.TabIndex = 23
        Me.RapidatxtSubDescuentoValor2.TabStop = False
        Me.RapidatxtSubDescuentoValor2.Text = "0"
        '
        'RapidatxtMontoItem2
        '
        Me.RapidatxtMontoItem2.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.RapidatxtMontoItem2.Enabled = False
        Me.RapidatxtMontoItem2.Location = New System.Drawing.Point(708, 303)
        Me.RapidatxtMontoItem2.MaxLength = 17
        Me.RapidatxtMontoItem2.Name = "RapidatxtMontoItem2"
        Me.RapidatxtMontoItem2.Size = New System.Drawing.Size(73, 20)
        Me.RapidatxtMontoItem2.TabIndex = 24
        Me.RapidatxtMontoItem2.Text = "0"
        '
        'RapidacmbIndicadorFacturacion2
        '
        Me.RapidacmbIndicadorFacturacion2.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.RapidacmbIndicadorFacturacion2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.RapidacmbIndicadorFacturacion2.FormattingEnabled = True
        Me.RapidacmbIndicadorFacturacion2.Location = New System.Drawing.Point(3, 303)
        Me.RapidacmbIndicadorFacturacion2.Name = "RapidacmbIndicadorFacturacion2"
        Me.RapidacmbIndicadorFacturacion2.Size = New System.Drawing.Size(68, 21)
        Me.RapidacmbIndicadorFacturacion2.TabIndex = 176
        Me.RapidacmbIndicadorFacturacion2.TabStop = False
        '
        'RapidatxtNombreItem2
        '
        Me.RapidatxtNombreItem2.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.RapidatxtNombreItem2.Location = New System.Drawing.Point(75, 303)
        Me.RapidatxtNombreItem2.MaxLength = 80
        Me.RapidatxtNombreItem2.Name = "RapidatxtNombreItem2"
        Me.RapidatxtNombreItem2.Size = New System.Drawing.Size(399, 20)
        Me.RapidatxtNombreItem2.TabIndex = 20
        '
        'RapidatxtCantidad2
        '
        Me.RapidatxtCantidad2.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.RapidatxtCantidad2.Location = New System.Drawing.Point(508, 303)
        Me.RapidatxtCantidad2.MaxLength = 17
        Me.RapidatxtCantidad2.Name = "RapidatxtCantidad2"
        Me.RapidatxtCantidad2.Size = New System.Drawing.Size(46, 20)
        Me.RapidatxtCantidad2.TabIndex = 21
        Me.RapidatxtCantidad2.Text = "1"
        '
        'RapidatxtPrecioUnitario2
        '
        Me.RapidatxtPrecioUnitario2.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.RapidatxtPrecioUnitario2.Location = New System.Drawing.Point(560, 303)
        Me.RapidatxtPrecioUnitario2.MaxLength = 17
        Me.RapidatxtPrecioUnitario2.Name = "RapidatxtPrecioUnitario2"
        Me.RapidatxtPrecioUnitario2.Size = New System.Drawing.Size(69, 20)
        Me.RapidatxtPrecioUnitario2.TabIndex = 22
        Me.RapidatxtPrecioUnitario2.Text = "0"
        '
        'RapidatxtSubDescuentoValor1
        '
        Me.RapidatxtSubDescuentoValor1.Location = New System.Drawing.Point(635, 282)
        Me.RapidatxtSubDescuentoValor1.MaxLength = 17
        Me.RapidatxtSubDescuentoValor1.Name = "RapidatxtSubDescuentoValor1"
        Me.RapidatxtSubDescuentoValor1.Size = New System.Drawing.Size(67, 20)
        Me.RapidatxtSubDescuentoValor1.TabIndex = 18
        Me.RapidatxtSubDescuentoValor1.TabStop = False
        Me.RapidatxtSubDescuentoValor1.Text = "0"
        '
        'Label179
        '
        Me.Label179.AutoSize = True
        Me.Label179.Location = New System.Drawing.Point(632, 267)
        Me.Label179.Name = "Label179"
        Me.Label179.Size = New System.Drawing.Size(70, 13)
        Me.Label179.TabIndex = 174
        Me.Label179.Text = "% Descuento"
        '
        'RapidatxtMontoItem1
        '
        Me.RapidatxtMontoItem1.Enabled = False
        Me.RapidatxtMontoItem1.Location = New System.Drawing.Point(708, 282)
        Me.RapidatxtMontoItem1.MaxLength = 17
        Me.RapidatxtMontoItem1.Name = "RapidatxtMontoItem1"
        Me.RapidatxtMontoItem1.Size = New System.Drawing.Size(73, 20)
        Me.RapidatxtMontoItem1.TabIndex = 19
        Me.RapidatxtMontoItem1.Text = "0"
        '
        'Label177
        '
        Me.Label177.AutoSize = True
        Me.Label177.Location = New System.Drawing.Point(727, 267)
        Me.Label177.Name = "Label177"
        Me.Label177.Size = New System.Drawing.Size(31, 13)
        Me.Label177.TabIndex = 172
        Me.Label177.Text = "Total"
        '
        'RapidacmbIndicadorFacturacion1
        '
        Me.RapidacmbIndicadorFacturacion1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.RapidacmbIndicadorFacturacion1.FormattingEnabled = True
        Me.RapidacmbIndicadorFacturacion1.Location = New System.Drawing.Point(3, 282)
        Me.RapidacmbIndicadorFacturacion1.Name = "RapidacmbIndicadorFacturacion1"
        Me.RapidacmbIndicadorFacturacion1.Size = New System.Drawing.Size(68, 21)
        Me.RapidacmbIndicadorFacturacion1.TabIndex = 3
        Me.RapidacmbIndicadorFacturacion1.TabStop = False
        '
        'BtnLimpiarCampos
        '
        Me.BtnLimpiarCampos.Location = New System.Drawing.Point(5, 881)
        Me.BtnLimpiarCampos.Name = "BtnLimpiarCampos"
        Me.BtnLimpiarCampos.Size = New System.Drawing.Size(123, 37)
        Me.BtnLimpiarCampos.TabIndex = 100
        Me.BtnLimpiarCampos.Text = "Limpiar campos (F12)"
        Me.BtnLimpiarCampos.UseVisualStyleBackColor = True
        '
        'RapidatxtNombreItem1
        '
        Me.RapidatxtNombreItem1.Location = New System.Drawing.Point(75, 282)
        Me.RapidatxtNombreItem1.MaxLength = 80
        Me.RapidatxtNombreItem1.Name = "RapidatxtNombreItem1"
        Me.RapidatxtNombreItem1.Size = New System.Drawing.Size(399, 20)
        Me.RapidatxtNombreItem1.TabIndex = 15
        '
        'BtnFacturar
        '
        Me.BtnFacturar.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnFacturar.Location = New System.Drawing.Point(896, 854)
        Me.BtnFacturar.Name = "BtnFacturar"
        Me.BtnFacturar.Size = New System.Drawing.Size(179, 75)
        Me.BtnFacturar.TabIndex = 98
        Me.BtnFacturar.Text = "Facturar (F8)"
        Me.BtnFacturar.UseVisualStyleBackColor = True
        '
        'GBx_Totales
        '
        Me.GBx_Totales.Controls.Add(Me.TBx_ExportacionYAsimilados)
        Me.GBx_Totales.Controls.Add(Me.Label26)
        Me.GBx_Totales.Controls.Add(Me.TBx_MontoRetenido)
        Me.GBx_Totales.Controls.Add(Me.Label11)
        Me.GBx_Totales.Controls.Add(Me.RapidatxtTotalMontoNoGravado)
        Me.GBx_Totales.Controls.Add(Me.Label8)
        Me.GBx_Totales.Controls.Add(Me.RapidaBtn_guardar)
        Me.GBx_Totales.Controls.Add(Me.lblDolares)
        Me.GBx_Totales.Controls.Add(Me.lblPesos)
        Me.GBx_Totales.Controls.Add(Me.RapidaLbl_tipoCambio)
        Me.GBx_Totales.Controls.Add(Me.RapidaTxtMontoNoFacturable)
        Me.GBx_Totales.Controls.Add(Me.RapidaTbx_tipoCambio)
        Me.GBx_Totales.Controls.Add(Me.Label5)
        Me.GBx_Totales.Controls.Add(Me.RapidatxtMontoTotalPagar)
        Me.GBx_Totales.Controls.Add(Me.lblMonedaSimbolo)
        Me.GBx_Totales.Controls.Add(Me.RapidatxtTotalIVATasaBasica)
        Me.GBx_Totales.Controls.Add(Me.Label186)
        Me.GBx_Totales.Controls.Add(Me.RapidatxtTotalIVATasaMinima)
        Me.GBx_Totales.Controls.Add(Me.Label187)
        Me.GBx_Totales.Controls.Add(Me.RapidatxtTotalMontoNetoIVATasaBasica)
        Me.GBx_Totales.Controls.Add(Me.Label172)
        Me.GBx_Totales.Controls.Add(Me.RapidatxtTotalMontoNetoIVATasaMinima)
        Me.GBx_Totales.Controls.Add(Me.Label185)
        Me.GBx_Totales.Location = New System.Drawing.Point(243, 646)
        Me.GBx_Totales.Name = "GBx_Totales"
        Me.GBx_Totales.Size = New System.Drawing.Size(830, 201)
        Me.GBx_Totales.TabIndex = 37
        Me.GBx_Totales.TabStop = False
        Me.GBx_Totales.Text = "Totales"
        '
        'TBx_ExportacionYAsimilados
        '
        Me.TBx_ExportacionYAsimilados.Enabled = False
        Me.TBx_ExportacionYAsimilados.Location = New System.Drawing.Point(190, 151)
        Me.TBx_ExportacionYAsimilados.MaxLength = 17
        Me.TBx_ExportacionYAsimilados.Name = "TBx_ExportacionYAsimilados"
        Me.TBx_ExportacionYAsimilados.Size = New System.Drawing.Size(128, 20)
        Me.TBx_ExportacionYAsimilados.TabIndex = 228
        Me.TBx_ExportacionYAsimilados.Text = "0"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(30, 154)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(154, 13)
        Me.Label26.TabIndex = 227
        Me.Label26.Text = "Monto exportacion y asimilados"
        '
        'TBx_MontoRetenido
        '
        Me.TBx_MontoRetenido.Enabled = False
        Me.TBx_MontoRetenido.Location = New System.Drawing.Point(190, 128)
        Me.TBx_MontoRetenido.MaxLength = 17
        Me.TBx_MontoRetenido.Name = "TBx_MontoRetenido"
        Me.TBx_MontoRetenido.Size = New System.Drawing.Size(128, 20)
        Me.TBx_MontoRetenido.TabIndex = 226
        Me.TBx_MontoRetenido.Text = "0"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(103, 131)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(78, 13)
        Me.Label11.TabIndex = 225
        Me.Label11.Text = "Monto retenido"
        '
        'RapidatxtTotalMontoNoGravado
        '
        Me.RapidatxtTotalMontoNoGravado.Enabled = False
        Me.RapidatxtTotalMontoNoGravado.Location = New System.Drawing.Point(190, 59)
        Me.RapidatxtTotalMontoNoGravado.MaxLength = 17
        Me.RapidatxtTotalMontoNoGravado.Name = "RapidatxtTotalMontoNoGravado"
        Me.RapidatxtTotalMontoNoGravado.Size = New System.Drawing.Size(128, 20)
        Me.RapidatxtTotalMontoNoGravado.TabIndex = 224
        Me.RapidatxtTotalMontoNoGravado.Text = "0"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(61, 62)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(120, 13)
        Me.Label8.TabIndex = 223
        Me.Label8.Text = "Total monto no gravado"
        '
        'RapidaBtn_guardar
        '
        Me.RapidaBtn_guardar.Location = New System.Drawing.Point(757, 160)
        Me.RapidaBtn_guardar.Name = "RapidaBtn_guardar"
        Me.RapidaBtn_guardar.Size = New System.Drawing.Size(54, 21)
        Me.RapidaBtn_guardar.TabIndex = 97
        Me.RapidaBtn_guardar.Text = "Guardar"
        Me.RapidaBtn_guardar.UseVisualStyleBackColor = True
        Me.RapidaBtn_guardar.Visible = False
        '
        'lblDolares
        '
        Me.lblDolares.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblDolares.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lblDolares.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDolares.Location = New System.Drawing.Point(365, 161)
        Me.lblDolares.Name = "lblDolares"
        Me.lblDolares.Size = New System.Drawing.Size(42, 20)
        Me.lblDolares.TabIndex = 95
        Me.lblDolares.Text = "US$"
        Me.lblDolares.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblPesos
        '
        Me.lblPesos.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.lblPesos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblPesos.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lblPesos.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPesos.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.lblPesos.Location = New System.Drawing.Point(324, 161)
        Me.lblPesos.Name = "lblPesos"
        Me.lblPesos.Size = New System.Drawing.Size(42, 20)
        Me.lblPesos.TabIndex = 94
        Me.lblPesos.Text = "$"
        Me.lblPesos.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'RapidaLbl_tipoCambio
        '
        Me.RapidaLbl_tipoCambio.AutoSize = True
        Me.RapidaLbl_tipoCambio.Location = New System.Drawing.Point(610, 163)
        Me.RapidaLbl_tipoCambio.Name = "RapidaLbl_tipoCambio"
        Me.RapidaLbl_tipoCambio.Size = New System.Drawing.Size(80, 13)
        Me.RapidaLbl_tipoCambio.TabIndex = 61
        Me.RapidaLbl_tipoCambio.Text = "Tipo de cambio"
        Me.RapidaLbl_tipoCambio.Visible = False
        '
        'RapidaTxtMontoNoFacturable
        '
        Me.RapidaTxtMontoNoFacturable.Location = New System.Drawing.Point(190, 174)
        Me.RapidaTxtMontoNoFacturable.MaxLength = 17
        Me.RapidaTxtMontoNoFacturable.Name = "RapidaTxtMontoNoFacturable"
        Me.RapidaTxtMontoNoFacturable.Size = New System.Drawing.Size(128, 20)
        Me.RapidaTxtMontoNoFacturable.TabIndex = 220
        Me.RapidaTxtMontoNoFacturable.Text = "0"
        '
        'RapidaTbx_tipoCambio
        '
        Me.RapidaTbx_tipoCambio.Location = New System.Drawing.Point(699, 160)
        Me.RapidaTbx_tipoCambio.Name = "RapidaTbx_tipoCambio"
        Me.RapidaTbx_tipoCambio.Size = New System.Drawing.Size(51, 20)
        Me.RapidaTbx_tipoCambio.TabIndex = 96
        Me.RapidaTbx_tipoCambio.Visible = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(79, 177)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(102, 13)
        Me.Label5.TabIndex = 219
        Me.Label5.Text = "Monto no facturable"
        '
        'RapidatxtMontoTotalPagar
        '
        Me.RapidatxtMontoTotalPagar.BackColor = System.Drawing.SystemColors.HotTrack
        Me.RapidatxtMontoTotalPagar.Font = New System.Drawing.Font("Microsoft Sans Serif", 50.0!)
        Me.RapidatxtMontoTotalPagar.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.RapidatxtMontoTotalPagar.Location = New System.Drawing.Point(453, 14)
        Me.RapidatxtMontoTotalPagar.Name = "RapidatxtMontoTotalPagar"
        Me.RapidatxtMontoTotalPagar.Padding = New System.Windows.Forms.Padding(1, 0, 1, 0)
        Me.RapidatxtMontoTotalPagar.Size = New System.Drawing.Size(369, 142)
        Me.RapidatxtMontoTotalPagar.TabIndex = 216
        Me.RapidatxtMontoTotalPagar.Text = "0"
        Me.RapidatxtMontoTotalPagar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblMonedaSimbolo
        '
        Me.lblMonedaSimbolo.BackColor = System.Drawing.SystemColors.HotTrack
        Me.lblMonedaSimbolo.Font = New System.Drawing.Font("Microsoft Sans Serif", 45.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMonedaSimbolo.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.lblMonedaSimbolo.Location = New System.Drawing.Point(324, 14)
        Me.lblMonedaSimbolo.Name = "lblMonedaSimbolo"
        Me.lblMonedaSimbolo.Padding = New System.Windows.Forms.Padding(1, 0, 1, 0)
        Me.lblMonedaSimbolo.Size = New System.Drawing.Size(158, 142)
        Me.lblMonedaSimbolo.TabIndex = 216
        Me.lblMonedaSimbolo.Text = "$"
        Me.lblMonedaSimbolo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'RapidatxtTotalIVATasaBasica
        '
        Me.RapidatxtTotalIVATasaBasica.Enabled = False
        Me.RapidatxtTotalIVATasaBasica.Location = New System.Drawing.Point(190, 105)
        Me.RapidatxtTotalIVATasaBasica.MaxLength = 17
        Me.RapidatxtTotalIVATasaBasica.Name = "RapidatxtTotalIVATasaBasica"
        Me.RapidatxtTotalIVATasaBasica.Size = New System.Drawing.Size(128, 20)
        Me.RapidatxtTotalIVATasaBasica.TabIndex = 38
        Me.RapidatxtTotalIVATasaBasica.Text = "0"
        '
        'Label186
        '
        Me.Label186.AutoSize = True
        Me.Label186.Location = New System.Drawing.Point(67, 108)
        Me.Label186.Name = "Label186"
        Me.Label186.Size = New System.Drawing.Size(118, 13)
        Me.Label186.TabIndex = 36
        Me.Label186.Text = "Total IVA - Tasa basica"
        '
        'RapidatxtTotalIVATasaMinima
        '
        Me.RapidatxtTotalIVATasaMinima.Enabled = False
        Me.RapidatxtTotalIVATasaMinima.Location = New System.Drawing.Point(190, 36)
        Me.RapidatxtTotalIVATasaMinima.MaxLength = 17
        Me.RapidatxtTotalIVATasaMinima.Name = "RapidatxtTotalIVATasaMinima"
        Me.RapidatxtTotalIVATasaMinima.Size = New System.Drawing.Size(128, 20)
        Me.RapidatxtTotalIVATasaMinima.TabIndex = 37
        Me.RapidatxtTotalIVATasaMinima.Text = "0"
        '
        'Label187
        '
        Me.Label187.AutoSize = True
        Me.Label187.Location = New System.Drawing.Point(66, 39)
        Me.Label187.Name = "Label187"
        Me.Label187.Size = New System.Drawing.Size(119, 13)
        Me.Label187.TabIndex = 35
        Me.Label187.Text = "Total IVA - Tasa minima"
        '
        'RapidatxtTotalMontoNetoIVATasaBasica
        '
        Me.RapidatxtTotalMontoNetoIVATasaBasica.Enabled = False
        Me.RapidatxtTotalMontoNetoIVATasaBasica.Location = New System.Drawing.Point(190, 82)
        Me.RapidatxtTotalMontoNetoIVATasaBasica.MaxLength = 17
        Me.RapidatxtTotalMontoNetoIVATasaBasica.Name = "RapidatxtTotalMontoNetoIVATasaBasica"
        Me.RapidatxtTotalMontoNetoIVATasaBasica.Size = New System.Drawing.Size(128, 20)
        Me.RapidatxtTotalMontoNetoIVATasaBasica.TabIndex = 33
        Me.RapidatxtTotalMontoNetoIVATasaBasica.Text = "0"
        '
        'Label172
        '
        Me.Label172.AutoSize = True
        Me.Label172.Location = New System.Drawing.Point(8, 85)
        Me.Label172.Name = "Label172"
        Me.Label172.Size = New System.Drawing.Size(170, 13)
        Me.Label172.TabIndex = 31
        Me.Label172.Text = "Total monto neto - IVA tasa basica"
        '
        'RapidatxtTotalMontoNetoIVATasaMinima
        '
        Me.RapidatxtTotalMontoNetoIVATasaMinima.Enabled = False
        Me.RapidatxtTotalMontoNetoIVATasaMinima.Location = New System.Drawing.Point(190, 13)
        Me.RapidatxtTotalMontoNetoIVATasaMinima.MaxLength = 17
        Me.RapidatxtTotalMontoNetoIVATasaMinima.Name = "RapidatxtTotalMontoNetoIVATasaMinima"
        Me.RapidatxtTotalMontoNetoIVATasaMinima.Size = New System.Drawing.Size(128, 20)
        Me.RapidatxtTotalMontoNetoIVATasaMinima.TabIndex = 32
        Me.RapidatxtTotalMontoNetoIVATasaMinima.Text = "0"
        '
        'Label185
        '
        Me.Label185.AutoSize = True
        Me.Label185.Location = New System.Drawing.Point(7, 16)
        Me.Label185.Name = "Label185"
        Me.Label185.Size = New System.Drawing.Size(175, 13)
        Me.Label185.TabIndex = 30
        Me.Label185.Text = "Total monto neto - IVA Tasa minima"
        '
        'RapidatxtCantidad1
        '
        Me.RapidatxtCantidad1.Location = New System.Drawing.Point(508, 282)
        Me.RapidatxtCantidad1.MaxLength = 17
        Me.RapidatxtCantidad1.Name = "RapidatxtCantidad1"
        Me.RapidatxtCantidad1.Size = New System.Drawing.Size(46, 20)
        Me.RapidatxtCantidad1.TabIndex = 16
        Me.RapidatxtCantidad1.Text = "1"
        '
        'GBx_MediosPagos
        '
        Me.GBx_MediosPagos.BackColor = System.Drawing.Color.Transparent
        Me.GBx_MediosPagos.Controls.Add(Me.RapidatxtVuelto)
        Me.GBx_MediosPagos.Controls.Add(Me.Label169)
        Me.GBx_MediosPagos.Controls.Add(Me.RapidatxtValorMediosDePago2)
        Me.GBx_MediosPagos.Controls.Add(Me.RapidatxtValorMediosDePago1)
        Me.GBx_MediosPagos.Controls.Add(Me.Label167)
        Me.GBx_MediosPagos.Controls.Add(Me.RapidatxtGlosaMediosDePago2)
        Me.GBx_MediosPagos.Controls.Add(Me.RapidatxtGlosaMediosDePago1)
        Me.GBx_MediosPagos.Controls.Add(Me.Label168)
        Me.GBx_MediosPagos.Controls.Add(Me.Label173)
        Me.GBx_MediosPagos.Controls.Add(Me.Label174)
        Me.GBx_MediosPagos.Controls.Add(Me.Label181)
        Me.GBx_MediosPagos.Location = New System.Drawing.Point(8, 646)
        Me.GBx_MediosPagos.Name = "GBx_MediosPagos"
        Me.GBx_MediosPagos.Size = New System.Drawing.Size(229, 176)
        Me.GBx_MediosPagos.TabIndex = 33
        Me.GBx_MediosPagos.TabStop = False
        Me.GBx_MediosPagos.Text = "Medios de pago"
        '
        'RapidatxtVuelto
        '
        Me.RapidatxtVuelto.Enabled = False
        Me.RapidatxtVuelto.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RapidatxtVuelto.Location = New System.Drawing.Point(127, 100)
        Me.RapidatxtVuelto.MaxLength = 17
        Me.RapidatxtVuelto.Name = "RapidatxtVuelto"
        Me.RapidatxtVuelto.Size = New System.Drawing.Size(92, 26)
        Me.RapidatxtVuelto.TabIndex = 104
        Me.RapidatxtVuelto.Text = "0"
        Me.RapidatxtVuelto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label169
        '
        Me.Label169.AutoSize = True
        Me.Label169.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label169.Location = New System.Drawing.Point(52, 102)
        Me.Label169.Name = "Label169"
        Me.Label169.Size = New System.Drawing.Size(66, 20)
        Me.Label169.TabIndex = 103
        Me.Label169.Text = "Vuelto:"
        '
        'RapidatxtValorMediosDePago2
        '
        Me.RapidatxtValorMediosDePago2.Location = New System.Drawing.Point(127, 67)
        Me.RapidatxtValorMediosDePago2.MaxLength = 17
        Me.RapidatxtValorMediosDePago2.Name = "RapidatxtValorMediosDePago2"
        Me.RapidatxtValorMediosDePago2.Size = New System.Drawing.Size(92, 20)
        Me.RapidatxtValorMediosDePago2.TabIndex = 93
        Me.RapidatxtValorMediosDePago2.Text = "0"
        '
        'RapidatxtValorMediosDePago1
        '
        Me.RapidatxtValorMediosDePago1.Location = New System.Drawing.Point(127, 41)
        Me.RapidatxtValorMediosDePago1.MaxLength = 17
        Me.RapidatxtValorMediosDePago1.Name = "RapidatxtValorMediosDePago1"
        Me.RapidatxtValorMediosDePago1.Size = New System.Drawing.Size(92, 20)
        Me.RapidatxtValorMediosDePago1.TabIndex = 91
        Me.RapidatxtValorMediosDePago1.Text = "0"
        '
        'Label167
        '
        Me.Label167.AutoSize = True
        Me.Label167.Location = New System.Drawing.Point(153, 25)
        Me.Label167.Name = "Label167"
        Me.Label167.Size = New System.Drawing.Size(31, 13)
        Me.Label167.TabIndex = 102
        Me.Label167.Text = "Valor"
        '
        'RapidatxtGlosaMediosDePago2
        '
        Me.RapidatxtGlosaMediosDePago2.Location = New System.Drawing.Point(26, 67)
        Me.RapidatxtGlosaMediosDePago2.MaxLength = 50
        Me.RapidatxtGlosaMediosDePago2.Name = "RapidatxtGlosaMediosDePago2"
        Me.RapidatxtGlosaMediosDePago2.Size = New System.Drawing.Size(92, 20)
        Me.RapidatxtGlosaMediosDePago2.TabIndex = 92
        '
        'RapidatxtGlosaMediosDePago1
        '
        Me.RapidatxtGlosaMediosDePago1.Location = New System.Drawing.Point(26, 41)
        Me.RapidatxtGlosaMediosDePago1.MaxLength = 50
        Me.RapidatxtGlosaMediosDePago1.Name = "RapidatxtGlosaMediosDePago1"
        Me.RapidatxtGlosaMediosDePago1.Size = New System.Drawing.Size(92, 20)
        Me.RapidatxtGlosaMediosDePago1.TabIndex = 90
        '
        'Label168
        '
        Me.Label168.AutoSize = True
        Me.Label168.Location = New System.Drawing.Point(41, 25)
        Me.Label168.Name = "Label168"
        Me.Label168.Size = New System.Drawing.Size(63, 13)
        Me.Label168.TabIndex = 96
        Me.Label168.Text = "Descripcion"
        '
        'Label173
        '
        Me.Label173.AutoSize = True
        Me.Label173.Location = New System.Drawing.Point(6, 70)
        Me.Label173.Name = "Label173"
        Me.Label173.Size = New System.Drawing.Size(13, 13)
        Me.Label173.TabIndex = 86
        Me.Label173.Text = "2"
        '
        'Label174
        '
        Me.Label174.AutoSize = True
        Me.Label174.Location = New System.Drawing.Point(6, 44)
        Me.Label174.Name = "Label174"
        Me.Label174.Size = New System.Drawing.Size(13, 13)
        Me.Label174.TabIndex = 81
        Me.Label174.Text = "1"
        '
        'Label181
        '
        Me.Label181.AutoSize = True
        Me.Label181.Location = New System.Drawing.Point(6, 25)
        Me.Label181.Name = "Label181"
        Me.Label181.Size = New System.Drawing.Size(19, 13)
        Me.Label181.TabIndex = 78
        Me.Label181.Text = "N°"
        '
        'RapidatxtPrecioUnitario1
        '
        Me.RapidatxtPrecioUnitario1.Location = New System.Drawing.Point(560, 282)
        Me.RapidatxtPrecioUnitario1.MaxLength = 17
        Me.RapidatxtPrecioUnitario1.Name = "RapidatxtPrecioUnitario1"
        Me.RapidatxtPrecioUnitario1.Size = New System.Drawing.Size(69, 20)
        Me.RapidatxtPrecioUnitario1.TabIndex = 17
        Me.RapidatxtPrecioUnitario1.Text = "0"
        '
        'Label176
        '
        Me.Label176.AutoSize = True
        Me.Label176.Location = New System.Drawing.Point(574, 267)
        Me.Label176.Name = "Label176"
        Me.Label176.Size = New System.Drawing.Size(37, 13)
        Me.Label176.TabIndex = 125
        Me.Label176.Text = "Precio"
        '
        'Label178
        '
        Me.Label178.AutoSize = True
        Me.Label178.Location = New System.Drawing.Point(506, 267)
        Me.Label178.Name = "Label178"
        Me.Label178.Size = New System.Drawing.Size(49, 13)
        Me.Label178.TabIndex = 123
        Me.Label178.Text = "Cantidad"
        '
        'Label180
        '
        Me.Label180.AutoSize = True
        Me.Label180.Location = New System.Drawing.Point(240, 267)
        Me.Label180.Name = "Label180"
        Me.Label180.Size = New System.Drawing.Size(67, 13)
        Me.Label180.TabIndex = 121
        Me.Label180.Text = "Nombre Item"
        '
        'Label182
        '
        Me.Label182.AutoSize = True
        Me.Label182.Location = New System.Drawing.Point(7, 267)
        Me.Label182.Name = "Label182"
        Me.Label182.Size = New System.Drawing.Size(41, 13)
        Me.Label182.TabIndex = 119
        Me.Label182.Text = "Lineas:"
        '
        'GroupBox8
        '
        Me.GroupBox8.Controls.Add(Me.Btn_Buscar)
        Me.GroupBox8.Controls.Add(Me.Ckb_Exportacion)
        Me.GroupBox8.Controls.Add(Me.chkConsumoFinal)
        Me.GroupBox8.Controls.Add(Me.RapidatxtDepartamentoReceptor)
        Me.GroupBox8.Controls.Add(Me.Label210)
        Me.GroupBox8.Controls.Add(Me.RapidatxtCiudadReceptor)
        Me.GroupBox8.Controls.Add(Me.Label197)
        Me.GroupBox8.Controls.Add(Me.RapidatxtDireccionReceptor)
        Me.GroupBox8.Controls.Add(Me.Label85)
        Me.GroupBox8.Controls.Add(Me.RapidatxtNombreReceptor)
        Me.GroupBox8.Controls.Add(Me.Label86)
        Me.GroupBox8.Controls.Add(Me.RapidatxtDocumentoReceptor)
        Me.GroupBox8.Controls.Add(Me.Label163)
        Me.GroupBox8.Controls.Add(Me.RapidacmbCodigoPaisReceptor)
        Me.GroupBox8.Controls.Add(Me.RapidacmbTipoDocumentoReceptor)
        Me.GroupBox8.Controls.Add(Me.Label75)
        Me.GroupBox8.Controls.Add(Me.Label84)
        Me.GroupBox8.Location = New System.Drawing.Point(3, 106)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(1072, 65)
        Me.GroupBox8.TabIndex = 0
        Me.GroupBox8.TabStop = False
        Me.GroupBox8.Text = "Cliente"
        '
        'Btn_Buscar
        '
        Me.Btn_Buscar.Location = New System.Drawing.Point(985, 11)
        Me.Btn_Buscar.Name = "Btn_Buscar"
        Me.Btn_Buscar.Size = New System.Drawing.Size(75, 23)
        Me.Btn_Buscar.TabIndex = 59
        Me.Btn_Buscar.Text = "Buscar"
        Me.Btn_Buscar.UseVisualStyleBackColor = True
        '
        'Ckb_Exportacion
        '
        Me.Ckb_Exportacion.AutoSize = True
        Me.Ckb_Exportacion.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Ckb_Exportacion.Location = New System.Drawing.Point(71, 35)
        Me.Ckb_Exportacion.Name = "Ckb_Exportacion"
        Me.Ckb_Exportacion.Size = New System.Drawing.Size(82, 17)
        Me.Ckb_Exportacion.TabIndex = 58
        Me.Ckb_Exportacion.Text = "Exportacion"
        Me.Ckb_Exportacion.UseVisualStyleBackColor = True
        '
        'chkConsumoFinal
        '
        Me.chkConsumoFinal.AutoSize = True
        Me.chkConsumoFinal.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkConsumoFinal.Checked = True
        Me.chkConsumoFinal.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkConsumoFinal.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkConsumoFinal.Location = New System.Drawing.Point(12, 12)
        Me.chkConsumoFinal.Name = "chkConsumoFinal"
        Me.chkConsumoFinal.Size = New System.Drawing.Size(141, 22)
        Me.chkConsumoFinal.TabIndex = 57
        Me.chkConsumoFinal.Text = "Consumo Final"
        Me.chkConsumoFinal.UseVisualStyleBackColor = True
        '
        'RapidatxtDepartamentoReceptor
        '
        Me.RapidatxtDepartamentoReceptor.Location = New System.Drawing.Point(434, 37)
        Me.RapidatxtDepartamentoReceptor.MaxLength = 30
        Me.RapidatxtDepartamentoReceptor.Name = "RapidatxtDepartamentoReceptor"
        Me.RapidatxtDepartamentoReceptor.Size = New System.Drawing.Size(100, 20)
        Me.RapidatxtDepartamentoReceptor.TabIndex = 4
        '
        'Label210
        '
        Me.Label210.AutoSize = True
        Me.Label210.Location = New System.Drawing.Point(358, 41)
        Me.Label210.Name = "Label210"
        Me.Label210.Size = New System.Drawing.Size(74, 13)
        Me.Label210.TabIndex = 50
        Me.Label210.Text = "Departamento"
        '
        'RapidatxtCiudadReceptor
        '
        Me.RapidatxtCiudadReceptor.Location = New System.Drawing.Point(590, 37)
        Me.RapidatxtCiudadReceptor.MaxLength = 30
        Me.RapidatxtCiudadReceptor.Name = "RapidatxtCiudadReceptor"
        Me.RapidatxtCiudadReceptor.Size = New System.Drawing.Size(100, 20)
        Me.RapidatxtCiudadReceptor.TabIndex = 5
        '
        'Label197
        '
        Me.Label197.AutoSize = True
        Me.Label197.Location = New System.Drawing.Point(544, 40)
        Me.Label197.Name = "Label197"
        Me.Label197.Size = New System.Drawing.Size(40, 13)
        Me.Label197.TabIndex = 48
        Me.Label197.Text = "Ciudad"
        '
        'RapidatxtDireccionReceptor
        '
        Me.RapidatxtDireccionReceptor.Location = New System.Drawing.Point(759, 37)
        Me.RapidatxtDireccionReceptor.MaxLength = 70
        Me.RapidatxtDireccionReceptor.Name = "RapidatxtDireccionReceptor"
        Me.RapidatxtDireccionReceptor.Size = New System.Drawing.Size(190, 20)
        Me.RapidatxtDireccionReceptor.TabIndex = 6
        '
        'Label85
        '
        Me.Label85.AutoSize = True
        Me.Label85.Location = New System.Drawing.Point(703, 40)
        Me.Label85.Name = "Label85"
        Me.Label85.Size = New System.Drawing.Size(52, 13)
        Me.Label85.TabIndex = 47
        Me.Label85.Text = "Direccion"
        '
        'RapidatxtNombreReceptor
        '
        Me.RapidatxtNombreReceptor.Location = New System.Drawing.Point(590, 11)
        Me.RapidatxtNombreReceptor.MaxLength = 150
        Me.RapidatxtNombreReceptor.Name = "RapidatxtNombreReceptor"
        Me.RapidatxtNombreReceptor.Size = New System.Drawing.Size(223, 20)
        Me.RapidatxtNombreReceptor.TabIndex = 2
        '
        'Label86
        '
        Me.Label86.AutoSize = True
        Me.Label86.Location = New System.Drawing.Point(540, 16)
        Me.Label86.Name = "Label86"
        Me.Label86.Size = New System.Drawing.Size(44, 13)
        Me.Label86.TabIndex = 46
        Me.Label86.Text = "Nombre"
        '
        'RapidatxtDocumentoReceptor
        '
        Me.RapidatxtDocumentoReceptor.Location = New System.Drawing.Point(434, 11)
        Me.RapidatxtDocumentoReceptor.MaxLength = 12
        Me.RapidatxtDocumentoReceptor.Name = "RapidatxtDocumentoReceptor"
        Me.RapidatxtDocumentoReceptor.Size = New System.Drawing.Size(100, 20)
        Me.RapidatxtDocumentoReceptor.TabIndex = 1
        '
        'Label163
        '
        Me.Label163.AutoSize = True
        Me.Label163.Location = New System.Drawing.Point(370, 16)
        Me.Label163.Name = "Label163"
        Me.Label163.Size = New System.Drawing.Size(62, 13)
        Me.Label163.TabIndex = 45
        Me.Label163.Text = "Documento"
        '
        'RapidacmbCodigoPaisReceptor
        '
        Me.RapidacmbCodigoPaisReceptor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.RapidacmbCodigoPaisReceptor.FormattingEnabled = True
        Me.RapidacmbCodigoPaisReceptor.Location = New System.Drawing.Point(252, 37)
        Me.RapidacmbCodigoPaisReceptor.Name = "RapidacmbCodigoPaisReceptor"
        Me.RapidacmbCodigoPaisReceptor.Size = New System.Drawing.Size(100, 21)
        Me.RapidacmbCodigoPaisReceptor.TabIndex = 3
        '
        'RapidacmbTipoDocumentoReceptor
        '
        Me.RapidacmbTipoDocumentoReceptor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.RapidacmbTipoDocumentoReceptor.FormattingEnabled = True
        Me.RapidacmbTipoDocumentoReceptor.Location = New System.Drawing.Point(252, 11)
        Me.RapidacmbTipoDocumentoReceptor.Name = "RapidacmbTipoDocumentoReceptor"
        Me.RapidacmbTipoDocumentoReceptor.Size = New System.Drawing.Size(100, 21)
        Me.RapidacmbTipoDocumentoReceptor.TabIndex = 0
        '
        'Label75
        '
        Me.Label75.AutoSize = True
        Me.Label75.Location = New System.Drawing.Point(223, 42)
        Me.Label75.Name = "Label75"
        Me.Label75.Size = New System.Drawing.Size(27, 13)
        Me.Label75.TabIndex = 42
        Me.Label75.Text = "Pais"
        '
        'Label84
        '
        Me.Label84.AutoSize = True
        Me.Label84.Location = New System.Drawing.Point(196, 16)
        Me.Label84.Name = "Label84"
        Me.Label84.Size = New System.Drawing.Size(52, 13)
        Me.Label84.TabIndex = 41
        Me.Label84.Text = "Tipo doc."
        '
        'GroupBox11
        '
        Me.GroupBox11.Controls.Add(Me.CmbIndicadorMontoBrutoEncabezado)
        Me.GroupBox11.Controls.Add(Me.chkVencimientoCredito)
        Me.GroupBox11.Controls.Add(Me.lblVencimiento)
        Me.GroupBox11.Controls.Add(Me.dtpFechaVencimiento)
        Me.GroupBox11.Controls.Add(Me.lblFormaDePago)
        Me.GroupBox11.Controls.Add(Me.cmbFormaPago)
        Me.GroupBox11.Controls.Add(Me.chkIndicadorRefGlobal)
        Me.GroupBox11.Controls.Add(Me.Label2)
        Me.GroupBox11.Controls.Add(Me.lblReferencia)
        Me.GroupBox11.Controls.Add(Me.lblFechaCFEInformacionReferencia)
        Me.GroupBox11.Controls.Add(Me.Label164)
        Me.GroupBox11.Controls.Add(Me.dtpFechaCFEInformacionReferencia)
        Me.GroupBox11.Controls.Add(Me.RapidatxtNumeroComprobanteEncabezado)
        Me.GroupBox11.Controls.Add(Me.RapidatxtSerieComprobanteEncabezado)
        Me.GroupBox11.Controls.Add(Me.Label165)
        Me.GroupBox11.Controls.Add(Me.txtRazonInformacionReferencia)
        Me.GroupBox11.Controls.Add(Me.txtNumeroCFEInformacionReferencia)
        Me.GroupBox11.Controls.Add(Me.Label166)
        Me.GroupBox11.Controls.Add(Me.lblRazonInformacionReferencia)
        Me.GroupBox11.Controls.Add(Me.lblNumeroCFEInformacionReferencia)
        Me.GroupBox11.Controls.Add(Me.cmbTipoVenta)
        Me.GroupBox11.Controls.Add(Me.txtSerieCFEInformacionReferencia)
        Me.GroupBox11.Controls.Add(Me.cmbTipoCFEInformacionReferencia)
        Me.GroupBox11.Controls.Add(Me.lblSerieCFEInformacionReferencia)
        Me.GroupBox11.Controls.Add(Me.lblTipoCFEInformacionReferencia)
        Me.GroupBox11.Location = New System.Drawing.Point(3, 177)
        Me.GroupBox11.Name = "GroupBox11"
        Me.GroupBox11.Size = New System.Drawing.Size(1072, 67)
        Me.GroupBox11.TabIndex = 5
        Me.GroupBox11.TabStop = False
        Me.GroupBox11.Text = "Factura"
        '
        'CmbIndicadorMontoBrutoEncabezado
        '
        Me.CmbIndicadorMontoBrutoEncabezado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CmbIndicadorMontoBrutoEncabezado.FormattingEnabled = True
        Me.CmbIndicadorMontoBrutoEncabezado.Location = New System.Drawing.Point(583, 14)
        Me.CmbIndicadorMontoBrutoEncabezado.Name = "CmbIndicadorMontoBrutoEncabezado"
        Me.CmbIndicadorMontoBrutoEncabezado.Size = New System.Drawing.Size(104, 21)
        Me.CmbIndicadorMontoBrutoEncabezado.TabIndex = 284
        '
        'chkVencimientoCredito
        '
        Me.chkVencimientoCredito.AutoSize = True
        Me.chkVencimientoCredito.Checked = True
        Me.chkVencimientoCredito.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkVencimientoCredito.Location = New System.Drawing.Point(944, 16)
        Me.chkVencimientoCredito.Name = "chkVencimientoCredito"
        Me.chkVencimientoCredito.Size = New System.Drawing.Size(15, 14)
        Me.chkVencimientoCredito.TabIndex = 283
        Me.chkVencimientoCredito.UseVisualStyleBackColor = True
        Me.chkVencimientoCredito.Visible = False
        '
        'lblVencimiento
        '
        Me.lblVencimiento.AutoSize = True
        Me.lblVencimiento.Location = New System.Drawing.Point(877, 17)
        Me.lblVencimiento.Name = "lblVencimiento"
        Me.lblVencimiento.Size = New System.Drawing.Size(65, 13)
        Me.lblVencimiento.TabIndex = 220
        Me.lblVencimiento.Text = "Vencimiento"
        Me.lblVencimiento.Visible = False
        '
        'dtpFechaVencimiento
        '
        Me.dtpFechaVencimiento.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaVencimiento.Location = New System.Drawing.Point(961, 13)
        Me.dtpFechaVencimiento.MinDate = New Date(2011, 1, 10, 0, 0, 0, 0)
        Me.dtpFechaVencimiento.Name = "dtpFechaVencimiento"
        Me.dtpFechaVencimiento.Size = New System.Drawing.Size(101, 20)
        Me.dtpFechaVencimiento.TabIndex = 219
        Me.dtpFechaVencimiento.Visible = False
        '
        'lblFormaDePago
        '
        Me.lblFormaDePago.AutoSize = True
        Me.lblFormaDePago.Location = New System.Drawing.Point(688, 18)
        Me.lblFormaDePago.Name = "lblFormaDePago"
        Me.lblFormaDePago.Size = New System.Drawing.Size(81, 13)
        Me.lblFormaDePago.TabIndex = 218
        Me.lblFormaDePago.Text = "Forma De Pago"
        '
        'cmbFormaPago
        '
        Me.cmbFormaPago.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbFormaPago.FormattingEnabled = True
        Me.cmbFormaPago.Location = New System.Drawing.Point(773, 14)
        Me.cmbFormaPago.Name = "cmbFormaPago"
        Me.cmbFormaPago.Size = New System.Drawing.Size(99, 21)
        Me.cmbFormaPago.TabIndex = 217
        '
        'chkIndicadorRefGlobal
        '
        Me.chkIndicadorRefGlobal.AutoSize = True
        Me.chkIndicadorRefGlobal.Location = New System.Drawing.Point(110, 44)
        Me.chkIndicadorRefGlobal.Name = "chkIndicadorRefGlobal"
        Me.chkIndicadorRefGlobal.Size = New System.Drawing.Size(15, 14)
        Me.chkIndicadorRefGlobal.TabIndex = 4
        Me.chkIndicadorRefGlobal.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(471, 17)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(111, 13)
        Me.Label2.TabIndex = 56
        Me.Label2.Text = "Indicador Monto bruto"
        '
        'lblReferencia
        '
        Me.lblReferencia.AutoSize = True
        Me.lblReferencia.Location = New System.Drawing.Point(12, 44)
        Me.lblReferencia.Name = "lblReferencia"
        Me.lblReferencia.Size = New System.Drawing.Size(92, 13)
        Me.lblReferencia.TabIndex = 213
        Me.lblReferencia.Text = "Referencia Global"
        '
        'lblFechaCFEInformacionReferencia
        '
        Me.lblFechaCFEInformacionReferencia.AutoSize = True
        Me.lblFechaCFEInformacionReferencia.Location = New System.Drawing.Point(899, 40)
        Me.lblFechaCFEInformacionReferencia.Name = "lblFechaCFEInformacionReferencia"
        Me.lblFechaCFEInformacionReferencia.Size = New System.Drawing.Size(60, 13)
        Me.lblFechaCFEInformacionReferencia.TabIndex = 216
        Me.lblFechaCFEInformacionReferencia.Text = "Fecha CFE"
        '
        'Label164
        '
        Me.Label164.AutoSize = True
        Me.Label164.Location = New System.Drawing.Point(322, 18)
        Me.Label164.Name = "Label164"
        Me.Label164.Size = New System.Drawing.Size(44, 13)
        Me.Label164.TabIndex = 22
        Me.Label164.Text = "Numero"
        '
        'dtpFechaCFEInformacionReferencia
        '
        Me.dtpFechaCFEInformacionReferencia.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaCFEInformacionReferencia.Location = New System.Drawing.Point(961, 38)
        Me.dtpFechaCFEInformacionReferencia.MinDate = New Date(2011, 1, 10, 0, 0, 0, 0)
        Me.dtpFechaCFEInformacionReferencia.Name = "dtpFechaCFEInformacionReferencia"
        Me.dtpFechaCFEInformacionReferencia.Size = New System.Drawing.Size(101, 20)
        Me.dtpFechaCFEInformacionReferencia.TabIndex = 9
        '
        'RapidatxtNumeroComprobanteEncabezado
        '
        Me.RapidatxtNumeroComprobanteEncabezado.Location = New System.Drawing.Point(372, 14)
        Me.RapidatxtNumeroComprobanteEncabezado.MaxLength = 20
        Me.RapidatxtNumeroComprobanteEncabezado.Name = "RapidatxtNumeroComprobanteEncabezado"
        Me.RapidatxtNumeroComprobanteEncabezado.Size = New System.Drawing.Size(92, 20)
        Me.RapidatxtNumeroComprobanteEncabezado.TabIndex = 3
        '
        'RapidatxtSerieComprobanteEncabezado
        '
        Me.RapidatxtSerieComprobanteEncabezado.Location = New System.Drawing.Point(280, 14)
        Me.RapidatxtSerieComprobanteEncabezado.MaxLength = 3
        Me.RapidatxtSerieComprobanteEncabezado.Name = "RapidatxtSerieComprobanteEncabezado"
        Me.RapidatxtSerieComprobanteEncabezado.Size = New System.Drawing.Size(33, 20)
        Me.RapidatxtSerieComprobanteEncabezado.TabIndex = 2
        Me.RapidatxtSerieComprobanteEncabezado.Text = "A"
        '
        'Label165
        '
        Me.Label165.AutoSize = True
        Me.Label165.Location = New System.Drawing.Point(243, 18)
        Me.Label165.Name = "Label165"
        Me.Label165.Size = New System.Drawing.Size(31, 13)
        Me.Label165.TabIndex = 21
        Me.Label165.Text = "Serie"
        '
        'txtRazonInformacionReferencia
        '
        Me.txtRazonInformacionReferencia.Location = New System.Drawing.Point(700, 38)
        Me.txtRazonInformacionReferencia.MaxLength = 90
        Me.txtRazonInformacionReferencia.Name = "txtRazonInformacionReferencia"
        Me.txtRazonInformacionReferencia.Size = New System.Drawing.Size(193, 20)
        Me.txtRazonInformacionReferencia.TabIndex = 8
        '
        'txtNumeroCFEInformacionReferencia
        '
        Me.txtNumeroCFEInformacionReferencia.Location = New System.Drawing.Point(550, 38)
        Me.txtNumeroCFEInformacionReferencia.MaxLength = 7
        Me.txtNumeroCFEInformacionReferencia.Name = "txtNumeroCFEInformacionReferencia"
        Me.txtNumeroCFEInformacionReferencia.Size = New System.Drawing.Size(100, 20)
        Me.txtNumeroCFEInformacionReferencia.TabIndex = 7
        '
        'Label166
        '
        Me.Label166.AutoSize = True
        Me.Label166.Location = New System.Drawing.Point(6, 16)
        Me.Label166.Name = "Label166"
        Me.Label166.Size = New System.Drawing.Size(59, 13)
        Me.Label166.TabIndex = 20
        Me.Label166.Text = "Tipo Venta"
        '
        'lblRazonInformacionReferencia
        '
        Me.lblRazonInformacionReferencia.AutoSize = True
        Me.lblRazonInformacionReferencia.Location = New System.Drawing.Point(656, 40)
        Me.lblRazonInformacionReferencia.Name = "lblRazonInformacionReferencia"
        Me.lblRazonInformacionReferencia.Size = New System.Drawing.Size(38, 13)
        Me.lblRazonInformacionReferencia.TabIndex = 215
        Me.lblRazonInformacionReferencia.Text = "Razón"
        '
        'lblNumeroCFEInformacionReferencia
        '
        Me.lblNumeroCFEInformacionReferencia.AutoSize = True
        Me.lblNumeroCFEInformacionReferencia.Location = New System.Drawing.Point(504, 41)
        Me.lblNumeroCFEInformacionReferencia.Name = "lblNumeroCFEInformacionReferencia"
        Me.lblNumeroCFEInformacionReferencia.Size = New System.Drawing.Size(44, 13)
        Me.lblNumeroCFEInformacionReferencia.TabIndex = 214
        Me.lblNumeroCFEInformacionReferencia.Text = "Numero"
        '
        'cmbTipoVenta
        '
        Me.cmbTipoVenta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTipoVenta.FormattingEnabled = True
        Me.cmbTipoVenta.Location = New System.Drawing.Point(66, 13)
        Me.cmbTipoVenta.Name = "cmbTipoVenta"
        Me.cmbTipoVenta.Size = New System.Drawing.Size(168, 21)
        Me.cmbTipoVenta.TabIndex = 0
        '
        'txtSerieCFEInformacionReferencia
        '
        Me.txtSerieCFEInformacionReferencia.Location = New System.Drawing.Point(459, 38)
        Me.txtSerieCFEInformacionReferencia.MaxLength = 2
        Me.txtSerieCFEInformacionReferencia.Name = "txtSerieCFEInformacionReferencia"
        Me.txtSerieCFEInformacionReferencia.Size = New System.Drawing.Size(33, 20)
        Me.txtSerieCFEInformacionReferencia.TabIndex = 6
        '
        'cmbTipoCFEInformacionReferencia
        '
        Me.cmbTipoCFEInformacionReferencia.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTipoCFEInformacionReferencia.FormattingEnabled = True
        Me.cmbTipoCFEInformacionReferencia.Location = New System.Drawing.Point(199, 38)
        Me.cmbTipoCFEInformacionReferencia.Name = "cmbTipoCFEInformacionReferencia"
        Me.cmbTipoCFEInformacionReferencia.Size = New System.Drawing.Size(209, 21)
        Me.cmbTipoCFEInformacionReferencia.TabIndex = 5
        '
        'lblSerieCFEInformacionReferencia
        '
        Me.lblSerieCFEInformacionReferencia.AutoSize = True
        Me.lblSerieCFEInformacionReferencia.Location = New System.Drawing.Point(422, 42)
        Me.lblSerieCFEInformacionReferencia.Name = "lblSerieCFEInformacionReferencia"
        Me.lblSerieCFEInformacionReferencia.Size = New System.Drawing.Size(31, 13)
        Me.lblSerieCFEInformacionReferencia.TabIndex = 213
        Me.lblSerieCFEInformacionReferencia.Text = "Serie"
        '
        'lblTipoCFEInformacionReferencia
        '
        Me.lblTipoCFEInformacionReferencia.AutoSize = True
        Me.lblTipoCFEInformacionReferencia.Location = New System.Drawing.Point(142, 44)
        Me.lblTipoCFEInformacionReferencia.Name = "lblTipoCFEInformacionReferencia"
        Me.lblTipoCFEInformacionReferencia.Size = New System.Drawing.Size(51, 13)
        Me.lblTipoCFEInformacionReferencia.TabIndex = 212
        Me.lblTipoCFEInformacionReferencia.Text = "Tipo CFE"
        '
        'GroupBox_Remito
        '
        Me.GroupBox_Remito.Controls.Add(Me.Panel1)
        Me.GroupBox_Remito.Controls.Add(Me.cmbIndicadorPropMercaderiaTransp)
        Me.GroupBox_Remito.Controls.Add(Me.Label38)
        Me.GroupBox_Remito.Controls.Add(Me.cmbIndicadorTipoTrasladoBienesEncabezado)
        Me.GroupBox_Remito.Controls.Add(Me.Label158)
        Me.GroupBox_Remito.Location = New System.Drawing.Point(3, 177)
        Me.GroupBox_Remito.Name = "GroupBox_Remito"
        Me.GroupBox_Remito.Size = New System.Drawing.Size(1072, 71)
        Me.GroupBox_Remito.TabIndex = 299
        Me.GroupBox_Remito.TabStop = False
        Me.GroupBox_Remito.Text = "Factura"
        Me.GroupBox_Remito.Visible = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.textBox_NroDocPropMercaderiaTransp)
        Me.Panel1.Controls.Add(Me.cmbTipoDocMercaderiaTransp)
        Me.Panel1.Controls.Add(Me.Label41)
        Me.Panel1.Controls.Add(Me.Label39)
        Me.Panel1.Controls.Add(Me.cmbCodPaisPropMercaderiaTransp)
        Me.Panel1.Controls.Add(Me.Label40)
        Me.Panel1.Controls.Add(Me.TextBox_NombreRznSocPropMercaderiaTransp)
        Me.Panel1.Location = New System.Drawing.Point(324, 11)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(741, 57)
        Me.Panel1.TabIndex = 66
        Me.Panel1.Visible = False
        '
        'textBox_NroDocPropMercaderiaTransp
        '
        Me.textBox_NroDocPropMercaderiaTransp.Location = New System.Drawing.Point(241, 3)
        Me.textBox_NroDocPropMercaderiaTransp.MaxLength = 12
        Me.textBox_NroDocPropMercaderiaTransp.Name = "textBox_NroDocPropMercaderiaTransp"
        Me.textBox_NroDocPropMercaderiaTransp.Size = New System.Drawing.Size(100, 20)
        Me.textBox_NroDocPropMercaderiaTransp.TabIndex = 26
        '
        'cmbTipoDocMercaderiaTransp
        '
        Me.cmbTipoDocMercaderiaTransp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTipoDocMercaderiaTransp.FormattingEnabled = True
        Me.cmbTipoDocMercaderiaTransp.Location = New System.Drawing.Point(61, 3)
        Me.cmbTipoDocMercaderiaTransp.Name = "cmbTipoDocMercaderiaTransp"
        Me.cmbTipoDocMercaderiaTransp.Size = New System.Drawing.Size(100, 21)
        Me.cmbTipoDocMercaderiaTransp.TabIndex = 25
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Location = New System.Drawing.Point(173, 8)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(62, 13)
        Me.Label41.TabIndex = 65
        Me.Label41.Text = "Documento"
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Location = New System.Drawing.Point(5, 8)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(52, 13)
        Me.Label39.TabIndex = 61
        Me.Label39.Text = "Tipo doc."
        '
        'cmbCodPaisPropMercaderiaTransp
        '
        Me.cmbCodPaisPropMercaderiaTransp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbCodPaisPropMercaderiaTransp.FormattingEnabled = True
        Me.cmbCodPaisPropMercaderiaTransp.Location = New System.Drawing.Point(61, 30)
        Me.cmbCodPaisPropMercaderiaTransp.Name = "cmbCodPaisPropMercaderiaTransp"
        Me.cmbCodPaisPropMercaderiaTransp.Size = New System.Drawing.Size(280, 21)
        Me.cmbCodPaisPropMercaderiaTransp.TabIndex = 27
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Location = New System.Drawing.Point(27, 35)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(27, 13)
        Me.Label40.TabIndex = 63
        Me.Label40.Text = "Pais"
        '
        'TextBox_NombreRznSocPropMercaderiaTransp
        '
        Me.TextBox_NombreRznSocPropMercaderiaTransp.Location = New System.Drawing.Point(347, 2)
        Me.TextBox_NombreRznSocPropMercaderiaTransp.MaxLength = 150
        Me.TextBox_NombreRznSocPropMercaderiaTransp.Multiline = True
        Me.TextBox_NombreRznSocPropMercaderiaTransp.Name = "TextBox_NombreRznSocPropMercaderiaTransp"
        Me.TextBox_NombreRznSocPropMercaderiaTransp.Size = New System.Drawing.Size(388, 52)
        Me.TextBox_NombreRznSocPropMercaderiaTransp.TabIndex = 28
        Me.TextBox_NombreRznSocPropMercaderiaTransp.Text = "Nombre o denominacion propietario mercaderia transportada..."
        '
        'cmbIndicadorPropMercaderiaTransp
        '
        Me.cmbIndicadorPropMercaderiaTransp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbIndicadorPropMercaderiaTransp.FormattingEnabled = True
        Me.cmbIndicadorPropMercaderiaTransp.Location = New System.Drawing.Point(199, 43)
        Me.cmbIndicadorPropMercaderiaTransp.Name = "cmbIndicadorPropMercaderiaTransp"
        Me.cmbIndicadorPropMercaderiaTransp.Size = New System.Drawing.Size(121, 21)
        Me.cmbIndicadorPropMercaderiaTransp.TabIndex = 24
        Me.cmbIndicadorPropMercaderiaTransp.Visible = False
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Location = New System.Drawing.Point(8, 46)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(191, 13)
        Me.Label38.TabIndex = 24
        Me.Label38.Text = "Indicador propiedad mercaderia transp."
        Me.Label38.Visible = False
        '
        'cmbIndicadorTipoTrasladoBienesEncabezado
        '
        Me.cmbIndicadorTipoTrasladoBienesEncabezado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbIndicadorTipoTrasladoBienesEncabezado.FormattingEnabled = True
        Me.cmbIndicadorTipoTrasladoBienesEncabezado.Location = New System.Drawing.Point(199, 15)
        Me.cmbIndicadorTipoTrasladoBienesEncabezado.Name = "cmbIndicadorTipoTrasladoBienesEncabezado"
        Me.cmbIndicadorTipoTrasladoBienesEncabezado.Size = New System.Drawing.Size(121, 21)
        Me.cmbIndicadorTipoTrasladoBienesEncabezado.TabIndex = 23
        '
        'Label158
        '
        Me.Label158.AutoSize = True
        Me.Label158.Location = New System.Drawing.Point(8, 18)
        Me.Label158.Name = "Label158"
        Me.Label158.Size = New System.Drawing.Size(175, 13)
        Me.Label158.TabIndex = 22
        Me.Label158.Text = "Indicador tipo de traslado de bienes"
        '
        'Configuracion
        '
        Me.Configuracion.Controls.Add(Me.GroupBox10)
        Me.Configuracion.Location = New System.Drawing.Point(4, 22)
        Me.Configuracion.Name = "Configuracion"
        Me.Configuracion.Padding = New System.Windows.Forms.Padding(3)
        Me.Configuracion.Size = New System.Drawing.Size(1085, 1030)
        Me.Configuracion.TabIndex = 1
        Me.Configuracion.Text = "Configuracion"
        Me.Configuracion.UseVisualStyleBackColor = True
        '
        'GroupBox10
        '
        Me.GroupBox10.Controls.Add(Me.Button1)
        Me.GroupBox10.Controls.Add(Me.Label35)
        Me.GroupBox10.Controls.Add(Me.GroupBox2)
        Me.GroupBox10.Controls.Add(Me.Chk_IVAalDia)
        Me.GroupBox10.Controls.Add(Me.Chk_emiteRemitos)
        Me.GroupBox10.Controls.Add(Me.Chk_usaExportacion)
        Me.GroupBox10.Controls.Add(Me.Chk_PreFactura)
        Me.GroupBox10.Controls.Add(Me.Chk_UsaRetPerc)
        Me.GroupBox10.Controls.Add(Me.Chk_UsaSubTotales)
        Me.GroupBox10.Controls.Add(Me.Label7)
        Me.GroupBox10.Controls.Add(Me.txtCfgNombreEmisor)
        Me.GroupBox10.Controls.Add(Me.Chk_Bloqueado)
        Me.GroupBox10.Controls.Add(Me.Label6)
        Me.GroupBox10.Controls.Add(Me.txtCfgTelefono2Emisor)
        Me.GroupBox10.Controls.Add(Me.Button_Sincronizar)
        Me.GroupBox10.Controls.Add(Me.chkConsumoFinalDefault)
        Me.GroupBox10.Controls.Add(Me.Label36)
        Me.GroupBox10.Controls.Add(Me.Label4)
        Me.GroupBox10.Controls.Add(Me.cmbCfgIndicadorFacturacion)
        Me.GroupBox10.Controls.Add(Me.Label211)
        Me.GroupBox10.Controls.Add(Me.txtNroSucursal)
        Me.GroupBox10.Controls.Add(Me.gbxImpresion)
        Me.GroupBox10.Controls.Add(Me.grpDatosServidor)
        Me.GroupBox10.Controls.Add(Me.BtnGuardarConfig)
        Me.GroupBox10.Controls.Add(Me.cmbTipoNegocio)
        Me.GroupBox10.Controls.Add(Me.cmbCfgTipoMonedaTransaccion)
        Me.GroupBox10.Controls.Add(Me.Label209)
        Me.GroupBox10.Controls.Add(Me.Label202)
        Me.GroupBox10.Controls.Add(Me.txtCfgVendedorNom)
        Me.GroupBox10.Controls.Add(Me.Label203)
        Me.GroupBox10.Controls.Add(Me.txtCfgVendedorNro)
        Me.GroupBox10.Controls.Add(Me.Label205)
        Me.GroupBox10.Controls.Add(Me.txtCfgValorUnidadIndexada)
        Me.GroupBox10.Controls.Add(Me.Label206)
        Me.GroupBox10.Controls.Add(Me.txtCfgTasaMinimaIVA)
        Me.GroupBox10.Controls.Add(Me.Label207)
        Me.GroupBox10.Controls.Add(Me.txtCfgTasaBasicaIVA)
        Me.GroupBox10.Controls.Add(Me.Label208)
        Me.GroupBox10.Controls.Add(Me.Label175)
        Me.GroupBox10.Controls.Add(Me.txtCfgTipoDeCambio)
        Me.GroupBox10.Controls.Add(Me.Label194)
        Me.GroupBox10.Controls.Add(Me.Label195)
        Me.GroupBox10.Controls.Add(Me.txtCfgCajeroNombre)
        Me.GroupBox10.Controls.Add(Me.Label196)
        Me.GroupBox10.Controls.Add(Me.txtCfgCajeroNro)
        Me.GroupBox10.Controls.Add(Me.txtCfgCajaNro)
        Me.GroupBox10.Controls.Add(Me.Label198)
        Me.GroupBox10.Controls.Add(Me.txtCfgNombreSucursalEmisor)
        Me.GroupBox10.Controls.Add(Me.Label199)
        Me.GroupBox10.Controls.Add(Me.txtCfgCodigoSucursalEmisor)
        Me.GroupBox10.Controls.Add(Me.Label200)
        Me.GroupBox10.Controls.Add(Me.txtCfgTelefonoEmisor)
        Me.GroupBox10.Controls.Add(Me.Label201)
        Me.GroupBox10.Controls.Add(Me.txtCfgCiudadEmisor)
        Me.GroupBox10.Controls.Add(Me.Label193)
        Me.GroupBox10.Controls.Add(Me.txtCfgDepartamentoEmisor)
        Me.GroupBox10.Controls.Add(Me.Label192)
        Me.GroupBox10.Controls.Add(Me.txtCfgDomicilioFiscalEmisor)
        Me.GroupBox10.Controls.Add(Me.Label191)
        Me.GroupBox10.Controls.Add(Me.txtCfgCorreoEmisor)
        Me.GroupBox10.Controls.Add(Me.Label190)
        Me.GroupBox10.Controls.Add(Me.txtCfgGiroNegocioEmisor)
        Me.GroupBox10.Controls.Add(Me.Label188)
        Me.GroupBox10.Controls.Add(Me.txtCfgRUTEmisor)
        Me.GroupBox10.Controls.Add(Me.Label184)
        Me.GroupBox10.Controls.Add(Me.txtCfgRazonSocialEmisor)
        Me.GroupBox10.Location = New System.Drawing.Point(8, 6)
        Me.GroupBox10.Name = "GroupBox10"
        Me.GroupBox10.Size = New System.Drawing.Size(1071, 667)
        Me.GroupBox10.TabIndex = 0
        Me.GroupBox10.TabStop = False
        Me.GroupBox10.Text = "Configuracion Factura Manual"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.GroupBox4)
        Me.GroupBox2.Controls.Add(Me.txtTermCod)
        Me.GroupBox2.Controls.Add(Me.txtEmpCod)
        Me.GroupBox2.Controls.Add(Me.Label32)
        Me.GroupBox2.Controls.Add(Me.Label34)
        Me.GroupBox2.Controls.Add(Me.GroupBox9)
        Me.GroupBox2.Location = New System.Drawing.Point(9, 348)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(1029, 128)
        Me.GroupBox2.TabIndex = 310
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Configuracion TransAct"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.chkModificarTipoCuenta)
        Me.GroupBox4.Controls.Add(Me.chkModificarDecretoLey)
        Me.GroupBox4.Controls.Add(Me.chkModificarPlan)
        Me.GroupBox4.Controls.Add(Me.chkModificarTarjeta)
        Me.GroupBox4.Controls.Add(Me.chkModificarFactura)
        Me.GroupBox4.Controls.Add(Me.chkModificarCuotas)
        Me.GroupBox4.Controls.Add(Me.chkModificarMontos)
        Me.GroupBox4.Controls.Add(Me.chkModificarMoneda)
        Me.GroupBox4.Location = New System.Drawing.Point(300, 19)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(448, 92)
        Me.GroupBox4.TabIndex = 314
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Comportamiento:"
        '
        'chkModificarTipoCuenta
        '
        Me.chkModificarTipoCuenta.AutoSize = True
        Me.chkModificarTipoCuenta.Location = New System.Drawing.Point(321, 57)
        Me.chkModificarTipoCuenta.Name = "chkModificarTipoCuenta"
        Me.chkModificarTipoCuenta.Size = New System.Drawing.Size(127, 17)
        Me.chkModificarTipoCuenta.TabIndex = 7
        Me.chkModificarTipoCuenta.Text = "Modificar TipoCuenta"
        Me.chkModificarTipoCuenta.UseVisualStyleBackColor = True
        '
        'chkModificarDecretoLey
        '
        Me.chkModificarDecretoLey.AutoSize = True
        Me.chkModificarDecretoLey.Location = New System.Drawing.Point(321, 28)
        Me.chkModificarDecretoLey.Name = "chkModificarDecretoLey"
        Me.chkModificarDecretoLey.Size = New System.Drawing.Size(127, 17)
        Me.chkModificarDecretoLey.TabIndex = 6
        Me.chkModificarDecretoLey.Text = "Modificar DecretoLey"
        Me.chkModificarDecretoLey.UseVisualStyleBackColor = True
        '
        'chkModificarPlan
        '
        Me.chkModificarPlan.AutoSize = True
        Me.chkModificarPlan.Location = New System.Drawing.Point(219, 57)
        Me.chkModificarPlan.Name = "chkModificarPlan"
        Me.chkModificarPlan.Size = New System.Drawing.Size(93, 17)
        Me.chkModificarPlan.TabIndex = 5
        Me.chkModificarPlan.Text = "Modificar Plan"
        Me.chkModificarPlan.UseVisualStyleBackColor = True
        '
        'chkModificarTarjeta
        '
        Me.chkModificarTarjeta.AutoSize = True
        Me.chkModificarTarjeta.Location = New System.Drawing.Point(219, 28)
        Me.chkModificarTarjeta.Name = "chkModificarTarjeta"
        Me.chkModificarTarjeta.Size = New System.Drawing.Size(105, 17)
        Me.chkModificarTarjeta.TabIndex = 4
        Me.chkModificarTarjeta.Text = "Modificar Tarjeta"
        Me.chkModificarTarjeta.UseVisualStyleBackColor = True
        '
        'chkModificarFactura
        '
        Me.chkModificarFactura.AutoSize = True
        Me.chkModificarFactura.Location = New System.Drawing.Point(114, 56)
        Me.chkModificarFactura.Name = "chkModificarFactura"
        Me.chkModificarFactura.Size = New System.Drawing.Size(108, 17)
        Me.chkModificarFactura.TabIndex = 3
        Me.chkModificarFactura.Text = "Modificar Factura"
        Me.chkModificarFactura.UseVisualStyleBackColor = True
        '
        'chkModificarCuotas
        '
        Me.chkModificarCuotas.AutoSize = True
        Me.chkModificarCuotas.Location = New System.Drawing.Point(114, 27)
        Me.chkModificarCuotas.Name = "chkModificarCuotas"
        Me.chkModificarCuotas.Size = New System.Drawing.Size(105, 17)
        Me.chkModificarCuotas.TabIndex = 2
        Me.chkModificarCuotas.Text = "Modificar Cuotas"
        Me.chkModificarCuotas.UseVisualStyleBackColor = True
        '
        'chkModificarMontos
        '
        Me.chkModificarMontos.AutoSize = True
        Me.chkModificarMontos.Location = New System.Drawing.Point(6, 56)
        Me.chkModificarMontos.Name = "chkModificarMontos"
        Me.chkModificarMontos.Size = New System.Drawing.Size(107, 17)
        Me.chkModificarMontos.TabIndex = 1
        Me.chkModificarMontos.Text = "Modificar Montos"
        Me.chkModificarMontos.UseVisualStyleBackColor = True
        '
        'chkModificarMoneda
        '
        Me.chkModificarMoneda.AutoSize = True
        Me.chkModificarMoneda.Location = New System.Drawing.Point(6, 27)
        Me.chkModificarMoneda.Name = "chkModificarMoneda"
        Me.chkModificarMoneda.Size = New System.Drawing.Size(111, 17)
        Me.chkModificarMoneda.TabIndex = 0
        Me.chkModificarMoneda.Text = "Modificar Moneda"
        Me.chkModificarMoneda.UseVisualStyleBackColor = True
        '
        'txtTermCod
        '
        Me.txtTermCod.Location = New System.Drawing.Point(99, 55)
        Me.txtTermCod.Name = "txtTermCod"
        Me.txtTermCod.Size = New System.Drawing.Size(154, 20)
        Me.txtTermCod.TabIndex = 312
        Me.txtTermCod.Text = "123456"
        '
        'txtEmpCod
        '
        Me.txtEmpCod.Location = New System.Drawing.Point(100, 28)
        Me.txtEmpCod.Name = "txtEmpCod"
        Me.txtEmpCod.Size = New System.Drawing.Size(154, 20)
        Me.txtEmpCod.TabIndex = 311
        Me.txtEmpCod.Text = "EMPMIA"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(11, 31)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(83, 13)
        Me.Label32.TabIndex = 79
        Me.Label32.Text = "Código empresa"
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(14, 56)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(79, 13)
        Me.Label34.TabIndex = 77
        Me.Label34.Text = "Código terminal"
        '
        'GroupBox9
        '
        Me.GroupBox9.Controls.Add(Me.chkCierreCentralizado)
        Me.GroupBox9.Controls.Add(Me.chkConsultarUltimoCierre)
        Me.GroupBox9.Controls.Add(Me.Label31)
        Me.GroupBox9.Controls.Add(Me.cmbIdProcesadorTransAct)
        Me.GroupBox9.Controls.Add(Me.btnCerrarLote)
        Me.GroupBox9.Location = New System.Drawing.Point(769, 19)
        Me.GroupBox9.Name = "GroupBox9"
        Me.GroupBox9.Size = New System.Drawing.Size(251, 92)
        Me.GroupBox9.TabIndex = 316
        Me.GroupBox9.TabStop = False
        Me.GroupBox9.Text = "Cierre de lote"
        '
        'chkCierreCentralizado
        '
        Me.chkCierreCentralizado.AutoSize = True
        Me.chkCierreCentralizado.Location = New System.Drawing.Point(156, 33)
        Me.chkCierreCentralizado.Name = "chkCierreCentralizado"
        Me.chkCierreCentralizado.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.chkCierreCentralizado.Size = New System.Drawing.Size(84, 17)
        Me.chkCierreCentralizado.TabIndex = 317
        Me.chkCierreCentralizado.Text = "Centralizado"
        Me.chkCierreCentralizado.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.chkCierreCentralizado.UseVisualStyleBackColor = True
        '
        'chkConsultarUltimoCierre
        '
        Me.chkConsultarUltimoCierre.AutoSize = True
        Me.chkConsultarUltimoCierre.Location = New System.Drawing.Point(140, 11)
        Me.chkConsultarUltimoCierre.Name = "chkConsultarUltimoCierre"
        Me.chkConsultarUltimoCierre.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.chkConsultarUltimoCierre.Size = New System.Drawing.Size(100, 17)
        Me.chkConsultarUltimoCierre.TabIndex = 316
        Me.chkConsultarUltimoCierre.Text = "Consultar ultimo"
        Me.chkConsultarUltimoCierre.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.chkConsultarUltimoCierre.UseVisualStyleBackColor = True
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(6, 27)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(75, 13)
        Me.Label31.TabIndex = 315
        Me.Label31.Text = "Id procesador:"
        '
        'cmbIdProcesadorTransAct
        '
        Me.cmbIdProcesadorTransAct.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbIdProcesadorTransAct.FormattingEnabled = True
        Me.cmbIdProcesadorTransAct.Location = New System.Drawing.Point(6, 62)
        Me.cmbIdProcesadorTransAct.Name = "cmbIdProcesadorTransAct"
        Me.cmbIdProcesadorTransAct.Size = New System.Drawing.Size(187, 21)
        Me.cmbIdProcesadorTransAct.TabIndex = 314
        '
        'btnCerrarLote
        '
        Me.btnCerrarLote.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCerrarLote.Location = New System.Drawing.Point(199, 60)
        Me.btnCerrarLote.Name = "btnCerrarLote"
        Me.btnCerrarLote.Size = New System.Drawing.Size(46, 24)
        Me.btnCerrarLote.TabIndex = 313
        Me.btnCerrarLote.Text = "Cerrar"
        Me.btnCerrarLote.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCerrarLote.UseVisualStyleBackColor = True
        '
        'Chk_IVAalDia
        '
        Me.Chk_IVAalDia.AutoSize = True
        Me.Chk_IVAalDia.Checked = True
        Me.Chk_IVAalDia.CheckState = System.Windows.Forms.CheckState.Checked
        Me.Chk_IVAalDia.Location = New System.Drawing.Point(342, 642)
        Me.Chk_IVAalDia.Name = "Chk_IVAalDia"
        Me.Chk_IVAalDia.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Chk_IVAalDia.Size = New System.Drawing.Size(71, 17)
        Me.Chk_IVAalDia.TabIndex = 308
        Me.Chk_IVAalDia.Text = "IVA al dia"
        Me.Chk_IVAalDia.UseVisualStyleBackColor = True
        '
        'Chk_emiteRemitos
        '
        Me.Chk_emiteRemitos.AutoSize = True
        Me.Chk_emiteRemitos.Location = New System.Drawing.Point(313, 617)
        Me.Chk_emiteRemitos.Name = "Chk_emiteRemitos"
        Me.Chk_emiteRemitos.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Chk_emiteRemitos.Size = New System.Drawing.Size(101, 17)
        Me.Chk_emiteRemitos.TabIndex = 307
        Me.Chk_emiteRemitos.Text = "Emite extra CFE"
        Me.Chk_emiteRemitos.UseVisualStyleBackColor = True
        '
        'Chk_usaExportacion
        '
        Me.Chk_usaExportacion.AutoSize = True
        Me.Chk_usaExportacion.Location = New System.Drawing.Point(311, 594)
        Me.Chk_usaExportacion.Name = "Chk_usaExportacion"
        Me.Chk_usaExportacion.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Chk_usaExportacion.Size = New System.Drawing.Size(103, 17)
        Me.Chk_usaExportacion.TabIndex = 92
        Me.Chk_usaExportacion.Text = "Usa exportacion"
        Me.Chk_usaExportacion.UseVisualStyleBackColor = True
        '
        'Chk_PreFactura
        '
        Me.Chk_PreFactura.AutoSize = True
        Me.Chk_PreFactura.Location = New System.Drawing.Point(219, 640)
        Me.Chk_PreFactura.Name = "Chk_PreFactura"
        Me.Chk_PreFactura.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Chk_PreFactura.Size = New System.Drawing.Size(75, 17)
        Me.Chk_PreFactura.TabIndex = 91
        Me.Chk_PreFactura.Text = "Prefactura"
        Me.Chk_PreFactura.UseVisualStyleBackColor = True
        '
        'Chk_UsaRetPerc
        '
        Me.Chk_UsaRetPerc.AutoSize = True
        Me.Chk_UsaRetPerc.Location = New System.Drawing.Point(122, 617)
        Me.Chk_UsaRetPerc.Name = "Chk_UsaRetPerc"
        Me.Chk_UsaRetPerc.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Chk_UsaRetPerc.Size = New System.Drawing.Size(172, 17)
        Me.Chk_UsaRetPerc.TabIndex = 90
        Me.Chk_UsaRetPerc.Text = "Usa retenciones/percepciones"
        Me.Chk_UsaRetPerc.UseVisualStyleBackColor = True
        '
        'Chk_UsaSubTotales
        '
        Me.Chk_UsaSubTotales.AutoSize = True
        Me.Chk_UsaSubTotales.Location = New System.Drawing.Point(139, 594)
        Me.Chk_UsaSubTotales.Name = "Chk_UsaSubTotales"
        Me.Chk_UsaSubTotales.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Chk_UsaSubTotales.Size = New System.Drawing.Size(155, 17)
        Me.Chk_UsaSubTotales.TabIndex = 89
        Me.Chk_UsaSubTotales.Text = "Usa subtotales informativos"
        Me.Chk_UsaSubTotales.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(68, 63)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(77, 13)
        Me.Label7.TabIndex = 88
        Me.Label7.Text = "Nombre emisor"
        '
        'txtCfgNombreEmisor
        '
        Me.txtCfgNombreEmisor.Location = New System.Drawing.Point(157, 60)
        Me.txtCfgNombreEmisor.Name = "txtCfgNombreEmisor"
        Me.txtCfgNombreEmisor.Size = New System.Drawing.Size(154, 20)
        Me.txtCfgNombreEmisor.TabIndex = 87
        '
        'Chk_Bloqueado
        '
        Me.Chk_Bloqueado.AutoSize = True
        Me.Chk_Bloqueado.Location = New System.Drawing.Point(921, 218)
        Me.Chk_Bloqueado.Name = "Chk_Bloqueado"
        Me.Chk_Bloqueado.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Chk_Bloqueado.Size = New System.Drawing.Size(77, 17)
        Me.Chk_Bloqueado.TabIndex = 86
        Me.Chk_Bloqueado.Text = "Bloqueado"
        Me.Chk_Bloqueado.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.Chk_Bloqueado.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(385, 63)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(91, 13)
        Me.Label6.TabIndex = 85
        Me.Label6.Text = "Telefono 2 emisor"
        '
        'txtCfgTelefono2Emisor
        '
        Me.txtCfgTelefono2Emisor.Location = New System.Drawing.Point(489, 60)
        Me.txtCfgTelefono2Emisor.Name = "txtCfgTelefono2Emisor"
        Me.txtCfgTelefono2Emisor.Size = New System.Drawing.Size(162, 20)
        Me.txtCfgTelefono2Emisor.TabIndex = 9
        '
        'Button_Sincronizar
        '
        Me.Button_Sincronizar.BackColor = System.Drawing.Color.DarkGreen
        Me.Button_Sincronizar.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button_Sincronizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button_Sincronizar.ForeColor = System.Drawing.SystemColors.Control
        Me.Button_Sincronizar.Location = New System.Drawing.Point(63, 588)
        Me.Button_Sincronizar.Name = "Button_Sincronizar"
        Me.Button_Sincronizar.Size = New System.Drawing.Size(19, 23)
        Me.Button_Sincronizar.TabIndex = 309
        Me.Button_Sincronizar.UseVisualStyleBackColor = False
        '
        'chkConsumoFinalDefault
        '
        Me.chkConsumoFinalDefault.AutoSize = True
        Me.chkConsumoFinalDefault.Location = New System.Drawing.Point(757, 245)
        Me.chkConsumoFinalDefault.Name = "chkConsumoFinalDefault"
        Me.chkConsumoFinalDefault.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.chkConsumoFinalDefault.Size = New System.Drawing.Size(92, 17)
        Me.chkConsumoFinalDefault.TabIndex = 24
        Me.chkConsumoFinalDefault.Text = "Consumo final"
        Me.chkConsumoFinalDefault.UseVisualStyleBackColor = True
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Location = New System.Drawing.Point(6, 594)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(59, 13)
        Me.Label36.TabIndex = 308
        Me.Label36.Text = "Sincronizar"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(719, 218)
        Me.Label4.Name = "Label4"
        Me.Label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label4.Size = New System.Drawing.Size(107, 13)
        Me.Label4.TabIndex = 82
        Me.Label4.Text = "Impuesto por defecto"
        '
        'cmbCfgIndicadorFacturacion
        '
        Me.cmbCfgIndicadorFacturacion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbCfgIndicadorFacturacion.FormattingEnabled = True
        Me.cmbCfgIndicadorFacturacion.Location = New System.Drawing.Point(836, 217)
        Me.cmbCfgIndicadorFacturacion.Name = "cmbCfgIndicadorFacturacion"
        Me.cmbCfgIndicadorFacturacion.Size = New System.Drawing.Size(55, 21)
        Me.cmbCfgIndicadorFacturacion.TabIndex = 23
        '
        'Label211
        '
        Me.Label211.AutoSize = True
        Me.Label211.Location = New System.Drawing.Point(374, 140)
        Me.Label211.Name = "Label211"
        Me.Label211.Size = New System.Drawing.Size(102, 13)
        Me.Label211.TabIndex = 80
        Me.Label211.Text = "Nro. sucursal emisor"
        '
        'txtNroSucursal
        '
        Me.txtNroSucursal.Location = New System.Drawing.Point(489, 138)
        Me.txtNroSucursal.Name = "txtNroSucursal"
        Me.txtNroSucursal.Size = New System.Drawing.Size(52, 20)
        Me.txtNroSucursal.TabIndex = 12
        '
        'gbxImpresion
        '
        Me.gbxImpresion.Controls.Add(Me.GroupBox5)
        Me.gbxImpresion.Controls.Add(Me.chkAbrirCajon)
        Me.gbxImpresion.Controls.Add(Me.txtCantCopias)
        Me.gbxImpresion.Controls.Add(Me.lblCopias)
        Me.gbxImpresion.Controls.Add(Me.btnImprimirPrueba)
        Me.gbxImpresion.Controls.Add(Me.chkImprimeApi)
        Me.gbxImpresion.Controls.Add(Me.cmbImpresora)
        Me.gbxImpresion.Controls.Add(Me.cmbTipoImpresora)
        Me.gbxImpresion.Controls.Add(Me.lblTipoImpresora)
        Me.gbxImpresion.Controls.Add(Me.lblNomImpresora)
        Me.gbxImpresion.Location = New System.Drawing.Point(9, 478)
        Me.gbxImpresion.Name = "gbxImpresion"
        Me.gbxImpresion.Size = New System.Drawing.Size(1029, 100)
        Me.gbxImpresion.TabIndex = 77
        Me.gbxImpresion.TabStop = False
        Me.gbxImpresion.Text = "Impresion"
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.rdButtonImprimeVoucherAdenda)
        Me.GroupBox5.Controls.Add(Me.rdButtonImprimeVoucherTransAct)
        Me.GroupBox5.Location = New System.Drawing.Point(854, 8)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(169, 44)
        Me.GroupBox5.TabIndex = 1
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Imprimir voucher"
        '
        'rdButtonImprimeVoucherAdenda
        '
        Me.rdButtonImprimeVoucherAdenda.AutoSize = True
        Me.rdButtonImprimeVoucherAdenda.Checked = True
        Me.rdButtonImprimeVoucherAdenda.Cursor = System.Windows.Forms.Cursors.Default
        Me.rdButtonImprimeVoucherAdenda.Location = New System.Drawing.Point(6, 18)
        Me.rdButtonImprimeVoucherAdenda.Name = "rdButtonImprimeVoucherAdenda"
        Me.rdButtonImprimeVoucherAdenda.Size = New System.Drawing.Size(77, 17)
        Me.rdButtonImprimeVoucherAdenda.TabIndex = 90
        Me.rdButtonImprimeVoucherAdenda.TabStop = True
        Me.rdButtonImprimeVoucherAdenda.Text = "En adenda"
        Me.rdButtonImprimeVoucherAdenda.UseVisualStyleBackColor = True
        '
        'rdButtonImprimeVoucherTransAct
        '
        Me.rdButtonImprimeVoucherTransAct.AutoSize = True
        Me.rdButtonImprimeVoucherTransAct.Location = New System.Drawing.Point(93, 18)
        Me.rdButtonImprimeVoucherTransAct.Name = "rdButtonImprimeVoucherTransAct"
        Me.rdButtonImprimeVoucherTransAct.Size = New System.Drawing.Size(68, 17)
        Me.rdButtonImprimeVoucherTransAct.TabIndex = 91
        Me.rdButtonImprimeVoucherTransAct.Text = "TransAct"
        Me.rdButtonImprimeVoucherTransAct.UseVisualStyleBackColor = True
        '
        'chkAbrirCajon
        '
        Me.chkAbrirCajon.AutoSize = True
        Me.chkAbrirCajon.Location = New System.Drawing.Point(774, 64)
        Me.chkAbrirCajon.Name = "chkAbrirCajon"
        Me.chkAbrirCajon.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.chkAbrirCajon.Size = New System.Drawing.Size(76, 17)
        Me.chkAbrirCajon.TabIndex = 89
        Me.chkAbrirCajon.Text = "Abrir cajon"
        Me.chkAbrirCajon.UseVisualStyleBackColor = True
        '
        'txtCantCopias
        '
        Me.txtCantCopias.BackColor = System.Drawing.SystemColors.Window
        Me.txtCantCopias.Location = New System.Drawing.Point(732, 62)
        Me.txtCantCopias.Maximum = New Decimal(New Integer() {4, 0, 0, 0})
        Me.txtCantCopias.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.txtCantCopias.Name = "txtCantCopias"
        Me.txtCantCopias.ReadOnly = True
        Me.txtCantCopias.Size = New System.Drawing.Size(36, 20)
        Me.txtCantCopias.TabIndex = 82
        Me.txtCantCopias.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'lblCopias
        '
        Me.lblCopias.AutoSize = True
        Me.lblCopias.Location = New System.Drawing.Point(689, 65)
        Me.lblCopias.Name = "lblCopias"
        Me.lblCopias.Size = New System.Drawing.Size(39, 13)
        Me.lblCopias.TabIndex = 81
        Me.lblCopias.Text = "Copias"
        '
        'btnImprimirPrueba
        '
        Me.btnImprimirPrueba.Location = New System.Drawing.Point(871, 61)
        Me.btnImprimirPrueba.Name = "btnImprimirPrueba"
        Me.btnImprimirPrueba.Size = New System.Drawing.Size(118, 23)
        Me.btnImprimirPrueba.TabIndex = 2
        Me.btnImprimirPrueba.Text = "Imprimir prueba"
        Me.btnImprimirPrueba.UseVisualStyleBackColor = True
        '
        'chkImprimeApi
        '
        Me.chkImprimeApi.AutoSize = True
        Me.chkImprimeApi.Location = New System.Drawing.Point(17, 30)
        Me.chkImprimeApi.Name = "chkImprimeApi"
        Me.chkImprimeApi.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.chkImprimeApi.Size = New System.Drawing.Size(91, 17)
        Me.chkImprimeApi.TabIndex = 0
        Me.chkImprimeApi.Text = "Imprime ticket"
        Me.chkImprimeApi.UseVisualStyleBackColor = True
        '
        'cmbImpresora
        '
        Me.cmbImpresora.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbImpresora.Enabled = False
        Me.cmbImpresora.FormattingEnabled = True
        Me.cmbImpresora.Location = New System.Drawing.Point(387, 62)
        Me.cmbImpresora.Name = "cmbImpresora"
        Me.cmbImpresora.Size = New System.Drawing.Size(285, 21)
        Me.cmbImpresora.TabIndex = 1
        '
        'cmbTipoImpresora
        '
        Me.cmbTipoImpresora.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTipoImpresora.Enabled = False
        Me.cmbTipoImpresora.FormattingEnabled = True
        Me.cmbTipoImpresora.Location = New System.Drawing.Point(100, 61)
        Me.cmbTipoImpresora.Name = "cmbTipoImpresora"
        Me.cmbTipoImpresora.Size = New System.Drawing.Size(171, 21)
        Me.cmbTipoImpresora.TabIndex = 0
        '
        'lblTipoImpresora
        '
        Me.lblTipoImpresora.AutoSize = True
        Me.lblTipoImpresora.Location = New System.Drawing.Point(17, 64)
        Me.lblTipoImpresora.Name = "lblTipoImpresora"
        Me.lblTipoImpresora.Size = New System.Drawing.Size(76, 13)
        Me.lblTipoImpresora.TabIndex = 79
        Me.lblTipoImpresora.Text = "Tipo impresora"
        '
        'lblNomImpresora
        '
        Me.lblNomImpresora.AutoSize = True
        Me.lblNomImpresora.Location = New System.Drawing.Point(285, 65)
        Me.lblNomImpresora.Name = "lblNomImpresora"
        Me.lblNomImpresora.Size = New System.Drawing.Size(92, 13)
        Me.lblNomImpresora.TabIndex = 77
        Me.lblNomImpresora.Text = "Nombre impresora"
        '
        'grpDatosServidor
        '
        Me.grpDatosServidor.Controls.Add(Me.Button3)
        Me.grpDatosServidor.Controls.Add(Me.txturlServidorGateway)
        Me.grpDatosServidor.Controls.Add(Me.Label1)
        Me.grpDatosServidor.Controls.Add(Me.TextBox_CarpetaOperacion)
        Me.grpDatosServidor.Controls.Add(Me.Label3)
        Me.grpDatosServidor.Controls.Add(Me.Label33)
        Me.grpDatosServidor.Controls.Add(Me.txtSegundosTimeout)
        Me.grpDatosServidor.Location = New System.Drawing.Point(23, 270)
        Me.grpDatosServidor.Name = "grpDatosServidor"
        Me.grpDatosServidor.Size = New System.Drawing.Size(1029, 63)
        Me.grpDatosServidor.TabIndex = 78
        Me.grpDatosServidor.TabStop = False
        Me.grpDatosServidor.Text = "Datos Servidor"
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(990, 24)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(32, 23)
        Me.Button3.TabIndex = 307
        Me.Button3.Text = "..."
        Me.Button3.UseVisualStyleBackColor = True
        '
        'txturlServidorGateway
        '
        Me.txturlServidorGateway.Location = New System.Drawing.Point(104, 27)
        Me.txturlServidorGateway.Name = "txturlServidorGateway"
        Me.txturlServidorGateway.Size = New System.Drawing.Size(414, 20)
        Me.txturlServidorGateway.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(36, 30)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(60, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Url servidor"
        '
        'TextBox_CarpetaOperacion
        '
        Me.TextBox_CarpetaOperacion.Location = New System.Drawing.Point(808, 26)
        Me.TextBox_CarpetaOperacion.Name = "TextBox_CarpetaOperacion"
        Me.TextBox_CarpetaOperacion.Size = New System.Drawing.Size(176, 20)
        Me.TextBox_CarpetaOperacion.TabIndex = 305
        Me.TextBox_CarpetaOperacion.Text = "C:\\"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(538, 30)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(92, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Segundos timeout"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(708, 30)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(94, 13)
        Me.Label33.TabIndex = 3
        Me.Label33.Text = "Carpeta operacion"
        '
        'txtSegundosTimeout
        '
        Me.txtSegundosTimeout.Location = New System.Drawing.Point(635, 27)
        Me.txtSegundosTimeout.Name = "txtSegundosTimeout"
        Me.txtSegundosTimeout.Size = New System.Drawing.Size(50, 20)
        Me.txtSegundosTimeout.TabIndex = 1
        Me.txtSegundosTimeout.Text = "40"
        '
        'BtnGuardarConfig
        '
        Me.BtnGuardarConfig.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BtnGuardarConfig.Location = New System.Drawing.Point(968, 627)
        Me.BtnGuardarConfig.Name = "BtnGuardarConfig"
        Me.BtnGuardarConfig.Size = New System.Drawing.Size(97, 35)
        Me.BtnGuardarConfig.TabIndex = 25
        Me.BtnGuardarConfig.Text = "Guardar config."
        Me.BtnGuardarConfig.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BtnGuardarConfig.UseVisualStyleBackColor = True
        '
        'cmbTipoNegocio
        '
        Me.cmbTipoNegocio.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTipoNegocio.FormattingEnabled = True
        Me.cmbTipoNegocio.Items.AddRange(New Object() {"Genérico Plaza", "Tienda Plaza", "Tienda Free Shop"})
        Me.cmbTipoNegocio.Location = New System.Drawing.Point(157, 242)
        Me.cmbTipoNegocio.Name = "cmbTipoNegocio"
        Me.cmbTipoNegocio.Size = New System.Drawing.Size(154, 21)
        Me.cmbTipoNegocio.TabIndex = 7
        '
        'cmbCfgTipoMonedaTransaccion
        '
        Me.cmbCfgTipoMonedaTransaccion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbCfgTipoMonedaTransaccion.FormattingEnabled = True
        Me.cmbCfgTipoMonedaTransaccion.Items.AddRange(New Object() {"Pesos", "Dolares"})
        Me.cmbCfgTipoMonedaTransaccion.Location = New System.Drawing.Point(836, 190)
        Me.cmbCfgTipoMonedaTransaccion.Name = "cmbCfgTipoMonedaTransaccion"
        Me.cmbCfgTipoMonedaTransaccion.Size = New System.Drawing.Size(162, 21)
        Me.cmbCfgTipoMonedaTransaccion.TabIndex = 22
        '
        'Label209
        '
        Me.Label209.AutoSize = True
        Me.Label209.Location = New System.Drawing.Point(430, 167)
        Me.Label209.Name = "Label209"
        Me.Label209.Size = New System.Drawing.Size(46, 13)
        Me.Label209.TabIndex = 73
        Me.Label209.Text = "Caja nro"
        '
        'Label202
        '
        Me.Label202.AutoSize = True
        Me.Label202.Location = New System.Drawing.Point(750, 167)
        Me.Label202.Name = "Label202"
        Me.Label202.Size = New System.Drawing.Size(79, 13)
        Me.Label202.TabIndex = 71
        Me.Label202.Text = "Vendedor nom."
        '
        'txtCfgVendedorNom
        '
        Me.txtCfgVendedorNom.Location = New System.Drawing.Point(836, 164)
        Me.txtCfgVendedorNom.Name = "txtCfgVendedorNom"
        Me.txtCfgVendedorNom.Size = New System.Drawing.Size(162, 20)
        Me.txtCfgVendedorNom.TabIndex = 21
        '
        'Label203
        '
        Me.Label203.AutoSize = True
        Me.Label203.Location = New System.Drawing.Point(754, 141)
        Me.Label203.Name = "Label203"
        Me.Label203.Size = New System.Drawing.Size(74, 13)
        Me.Label203.TabIndex = 69
        Me.Label203.Text = "Vendedor nro."
        '
        'txtCfgVendedorNro
        '
        Me.txtCfgVendedorNro.Location = New System.Drawing.Point(836, 138)
        Me.txtCfgVendedorNro.Name = "txtCfgVendedorNro"
        Me.txtCfgVendedorNro.Size = New System.Drawing.Size(51, 20)
        Me.txtCfgVendedorNro.TabIndex = 20
        '
        'Label205
        '
        Me.Label205.AutoSize = True
        Me.Label205.Location = New System.Drawing.Point(716, 115)
        Me.Label205.Name = "Label205"
        Me.Label205.Size = New System.Drawing.Size(112, 13)
        Me.Label205.TabIndex = 65
        Me.Label205.Text = "Valor unidad indexada"
        '
        'txtCfgValorUnidadIndexada
        '
        Me.txtCfgValorUnidadIndexada.Location = New System.Drawing.Point(836, 112)
        Me.txtCfgValorUnidadIndexada.Name = "txtCfgValorUnidadIndexada"
        Me.txtCfgValorUnidadIndexada.Size = New System.Drawing.Size(51, 20)
        Me.txtCfgValorUnidadIndexada.TabIndex = 19
        '
        'Label206
        '
        Me.Label206.AutoSize = True
        Me.Label206.Location = New System.Drawing.Point(742, 89)
        Me.Label206.Name = "Label206"
        Me.Label206.Size = New System.Drawing.Size(86, 13)
        Me.Label206.TabIndex = 63
        Me.Label206.Text = "Tasa minima IVA"
        '
        'txtCfgTasaMinimaIVA
        '
        Me.txtCfgTasaMinimaIVA.Location = New System.Drawing.Point(836, 86)
        Me.txtCfgTasaMinimaIVA.Name = "txtCfgTasaMinimaIVA"
        Me.txtCfgTasaMinimaIVA.Size = New System.Drawing.Size(51, 20)
        Me.txtCfgTasaMinimaIVA.TabIndex = 18
        '
        'Label207
        '
        Me.Label207.AutoSize = True
        Me.Label207.Location = New System.Drawing.Point(744, 63)
        Me.Label207.Name = "Label207"
        Me.Label207.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label207.Size = New System.Drawing.Size(85, 13)
        Me.Label207.TabIndex = 61
        Me.Label207.Text = "Tasa basica IVA"
        '
        'txtCfgTasaBasicaIVA
        '
        Me.txtCfgTasaBasicaIVA.Location = New System.Drawing.Point(836, 60)
        Me.txtCfgTasaBasicaIVA.Name = "txtCfgTasaBasicaIVA"
        Me.txtCfgTasaBasicaIVA.Size = New System.Drawing.Size(51, 20)
        Me.txtCfgTasaBasicaIVA.TabIndex = 17
        '
        'Label208
        '
        Me.Label208.AutoSize = True
        Me.Label208.Location = New System.Drawing.Point(749, 37)
        Me.Label208.Name = "Label208"
        Me.Label208.Size = New System.Drawing.Size(80, 13)
        Me.Label208.TabIndex = 59
        Me.Label208.Text = "Tipo de cambio"
        '
        'Label175
        '
        Me.Label175.AutoSize = True
        Me.Label175.Location = New System.Drawing.Point(46, 246)
        Me.Label175.Name = "Label175"
        Me.Label175.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label175.Size = New System.Drawing.Size(102, 13)
        Me.Label175.TabIndex = 57
        Me.Label175.Text = "Tipo negocio emisor"
        '
        'txtCfgTipoDeCambio
        '
        Me.txtCfgTipoDeCambio.Location = New System.Drawing.Point(836, 34)
        Me.txtCfgTipoDeCambio.Name = "txtCfgTipoDeCambio"
        Me.txtCfgTipoDeCambio.Size = New System.Drawing.Size(51, 20)
        Me.txtCfgTipoDeCambio.TabIndex = 16
        '
        'Label194
        '
        Me.Label194.AutoSize = True
        Me.Label194.Location = New System.Drawing.Point(704, 193)
        Me.Label194.Name = "Label194"
        Me.Label194.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label194.Size = New System.Drawing.Size(127, 13)
        Me.Label194.TabIndex = 57
        Me.Label194.Text = "Tipo moneda transaccion"
        '
        'Label195
        '
        Me.Label195.AutoSize = True
        Me.Label195.Location = New System.Drawing.Point(401, 218)
        Me.Label195.Name = "Label195"
        Me.Label195.Size = New System.Drawing.Size(75, 13)
        Me.Label195.TabIndex = 55
        Me.Label195.Text = "Cajero nombre"
        '
        'txtCfgCajeroNombre
        '
        Me.txtCfgCajeroNombre.Location = New System.Drawing.Point(489, 216)
        Me.txtCfgCajeroNombre.Name = "txtCfgCajeroNombre"
        Me.txtCfgCajeroNombre.Size = New System.Drawing.Size(162, 20)
        Me.txtCfgCajeroNombre.TabIndex = 15
        '
        'Label196
        '
        Me.Label196.AutoSize = True
        Me.Label196.Location = New System.Drawing.Point(421, 193)
        Me.Label196.Name = "Label196"
        Me.Label196.Size = New System.Drawing.Size(55, 13)
        Me.Label196.TabIndex = 53
        Me.Label196.Text = "Cajero nro"
        '
        'txtCfgCajeroNro
        '
        Me.txtCfgCajeroNro.Location = New System.Drawing.Point(489, 190)
        Me.txtCfgCajeroNro.Name = "txtCfgCajeroNro"
        Me.txtCfgCajeroNro.Size = New System.Drawing.Size(52, 20)
        Me.txtCfgCajeroNro.TabIndex = 14
        '
        'txtCfgCajaNro
        '
        Me.txtCfgCajaNro.Location = New System.Drawing.Point(489, 164)
        Me.txtCfgCajaNro.Name = "txtCfgCajaNro"
        Me.txtCfgCajaNro.Size = New System.Drawing.Size(52, 20)
        Me.txtCfgCajaNro.TabIndex = 13
        '
        'Label198
        '
        Me.Label198.AutoSize = True
        Me.Label198.Location = New System.Drawing.Point(357, 115)
        Me.Label198.Name = "Label198"
        Me.Label198.Size = New System.Drawing.Size(119, 13)
        Me.Label198.TabIndex = 49
        Me.Label198.Text = "Nombre sucursal emisor"
        '
        'txtCfgNombreSucursalEmisor
        '
        Me.txtCfgNombreSucursalEmisor.Location = New System.Drawing.Point(489, 112)
        Me.txtCfgNombreSucursalEmisor.Name = "txtCfgNombreSucursalEmisor"
        Me.txtCfgNombreSucursalEmisor.Size = New System.Drawing.Size(162, 20)
        Me.txtCfgNombreSucursalEmisor.TabIndex = 11
        '
        'Label199
        '
        Me.Label199.AutoSize = True
        Me.Label199.Location = New System.Drawing.Point(339, 89)
        Me.Label199.Name = "Label199"
        Me.Label199.Size = New System.Drawing.Size(137, 13)
        Me.Label199.TabIndex = 47
        Me.Label199.Text = "Codigo DGI sucursal emisor"
        '
        'txtCfgCodigoSucursalEmisor
        '
        Me.txtCfgCodigoSucursalEmisor.Location = New System.Drawing.Point(489, 86)
        Me.txtCfgCodigoSucursalEmisor.Name = "txtCfgCodigoSucursalEmisor"
        Me.txtCfgCodigoSucursalEmisor.Size = New System.Drawing.Size(52, 20)
        Me.txtCfgCodigoSucursalEmisor.TabIndex = 10
        '
        'Label200
        '
        Me.Label200.AutoSize = True
        Me.Label200.Location = New System.Drawing.Point(394, 37)
        Me.Label200.Name = "Label200"
        Me.Label200.Size = New System.Drawing.Size(82, 13)
        Me.Label200.TabIndex = 45
        Me.Label200.Text = "Telefono emisor"
        '
        'txtCfgTelefonoEmisor
        '
        Me.txtCfgTelefonoEmisor.Location = New System.Drawing.Point(489, 34)
        Me.txtCfgTelefonoEmisor.Name = "txtCfgTelefonoEmisor"
        Me.txtCfgTelefonoEmisor.Size = New System.Drawing.Size(162, 20)
        Me.txtCfgTelefonoEmisor.TabIndex = 8
        '
        'Label201
        '
        Me.Label201.AutoSize = True
        Me.Label201.Location = New System.Drawing.Point(72, 167)
        Me.Label201.Name = "Label201"
        Me.Label201.Size = New System.Drawing.Size(73, 13)
        Me.Label201.TabIndex = 43
        Me.Label201.Text = "Ciudad emisor"
        '
        'txtCfgCiudadEmisor
        '
        Me.txtCfgCiudadEmisor.Location = New System.Drawing.Point(157, 164)
        Me.txtCfgCiudadEmisor.Name = "txtCfgCiudadEmisor"
        Me.txtCfgCiudadEmisor.Size = New System.Drawing.Size(154, 20)
        Me.txtCfgCiudadEmisor.TabIndex = 4
        '
        'Label193
        '
        Me.Label193.AutoSize = True
        Me.Label193.Location = New System.Drawing.Point(40, 193)
        Me.Label193.Name = "Label193"
        Me.Label193.Size = New System.Drawing.Size(107, 13)
        Me.Label193.TabIndex = 41
        Me.Label193.Text = "Departamento emisor"
        '
        'txtCfgDepartamentoEmisor
        '
        Me.txtCfgDepartamentoEmisor.Location = New System.Drawing.Point(157, 190)
        Me.txtCfgDepartamentoEmisor.Name = "txtCfgDepartamentoEmisor"
        Me.txtCfgDepartamentoEmisor.Size = New System.Drawing.Size(154, 20)
        Me.txtCfgDepartamentoEmisor.TabIndex = 5
        '
        'Label192
        '
        Me.Label192.AutoSize = True
        Me.Label192.Location = New System.Drawing.Point(38, 219)
        Me.Label192.Name = "Label192"
        Me.Label192.Size = New System.Drawing.Size(109, 13)
        Me.Label192.TabIndex = 39
        Me.Label192.Text = "Domicilio fiscal emisor"
        '
        'txtCfgDomicilioFiscalEmisor
        '
        Me.txtCfgDomicilioFiscalEmisor.Location = New System.Drawing.Point(157, 216)
        Me.txtCfgDomicilioFiscalEmisor.Name = "txtCfgDomicilioFiscalEmisor"
        Me.txtCfgDomicilioFiscalEmisor.Size = New System.Drawing.Size(154, 20)
        Me.txtCfgDomicilioFiscalEmisor.TabIndex = 6
        '
        'Label191
        '
        Me.Label191.AutoSize = True
        Me.Label191.Location = New System.Drawing.Point(74, 140)
        Me.Label191.Name = "Label191"
        Me.Label191.Size = New System.Drawing.Size(71, 13)
        Me.Label191.TabIndex = 37
        Me.Label191.Text = "Correo emisor"
        '
        'txtCfgCorreoEmisor
        '
        Me.txtCfgCorreoEmisor.Location = New System.Drawing.Point(157, 138)
        Me.txtCfgCorreoEmisor.Name = "txtCfgCorreoEmisor"
        Me.txtCfgCorreoEmisor.Size = New System.Drawing.Size(154, 20)
        Me.txtCfgCorreoEmisor.TabIndex = 3
        '
        'Label190
        '
        Me.Label190.AutoSize = True
        Me.Label190.Location = New System.Drawing.Point(47, 115)
        Me.Label190.Name = "Label190"
        Me.Label190.Size = New System.Drawing.Size(100, 13)
        Me.Label190.TabIndex = 35
        Me.Label190.Text = "Giro negocio emisor"
        '
        'txtCfgGiroNegocioEmisor
        '
        Me.txtCfgGiroNegocioEmisor.Location = New System.Drawing.Point(157, 112)
        Me.txtCfgGiroNegocioEmisor.Name = "txtCfgGiroNegocioEmisor"
        Me.txtCfgGiroNegocioEmisor.Size = New System.Drawing.Size(154, 20)
        Me.txtCfgGiroNegocioEmisor.TabIndex = 2
        '
        'Label188
        '
        Me.Label188.AutoSize = True
        Me.Label188.Location = New System.Drawing.Point(82, 89)
        Me.Label188.Name = "Label188"
        Me.Label188.Size = New System.Drawing.Size(63, 13)
        Me.Label188.TabIndex = 31
        Me.Label188.Text = "RUT emisor"
        '
        'txtCfgRUTEmisor
        '
        Me.txtCfgRUTEmisor.Location = New System.Drawing.Point(157, 86)
        Me.txtCfgRUTEmisor.MaxLength = 12
        Me.txtCfgRUTEmisor.Name = "txtCfgRUTEmisor"
        Me.txtCfgRUTEmisor.Size = New System.Drawing.Size(154, 20)
        Me.txtCfgRUTEmisor.TabIndex = 1
        '
        'Label184
        '
        Me.Label184.AutoSize = True
        Me.Label184.Location = New System.Drawing.Point(43, 37)
        Me.Label184.Name = "Label184"
        Me.Label184.Size = New System.Drawing.Size(101, 13)
        Me.Label184.TabIndex = 29
        Me.Label184.Text = "Razon social emisor"
        '
        'txtCfgRazonSocialEmisor
        '
        Me.txtCfgRazonSocialEmisor.Location = New System.Drawing.Point(157, 34)
        Me.txtCfgRazonSocialEmisor.Name = "txtCfgRazonSocialEmisor"
        Me.txtCfgRazonSocialEmisor.Size = New System.Drawing.Size(154, 20)
        Me.txtCfgRazonSocialEmisor.TabIndex = 0
        '
        'NotifyIcon1
        '
        Me.NotifyIcon1.Text = "NotifyIcon1"
        Me.NotifyIcon1.Visible = True
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DodgerBlue
        Me.Button1.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.ForeColor = System.Drawing.SystemColors.Control
        Me.Button1.Location = New System.Drawing.Point(63, 621)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(19, 23)
        Me.Button1.TabIndex = 312
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Location = New System.Drawing.Point(6, 627)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(60, 13)
        Me.Label35.TabIndex = 311
        Me.Label35.Text = "Leer tarjeta"
        '
        'frmFactura
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1124, 835)
        Me.Controls.Add(Me.TabControl1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximumSize = New System.Drawing.Size(1140, 1378)
        Me.MinimumSize = New System.Drawing.Size(1140, 726)
        Me.Name = "frmFactura"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Factura - TA-Face"
        Me.TabControl1.ResumeLayout(False)
        Me.FacturaRapida.ResumeLayout(False)
        Me.FacturaRapida.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GBx_Exportaciones.ResumeLayout(False)
        Me.GBx_Exportaciones.PerformLayout()
        Me.GBx_RetencionesPercepciones.ResumeLayout(False)
        Me.GBx_RetencionesPercepciones.PerformLayout()
        Me.GBx_PreFactura.ResumeLayout(False)
        Me.GBx_SubTotalesInformativos.ResumeLayout(False)
        Me.GBx_SubTotalesInformativos.PerformLayout()
        Me.panAdvertencia.ResumeLayout(False)
        Me.panAdvertencia.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GBx_Totales.ResumeLayout(False)
        Me.GBx_Totales.PerformLayout()
        Me.GBx_MediosPagos.ResumeLayout(False)
        Me.GBx_MediosPagos.PerformLayout()
        Me.GroupBox8.ResumeLayout(False)
        Me.GroupBox8.PerformLayout()
        Me.GroupBox11.ResumeLayout(False)
        Me.GroupBox11.PerformLayout()
        Me.GroupBox_Remito.ResumeLayout(False)
        Me.GroupBox_Remito.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Configuracion.ResumeLayout(False)
        Me.GroupBox10.ResumeLayout(False)
        Me.GroupBox10.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox9.ResumeLayout(False)
        Me.GroupBox9.PerformLayout()
        Me.gbxImpresion.ResumeLayout(False)
        Me.gbxImpresion.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        CType(Me.txtCantCopias, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpDatosServidor.ResumeLayout(False)
        Me.grpDatosServidor.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents FacturaRapida As TabPage
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Rdb_esTckFact As RadioButton
    Friend WithEvents Rdb_esRemito As RadioButton
    Friend WithEvents Rdb_esBoleta As RadioButton
    Friend WithEvents Ckb_esSecretoProfesional As CheckBox
    Friend WithEvents btn_buscarArticulo10 As Button
    Friend WithEvents btn_buscarArticulo9 As Button
    Friend WithEvents btn_buscarArticulo8 As Button
    Friend WithEvents btn_buscarArticulo7 As Button
    Friend WithEvents btn_buscarArticulo6 As Button
    Friend WithEvents btn_buscarArticulo5 As Button
    Friend WithEvents btn_buscarArticulo4 As Button
    Friend WithEvents btn_buscarArticulo3 As Button
    Friend WithEvents btn_buscarArticulo2 As Button
    Friend WithEvents btn_buscarArticulo1 As Button
    Friend WithEvents GBx_Exportaciones As GroupBox
    Friend WithEvents CBx_ViaTransporte As ComboBox
    Friend WithEvents Label30 As Label
    Friend WithEvents Label29 As Label
    Friend WithEvents TBx_ClausulaDeVenta As TextBox
    Friend WithEvents CBx_ModalidadVenta As ComboBox
    Friend WithEvents Label28 As Label
    Friend WithEvents Lbl_Comentarios As Label
    Friend WithEvents GBx_RetencionesPercepciones As GroupBox
    Friend WithEvents Label12 As Label
    Friend WithEvents LBx_RetencionesPercepciones As ListBox
    Friend WithEvents Btn_RetencionesPercepciones10 As Button
    Friend WithEvents Btn_RetencionesPercepciones9 As Button
    Friend WithEvents Btn_RetencionesPercepciones8 As Button
    Friend WithEvents Btn_RetencionesPercepciones7 As Button
    Friend WithEvents Btn_RetencionesPercepciones6 As Button
    Friend WithEvents Btn_RetencionesPercepciones5 As Button
    Friend WithEvents Btn_RetencionesPercepciones4 As Button
    Friend WithEvents Btn_RetencionesPercepciones3 As Button
    Friend WithEvents Btn_RetencionesPercepciones2 As Button
    Friend WithEvents Btn_RetencionesPercepciones1 As Button
    Friend WithEvents CBx_AgenteResponsable10 As ComboBox
    Friend WithEvents CBx_AgenteResponsable9 As ComboBox
    Friend WithEvents CBx_AgenteResponsable8 As ComboBox
    Friend WithEvents CBx_AgenteResponsable7 As ComboBox
    Friend WithEvents CBx_AgenteResponsable6 As ComboBox
    Friend WithEvents CBx_AgenteResponsable5 As ComboBox
    Friend WithEvents CBx_AgenteResponsable4 As ComboBox
    Friend WithEvents CBx_AgenteResponsable3 As ComboBox
    Friend WithEvents CBx_AgenteResponsable2 As ComboBox
    Friend WithEvents CBx_AgenteResponsable1 As ComboBox
    Friend WithEvents TBx_MontoRetenidoPorLinea10 As TextBox
    Friend WithEvents TBx_MontoRetenidoPorLinea9 As TextBox
    Friend WithEvents TBx_MontoRetenidoPorLinea8 As TextBox
    Friend WithEvents TBx_MontoRetenidoPorLinea7 As TextBox
    Friend WithEvents TBx_MontoRetenidoPorLinea6 As TextBox
    Friend WithEvents TBx_MontoRetenidoPorLinea5 As TextBox
    Friend WithEvents TBx_MontoRetenidoPorLinea4 As TextBox
    Friend WithEvents TBx_MontoRetenidoPorLinea3 As TextBox
    Friend WithEvents TBx_MontoRetenidoPorLinea2 As TextBox
    Friend WithEvents TBx_MontoRetenidoPorLinea1 As TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents GBx_PreFactura As GroupBox
    Friend WithEvents Btn_PreFacturaImprimir As Button
    Friend WithEvents Btn_PreFacturaAbrir As Button
    Friend WithEvents Btn_PreFacturaGuardar As Button
    Friend WithEvents GBx_SubTotalesInformativos As GroupBox
    Friend WithEvents TBx_ValorSubTotales5 As TextBox
    Friend WithEvents TBx_ValorSubTotales4 As TextBox
    Friend WithEvents TBx_ValorSubTotales3 As TextBox
    Friend WithEvents TBx_ValorSubTotales2 As TextBox
    Friend WithEvents Label25 As Label
    Friend WithEvents TBx_ValorSubTotales1 As TextBox
    Friend WithEvents Label20 As Label
    Friend WithEvents Label21 As Label
    Friend WithEvents Label22 As Label
    Friend WithEvents Label23 As Label
    Friend WithEvents Label24 As Label
    Friend WithEvents TBx_GlosaSubTotales5 As TextBox
    Friend WithEvents TBx_GlosaSubTotales4 As TextBox
    Friend WithEvents TBx_GlosaSubTotales3 As TextBox
    Friend WithEvents TBx_GlosaSubTotales2 As TextBox
    Friend WithEvents TBx_GlosaSubTotales1 As TextBox
    Friend WithEvents Label19 As Label
    Friend WithEvents Label18 As Label
    Friend WithEvents Label17 As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Ckb_esContingencia As CheckBox
    Friend WithEvents btnReimprimir As Button
    Friend WithEvents RapidatxtSubDescuentoValor10 As TextBox
    Friend WithEvents RapidatxtMontoItem10 As TextBox
    Friend WithEvents RapidacmbIndicadorFacturacion10 As ComboBox
    Friend WithEvents RapidatxtNombreItem10 As TextBox
    Friend WithEvents RapidatxtCantidad10 As TextBox
    Friend WithEvents RapidatxtPrecioUnitario10 As TextBox
    Friend WithEvents RapidatxtSubDescuentoValor9 As TextBox
    Friend WithEvents RapidatxtMontoItem9 As TextBox
    Friend WithEvents RapidacmbIndicadorFacturacion9 As ComboBox
    Friend WithEvents RapidatxtNombreItem9 As TextBox
    Friend WithEvents RapidatxtCantidad9 As TextBox
    Friend WithEvents RapidatxtPrecioUnitario9 As TextBox
    Friend WithEvents RapidatxtSubDescuentoValor8 As TextBox
    Friend WithEvents RapidatxtMontoItem8 As TextBox
    Friend WithEvents RapidacmbIndicadorFacturacion8 As ComboBox
    Friend WithEvents RapidatxtNombreItem8 As TextBox
    Friend WithEvents RapidatxtCantidad8 As TextBox
    Friend WithEvents RapidatxtPrecioUnitario8 As TextBox
    Friend WithEvents RapidatxtSubDescuentoValor7 As TextBox
    Friend WithEvents RapidatxtMontoItem7 As TextBox
    Friend WithEvents RapidacmbIndicadorFacturacion7 As ComboBox
    Friend WithEvents RapidatxtNombreItem7 As TextBox
    Friend WithEvents RapidatxtCantidad7 As TextBox
    Friend WithEvents RapidatxtPrecioUnitario7 As TextBox
    Friend WithEvents RapidatxtSubDescuentoValor6 As TextBox
    Friend WithEvents RapidatxtMontoItem6 As TextBox
    Friend WithEvents RapidacmbIndicadorFacturacion6 As ComboBox
    Friend WithEvents RapidatxtNombreItem6 As TextBox
    Friend WithEvents RapidatxtCantidad6 As TextBox
    Friend WithEvents RapidatxtPrecioUnitario6 As TextBox
    Friend WithEvents panAdvertencia As Panel
    Friend WithEvents lblCerrar As Label
    Friend WithEvents Label27 As Label
    Friend WithEvents lblAdvertencia As Label
    Friend WithEvents Lbl_NewAge As Label
    Friend WithEvents RapidatxtAdenda As TextBox
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents RapidatxtSubDescuentoValor5 As TextBox
    Friend WithEvents RapidatxtMontoItem5 As TextBox
    Friend WithEvents RapidacmbIndicadorFacturacion5 As ComboBox
    Friend WithEvents RapidatxtNombreItem5 As TextBox
    Friend WithEvents RapidatxtCantidad5 As TextBox
    Friend WithEvents RapidatxtPrecioUnitario5 As TextBox
    Friend WithEvents RapidatxtSubDescuentoValor4 As TextBox
    Friend WithEvents RapidatxtMontoItem4 As TextBox
    Friend WithEvents RapidacmbIndicadorFacturacion4 As ComboBox
    Friend WithEvents RapidatxtNombreItem4 As TextBox
    Friend WithEvents RapidatxtCantidad4 As TextBox
    Friend WithEvents RapidatxtPrecioUnitario4 As TextBox
    Friend WithEvents RapidatxtSubDescuentoValor3 As TextBox
    Friend WithEvents RapidatxtMontoItem3 As TextBox
    Friend WithEvents RapidacmbIndicadorFacturacion3 As ComboBox
    Friend WithEvents RapidatxtNombreItem3 As TextBox
    Friend WithEvents RapidatxtCantidad3 As TextBox
    Friend WithEvents RapidatxtPrecioUnitario3 As TextBox
    Friend WithEvents RapidatxtSubDescuentoValor2 As TextBox
    Friend WithEvents RapidatxtMontoItem2 As TextBox
    Friend WithEvents RapidacmbIndicadorFacturacion2 As ComboBox
    Friend WithEvents RapidatxtNombreItem2 As TextBox
    Friend WithEvents RapidatxtCantidad2 As TextBox
    Friend WithEvents RapidatxtPrecioUnitario2 As TextBox
    Friend WithEvents RapidatxtSubDescuentoValor1 As TextBox
    Friend WithEvents Label179 As Label
    Friend WithEvents RapidatxtMontoItem1 As TextBox
    Friend WithEvents Label177 As Label
    Friend WithEvents RapidacmbIndicadorFacturacion1 As ComboBox
    Friend WithEvents BtnLimpiarCampos As Button
    Friend WithEvents RapidatxtNombreItem1 As TextBox
    Friend WithEvents BtnFacturar As Button
    Friend WithEvents GBx_Totales As GroupBox
    Friend WithEvents TBx_ExportacionYAsimilados As TextBox
    Friend WithEvents Label26 As Label
    Friend WithEvents TBx_MontoRetenido As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents RapidatxtTotalMontoNoGravado As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents RapidaBtn_guardar As Button
    Friend WithEvents lblDolares As Label
    Friend WithEvents lblPesos As Label
    Friend WithEvents RapidaLbl_tipoCambio As Label
    Friend WithEvents RapidaTxtMontoNoFacturable As TextBox
    Friend WithEvents RapidaTbx_tipoCambio As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents RapidatxtMontoTotalPagar As Label
    Friend WithEvents lblMonedaSimbolo As Label
    Friend WithEvents RapidatxtTotalIVATasaBasica As TextBox
    Friend WithEvents Label186 As Label
    Friend WithEvents RapidatxtTotalIVATasaMinima As TextBox
    Friend WithEvents Label187 As Label
    Friend WithEvents RapidatxtTotalMontoNetoIVATasaBasica As TextBox
    Friend WithEvents Label172 As Label
    Friend WithEvents RapidatxtTotalMontoNetoIVATasaMinima As TextBox
    Friend WithEvents Label185 As Label
    Friend WithEvents RapidatxtCantidad1 As TextBox
    Friend WithEvents GBx_MediosPagos As GroupBox
    Friend WithEvents RapidatxtVuelto As TextBox
    Friend WithEvents Label169 As Label
    Friend WithEvents RapidatxtValorMediosDePago2 As TextBox
    Friend WithEvents RapidatxtValorMediosDePago1 As TextBox
    Friend WithEvents Label167 As Label
    Friend WithEvents RapidatxtGlosaMediosDePago2 As TextBox
    Friend WithEvents RapidatxtGlosaMediosDePago1 As TextBox
    Friend WithEvents Label168 As Label
    Friend WithEvents Label173 As Label
    Friend WithEvents Label174 As Label
    Friend WithEvents Label181 As Label
    Friend WithEvents RapidatxtPrecioUnitario1 As TextBox
    Friend WithEvents Label176 As Label
    Friend WithEvents Label178 As Label
    Friend WithEvents Label180 As Label
    Friend WithEvents Label182 As Label
    Friend WithEvents GroupBox8 As GroupBox
    Friend WithEvents Btn_Buscar As Button
    Friend WithEvents Ckb_Exportacion As CheckBox
    Friend WithEvents chkConsumoFinal As CheckBox
    Friend WithEvents RapidatxtDepartamentoReceptor As TextBox
    Friend WithEvents Label210 As Label
    Friend WithEvents RapidatxtCiudadReceptor As TextBox
    Friend WithEvents Label197 As Label
    Friend WithEvents RapidatxtDireccionReceptor As TextBox
    Friend WithEvents Label85 As Label
    Friend WithEvents RapidatxtNombreReceptor As TextBox
    Friend WithEvents Label86 As Label
    Friend WithEvents RapidatxtDocumentoReceptor As TextBox
    Friend WithEvents Label163 As Label
    Friend WithEvents RapidacmbCodigoPaisReceptor As ComboBox
    Friend WithEvents RapidacmbTipoDocumentoReceptor As ComboBox
    Friend WithEvents Label75 As Label
    Friend WithEvents Label84 As Label
    Friend WithEvents GroupBox11 As GroupBox
    Friend WithEvents CmbIndicadorMontoBrutoEncabezado As ComboBox
    Friend WithEvents chkVencimientoCredito As CheckBox
    Friend WithEvents lblVencimiento As Label
    Friend WithEvents dtpFechaVencimiento As DateTimePicker
    Friend WithEvents lblFormaDePago As Label
    Friend WithEvents cmbFormaPago As ComboBox
    Friend WithEvents chkIndicadorRefGlobal As CheckBox
    Friend WithEvents Label2 As Label
    Friend WithEvents lblReferencia As Label
    Friend WithEvents lblFechaCFEInformacionReferencia As Label
    Friend WithEvents Label164 As Label
    Friend WithEvents dtpFechaCFEInformacionReferencia As DateTimePicker
    Friend WithEvents RapidatxtNumeroComprobanteEncabezado As TextBox
    Friend WithEvents RapidatxtSerieComprobanteEncabezado As TextBox
    Friend WithEvents Label165 As Label
    Friend WithEvents txtRazonInformacionReferencia As TextBox
    Friend WithEvents txtNumeroCFEInformacionReferencia As TextBox
    Friend WithEvents Label166 As Label
    Friend WithEvents lblRazonInformacionReferencia As Label
    Friend WithEvents lblNumeroCFEInformacionReferencia As Label
    Friend WithEvents cmbTipoVenta As ComboBox
    Friend WithEvents txtSerieCFEInformacionReferencia As TextBox
    Friend WithEvents cmbTipoCFEInformacionReferencia As ComboBox
    Friend WithEvents lblSerieCFEInformacionReferencia As Label
    Friend WithEvents lblTipoCFEInformacionReferencia As Label
    Friend WithEvents GroupBox_Remito As GroupBox
    Friend WithEvents Panel1 As Panel
    Friend WithEvents textBox_NroDocPropMercaderiaTransp As TextBox
    Friend WithEvents cmbTipoDocMercaderiaTransp As ComboBox
    Friend WithEvents Label41 As Label
    Friend WithEvents Label39 As Label
    Friend WithEvents cmbCodPaisPropMercaderiaTransp As ComboBox
    Friend WithEvents Label40 As Label
    Friend WithEvents TextBox_NombreRznSocPropMercaderiaTransp As TextBox
    Friend WithEvents cmbIndicadorPropMercaderiaTransp As ComboBox
    Friend WithEvents Label38 As Label
    Friend WithEvents cmbIndicadorTipoTrasladoBienesEncabezado As ComboBox
    Friend WithEvents Label158 As Label
    Friend WithEvents Configuracion As TabPage
    Friend WithEvents GroupBox10 As GroupBox
    Friend WithEvents Chk_IVAalDia As CheckBox
    Friend WithEvents Chk_emiteRemitos As CheckBox
    Friend WithEvents Chk_usaExportacion As CheckBox
    Friend WithEvents Chk_PreFactura As CheckBox
    Friend WithEvents Chk_UsaRetPerc As CheckBox
    Friend WithEvents Chk_UsaSubTotales As CheckBox
    Friend WithEvents Label7 As Label
    Friend WithEvents txtCfgNombreEmisor As TextBox
    Friend WithEvents Chk_Bloqueado As CheckBox
    Friend WithEvents Label6 As Label
    Friend WithEvents txtCfgTelefono2Emisor As TextBox
    Friend WithEvents chkConsumoFinalDefault As CheckBox
    Friend WithEvents Label4 As Label
    Friend WithEvents cmbCfgIndicadorFacturacion As ComboBox
    Friend WithEvents Label211 As Label
    Friend WithEvents txtNroSucursal As TextBox
    Friend WithEvents gbxImpresion As GroupBox
    Friend WithEvents chkAbrirCajon As CheckBox
    Friend WithEvents txtCantCopias As NumericUpDown
    Friend WithEvents lblCopias As Label
    Friend WithEvents btnImprimirPrueba As Button
    Friend WithEvents chkImprimeApi As CheckBox
    Friend WithEvents cmbImpresora As ComboBox
    Friend WithEvents cmbTipoImpresora As ComboBox
    Friend WithEvents lblTipoImpresora As Label
    Friend WithEvents lblNomImpresora As Label
    Friend WithEvents grpDatosServidor As GroupBox
    Friend WithEvents Button3 As Button
    Friend WithEvents txturlServidorGateway As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents TextBox_CarpetaOperacion As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Label33 As Label
    Friend WithEvents txtSegundosTimeout As TextBox
    Friend WithEvents BtnGuardarConfig As Button
    Friend WithEvents cmbTipoNegocio As ComboBox
    Friend WithEvents cmbCfgTipoMonedaTransaccion As ComboBox
    Friend WithEvents Label209 As Label
    Friend WithEvents Label202 As Label
    Friend WithEvents txtCfgVendedorNom As TextBox
    Friend WithEvents Label203 As Label
    Friend WithEvents txtCfgVendedorNro As TextBox
    Friend WithEvents Label205 As Label
    Friend WithEvents txtCfgValorUnidadIndexada As TextBox
    Friend WithEvents Label206 As Label
    Friend WithEvents txtCfgTasaMinimaIVA As TextBox
    Friend WithEvents Label207 As Label
    Friend WithEvents txtCfgTasaBasicaIVA As TextBox
    Friend WithEvents Label208 As Label
    Friend WithEvents Label175 As Label
    Friend WithEvents txtCfgTipoDeCambio As TextBox
    Friend WithEvents Label194 As Label
    Friend WithEvents Label195 As Label
    Friend WithEvents txtCfgCajeroNombre As TextBox
    Friend WithEvents Label196 As Label
    Friend WithEvents txtCfgCajeroNro As TextBox
    Friend WithEvents txtCfgCajaNro As TextBox
    Friend WithEvents Label198 As Label
    Friend WithEvents txtCfgNombreSucursalEmisor As TextBox
    Friend WithEvents Label199 As Label
    Friend WithEvents txtCfgCodigoSucursalEmisor As TextBox
    Friend WithEvents Label200 As Label
    Friend WithEvents txtCfgTelefonoEmisor As TextBox
    Friend WithEvents Label201 As Label
    Friend WithEvents txtCfgCiudadEmisor As TextBox
    Friend WithEvents Label193 As Label
    Friend WithEvents txtCfgDepartamentoEmisor As TextBox
    Friend WithEvents Label192 As Label
    Friend WithEvents txtCfgDomicilioFiscalEmisor As TextBox
    Friend WithEvents Label191 As Label
    Friend WithEvents txtCfgCorreoEmisor As TextBox
    Friend WithEvents Label190 As Label
    Friend WithEvents txtCfgGiroNegocioEmisor As TextBox
    Friend WithEvents Label188 As Label
    Friend WithEvents txtCfgRUTEmisor As TextBox
    Friend WithEvents Label184 As Label
    Friend WithEvents txtCfgRazonSocialEmisor As TextBox
    Friend WithEvents Button_Sincronizar As Button
    Friend WithEvents Label36 As Label
    Friend WithEvents NotifyIcon1 As NotifyIcon
    Friend WithEvents FolderBrowserDialog1 As FolderBrowserDialog
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents Label32 As Label
    Friend WithEvents Label34 As Label
    Friend WithEvents txtTermCod As TextBox
    Friend WithEvents txtEmpCod As TextBox
    Friend WithEvents btnCerrarLote As Button
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents rbDevolucion As RadioButton
    Friend WithEvents rbVenta As RadioButton
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents chkModificarTipoCuenta As CheckBox
    Friend WithEvents chkModificarDecretoLey As CheckBox
    Friend WithEvents chkModificarPlan As CheckBox
    Friend WithEvents chkModificarTarjeta As CheckBox
    Friend WithEvents chkModificarFactura As CheckBox
    Friend WithEvents chkModificarCuotas As CheckBox
    Friend WithEvents chkModificarMontos As CheckBox
    Friend WithEvents chkModificarMoneda As CheckBox
    Friend WithEvents GroupBox9 As GroupBox
    Friend WithEvents cmbIdProcesadorTransAct As ComboBox
    Friend WithEvents Label31 As Label
    Friend WithEvents chkConsultarUltimoCierre As CheckBox
    Friend WithEvents rdButtonImprimeVoucherTransAct As RadioButton
    Friend WithEvents rdButtonImprimeVoucherAdenda As RadioButton
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents chkCierreCentralizado As CheckBox
    Friend WithEvents chkSoloCobrar As CheckBox
    Friend WithEvents chkSoloFacturar As CheckBox
    Friend WithEvents Button1 As Button
    Friend WithEvents Label35 As Label
End Class
