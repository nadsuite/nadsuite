﻿Public Class frmReimpresion
    Private _TipoCFE As Integer
    Private _SerieCFE As String
    Private _NumeroCFE As Long
    Private _ProcesoOk As Boolean = False

    Public ReadOnly Property TipoCFE() As Integer
        Get
            Return _TipoCFE
        End Get
    End Property

    Public ReadOnly Property SerieCFE() As String
        Get
            Return _SerieCFE
        End Get
    End Property

    Public ReadOnly Property NumeroCFE() As Long
        Get
            Return _NumeroCFE
        End Get
    End Property

    Public ReadOnly Property ProcesoOk() As Boolean
        Get
            Return _ProcesoOk
        End Get
    End Property

    Sub New(ByVal pTipoCFE As Integer, ByVal pSerieCFE As String, ByVal pNumeroCFE As Long)

        InitializeComponent()

        _TipoCFE = pTipoCFE
        _SerieCFE = pSerieCFE
        _NumeroCFE = pNumeroCFE

    End Sub

    Private Sub btnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles btnAceptar.Click
        Try
            _TipoCFE = cmbTipoComprobante.SelectedValue
            _SerieCFE = txtSeriCFE.Text
            _NumeroCFE = txtNumeroCFE.Text
            _ProcesoOk = True
            Me.Close()
        Catch ex As Exception
            MsgBox(IIf(ex.Message = "", ex.InnerException.Message, ex.Message))
        End Try
    End Sub

    Private Sub frmReimpresion_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        CargarComprobantes()
        cmbTipoComprobante.SelectedValue = _TipoCFE
        txtSeriCFE.Text = _SerieCFE
        txtNumeroCFE.Text = _NumeroCFE
        btnAceptar.Focus()
    End Sub

    Private Sub CargarComprobantes()

        Dim Comprobantes As New Dictionary(Of Integer, String)
        Comprobantes.Add(101, "eTicket")
        Comprobantes.Add(102, "eTicket Nota Cred.")
        Comprobantes.Add(103, "eTicket Nota Deb.")
        Comprobantes.Add(111, "eFactura")
        Comprobantes.Add(112, "eFactura Nota Cred.")
        Comprobantes.Add(113, "eFactura Nota Deb.")
        cmbTipoComprobante.DataSource = Comprobantes.ToArray
        cmbTipoComprobante.DisplayMember = "Value"
        cmbTipoComprobante.ValueMember = "Key"

    End Sub


    Private Sub txtNumeroCFE_Click(sender As Object, e As System.EventArgs) Handles txtNumeroCFE.Click
        txtNumeroCFE.SelectAll()
    End Sub
End Class