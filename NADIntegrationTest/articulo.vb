﻿Public Class articulo
    Private nombreField As String
    Private precioField As Double
    Private guardaPrecioField As Boolean


    Public Property Nombre As String
        Get
            Return nombreField
        End Get
        Set(value As String)
            nombreField = value
        End Set
    End Property

    Public Property Precio As Double
        Get
            Return precioField
        End Get
        Set(value As Double)
            precioField = value
        End Set
    End Property

    Public Property GuardaPrecio As Boolean
        Get
            Return guardaPrecioField
        End Get
        Set(value As Boolean)
            guardaPrecioField = value
        End Set
    End Property
End Class
