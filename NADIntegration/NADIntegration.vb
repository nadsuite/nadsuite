﻿Imports TAFE2Api
Imports TAFACE2ApiParsing
Imports TransActV4.API.NET.Tarjetas
Imports System.ComponentModel

Public Class NADIntegration
    Private objTAParsing As New TAParsing_v0206             'objeto de TAFACE para armado de XML
    Private objTAFEApi As New TAFEApi_v0206                 'objeto de TAFACE para firma, reserva cae y confirmacion
    Private TransActTarjetas As New TransActAPI_Tarjetas_v400   'objeto de TransAct para el manejo de la transaccion
    Private objImpresionBrigde As New TransActV4.BridgeUSB.NET.Impresion.ImpresionImp
    Private UltimoXMLFacturaField As String = ""
    Private UltimoXMLRespuestaField As String = ""

    Private TipoImpresoraTAFACE As Integer 'Tipo impresora seteada en TAFACE
    Private NombreImpresoraTAFACE As String 'Nombre impresora seteada en TAFACE

    Sub New()
        UltimoXMLFacturaField = ""
        UltimoXMLRespuestaField = ""
    End Sub

    Public Property UltimoXMLFactura As String
        Get
            Return UltimoXMLFacturaField
        End Get
        Set(value As String)
            UltimoXMLFacturaField = value
        End Set
    End Property

    Public Property UltimoXMLRespuesta As String
        Get
            Return UltimoXMLRespuestaField
        End Get
        Set(value As String)
            UltimoXMLRespuestaField = value
        End Set
    End Property

    'comprende los enum necesarios para la integracion de los dos productos
#Region "Estructuras Enum"
#Region "Enum TAFACE"
    Enum enumTipoDeComprobanteCFE
        eTicket = 101
        eTicketNotaCred = 102
        eTicketNotaDeb = 103
        eFactura = 111
        eFacturaNotaCred = 112
        eFacturaNotaDeb = 113
        eRemito = 181
        eResguardo = 182
        eFacturaExportacion = 121
        eFacturaNotaCredExportacion = 122
        eFacturaNotaDebExportacion = 123
        eRemitoExportacion = 124
        eTicketCuentaAjena = 131
        eTicketNotaCredCuentaAjena = 132
        eTicketNotaDebCuentaAjena = 133
        eFacturaCuentaAjena = 141
        eFacturaNotaCredCuentaAjena = 142
        eFacturaNotaDebCuentaAjena = 143
        eBoleta = 151
        eBoletaNotaCred = 152
        eBoletaNotaDeb = 153
        eTicketContingencia = 201
        eTicketNotaCredContingencia = 202
        eTicketNotaDebContingencia = 203
        eFacturaContingencia = 211
        eFacturaNotaCredContingencia = 212
        eFacturaNotaDebContingencia = 213
        eRemitoContingencia = 281
        eResguardoContingencia = 282
        eFacturaExportacionContingencia = 221
        eFacturaNotaCredExportacionContingencia = 222
        eFacturaNotaDebExportacionContingencia = 223
        eRemitoExportacionContingencia = 224
        eTicketCuentaAjenaContingencia = 231
        eTicketNotaCredCuentaAjenaContingencia = 232
        eTicketNotaDebCuentaAjenaContingencia = 233
        eFacturaCuentaAjenaContingencia = 241
        eFacturaNotaCredCuentaAjenaContingencia = 242
        eFacturaNotaDebCuentaAjenaContingencia = 243
        eBoletaContingencia = 251
        eBoletaNotaCredContingencia = 252
        eBoletaNotaDebContingencia = 253
    End Enum

    Enum enumTipoMovimientoDescuentoORecargoGlobal
        D
        R
    End Enum

    Enum enumTipoDescuentoORecargoGlobal
        Monto = 1
        Porcentaje = 2
    End Enum

    Enum enumIndicadorDeFacturacionDescuentoGlobal
        SinDefinir = 0
        ExentoIVA = 1
        TasaMinima = 2
        TasaBasica = 3
        OtraTasa = 4
        NoFacturable = 6
        NoFacturableNegativo = 7
        ExportacionYAsimiladas = 10
        ImpuestoPercibido = 11
        IVAEnSuspenso = 12
        ItemVendidoPorNoContribuyente = 13
        ItemVendidoContribuyenteMonotributo = 14
        ItemVendidoContribuyenteIMEBA = 15
    End Enum

    Enum enumTipoSubDescuento
        Monto = 1
        Porcentaje = 2
    End Enum

    Enum enumTipoSubRecargo
        Monto = 1
        Porcentaje = 2
    End Enum

    Enum enumIndicadorDeFacturacionDetalle
        ExentoIVA = 1
        TasaMinima = 2
        TasaBasica = 3
        OtraTasa = 4
        EntregaGratuita = 5
        NoFacturable = 6
        NoFacturableNegativo = 7
        ItemARebajarEnRemito = 8
        ItemAAjustarEnResguardo = 9
        ExportacionYAsimiladas = 10
        ImpuestoPercibido = 11
        IVAEnSuspenso = 12
        ItemVendidoPorNoContribuyente = 13
        ItemVendidoContribuyenteMonotributo = 14
        ItemVendidoContribuyenteIMEBA = 15
    End Enum

    Enum enumTipoDeCodigoItem
        INT1
        INT2
        EAN
        PLU
        DUN
    End Enum

    Enum enumTipoDocumentoDelReceptor
        SinDefinir = 0
        RUC = 2
        CI = 3
        Otros = 4
        Pasaporte = 5
        DNI = 6
        NIFE = 7
    End Enum

    Enum enumIndicadorTipoTraslado
        SinDefinir = 0
        Venta = 1
        TransladoInterno = 2
    End Enum

    Enum enumIndicadorPropiedadMercaderiaTransportada
        SinDefinir = 0
        MercaderiaTerceros = 1
    End Enum

    Enum enumIndicadorMontosBrutos
        NoIcluyenIVA = 0
        IVAIncluido = 1
        IMEBAyAdicionalesIncluido = 2
    End Enum

    Enum enumFormaDePago
        SinDefinir = 0
        Contado = 1
        Credito = 2
    End Enum

    Enum enumModalidadDeVenta
        SinDefinir = 0
        RegimenGeneral = 1
        Consignacion = 2
        PrecioReservable = 3
        BienesPropiosAExclavesAduaneros = 4
        RegimenGeneralEsportacionDeServ = 90
        OtrasTransacciones = 99
    End Enum

    Enum enumViaDeTransporte
        SinDefinir = 0
        Maritimo = 1
        Aereo = 2
        Terrestre = 3
        NA = 8
        Otro = 9
    End Enum

    Enum enumTipoDeNegocio
        GenericoPlaza = 1
        TiendaPlaza = 2
        TiendaFreeShop = 3
        MayoristaPlaza = 4
    End Enum

    Enum enumTipoApi
        TAFE = 1
        TACE = 2
    End Enum

    Enum enumTipoDocumentoDelMandante
        SinDefinir = 0
        NIE = 1
        RUC = 2
        CI = 3
        Otros = 4
        Pasaporte = 5
        DNI = 6
    End Enum

    Enum enumCodigoPais
        <Description("Afganistan")> AF = 4
        <Description("Aland")> AX = 248
        <Description("Albania")> AL = 8
        <Description("Alemania")> DE = 276
        <Description("Andorra")> AD = 20
        <Description("Angola")> AO = 24
        <Description("Anguila")> AI = 660
        <Description("Antartida")> AQ = 10
        <Description("Antigua y Barbuda")> AG = 28
        <Description("Arabia Saudita")> SA = 682
        <Description("Argelia")> DZ = 12
        <Description("Argentina")> AR = 32
        <Description("Armenia")> AM = 51
        <Description("Aruba")> AW = 533
        <Description("Australia")> AU = 36
        <Description("Austria")> AT = 40
        <Description("Azerbaiyan")> AZ = 31
        <Description("Bahamas")> BS = 44
        <Description("Banglades")> BD = 50
        <Description("Barbados")> BB = 52
        <Description("Barein")> BH = 48
        <Description("Belgica")> BE = 56
        <Description("Belice")> BZ = 84
        <Description("Benin")> BJ = 204
        <Description("Bermudas")> BM = 60
        <Description("Bielorrusia")> BY = 112
        <Description("Bolivia")> BO = 68
        <Description("Bonaire San Eustaquio y Saba")> BQ = 535
        <Description("Bosnia y Herzegovina")> BA = 70
        <Description("Botsuana")> BW = 72
        <Description("Brasil")> BR = 76
        <Description("Brunei")> BN = 96
        <Description("Bulgaria")> BG = 100
        <Description("Burkina Faso")> BF = 854
        <Description("Burundi")> BI = 108
        <Description("Butan")> BT = 64
        <Description("Cabo Verde")> CV = 132
        <Description("Camboya")> KH = 116
        <Description("Camerun")> CM = 120
        <Description("Canada")> CA = 124
        <Description("Catar")> QA = 634
        <Description("Chad")> TD = 148
        <Description("Chile")> CL = 152
        <Description("China")> CN = 156
        <Description("Chipre")> CY = 196
        <Description("Colombia")> CO = 170
        <Description("Comoras")> KM = 174
        <Description("Corea del Norte")> KP = 408
        <Description("Corea del Sur")> KR = 410
        <Description("Costa de Marfil")> CI = 384
        <Description("Costa Rica")> CR = 188
        <Description("Croacia")> HR = 191
        <Description("Cuba")> CU = 192
        <Description("Curazao")> CW = 531
        <Description("Dinamarca")> DK = 208
        <Description("Dominica")> DM = 212
        <Description("Ecuador")> EC = 218
        <Description("Egipto")> EG = 818
        <Description("El Salvador")> SV = 222
        <Description("Emiratos Arabes Unidos")> AE = 784
        <Description("Eritrea")> ER = 232
        <Description("Eslovaquia")> SK = 703
        <Description("Eslovenia")> SI = 705
        <Description("Espana")> ES = 724
        <Description("Estados Unidos")> US = 840
        <Description("Estonia")> EE = 233
        <Description("Etiopia")> ET = 231
        <Description("Filipinas")> PH = 608
        <Description("Finlandia")> FI = 246
        <Description("Fiyi")> FJ = 242
        <Description("Francia")> FR = 250
        <Description("Gabon")> GA = 266
        <Description("Gambia")> GM = 270
        <Description("Georgia")> GE = 268
        <Description("Ghana")> GH = 288
        <Description("Gibraltar")> GI = 292
        <Description("Granada")> GD = 308
        <Description("Grecia")> GR = 300
        <Description("Groenlandia")> GL = 304
        <Description("Guadalupe")> GP = 312
        <Description("Guam")> GU = 316
        <Description("Guatemala")> GT = 320
        <Description("Guayana Francesa")> GF = 254
        <Description("Guernsey")> GG = 831
        <Description("Guinea")> GN = 324
        <Description("Guinea Bisau")> GW = 624
        <Description("Guinea Ecuatorial")> GQ = 226
        <Description("Guyana")> GY = 328
        <Description("Haiti")> HT = 332
        <Description("Honduras")> HN = 340
        <Description("Hong Kong")> HK = 344
        <Description("Hungria")> HU = 348
        <Description("India")> [IN] = 356
        <Description("Indonesia")> ID = 360
        <Description("Irak")> IQ = 368
        <Description("Iran")> IR = 364
        <Description("Irlanda")> IE = 372
        <Description("Isla Bouvet")> BV = 74
        <Description("Isla de Man")> IM = 833
        <Description("Isla de Navidad")> CX = 162
        <Description("Islandia")> [IS] = 352
        <Description("Islas Caiman")> KY = 136
        <Description("Islas Cocos")> CC = 166
        <Description("Islas Cook")> CK = 184
        <Description("Islas Feroe")> FO = 234
        <Description("Islas Georgias del Sur y Sandwich del Sur")> GS = 239
        <Description("Islas Heard y McDonald")> HM = 334
        <Description("Islas Malvinas")> FK = 238
        <Description("Islas Marianas del Norte")> MP = 580
        <Description("Islas Marshall")> MH = 584
        <Description("Islas Pitcairn")> PN = 612
        <Description("Islas Salomon")> SB = 90
        <Description("Islas Turcas y Caicos")> TC = 796
        <Description("Islas Ultramarinas de Estados Unidos")> UM = 581
        <Description("Islas Virgenes Britanicas")> VG = 92
        <Description("Islas Virgenes de los Estados Unidos")> VI = 850
        <Description("Israel")> IL = 376
        <Description("Italia")> IT = 380
        <Description("Jamaica")> JM = 388
        <Description("Japon")> JP = 392
        <Description("Jersey")> JE = 832
        <Description("Jordania")> JO = 400
        <Description("Kazajistan")> KZ = 398
        <Description("Kenia")> KE = 404
        <Description("Kirguistan")> KG = 417
        <Description("Kiribati")> KI = 296
        <Description("Kuwait")> KW = 414
        <Description("Laos")> LA = 418
        <Description("Lesoto")> LS = 426
        <Description("Letonia")> LV = 428
        <Description("Libano")> LB = 422
        <Description("Liberia")> LR = 430
        <Description("Libia")> LY = 434
        <Description("Liechtenstein")> LI = 438
        <Description("Lituania")> LT = 440
        <Description("Luxemburgo")> LU = 442
        <Description("Macao")> MO = 446
        <Description("Macedonia")> MK = 807
        <Description("Madagascar")> MG = 450
        <Description("Malasia")> MY = 458
        <Description("Malaui")> MW = 454
        <Description("Maldivas")> MV = 462
        <Description("Mali")> ML = 466
        <Description("Malta")> MT = 470
        <Description("Marruecos")> MA = 504
        <Description("Martinica")> MQ = 474
        <Description("Mauricio")> MU = 480
        <Description("Mauritania")> MR = 478
        <Description("Mayotte")> YT = 175
        <Description("Mexico")> MX = 484
        <Description("Micronesia")> FM = 583
        <Description("Moldavia")> MD = 498
        <Description("Monaco")> MC = 492
        <Description("Mongolia")> MN = 496
        <Description("Montenegro")> [ME] = 499
        <Description("Montserrat")> MS = 500
        <Description("Mozambique")> MZ = 508
        <Description("Myanmar")> MM = 104
        <Description("Namibia")> NA = 516
        <Description("Nauru")> NR = 520
        <Description("Nepal")> NP = 524
        <Description("Nicaragua")> NI = 558
        <Description("Niger")> NE = 562
        <Description("Nigeria")> NG = 566
        <Description("Niue")> NU = 570
        <Description("Norfolk")> NF = 574
        <Description("Noruega")> NO = 578
        <Description("Nueva Caledonia")> NC = 540
        <Description("Nueva Zelanda")> NZ = 554
        <Description("Oman")> OM = 512
        <Description("Paises Bajos")> NL = 528
        <Description("Pakistan")> PK = 586
        <Description("Palaos")> PW = 585
        <Description("Palestina")> PS = 275
        <Description("Panama")> PA = 591
        <Description("Papua Nueva Guinea")> PG = 598
        <Description("Paraguay")> PY = 600
        <Description("Peru")> PE = 604
        <Description("Polinesia Francesa")> PF = 258
        <Description("Polonia")> PL = 616
        <Description("Portugal")> PT = 620
        <Description("Puerto Rico")> PR = 630
        <Description("Reino Unido")> GB = 826
        <Description("Republica Arabe Saharaui Democratica")> EH = 732
        <Description("Republica Centroafricana")> CF = 140
        <Description("Republica Checa")> CZ = 203
        <Description("Republica del Congo")> CG = 178
        <Description("Republica Democratica del Congo")> CD = 180
        <Description("Republica Dominicana")> [DO] = 214
        <Description("Reunion")> RE = 638
        <Description("Ruanda")> RW = 646
        <Description("Rumania")> RO = 642
        <Description("Rusia")> RU = 643
        <Description("Samoa")> WS = 882
        <Description("Samoa Americana")> [AS] = 16
        <Description("San Bartolome")> BL = 652
        <Description("San Cristobal y Nieves")> KN = 659
        <Description("San Marino")> SM = 674
        <Description("San Martin")> MF = 663
        <Description("San Pedro y Miquelon")> PM = 666
        <Description("San Vicente y las Granadinas")> VC = 670
        <Description("Santa Elena Ascension y Tristan de Acuna")> SH = 654
        <Description("Santa Lucia")> LC = 662
        <Description("Santo Tome y Principe")> ST = 678
        <Description("Senegal")> SN = 686
        <Description("Serbia")> RS = 688
        <Description("Seychelles")> SC = 690
        <Description("Sierra Leona")> SL = 694
        <Description("Singapur")> SG = 702
        <Description("Sint Maarten")> SX = 534
        <Description("Siria")> SY = 760
        <Description("Somalia")> SO = 706
        <Description("Sri Lanka")> LK = 144
        <Description("Suazilandia")> SZ = 748
        <Description("Sudafrica")> ZA = 710
        <Description("Sudan")> SD = 729
        <Description("Sudan del Sur")> SS = 728
        <Description("Suecia")> SE = 752
        <Description("Suiza")> CH = 756
        <Description("Surinam")> SR = 740
        <Description("Svalbard y Jan Mayen")> SJ = 744
        <Description("Tailandia")> TH = 764
        <Description("Taiwan Republica de China")> TW = 158
        <Description("Tanzania")> TZ = 834
        <Description("Tayikistan")> TJ = 762
        <Description("Territorio Britanico del Oceano Indico")> IO = 86
        <Description("Tierras Australes y Antarticas Francesas")> TF = 260
        <Description("Timor Oriental")> TL = 626
        <Description("Togo")> TG = 768
        <Description("Tokelau")> TK = 772
        <Description("Tonga")> [TO] = 776
        <Description("Trinidad y Tobago")> TT = 780
        <Description("Tunez")> TN = 788
        <Description("Turkmenistan")> TM = 795
        <Description("Turquia")> TR = 792
        <Description("Tuvalu")> TV = 798
        <Description("Ucrania")> UA = 804
        <Description("Uganda")> UG = 800
        <Description("Uruguay")> UY = 858
        <Description("Uzbekistan")> UZ = 860
        <Description("Vanuatu")> VU = 548
        <Description("Ciudad del Vaticano")> VA = 336
        <Description("Venezuela")> VE = 862
        <Description("Vietnam")> VN = 704
        <Description("Wallis y Futuna")> WF = 876
        <Description("Yemen")> YE = 887
        <Description("Yibuti")> DJ = 262
        <Description("Zambia")> ZM = 894
        <Description("Zimbabue")> ZW = 716
        <Description("NoExisteCodigo")> NoExisteCodigo = 99
    End Enum

    Enum enumIVAalDia
        IVAalDia = 0
        NoIVAalDia = 1
    End Enum

    Enum enumSecretoProf
        OperacionNoAmparadaSecretoProf = 0
        OperacionAmparadaSecretoProf = 1
    End Enum
#End Region

#Region "Enum TRANSACT"
    Enum enumTipoImpresionTransAct
        NoImprimir = 9
        ImprimePOS = 0
        ImprimePDV = 1
    End Enum

    Enum enumTipoOperacion
        OperacionVenta = 0
        OperacionDevolucion = 1
    End Enum

    Enum enumTipoTarjetasAdquirente
        TarjetasTODAS = 0
        TarjetasMASTER = 1
        TarjetasVISA = 2
        TarjetaDINERS = 3
        TarjetaAMEX = 4
        TarjetaD = 5
        TarjetaOCA = 6
        TarjetaCABAL = 8
        TarjetaANDA = 9
        TarjetaCREDITEL = 12
        TarjetaPASSCARD = 14
        TarjetaLIDER = 15
        TarjetaCLUBDELESTE = 16
        TarjetaMAESTRO = 17
        TarjetaEDENRED = 19
        TarjetaSODEXO = 20
        TarjetaMIDINERO = 21
        TarjetaMIDES = 22
    End Enum

    Enum enumTipoTarjeta
        TarjetaTodas = 0
        TarjetaDebito = 1
        TarjetaCredito = 2
    End Enum

    Enum enumTipoCuenta
        CajaAhorroPesos = 1
        CuentaCorrientePesos = 2
        CajaAhorroDolares = 3
        CuentaCorrienteDolares = 4
    End Enum

    Enum enumTipoProcesador
        Todos = 0
        FirstData = 1
        Visanet = 2
        AMEX = 4
        TARJETAD = 5
        OCA = 6
        CABAL = 8
        ANDA = 9
        CREDITEL = 12
        PASSCARD = 14
        CLUBDELESTE = 16
        MIDES = 19
    End Enum

    Enum enumTipoMoneda
        Pesos = 1
        Dolares = 2
    End Enum
#End Region
#End Region

#Region "TAFACE"
#Region "TAFACEApiParsing"
#Region "Inicio y Fin"
    Public Function NuevaFactura(ByRef ErrorMsg As String, ByRef ErrorCod As Integer, ByVal TipoDeNegocio As ITAParsing_v0206.enumTipoDeNegocio, ByVal RutaCarpetaDebug As String) As Boolean
        Return objTAParsing.NuevaFactura(ErrorMsg, ErrorCod, TipoDeNegocio, RutaCarpetaDebug)
    End Function

    Public Function FinalizarFactura(ByRef ErrorMsg As String, ByRef ErrorCod As Integer, ByRef XMLFactura As String) As Boolean
        FinalizarFactura = False
        If objTAParsing.FinalizarFactura(ErrorMsg, ErrorCod, XMLFactura) Then
            UltimoXMLFactura = XMLFactura
            FinalizarFactura = True
        End If
    End Function
#End Region

#Region "Nodo - Datos Adicionales"
    Public Function DATOSADICIONALESsetDatos(ByRef ErrorMsg As String, ByRef ErrorCod As Integer, ByVal SOFTWAREFACTURADOR As String, ByVal VERSIONDESOFTWAREFACTURADOR As String,
                                             ByVal EMPRESARAZONSOCIAL As String, ByVal EMPRESARUT As Long, ByVal SUCURSALNRO As Integer, ByVal SUCURSALNOMBRE As String, ByVal CAJANRO As Integer,
                                             ByVal CAJERONRO As Long, ByVal CAJERONOMBRE As String, ByVal DOCUMENTOTIPO As String, ByVal DOCUMENTOSERIE As String, ByVal DOCUMENTONRO As Long,
                                             ByVal TRANSACCIONNRO As Long, ByVal CLIENTENRO As Long, ByVal CLIENTEDOC As String, ByVal CLIENTERAZONSOCIAL As String, ByVal CLIENTENOMBRE As String,
                                             ByVal CLIENTEDIRECCION As String, ByVal CLIENTETELEFONO As String, ByVal CLIENTEEMAIL As String, ByVal CLIENTEPAISNOM As String, ByVal VENDEDORNRO As Long,
                                             ByVal VENDEDORNOMBRE As String, ByVal VALORUNIDADINDEXADA As Double) As Boolean
        Return objTAParsing.DATOSADICIONALESsetDatos(ErrorMsg, ErrorCod, SOFTWAREFACTURADOR, VERSIONDESOFTWAREFACTURADOR, EMPRESARAZONSOCIAL, EMPRESARUT, SUCURSALNRO, SUCURSALNOMBRE, CAJANRO,
                                                     CAJERONRO, CAJERONOMBRE, DOCUMENTOTIPO, DOCUMENTOSERIE, DOCUMENTONRO, TRANSACCIONNRO, CLIENTENRO, CLIENTEDOC, CLIENTERAZONSOCIAL, CLIENTENOMBRE,
                                                     CLIENTEDIRECCION, CLIENTETELEFONO, CLIENTEEMAIL, CLIENTEPAISNOM, VENDEDORNRO, VENDEDORNOMBRE, VALORUNIDADINDEXADA)
    End Function
#End Region

#Region "Nodo - Datos Contingencia"
    Public Function DATOSCONTINGENCIAsetDatos(ByRef ErrorMsg As String, ByRef ErrorCod As Integer, ByVal TIPOCFE As Integer, ByVal SERIECOMPROBANTE As String, ByVal NROCOMPROBANTE As Long,
                                              ByVal CAENROAUTORIZACION As Long, ByVal CAENRODESDE As Long, ByVal CAENROHASTA As Long, ByVal CAEFECVENC As Date) As Boolean
        Return objTAParsing.DATOSCONTINGENCIAsetDatos(ErrorMsg, ErrorCod, TIPOCFE, SERIECOMPROBANTE, NROCOMPROBANTE, CAENROAUTORIZACION, CAENRODESDE, CAENROHASTA, CAEFECVENC)
    End Function
#End Region

#Region "Nodo - DGI"
    Public Function DGIA1setEncabezado(ByRef ErrorMsg As String, ByRef ErrorCod As Integer, ByVal TipoCFE As ITAParsing_v0206.enumTipoDeComprobanteCFE, ByVal FechaDeComprobante As Date,
                                       ByVal IndicadorTipoTraslado As ITAParsing_v0206.enumIndicadorTipoTraslado, ByVal PeriodoDesde As Date, ByVal PeriodoHasta As Date,
                                       ByVal IndicadorMontosBrutos As ITAParsing_v0206.enumIndicadorMontosBrutos, ByVal FormaDePago As ITAParsing_v0206.enumFormaDePago, ByVal FechaDeVencimiento As Date,
                                       ByVal ClausulaDeVenta As String, ByVal ModalidadDeVenta As ITAParsing_v0206.enumModalidadDeVenta, ByVal ViaDeTransporte As ITAParsing_v0206.enumViaDeTransporte,
                                       ByVal InfoAdicionalDoc As String, ByVal IVAalDia As ITAParsing_v0206.enumIVAalDia, ByVal SecretoProfesional As ITAParsing_v0206.enumSecretoProf,
                                       ByVal IndicadorPropMercaderiaTransp As ITAParsing_v0206.enumIndicadorPropiedadMercaderiaTransportada,
                                       ByVal TipoDocumentoMercaderiaTransp As ITAParsing_v0206.enumTipoDocumentoDelReceptor, ByVal CodigoPais As String,
                                       ByVal NroDocPropietarioUrugMercadTransp As String, ByVal NroDocPropietarioExtMercadTransp As String, ByVal NombrePropietarioMercadTransp As String) As Boolean

        Return objTAParsing.DGIA1setEncabezado(ErrorMsg, ErrorCod, TipoCFE, FechaDeComprobante, IndicadorTipoTraslado, PeriodoDesde, PeriodoHasta, IndicadorMontosBrutos, FormaDePago, FechaDeVencimiento,
                                               ClausulaDeVenta, ModalidadDeVenta, ViaDeTransporte, InfoAdicionalDoc, IVAalDia, SecretoProfesional, IndicadorPropMercaderiaTransp, TipoDocumentoMercaderiaTransp,
                                               CodigoPais, NroDocPropietarioUrugMercadTransp, NroDocPropietarioExtMercadTransp, NombrePropietarioMercadTransp)
    End Function

    Public Function DGIA2setEmisor(ByRef ErrorMsg As String, ByRef ErrorCod As Integer, ByVal RUCEmisor As Long, ByVal RazonSocialEmisor As String, ByVal NombreComercialEmisor As String,
                                   ByVal GiroNegocioEmisor As String, ByVal CorreoEmisor As String, ByVal NombreCasaPrincipalSucursalEmisor As String, ByVal CodigoCasaPrincipalSucursalEmisor As Integer,
                                   ByVal DomicilioFiscalEmisor As String, ByVal CiudadEmisor As String, ByVal DepartamentoEmisor As String, ByVal InfoAdicionalEmisor As String) As Boolean
        Return objTAParsing.DGIA2setEmisor(ErrorMsg, ErrorCod, RUCEmisor, RazonSocialEmisor, NombreComercialEmisor, GiroNegocioEmisor, CorreoEmisor, NombreCasaPrincipalSucursalEmisor,
                                           CodigoCasaPrincipalSucursalEmisor, DomicilioFiscalEmisor, CiudadEmisor, DepartamentoEmisor, InfoAdicionalEmisor)
    End Function

    Public Function DGIA3addTelefonoEmisor(ByRef ErrorMsg As String, ByRef ErrorCod As Integer, ByVal TelefonoEmisor As String) As Boolean
        Return objTAParsing.DGIA3addTelefonoEmisor(ErrorMsg, ErrorCod, TelefonoEmisor)
    End Function

    Public Function DGIA4setReceptor(ByRef ErrorMsg As String, ByRef ErrorCod As Integer, ByVal TipoDocumentoReceptor As ITAParsing_v0206.enumTipoDocumentoDelReceptor, ByVal CodigoPaisReceptor As String,
                                     ByVal NroDocumentoReceptorUruguayo As String, ByVal NroDocumentoReceptorExtranjero As String, ByVal RazonSocialReceptor As String, ByVal DireccionReceptor As String,
                                     ByVal CiudadReceptor As String, ByVal DepartamentoProviciaEstado As String, ByVal PaisReceptor As String, ByVal CodigoPostalReceptor As Integer,
                                     ByVal InfoAdicionalReceptor As String, ByVal LugarDestinoEntrega As String, ByVal NumeroOrdenCompra As String) As Boolean
        Return objTAParsing.DGIA4setReceptor(ErrorMsg, ErrorCod, TipoDocumentoReceptor, CodigoPaisReceptor, NroDocumentoReceptorUruguayo, NroDocumentoReceptorExtranjero, RazonSocialReceptor,
                                             DireccionReceptor, CiudadReceptor, DepartamentoProviciaEstado, PaisReceptor, CodigoPostalReceptor, InfoAdicionalReceptor, LugarDestinoEntrega,
                                             NumeroOrdenCompra)
    End Function

    Public Function DGIA5setTotalesDeEncabezado(ByRef ErrorMsg As String, ByRef ErrorCod As Integer, ByVal TipoMonedaTransaccion As String, ByVal TipoDeCambio As Double, ByVal TotalMontoNoGravado As Double,
                                                ByVal TotalMontoExportacionYAsimiladas As Double, ByVal TotalMontoImpuestoPercibido As Double, ByVal TotalMontoIVAEnSuspenso As Double,
                                                ByVal TotalMontoNetoIVATasaMinima As Double, ByVal TotalMontoNetoIVATasaBasica As Double, ByVal TotalMontoNetoIVAOtraTasa As Double, ByVal TasaMinimaIVA As Double,
                                                ByVal TasaBasicaIVA As Double, ByVal TotalIVATasaMinima As Double, ByVal TotalIVATasaBasica As Double, ByVal TotalIVAOtraTasa As Double, ByVal TotalMontoTotal As Double,
                                                ByVal TotalMontoRetenido As Double, ByVal CantLineasDetalle As Integer, ByVal MontoNoFacturable As Double, ByVal MontoTotalAPagar As Double, ByVal TotMntTotCredFisc As Double) As Boolean
        Return objTAParsing.DGIA5setTotalesDeEncabezado(ErrorMsg, ErrorCod, TipoMonedaTransaccion, TipoDeCambio, TotalMontoNoGravado, TotalMontoExportacionYAsimiladas, TotalMontoImpuestoPercibido,
                                                        TotalMontoIVAEnSuspenso, TotalMontoNetoIVATasaMinima, TotalMontoNetoIVATasaBasica, TotalMontoNetoIVAOtraTasa, TasaMinimaIVA, TasaBasicaIVA,
                                                        TotalIVATasaMinima, TotalIVATasaBasica, TotalIVAOtraTasa, TotalMontoTotal, TotalMontoRetenido, CantLineasDetalle, MontoNoFacturable,
                                                        MontoTotalAPagar, TotMntTotCredFisc)
    End Function

    Public Function DGIJ1setAdenda(ByRef ErrorMsg As String, ByRef ErrorCod As Integer, ByRef AdendaTexto As String, ByVal AdendaOtro As Byte()) As Boolean
        Return objTAParsing.DGIJ1setAdenda(ErrorMsg, ErrorCod, AdendaTexto, AdendaOtro)
    End Function

    Public Function DGIA6addTotalRetencionOPercepcion(ByRef ErrorMsg As String, ByRef ErrorCod As Integer, ByVal CodigoDeRetencionPercepcion As String, ByVal ValorRetencionPercepcion As Double) As Boolean
        Return objTAParsing.DGIA6addTotalRetencionOPercepcion(ErrorMsg, ErrorCod, CodigoDeRetencionPercepcion, ValorRetencionPercepcion)
    End Function

    Public Function DGIB1addDetalleNuevaLinea(ByRef ErrorMsg As String, ByRef ErrorCod As Integer, ByVal IndicadorDeFacturacion As ITAParsing_v0206.enumIndicadorDeFacturacionDetalle,
                                              ByVal IndicadorAgenteResponsable As String, ByVal NombreDelItem As String, ByVal DescripcionAdicional As String, ByVal Cantidad As Double,
                                              ByVal UnidadDeMedida As String, ByVal PrecioUnitario As Double, ByVal DescuentoEnPorcentaje As Double, ByVal MontoDescuento As Double,
                                              ByVal RecargoEnPorcentaje As Double, ByVal MontoRecargo As Double, ByVal MontoItem As Double) As Boolean
        Return objTAParsing.DGIB1addDetalleNuevaLinea(ErrorMsg, ErrorCod, IndicadorDeFacturacion, IndicadorAgenteResponsable, NombreDelItem, DescripcionAdicional, Cantidad, UnidadDeMedida,
                                                      PrecioUnitario, DescuentoEnPorcentaje, MontoDescuento, RecargoEnPorcentaje, MontoRecargo, MontoItem)
    End Function

    Public Function DGIB2addDetalleLineaActualNuevoCodigoItem(ByRef ErrorMsg As String, ByRef ErrorCod As Integer, ByVal TipoDeCodigoItem As ITAParsing_v0206.enumTipoDeCodigoItem,
                                                              ByVal CodigoDelItem As String) As Boolean
        Return objTAParsing.DGIB2addDetalleLineaActualNuevoCodigoItem(ErrorMsg, ErrorCod, TipoDeCodigoItem, CodigoDelItem)
    End Function

    Public Function DGIB3addDetalleLineaActualNuevoSubDescuento(ByRef ErrorMsg As String, ByRef ErrorCod As Integer, ByVal SubDescuentoTipo As ITAParsing_v0206.enumTipoSubDescuento,
                                                                ByVal SubDescuentoValor As Double) As Boolean
        Return objTAParsing.DGIB3addDetalleLineaActualNuevoSubDescuento(ErrorMsg, ErrorCod, SubDescuentoTipo, SubDescuentoValor)
    End Function

    Public Function DGIB4addDetalleLineaActualNuevoSubRecargo(ByRef ErrorMsg As String, ByRef ErrorCod As Integer, ByVal SubRecargoTipo As ITAParsing_v0206.enumTipoSubRecargo,
                                                              ByVal SubRecargoValor As Double) As Boolean
        Return objTAParsing.DGIB4addDetalleLineaActualNuevoSubRecargo(ErrorMsg, ErrorCod, SubRecargoTipo, SubRecargoValor)
    End Function

    Public Function DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ByRef ErrorMsg As String, ByRef ErrorCod As Integer, ByVal CodigoRetencionPercepcion As String, ByVal Tasa As Double,
                                                                        ByVal MontoSujetoARetencionPercepcion As Double, ByVal ValorDeRetencionPercepcion As Double, ByVal InformacionAdicional As String) As Boolean
        Return objTAParsing.DGIB5addDetalleLineaActualNuevaRetencionOPercepcion(ErrorMsg, ErrorCod, CodigoRetencionPercepcion, Tasa, MontoSujetoARetencionPercepcion, ValorDeRetencionPercepcion, InformacionAdicional)
    End Function

    Public Function DGIC1addSubTotalInformativo(ByRef ErrorMsg As String, ByRef ErrorCod As Integer, ByVal Glosa As String, ByVal Orden As Short, ByVal ValorDelSubTotal As Double) As Boolean
        Return objTAParsing.DGIC1addSubTotalInformativo(ErrorMsg, ErrorCod, Glosa, Orden, ValorDelSubTotal)
    End Function

    Public Function DGID1addDescuentoORecargoGlobal(ByRef ErrorMsg As String, ByRef ErrorCod As Integer, ByVal TipoDeMovimiento As ITAParsing_v0206.enumTipoMovimientoDescuentoORecargoGlobal,
                                                    ByVal TipoDescuentoORecargo As ITAParsing_v0206.enumTipoDescuentoORecargoGlobal, ByVal CodigoDelDescuentoORecargo As Short, ByVal Glosa As String,
                                                    ByVal Valor As Double, ByVal IndicadorDeFacturacion As ITAParsing_v0206.enumIndicadorDeFacturacionDescuentoGlobal) As Boolean
        Return objTAParsing.DGID1addDescuentoORecargoGlobal(ErrorMsg, ErrorCod, TipoDeMovimiento, TipoDescuentoORecargo, CodigoDelDescuentoORecargo, Glosa, Valor, IndicadorDeFacturacion)
    End Function

    Public Function DGIE1addMedioDePago(ByRef ErrorMsg As String, ByRef ErrorCod As Integer, ByVal CodigoMedioDePago As Short, ByVal Glosa As String, ByVal Orden As Short, ByVal ValorDelPago As Double) As Boolean
        Return objTAParsing.DGIE1addMedioDePago(ErrorMsg, ErrorCod, CodigoMedioDePago, Glosa, Orden, ValorDelPago)
    End Function

    Public Function DGIF1addInformacionDeReferencia(ByRef ErrorMsg As String, ByRef ErrorCod As Integer, ByVal IndicadorDeReferenciaGlobal As Short,
                                                    ByVal TipoCFEReferencia As ITAParsing_v0206.enumTipoDeComprobanteCFE, ByVal SerieCFEReferencia As String, ByVal NroCFEReferencia As Long,
                                                    ByVal RazonReferencia As String, ByVal FechaCFEReferencia As Date) As Boolean
        Return objTAParsing.DGIF1addInformacionDeReferencia(ErrorMsg, ErrorCod, IndicadorDeReferenciaGlobal, TipoCFEReferencia, SerieCFEReferencia, NroCFEReferencia, RazonReferencia, FechaCFEReferencia)
    End Function

    Public Function DGIK1setComplementoFiscal(ByRef ErrorMsg As String, ByRef ErrorCod As Integer, ByVal RUCEmisor As Long, ByVal TipoDocMandante As ITAParsing_v0206.enumTipoDocumentoDelMandante,
                                              ByVal CodPaisMandante As String, ByVal NroDocMandante As String, ByVal NombreMandante As String) As Boolean
        Return objTAParsing.DGIK1setComplementoFiscal(ErrorMsg, ErrorCod, RUCEmisor, TipoDocMandante, CodPaisMandante, NroDocMandante, NombreMandante)
    End Function

#End Region

#Region "Respuesta"
    Public Function UnParsingXMLRespuesta(ByVal XMLRESPUESTA As String, ByRef ERRORMSG As String, ByRef ERRORCOD As Integer, ByRef WARNINGMSG As String, ByRef FIRMADOOK As Boolean,
                                      ByRef FIRMADOFCHHORA As Date, ByRef CAENA As Long, ByRef CAENROINICIAL As Long, ByRef CAENROFINAL As Long, ByRef CAEVENCIMIENTO As Date,
                                      ByRef CAESERIE As String, ByRef CAENRO As Long, ByRef CODSEGURIDAD As String, ByRef URLPARAVERIFICARTEXTO As String, ByRef URLPARAVERIFICARQR As String,
                                      ByRef RESOLUCIONIVA As String) As Boolean
        Return objTAParsing.UnParsingXMLRespuesta(XMLRESPUESTA, ERRORMSG, ERRORCOD, WARNINGMSG, FIRMADOOK, FIRMADOFCHHORA, CAENA, CAENROINICIAL, CAENROFINAL, CAEVENCIMIENTO, CAESERIE, CAENRO,
                                                  CODSEGURIDAD, URLPARAVERIFICARTEXTO, URLPARAVERIFICARQR, RESOLUCIONIVA)
    End Function

    Public Function EnsamblarVoucherTransactAdenda(ByRef ErrorMsg As String, ByVal XMLFactura As String, ByVal voucherAdenda As String) As String
        Return objTAFEApi.EnsamblarVoucherTransactAdenda(ErrorMsg, XMLFactura, voucherAdenda)
    End Function

    Private Function ObtenerVoucherImpresionAdendaManual() As String
        Dim counter As Integer = 1
        Dim linea As String = ""
        While counter < 14
            Select Case counter
                Case 1
                    linea &= "                   " & IIf(TransActTarjetas.Transaccion.Respuesta.DatosTransaccion.Operacion = "VTA", "VENTA", "DEVOLUCION") & " " & TransActTarjetas.Transaccion.Respuesta.DatosTransaccion.Extendida.TarjetaNombre & vbCrLf
                Case 2
                    linea &= "              " & IIf(TransActTarjetas.Transaccion.Respuesta.TarjetaTipo = "DEB", "DEBITO", "CREDITO") & " - " & IIf(TransActTarjetas.Transaccion.Respuesta.EsOffline, "OFFLINE", "ONLINE" & " - " & IIf(TransActTarjetas.Transaccion.Respuesta.DatosTransaccion.Extendida.TarjetaMedio = "BAN", "BANDA", "CHIP")) & vbCrLf
                Case 3
                    linea &= "  " & "Ticket: " & TransActTarjetas.Transaccion.Respuesta.Ticket & "                    " & " Lote: " & TransActTarjetas.Transaccion.Respuesta.Lote & vbCrLf
                Case 4
                    linea &= "  " & "Tar:  " & TransActTarjetas.Transaccion.Respuesta.DatosTransaccion.TarjetaNro & "          " & "Vto: " & TransActTarjetas.Transaccion.Respuesta.DatosTransaccion.Extendida.TarjetaVencimiento & vbCrLf
                Case 5
                    linea &= "  " & "Tipo Plan: " & TransActTarjetas.Transaccion.Respuesta.DatosTransaccion.Extendida.PlanNombre & "(" & TransActTarjetas.Transaccion.Respuesta.DatosTransaccion.Extendida.PlanNroTipoPlan & ")" & vbCrLf
                Case 6
                    linea &= "  " & "Plan/Cuotas: " & TransActTarjetas.Transaccion.Respuesta.DatosTransaccion.Extendida.PlanNroPlan & "/" & TransActTarjetas.Transaccion.Respuesta.DatosTransaccion.Cuotas & "                " & "Aut: " & TransActTarjetas.Transaccion.Respuesta.NroAutorizacion & vbCrLf
                Case 7
                    linea &= "  " & "No Fact: " & TransActTarjetas.Transaccion.Respuesta.DatosTransaccion.Extendida.FacturaNro & vbCrLf
                Case 8
                    linea &= "  " & "Importe: " & "                       " & IIf(TransActTarjetas.Transaccion.Respuesta.DatosTransaccion.MonedaISO = "0858", "$ ", "U$S ") & Math.Round(TransActTarjetas.Transaccion.Respuesta.DatosTransaccion.Monto / 100, 2).ToString("N2") & vbCrLf
                Case 9
                    linea &= "  " & "Ley: " & TransActTarjetas.Transaccion.Respuesta.DatosTransaccion.DecretoLeyNro & ":" & "                     " & IIf(TransActTarjetas.Transaccion.Respuesta.DatosTransaccion.MonedaISO = "0858", "$ ", "U$S ") & Math.Round(TransActTarjetas.Transaccion.Respuesta.DatosTransaccion.DecretoLeyMonto / 100, 2).ToString("N2") & vbCrLf
                Case 10
                    linea &= "           " & IIf(TransActTarjetas.Transaccion.Respuesta.DatosTransaccion.DecretoLeyAplicado, "Aplica dev. IVA-Ley " & TransActTarjetas.Transaccion.Respuesta.DatosTransaccion.DecretoLeyNro, "        No aplica dev.") & vbCrLf
                    linea &= "        Sin nro. factura no aplica ley" & vbCrLf
                Case 11
                    linea &= "  " & "TOTAL Factura: " & "                 " & IIf(TransActTarjetas.Transaccion.Respuesta.DatosTransaccion.MonedaISO = "0858", "$ ", "U$S ") & Math.Round(TransActTarjetas.Transaccion.Respuesta.DatosTransaccion.Extendida.FacturaMonto / 100, 2).ToString("N2") & vbCrLf
                Case 12
                    linea &= "  " & "Importe Gravado TRX: " & "           " & IIf(TransActTarjetas.Transaccion.Respuesta.DatosTransaccion.MonedaISO = "0858", "$ ", "U$S ") & Math.Round(TransActTarjetas.Transaccion.Respuesta.DatosTransaccion.Extendida.FacturaMontoGravado / 100, 2).ToString("N2") & vbCrLf
                Case 13
                    linea &= "  " & "TOTAL: " & "                         " & IIf(TransActTarjetas.Transaccion.Respuesta.DatosTransaccion.MonedaISO = "0858", "$ ", "U$S ") & Math.Round((TransActTarjetas.Transaccion.Respuesta.DatosTransaccion.Monto - TransActTarjetas.Transaccion.Respuesta.DatosTransaccion.DecretoLeyMonto + TransActTarjetas.Transaccion.Respuesta.DatosTransaccion.MontoPropina + TransActTarjetas.Transaccion.Respuesta.DatosTransaccion.MontoCashBack) / 100, 2).ToString("N2") & vbCrLf
            End Select
            counter += 1
        End While
        Return linea
    End Function
#End Region
#End Region

#Region "TAFEApi"
#Region "Seteo de la API"
    Public Function InicializarImpresion(ByRef ErrorMsg As String, ByRef ErrorCod As Integer, ByVal NombreImpresora As String, ByVal TipoImpresora As Integer) As Boolean
        TipoImpresoraTAFACE = TipoImpresora
        NombreImpresoraTAFACE = NombreImpresora
        Return objTAFEApi.InicializarImpresion(ErrorMsg, ErrorCod, NombreImpresora, TipoImpresora)
    End Function

    Public Function Inicializar(ByRef ErrorMsg As String, ByRef ErrorCod As Integer, ByVal UrlServidorTAFirmoGateway As String, ByVal SegundosTimeout As Integer, ByVal EmpresaRUT As Long, ByVal SucursalId As Integer, ByVal CajaId As Integer, Optional ByVal RutaCarpetaOperacion As String = "") As Boolean
        Return objTAFEApi.Inicializar(ErrorMsg, ErrorCod, UrlServidorTAFirmoGateway, SegundosTimeout, EmpresaRUT, SucursalId, CajaId, RutaCarpetaOperacion)
    End Function
#End Region

#Region "Metodos de Funcionamiento"
    Public Function FirmarFactura(ByRef ErrorMsg As String, ByRef ErrorCod As Integer, ByVal XMLFactura As String, ByRef XMLRespuesta As String) As Boolean
        FirmarFactura = False
        If objTAFEApi.FirmarFactura(ErrorMsg, ErrorCod, XMLFactura, XMLRespuesta) Then
            UltimoXMLRespuesta = XMLRespuesta
            FirmarFactura = True
        End If
    End Function

    Public Function FacturaEsAnulable(ByRef ErrorMsg As String, ByRef ErrorCod As Integer, ByVal EmpresaRUC As Long, ByVal SucursalId As Short, ByVal Cajaid As Short, ByVal TipoDeComprobante As Integer, ByVal SerieComprobante As String, ByVal NroComprobante As Integer, ByVal Anulable As Boolean, ByVal Motivo As String) As Boolean
        Return objTAFEApi.FacturaEsAnulable(ErrorMsg, ErrorCod, EmpresaRUC, SucursalId, Cajaid, TipoDeComprobante, SerieComprobante, NroComprobante, Anulable, Motivo)
    End Function

    Public Function AnularFactura(ByRef ErrorMsg As String, ByRef ErrorCod As Integer, ByVal EmpresaRUC As Long, ByVal SucursalId As Short, ByVal Cajaid As Short, ByVal TipoDeComprobante As Integer, ByVal SerieComprobante As String, ByVal NroComprobante As Integer) As Boolean
        Return objTAFEApi.AnularFactura(ErrorMsg, ErrorCod, EmpresaRUC, SucursalId, Cajaid, TipoDeComprobante, SerieComprobante, NroComprobante)
    End Function

    Public Function ReimprimirFactura(ByRef ErrorMsg As String, ByVal ErrorCod As Integer, ByVal EmpresaRUC As Long, ByVal SucursalId As Short, ByVal Cajaid As Short, ByVal TipoDeComprobante As Integer, ByVal SerieComprobante As String, ByVal NroComprobante As Integer) As Boolean
        Return objTAFEApi.ReimprimirFactura(ErrorMsg, ErrorCod, EmpresaRUC, SucursalId, Cajaid, TipoDeComprobante, SerieComprobante, NroComprobante)
    End Function

    Public Function ImprimirFactura(ByRef ErrorMsg As String, ByVal ErrorCod As Integer, ByVal EmpresaRUC As Long, ByVal SucursalId As Short, ByVal Cajaid As Short, ByVal TipoDeComprobante As Integer, ByVal SerieComprobante As String, ByVal NroComprobante As Integer) As Boolean
        Return objTAFEApi.ImprimirFactura(ErrorMsg, ErrorCod, EmpresaRUC, SucursalId, Cajaid, TipoDeComprobante, SerieComprobante, NroComprobante)
    End Function

    Public Function Sincronizar(ByRef ErrorMsg As String, ByRef ErrorCod As Integer) As Boolean
        Return objTAFEApi.Sincronizar(ErrorMsg, ErrorCod)
    End Function

    Public Function CerrarCaja(ByRef ErrorMsg As String, ByRef ErrorCod As Integer, ByVal UrlServidorTAFirmoGateway As String, ByVal EmpresaRUT As Long, ByVal SucursalId As Integer, ByVal CajaId As Integer, Optional ByVal RutaCarpetaOperacion As String = "") As Boolean
        Return objTAFEApi.CerrarCaja(ErrorMsg, ErrorCod, UrlServidorTAFirmoGateway, EmpresaRUT, SucursalId, CajaId, RutaCarpetaOperacion)
    End Function

    Public Function ExisteCFC(ByRef ErrorMsg As String, ByRef ErrorCod As Integer, ByVal TipoCFC As Integer, ByVal SerieCFC As String, ByVal NroCFC As Long, ByVal NroAutorizacionCFC As Long, ByVal NroDesdeCFC As Long, ByVal NroHastaCFC As Long, ByVal FechaVencimientoCFC As Date) As Boolean
        Return objTAFEApi.ExisteCFC(ErrorMsg, ErrorCod, TipoCFC, SerieCFC, NroCFC, NroAutorizacionCFC, NroDesdeCFC, NroHastaCFC, FechaVencimientoCFC)
    End Function

    Public Function FirmarFacturaConReserva(ByRef ErrorMsg As String, ByRef ErrorCod As Integer, ByVal XMLFactura As String, ByRef XMLRespuesta As String, ByVal CAENroAutorizacion As Long, ByVal CAESerie As String, ByVal CAENroReservado As Long) As Boolean
        FirmarFacturaConReserva = False
        If objTAFEApi.FirmarFacturaConReserva(ErrorMsg, ErrorCod, XMLFactura, XMLRespuesta, CAENroAutorizacion, CAESerie, CAENroReservado) Then
            UltimoXMLRespuesta = XMLRespuesta
            FirmarFacturaConReserva = True
        End If
    End Function

    Public Function ReservarNroCAE(ByRef ErrorCod As Integer, ByRef ErrorMsg As String, ByVal TipoCFE As Integer, ByRef CAENroAutorizacion As Long, ByRef CAESerie As String, ByRef CAENroReservado As Long) As Boolean
        Return objTAFEApi.ReservarNroCAE(ErrorCod, ErrorMsg, TipoCFE, CAENroAutorizacion, CAESerie, CAENroReservado)
    End Function

    Public Function ReservaNroCAEEstaVigente(ByRef ErrorCod As Integer, ByRef ErrorMsg As String, ByRef CAENroAutorizacion As Long, ByVal CAESerie As String, ByVal CAENroReservado As Long, ByRef ReservaVigente As Boolean) As Boolean
        Return objTAFEApi.ReservaNroCAEEstaVigente(ErrorCod, ErrorMsg, CAENroAutorizacion, CAESerie, CAENroReservado, ReservaVigente)
    End Function

    Public Function CancelarReservaNroCAE(ByRef ErrorCod As Integer, ByRef ErrorMsg As String, ByVal CAENroAutorizacion As Long, ByVal CAESerie As String, ByVal CAENroReservado As Long) As Boolean
        Return objTAFEApi.CancelarReservaNroCAE(ErrorCod, ErrorMsg, CAENroAutorizacion, CAESerie, CAENroReservado)
    End Function

    Public Function Finalizar() As Boolean
        Return objTAFEApi.Finalizar()
    End Function
#End Region

#Region "Generar QR"
    Public Function GenerarQRCodeGX(ByRef ErrorMsg As String, ByRef ErrorCod As Integer, ByVal Url As String, ByRef ImagenQRBase64 As String) As Boolean
        Return objTAFEApi.GenerarQRCodeGX(ErrorMsg, ErrorCod, Url, ImagenQRBase64)
    End Function

    Public Function GenerarQRCode(ByRef ErrorMsg As String, ByRef ErrorCod As Integer, ByVal Url As String, ByRef ImagenQR As Object) As Boolean
        Return objTAFEApi.GenerarQRCode(ErrorMsg, ErrorCod, Url, ImagenQR)
    End Function

    Public Function GenerarQRCodeVB6(ByRef ErrorMsg As String, ByRef ErrorCod As Integer, ByVal Url As String, ByRef IPD As Object) As Boolean
        Return objTAFEApi.GenerarQRCodeVB6(ErrorMsg, ErrorCod, Url, IPD)
    End Function
#End Region

#Region "Emulador de Gateway"
    Public Sub ActivarEmulacionGatewayOK()
        objTAFEApi.ActivarEmulacionGatewayOK()
    End Sub
#End Region
#End Region
#End Region

#Region "TransAct"
    Public Function NuevaTransaccion(ByRef ErrorMsg As String, ByVal EmpCod As String, ByVal TermCod As String, ByVal TipoOperacion As enumTipoOperacion,
                                     ByVal TipoMoneda As enumTipoMoneda, ByVal NroTicketOriginal As Integer, ByVal TipoTarjetaAdquirente As enumTipoTarjetasAdquirente,
                                     ByVal TipoEmisor As Integer, ByVal TipoTarjeta As enumTipoTarjeta, ByVal TipoProcesador As enumTipoProcesador) As Boolean
        NuevaTransaccion = False
        Try
            'Creo una NuevaTransacción
            TransActTarjetas.Transaccion.NuevaTransaccion()

            'Seteo Valores de Opercion
            TransActTarjetas.Transaccion.EmpCod = Trim(EmpCod)
            TransActTarjetas.Transaccion.TermCod = Trim(TermCod)
            If TipoOperacion = enumTipoOperacion.OperacionVenta Then
                TransActTarjetas.Transaccion.Operacion = "VTA"
            Else
                TransActTarjetas.Transaccion.Operacion = "DEV"
            End If
            If TipoOperacion = enumTipoOperacion.OperacionDevolucion AndAlso NroTicketOriginal <> 0 Then
                TransActTarjetas.Transaccion.TicketOriginal = NroTicketOriginal
            End If

            'Seteo el id de tarjeta para restringir cual quiero que utilice el cliente
            TransActTarjetas.Transaccion.TarjetaId = TipoTarjetaAdquirente

            TransActTarjetas.Transaccion.EmisorId = TipoEmisor

            'Seteo el tipo de tarjeta para restringir cual quiero que utilice el cliente
            Select Case TipoTarjeta
                Case enumTipoTarjeta.TarjetaDebito
                    TransActTarjetas.Transaccion.TarjetaTipo = "DEB"
                Case enumTipoTarjeta.TarjetaCredito
                    TransActTarjetas.Transaccion.TarjetaTipo = "CRE"
            End Select
            If TipoMoneda = enumTipoMoneda.Pesos Then
                TransActTarjetas.Transaccion.MonedaISO = "0858"
            Else
                TransActTarjetas.Transaccion.MonedaISO = "0840"
            End If
            NuevaTransaccion = True
        Catch ex As Exception
            ErrorMsg = "Error NuevaTransaccion: " & ex.Message
        End Try
    End Function

    Public Function CargarConfiguracion(ByRef ErrorMsg As String, ByVal ModoEmulacion As Boolean, ByVal TipoImpresion As enumTipoImpresionTransAct, ByVal GUITipo As Integer, ByVal GuiModo As Integer, Optional GUIMostrarTouchPad As Integer = False) As Boolean
        CargarConfiguracion = False
        Try
            TransActTarjetas.Configuracion.ModoEmulacion = ModoEmulacion
            TransActTarjetas.Configuracion.GUI.GUIMostrarTouchPad = GUIMostrarTouchPad
            TransActTarjetas.Configuracion.GUI.GUITipo = GUITipo
            TransActTarjetas.Configuracion.GUI.GUIModo = GuiModo
            If TipoImpresion = enumTipoImpresionTransAct.NoImprimir Then
                TransActTarjetas.Configuracion.Impresion.ImpresionTipo = TipoImpresion
            Else
                TransActTarjetas.Configuracion.Impresion.ImpresionModo = TipoImpresion
            End If
            CargarConfiguracion = True
        Catch ex As Exception
            ErrorMsg = "Error CargarConfiguracion" & ex.Message
        End Try
    End Function

    Public Function CargarDatosTransaccion(ByRef ErrorMsg As String, ByVal NroFactura As Double, ByVal MontoTransaccion As Double, ByVal MontoFactura As Double, ByVal MontoGravadoFactura As Double,
                                           ByVal MontoIVAFactura As Double, ByVal MontoPropina As Double, ByVal MontoCashBack As Double, ByVal CantidadCuotas As Integer, ByVal FacturaConsumidorFinal As Boolean,
                                           ByVal IdDecretoLey As Integer, ByVal TipoCuenta As enumTipoCuenta) As Boolean
        CargarDatosTransaccion = False
        Try
            TransActTarjetas.Transaccion.Monto = CDec(MontoTransaccion) * 100
            TransActTarjetas.Transaccion.MontoPropina = MontoPropina * 100
            TransActTarjetas.Transaccion.MontoCashBack = MontoCashBack * 100

            'Seteo la cantidad de cuotas (solo aplica para credito)
            TransActTarjetas.Transaccion.Extendida.Cuotas = CantidadCuotas

            'Seteo Valores de Factura
            TransActTarjetas.Transaccion.FacturaNro = NroFactura
            TransActTarjetas.Transaccion.FacturaMonto = MontoFactura * 100
            TransActTarjetas.Transaccion.FacturaMontoGravado = MontoGravadoFactura * 100
            TransActTarjetas.Transaccion.FacturaMontoIVA = MontoIVAFactura * 100
            TransActTarjetas.Transaccion.FacturaConsumidorFinal = FacturaConsumidorFinal
            CargarDatosTransaccion = True
        Catch ex As Exception
            ErrorMsg = "Error CargarDatosTransaccion: " & ex.Message
        End Try
    End Function

    Public Function CargarDatosTransaccionExtendida(ByRef ErrorMsg As String, ByVal IdDecretoLey As Integer, ByVal TarjetaNro As Double, ByVal TarjetaTitular As String, ByVal TarjetaVencimento As String, ByVal TarjetaControl As String,
                                                    ByVal TarjetaCVC As String, ByVal TarjetaDocIdentidad As String, ByVal PlanId As Integer, ByVal TipoCuentaId As Integer) As Boolean
        CargarDatosTransaccionExtendida = False
        Try
            If IdDecretoLey <> 0 Then
                TransActTarjetas.Transaccion.Extendida.DecretoLeyId = IdDecretoLey
            End If
            'Seteo Valores de la Tarjeta
            If TarjetaNro <> 0 Then TransActTarjetas.Transaccion.Extendida.TarjetaNro = TarjetaNro
            If Trim(TarjetaTitular) <> "" Then TransActTarjetas.Transaccion.Extendida.TarjetaTitular = Trim(TarjetaTitular)
            If Trim(TarjetaVencimento) <> "" Then TransActTarjetas.Transaccion.Extendida.TarjetaVencimento = Trim(TarjetaVencimento)
            If Trim(TarjetaControl) <> "" Then TransActTarjetas.Transaccion.Extendida.TarjetaControl = Trim(TarjetaControl)
            If Trim(TarjetaCVC) <> "" Then TransActTarjetas.Transaccion.Extendida.TarjetaCVC = Trim(TarjetaCVC)
            If Trim(TarjetaDocIdentidad) <> "" Then TransActTarjetas.Transaccion.Extendida.TarjetaDocIdentidad = Trim(TarjetaDocIdentidad)

            'Seteo Plan a utilizar
            If PlanId <> 0 Then
                TransActTarjetas.Transaccion.Extendida.PlanId = PlanId
            End If

            'Seteo Tipo de Cuenta (solo aplica para debito)
            TransActTarjetas.Transaccion.Extendida.TipoCuentaId = TipoCuentaId

            CargarDatosTransaccionExtendida = True
        Catch ex As Exception
            ErrorMsg = "Error CargarDatosTransaccionExtendida: " & ex.Message
        End Try
    End Function

    Public Sub ConfigurarComportamientoTransaccion(ByVal ModificarMoneda As Boolean, ByVal ModificarMontos As Boolean, ByVal ModificarCuotas As Boolean, ByVal ModificarFactura As Boolean, ByVal ModificarTarjeta As Boolean,
                                                   ByVal ModificarPlan As Boolean, ByVal ModificarDecretoLey As Boolean, ByVal ModificarTipoCuenta As Boolean)
        If ModificarMoneda Then TransActTarjetas.Transaccion.Comportamiento.ModificarMoneda = True
        If ModificarMontos Then TransActTarjetas.Transaccion.Comportamiento.ModificarMontos = True
        If ModificarCuotas Then TransActTarjetas.Transaccion.Comportamiento.ModificarCuotas = True
        If ModificarFactura Then TransActTarjetas.Transaccion.Comportamiento.ModificarFactura = True
        If ModificarTarjeta Then TransActTarjetas.Transaccion.Comportamiento.ModificarTarjeta = True
        If ModificarPlan Then TransActTarjetas.Transaccion.Comportamiento.ModificarPlan = True
        If ModificarDecretoLey Then TransActTarjetas.Transaccion.Comportamiento.ModificarDecretoLey = True
        If ModificarTipoCuenta Then TransActTarjetas.Transaccion.Comportamiento.ModificarTipoCuenta = True
    End Sub

    Public Function ProcesarTransaccion(ByRef ErrorMsg As String, ByRef TrnId As Double, ByRef Ticket As Double, ByRef Lote As Double, ByRef NroAutorizacion As String) As Boolean
        'Realizo la Transacción
        TransActTarjetas.Transaccion.ProcesarTransaccion()

        'Verifico Respuesta del Autorizador
        If Not TransActTarjetas.Transaccion.Respuesta.Aprobada Then
            ErrorMsg = "Denegada - Respuesta: " & vbCrLf & TransActTarjetas.Transaccion.Respuesta.CodRespuesta & " - " & TransActTarjetas.Transaccion.Respuesta.MsgRespuesta
            Return False
        End If

        TrnId = TransActTarjetas.Transaccion.Respuesta.TransaccionId
        Ticket = TransActTarjetas.Transaccion.Respuesta.Ticket
        Lote = TransActTarjetas.Transaccion.Respuesta.Lote
        NroAutorizacion = TransActTarjetas.Transaccion.Respuesta.NroAutorizacion

        Return True
    End Function

    Public Function ConfirmarTransAct(ByRef ErrorMsg As String, ByVal TrnId As Double) As Boolean
        If Not TransActTarjetas.Transaccion.ConfirmarTransaccion(TrnId) Then
            ErrorMsg = "Error Confirmando Transaccion: " & vbCrLf & TransActTarjetas.Transaccion.Respuesta.CodRespuesta & " - " & TransActTarjetas.Transaccion.Respuesta.MsgRespuesta & ". "
            Return False
        End If

        Return True
    End Function

    Public Function ReversarTransaccion(ByRef ErrorMsg As String, ByVal TrnId As Double) As Boolean
        ReversarTransaccion = False
        Try
            If TransActTarjetas.Transaccion.ReversarTransaccion(TrnId) Then
                ReversarTransaccion = True
            Else
                ErrorMsg = TransActTarjetas.Transaccion.Respuesta.MsgRespuesta & ". "
            End If
        Catch ex As Exception
            ErrorMsg = "Error ReversarTransaccion: " & ex.Message
        End Try
    End Function

    Public Function CerrarLote(ByRef ErrorMsg As String, ByVal EmpCod As String, ByVal TermCod As String, ByVal TipoProcesador As enumTipoProcesador, ByVal CierreCentralizado As Boolean) As Boolean
        CerrarLote = False

        Try
            TransActTarjetas.CierreLote.EmpCod = Trim(EmpCod)
            TransActTarjetas.CierreLote.TermCod = Trim(TermCod)
            TransActTarjetas.CierreLote.ProcesadorId = TipoProcesador
            TransActTarjetas.CierreLote.CierreCentralizado = CierreCentralizado

            TransActTarjetas.CierreLote.ProcesarCierre()

            If TransActTarjetas.CierreLote.Respuesta.Finalizado Then
                ErrorMsg = "Error cerrando lotes: " & TransActTarjetas.CierreLote.Respuesta.Estado & ". "
            End If

            Dim TokenCierre As String = TransActTarjetas.CierreLote.Respuesta.TokenCierre
            'Cada 5 segundos verifico el estado de avance del procesamiento de cierre
            Dim ResultOK As Boolean = False
            While Not TransActTarjetas.CierreLote.Respuesta.Finalizado
                Threading.Thread.Sleep(3000)
                ResultOK = TransActTarjetas.CierreLote.ConsultarEstadoCierre(TokenCierre)
                If Not TransActTarjetas.CierreLote.Respuesta.Finalizado Then
                    ErrorMsg = "Cerrando Lotes: " & TransActTarjetas.CierreLote.Respuesta.Estado & ". "
                End If
            End While
            If ResultOK Then
                For Each Cierre In TransActTarjetas.CierreLote.Respuesta.DatosCierre
                    ' MsgBox("Lote " & Cierre.Lote & " de Procesador " & Cierre.ProcesadorId & " " & IIf(Cierre.Aprobado, "APROBADO", "DENEGADO"), MsgBoxStyle.Information)
                Next
                CerrarLote = True
            Else
                ErrorMsg = "Error Cerrando Lotes: " & TransActTarjetas.CierreLote.Respuesta.Estado & ". "
            End If

        Catch ex As Exception
            ErrorMsg = "Error CerrarLote: " & ErrorMsg & ex.Message
        End Try
    End Function

    Public Function ConsultarUltimoCierreLote(ByRef ErrorMsg As String, ByVal EmpCod As String, ByVal TermCod As String) As Boolean
        ConsultarUltimoCierreLote = False
        Try
            TransActTarjetas.CierreLote.EmpCod = Trim(EmpCod)
            TransActTarjetas.CierreLote.TermCod = Trim(TermCod)
            Dim ResultOK As Boolean = False
            ResultOK = TransActTarjetas.CierreLote.ConsultarEstadoCierre("ULTIMO")
            If Not TransActTarjetas.CierreLote.Respuesta.Finalizado Then
                ErrorMsg = "Cerrando Lotes: " & TransActTarjetas.CierreLote.Respuesta.Estado & ". "
            End If
            If ResultOK Then
                ConsultarUltimoCierreLote = True
            End If
        Catch ex As Exception
            ErrorMsg = "Error ConsultarUltimoCierreLote: " & ErrorMsg & ex.Message & ". "
        End Try
    End Function

    Public Function LeerTarjeta(ByRef ErrorMsg As String, ByVal EmpCod As String, ByVal TermCod As String, ByRef Respuesta As String, ByRef Track1 As String, ByRef Track2 As String, ByRef Track3 As String) As Boolean
        LeerTarjeta = False
        Try
            TransActTarjetas.LectorTarjeta.EmpCod = Trim(EmpCod)
            TransActTarjetas.LectorTarjeta.TermCod = Trim(TermCod)
            If TransActTarjetas.LectorTarjeta.LeerTarjeta() Then
                Respuesta = TransActTarjetas.LectorTarjeta.Respuesta.MsgRespuesta
                Track1 = TransActTarjetas.LectorTarjeta.Respuesta.Track1
                Track2 = TransActTarjetas.LectorTarjeta.Respuesta.Track2
                Track3 = TransActTarjetas.LectorTarjeta.Respuesta.Track3
            Else
                ErrorMsg = TransActTarjetas.LectorTarjeta.Respuesta.MsgRespuesta
            End If
        Catch ex As Exception
            ErrorMsg = "Error LeerTarjeta: " & ErrorMsg & ex.Message & ". "
        End Try
    End Function

    Public Function Imprimir(ByRef ErrorMsg As String, ByVal ErrorCod As Integer, ByVal ImprimeVoucherAdenda As Boolean, ByVal XMLEntrada As String, XMLRespuesta As String, ByVal voucherComercio As String(), ByVal EsReimpresion As Boolean, ByVal Copias As Integer) As Boolean
        Imprimir = False
        objImpresionBrigde.ImpresoraPorDefecto = NombreImpresoraTAFACE
        objImpresionBrigde.TipoImpresoraDefecto = ObtenerTipoImpresoraTransAct()

        If ImprimeVoucherAdenda Then
            Dim voucherAdenda As String = EnsamblarVoucherImpresionAdenda(voucherComercio)
            Dim XMLFactura As String = EnsamblarVoucherTransactAdenda(ErrorMsg, XMLEntrada, voucherAdenda)
            objTAFEApi.ImprimirFacturaTransActTAFACE(ErrorMsg, ErrorCod, XMLFactura, XMLRespuesta, EsReimpresion)
            objImpresionBrigde.Imprimir(ObtenerTipoImpresoraTransAct, NombreImpresoraTAFACE, Copias, ObtenerCopiaComercio(voucherComercio))
            Imprimir = True
        Else
            objImpresionBrigde.Imprimir(ObtenerTipoImpresoraTransAct, NombreImpresoraTAFACE, Copias, ObtenerVoucher(voucherComercio))
            Imprimir = True
        End If
    End Function

    Private Function ObtenerTipoImpresoraTransAct() As Integer
        ObtenerTipoImpresoraTransAct = 7
        Select Case TipoImpresoraTAFACE
            Case 1
                ObtenerTipoImpresoraTransAct = 4
            Case 2
                ObtenerTipoImpresoraTransAct = 1
            Case 3
                ObtenerTipoImpresoraTransAct = 6
            Case 4
                ObtenerTipoImpresoraTransAct = 1
            Case 5
                ObtenerTipoImpresoraTransAct = 5
            Case 6
                ObtenerTipoImpresoraTransAct = 2
            Case 7
                ObtenerTipoImpresoraTransAct = 3
            Case 8
                ObtenerTipoImpresoraTransAct = 3
        End Select
    End Function

    Private Function ObtenerCopiaComercio(ByVal voucherRenderizado As String()) As String
        Dim voucher As String = ""
        Dim copiaComercio As New List(Of String)
        Dim finCopiaComercio As Boolean = False
        For Each linea As String In voucherRenderizado
            If linea.Contains("COPIA COMERCIO") Then
                finCopiaComercio = True
                copiaComercio.Add(linea)
            End If
            If Not finCopiaComercio Then
                copiaComercio.Add(linea)
            End If
        Next
        For Each linea As String In copiaComercio
            voucher &= linea & vbCrLf
        Next
        Return vbCrLf & voucher & vbCrLf
    End Function

    'Obtener datos del voucher
    Private Function ObtenerVoucher(ByVal voucherRenderizado As String()) As String
        Dim voucher As String = ""
        Dim counter As Integer = 1
        Dim finCopiaComercio As Boolean = False
        For Each linea As String In voucherRenderizado
            voucher &= linea & vbCrLf
        Next
        Return voucher
    End Function

    'Obtener datos de la copia cliente en el voucher
    Private Function EnsamblarVoucherImpresionAdenda(ByVal voucherRenderizado As String()) As String
        Dim voucher As String = ""
        Dim counter As Integer = 1
        Dim copiaComercio As New List(Of String)
        Dim inicioCopiaComercio As Boolean = False
        For Each linea As String In voucherRenderizado
            If linea.Contains("COPIA COMERCIO") Then
                inicioCopiaComercio = True
            End If
            If inicioCopiaComercio Then
                copiaComercio.Add(linea)
            End If
        Next
        For Each linea As String In copiaComercio
            linea = linea.Replace("/H", "").Replace("/N", "")
            linea = linea.Replace("/HTOTAL", "TOTAL").Replace("/N", "").Replace("#CF#", "").Replace("#BR#", "")
            linea = linea.Replace("" & vbLf & "", "").Replace("" & vbLf, "").Replace(vbLf & "", "").Replace("//", "/")
            linea = linea.Replace(vbCrLf, "").Replace("    " & vbLf & "    " & vbLf, "").Replace(vbLf & "    " & vbLf, "")
            Select Case counter
                Case 7, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 27, 28, 29, 30, 31, 32
                    voucher = voucher & "    " & linea & vbLf
            End Select
            counter += 1
        Next
        voucher = voucher.Replace("    /I          *** COPIA CLIENTE ***           ", "")
        voucher = voucher.Replace(vbCrLf & vbCrLf & vbCrLf & vbCrLf, "")
        voucher = voucher.Replace("    " & vbLf & "    " & vbLf & "    " & vbLf, "").Replace(vbCrLf & vbCrLf, "")
        voucher = voucher.Replace(vbLf & "    " & vbLf & "    " & vbLf & vbLf & "    " & vbLf & "    " & vbLf, "")
        voucher = voucher.Replace(vbLf & "    " & vbLf & "    " & vbLf & vbLf & "    " & vbLf, "")
        voucher = voucher.Replace(vbLf & "    " & vbLf & "    " & vbLf & vbLf, "")
        Return voucher
    End Function

    Public Property GetObjetoTransActTarjetas As TransActAPI_Tarjetas_v400
        Get
            Return TransActTarjetas
        End Get
        Set(value As TransActAPI_Tarjetas_v400)
            TransActTarjetas = value
        End Set
    End Property
#End Region

End Class
